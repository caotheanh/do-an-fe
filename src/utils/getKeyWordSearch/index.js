export function getKeyWordSearch() {
  const keyWord = localStorage.getItem("key_word_search");
  const keyWordJson = JSON.parse(keyWord);
  return keyWordJson;
}
