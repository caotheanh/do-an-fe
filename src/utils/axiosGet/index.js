import axios from "axios";
import { api } from "../../api";
export function axiosGet(token) {
    const url = `${api}/cv/${token}`;
    const options = {
        method: 'GET',
        headers: { 'content-type': 'application/x-www-form-urlencoded' },
        url,
      };
      const gg =  axios(options);

    return gg
};
