function FormatNumber(money) {
  var moneyNew = Number(money);
  var p = moneyNew.toFixed(2).split(".");
  return (
    p[0]
      .split("")
      .reverse()
      .reduce(function (acc, moneyNew, i, orig) {
        return moneyNew === "-"
          ? acc
          : moneyNew + (i && !(i % 3) ? "," : "") + acc;
      }, "") + "VNĐ"
  );
}
export default FormatNumber;
