import { DataAddress } from "../../containers/Data/DataAddress";

export function getAddress(province_or_city, district, ward, details_address) {
  console.log(province_or_city, district, ward, details_address);
  let province_or_city_temp = "",
    district_temp = "",
    ward_temp = "";
  if (province_or_city || province_or_city === 0) {
    province_or_city_temp = DataAddress[province_or_city][1];
  }
  if (district) {
    district_temp = DataAddress[province_or_city][4][district][1];
  }
  if (ward) {
    ward_temp = DataAddress[province_or_city][4][district][4][ward][1];
  }
  return (
    details_address +
    " - " +
    ward_temp +
    " - " +
    district_temp +
    " - " +
    province_or_city_temp
  );
}
