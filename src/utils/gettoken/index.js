export function gettoken() {
    const tokenLocalStorage=localStorage.getItem("token")||"";
    const token=tokenLocalStorage.substr(1,tokenLocalStorage.length-2);
    return token;
}