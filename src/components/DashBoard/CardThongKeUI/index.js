import React from "react";
import { backgroundColor } from "../../../utils/backgroundColor";

function CardThongKeUi({ data }) {
  const percent = () => {
    const percentNew = Math.round(
      (Math.abs(data.total - data.total_old) / data.total_old) * 100
    );
    const status = data.total_old > data.total ? "-" : "+";
    const color = data.total_old > data.total ? "text-danger" : "text-success";
    return (
      <span className={`${color} text-sm font-weight-bolder pl-2`}>
        {status + percentNew + "%"}
      </span>
    );
  };
  const colorBackground = Math.floor(
    Math.random() * (backgroundColor.length - 1)
  );
  return (
    <div className="col-xl-3 col-sm-6 mb-xl-0 mb-4">
      <div className="card">
        <div className="card-body p-3">
          <div className="row">
            <div className="col-8">
              <div className="numbers">
                <p className="text-sm mb-0 text-capitalize font-weight-bold">
                  {data.title}
                </p>
                <h5 className="font-weight-bolder mb-0 mt-2">
                  {data.total}
                </h5>
              </div>
            </div>
            <div className="col-4 d-flex justify-content-end">
              <div
                className={`icon icon-shape  ${backgroundColor[colorBackground]} shadow text-center border-radius-md`}
              >
                <i style={{color:"#fff"}}
                  className={`${data.icon} text-while`}
                  aria-hidden="true"
                ></i>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CardThongKeUi;
