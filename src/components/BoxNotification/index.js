import React from "react";

function BoxNotification() {
  return (
    <li className="nav-item dropdown">
      <a
        className="nav-link"
        href="/#"
        role="button"
        data-toggle="dropdown"
        aria-haspopup="true"
        aria-expanded="false"
      >
        <i className="ni ni-bell-55" style={{ fontSize: 20 }}></i>
      </a>
      <div className="dropdown-menu dropdown-menu-xl  dropdown-menu-right  py-0 overflow-hidden">
        <div className="px-3 py-3">
          <h6 className="text-sm text-muted m-0">
            Bạn có <strong className="text-primary">13</strong> thông báo.
          </h6>
        </div>
        <div className="list-group list-group-flush">
          <a href="#!" className="list-group-item list-group-item-action">
            <div className="row align-items-center">
              <div className="col-auto">
                <img
                  alt=" placeholder"
                  src="../assets/img/theme/team-1.jpg"
                  className="avatar rounded-circle"
                />
              </div>
              <div className="col ml--2">
                <div className="d-flex justify-content-between align-items-center">
                  <div>
                    <h4 className="mb-0 text-sm">Phạm văn khải</h4>
                  </div>
                  <div className="text-right text-muted">
                    <small>2 hrs ago</small>
                  </div>
                </div>
                <p className="text-sm mb-0">
                  Đã phản hồi trạng thái ứng tuyển của bạn
                </p>
              </div>
            </div>
          </a>
        </div>
        <a
          href="#!"
          className="dropdown-item text-center text-primary font-weight-bold py-3"
        >
          View all
        </a>
      </div>
    </li>
  );
}

export default BoxNotification;
