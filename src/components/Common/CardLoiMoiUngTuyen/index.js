import { Link } from "react-router-dom/cjs/react-router-dom.min";
import FormatDate from "../../../utils/FormatDate";
import { api } from "../../../api";
import CardCV from "../../../containers/Page/PageHome/ViecLam/CardCV";
import { useState } from "react";
import axios from "axios";
import { gettoken } from "../../../utils/gettoken";
import NotificationManager from "react-notifications/lib/NotificationManager";
function CardLoiMoiUngTuyen({ invite, listCV }) {
  const [cvActive, setCvActive] = useState();
  const [isApply, setIsApply] = useState(false);
  const token = gettoken();
  const applyJob = (idInvite, id_job) => {
    const data = {
      token: token,
      id_recruitment: id_job,
      link_cv: cvActive,
    };
    axios.put(`${api}/invite/`, { id: idInvite, status: 2 });
    axios
      .post(`${api}/apply/`, data)
      .then(() => {
        NotificationManager.success("Ứng tuyển thành công", "Thành công", 3000);
      })
      .catch(() =>
        NotificationManager.error("Ứng tuyển thất bại", "Thất bại", 3000)
      );
    document.querySelector(".close").click();
  };
  const handleTuChoiLoiMoi = (idInvite) => {
    document.querySelector(".close").click();
    axios.put(`${api}/invite/`, { id: idInvite, status: 0 }).then(() => {
      NotificationManager.success(
        "Từ chối ứng tuyển thành công",
        "Thành công",
        3000
      );
    });
  };
  return (
    <>
      <div className="col-xl-6 col-md-6">
        <div className="card card-stats">
          <div className="card-body">
            <div className="row">
              <div className="col">
                <Link
                  to={`/viec-lam/${invite.id_recruitment}`}
                  target="_blank"
                  rel="noreferrer"
                  className="h6 mb-1 text-dark text-sm"
                >
                  Xem bài viết
                </Link>

                <h5 className={`card-title text-uppercase text-muted mb-0 `}>
                  <span
                    className={`card-title text-uppercase  mb-0 ${
                      ["text-danger", "text-info", "text-success"][
                        invite && invite.status
                      ]
                    }`}
                  >
                    {
                      ["Đã từ chối", "Đang chờ", "Đã đồng ý"][
                        invite && invite.status
                      ]
                    }
                  </span>
                </h5>
              </div>
              {invite.status === 1 && (
                <div className="col-auto">
                  <button
                    className="btn btn-outline-default"
                    type="button"
                    data-toggle="modal"
                    data-target={`#ModalReply_${invite.id}`}
                  >
                    <i className="fas fa-reply " />
                  </button>
                </div>
              )}
            </div>
            <p className="mt-3 mb-0 text-sm">
              <span
                className="text-sm text-dark "
                style={{ wordBreak: "keep-all" }}
              >
                <strong>Nội dung :</strong> {invite.text}
              </span>
            </p>
            <p className="mt-3 mb-0 text-sm">
              <span className="text-xs mt-2">
                {FormatDate(invite.created_at)}
              </span>
            </p>
          </div>
        </div>

        <div
          className="modal fade"
          id={`ModalReply_${invite.id}`}
          tabIndex="-1"
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-dialog-centered" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">
                  Trả lời lời mời ứng tuyển
                </h5>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                {isApply ? (
                  <div>
                    {listCV && listCV.length > 0
                      ? listCV &&
                        listCV.length > 0 &&
                        listCV.map((cv) => (
                          <CardCV
                            cvActive={cvActive}
                            cv={cv}
                            api={api}
                            setCvActive={setCvActive}
                          />
                        ))
                      : "Bạn đang không có CV nào. Hãy thêm CV để tiếp tục ứng tuyển"}
                    <div className="d-flex mt-3 justify-content-lg-center">
                      <button
                        type="button"
                        onClick={() => setIsApply(false)}
                        className="btn btn-outline-danger"
                      >
                        Hủy
                      </button>
                      <button
                        type="button"
                        className="btn btn-outline-primary"
                        onClick={() =>
                          applyJob(invite.id, invite.id_recruitment)
                        }
                      >
                        Ứng tuyển ngay
                      </button>
                    </div>
                  </div>
                ) : (
                  <div className="d-flex justify-content-lg-center m-3">
                    <button
                      type="button"
                      className="btn btn-outline-default"
                      data-dismiss="modal"
                    >
                      Hủy
                    </button>
                    <button
                      type="button"
                      className="btn btn-danger"
                      onClick={() => handleTuChoiLoiMoi(invite.id)}
                    >
                      Từ chối
                    </button>
                    <button
                      type="button"
                      className="btn btn-success"
                      onClick={() => setIsApply(!isApply)}
                    >
                      Ứng tuyển
                    </button>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default CardLoiMoiUngTuyen;
