import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { api } from "../../../api";
import { DataAddress } from "../../../containers/Data/DataAddress";
import FormatDate from "../../../utils/FormatDate";
import Image from "../../../components/Common/Image/index";
import StatusApply from "../../../utils/StatusApply";
function CardApplyJob({ job, apply, type }) {
  const [detailApply, setDetailApply] = useState({});
  useEffect(() => {
    const statusApplys = StatusApply.find((item) => +item.id === +apply.status);
    setDetailApply(statusApplys);
  }, [apply]);
  return type === 1 ? (
    <div className="col-lg-6">
      <li className="list-group-item border-0 d-flex  mb-2   bg-gray border-radius-lg p-3">
        <Link
          exact="true"
          to={`/ung-tuyen/chi-tiet/${apply.id}`}
          className="mr-3"
        >
          <img
            alt=" placeholder"
            src={`${api}/${job.img}`}
            className="img-fluid rounded shadow m-2"
            style={{ width: "100px", height: "100px" }}
          ></img>
        </Link>
        <div className="d-flex flex-column">
          <Link
            exact="true"
            to={`/ung-tuyen/chi-tiet/${apply.id}`}
            className="h5 mb-3 text-sm"
          >
            {job.title.substr(0, 35) + "..."}
          </Link>
          <span className="mb-2 text-xs">
            Hình thức làm việc:{" "}
            <span className="text-dark font-weight-bold ms-2">
              {job.type === 1 ? "Bán thời gian" : "Toàn thời gian"}
            </span>
          </span>
          <span className="mb-2 text-xs">
            Địa điểm làm việc:{" "}
            <span className="text-dark ms-2 font-weight-bold">
              {DataAddress[job.province_or_city][1]}
            </span>
          </span>
          <span className="text-xs">
            Hạn nộp hồ sơ:
            <span className="text-dark ms-2 font-weight-bold"> {job.end}</span>
          </span>
        </div>
        <div className="ms-auto">
          <div className="text-right">
            <p className="mt-3 mb-0 text-sm">
              <span className="text-dark font-weight-bold ms-2">
                {FormatDate(apply.created_at)}
              </span>
            </p>
            <Link
              to={`/ung-tuyen/chi-tiet/${apply.id}`}
              className="mb-2 text-xs"
            >
              {
                <span className={`btn ${detailApply.background} btn-sm`}>
                  {detailApply.textUser}{" "}
                </span>
              }
            </Link>
          </div>
        </div>
      </li>
    </div>
  ) : (
    <tr>
      <th scope="row">
        <Image url={`${api}/${job.img}`} size={"5rem"} />
      </th>
      <td>
        <Link
          exact="true"
          to={`/ung-tuyen/chi-tiet/${apply.id}`}
          className="media-body"
        >
          <span className="name mb-0 text-sm">
            {job.title.substr(0, 35) + "..."}
          </span>
        </Link>
      </td>
      <td>{FormatDate(job.end)}</td>
      <td>{FormatDate(apply.created_at)}</td>
      <td>
        <Link to={`/ung-tuyen/chi-tiet/${apply.id}`} className="mb-2 text-xs">
          {apply.status === 1 && (
            <span className="btn btn-info btn-sm">Đang chờ phản hồi</span>
          )}
          {apply.status === 0 && (
            <span className="btn btn-danger btn-sm">Không đạt </span>
          )}
          {apply.status === 2 && (
            <span className="btn btn-success btn-sm">Đang xem sét</span>
          )}
        </Link>
      </td>
      <td className="text-right">
        <div className="dropdown">
          <button
            className="btn btn-sm btn-icon-only text-light"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            <i className="fas fa-ellipsis-v"></i>
          </button>
          <div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
            <button className="dropdown-item">Xem</button>
            <button className="dropdown-item">Hủy ứng tuyển</button>
            <button className="dropdown-item">Xem CV ứng tuyển</button>
          </div>
        </div>
      </td>
    </tr>
  );
}

export default CardApplyJob;
