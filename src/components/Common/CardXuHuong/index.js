import React from "react";
import { Link } from "react-router-dom";
function CardXuHuong({ item }) {
  return (
    <div className="col-xl-3 col-md-6">
      <Link to={`tim-viec-lam/danh-muc/${item.id}`}>
        <div className="card card-stats btn-hover">
          <div className="card-body text-center">
            <span className="h4 font-weight-bold mb-0 mb-4 text-sm ">
              {item.name}
            </span>
          </div>
        </div>
      </Link>
    </div>
  );
}
export default CardXuHuong;
