import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { gettoken } from "../../../utils/gettoken";
import { api } from "../../../api";
import axios from "axios";
const DropdownUser = () => {
  const token = gettoken();
  const info_login = useSelector((state) => state.info_login);
  const info_user = useSelector((state) => state.info_user);
  const is_login = useSelector((state) => state.is_login);
  const [image, setImage] = useState("");
  let nameOrEmail = "";
  const dispatch = useDispatch();
  const logout = (token_logout) => {
    axios.post(`${api}/logout/`, { token: token_logout }).then(() => {
      localStorage.removeItem("token");
      dispatch({ type: "SET_INFO_USER", info_user: {} });
      dispatch({ type: "SET_INFO_LOGIN", info_login: null });
      dispatch({ type: "SET_TOKEN", token: null });
      dispatch({ type: "SET_IS_LOGIN", is_login: false });
    });
  };
  if (info_user) {
    nameOrEmail = info_user.name;
  } else if (info_login) {
    nameOrEmail = info_login.email;
  }
  useEffect(() => {
    if (info_login && info_login.images) {
      setImage(`${api}/avatar/${info_login.images}`);
    }
    if (info_user && info_user.email && info_user.email.images) {
      setImage(`${api}/avatar/${info_user.email.images}`);
    }
  }, [info_user, info_login]);
  let textImage = "";
  if (is_login && info_user && info_user.email) {
    textImage = info_user.name.substr(0, 1);
  } else if (is_login && info_login && info_login.email) {
    textImage = info_login.email.substr(0, 1);
  }
  return (
    <div>
      <ul className="navbar-nav navbar-nav-hover align-items-lg-center">
        <li className="nav-item dropdown">
          <a
            className="nav-link"
            data-toggle="dropdown"
            href="/#"
            role="button"
          >
            <div className="media align-items-center">
              <span className="avatar avatar-sm rounded-circle p--3">
                {image ? (
                  <img
                    alt="placeholder"
                    src={image}
                    style={{ width: "30px", height: "30px" }}
                  />
                ) : (
                  textImage
                )}
              </span>
              <h5 className="text-overflow m-0 pl-2">
                {nameOrEmail&&nameOrEmail.length > 10
                  ? nameOrEmail.substring(0, 10) + "..."
                  : nameOrEmail&&nameOrEmail}
              </h5>
            </div>
          </a>
          <div className="dropdown-menu">
            <div className="dropdown-header noti-title">
              <h6 className="text-overflow m-0">Welcome!</h6>
              <h5 className="m-0">{nameOrEmail}</h5>
            </div>
            <Link to="/tai-khoan" className="dropdown-item">
              <i className="fa fa-user-alt"></i>
              <span>Tài khoản của tôi</span>
            </Link>
            {info_login && +info_login.type === 2 && (
              <>
                <Link to="/viec-lam-da-luu" className="dropdown-item">
                  <i className="fa fa-heart"></i>
                  <span>Việc làm đã lưu</span>
                </Link>
                <Link to="/viec-lam-da-ung-tuyen" className="dropdown-item">
                  <i className="fa fa-cloud-upload-alt"></i>
                  <span>Việc làm đã ứng tuyển</span>
                </Link>
                <Link to="/loi-moi-ung-tuyen" className="dropdown-item">
                  <i className="fa fa-envelope"></i>
                  <span>Lời mời ứng tuyển</span>
                </Link>
              </>
            )}
            {info_login && +info_login.type === 1 && (
              <Link
                className="dropdown-item"
                exact="true"
                to="/employer/dashboard"
              >
                <i className="fa fa-plus-square"></i>
                <span className="nav-link-text">
                  Quản lý thông tin tuyển dụng
                </span>
              </Link>
            )}
            {info_login && +info_login.type === 3 && (
              <li className="nav-item">
                <Link
                  className="dropdown-item"
                  exact={true}
                  to="/admin/dashboard"
                >
                  <i className="fa fa-plus-square"></i>
                  <span className="nav-link-text">Quản lý Website</span>
                </Link>
              </li>
            )}
            <div className="dropdown-divider"></div>
            <button onClick={() => logout(token)} className="dropdown-item">
              <i className="fa fa-power-off"></i>
              <span>Logout</span>
            </button>
          </div>
        </li>
      </ul>
    </div>
  );
};

export default DropdownUser;
