import React from "react";
import { Link } from "react-router-dom";

function CardDangKiNTD() {
  return (
    <div
      className="card card-background shadow-none card-background-mask-secondary"
      id="sidenavCard"
    >
      <div
        className="full-background"
        style={{
          backgroundImage: `url('/assets/img/white-curved.jpeg')`,
          width: "100%",
          borderRadius: "10px",
        }}
      >
        <div className="card-body text-left p-3 w-100">
          <div className="icon icon-shape icon-sm bg-white shadow text-center mb-3 d-flex align-items-center justify-content-center border-radius-md">
            <i
              className="ni ni-diamond text-dark text-gradient text-lg top-0"
              aria-hidden="true"
              id="sidenavCardIcon"
            ></i>
          </div>
          <h4 className="up">Đăng tin Tuyển dụng Miễn phí</h4>
          <p className="text-sm mb-0 font-weight-bold">
            - Tặng ngay 3 tin tuyển dụng miễn phí.
          </p>
          <p className="text-sm mb-0 font-weight-bold">
            - Tiếp cận với 3,000,000+ ứng viên.
          </p>
          <p className="text-sm  font-weight-bold">
            - Đăng tin dễ dàng, không quá 1 phút.
          </p>
          <Link
            to="/register-employer"
            target="_blank"
            rel="noreferrer"
            className="btn btn-white btn-sm w-100 mb-4 mt-4"
          >
            Đăng kí
          </Link>
        </div>
      </div>
    </div>
  );
}

export default CardDangKiNTD;
