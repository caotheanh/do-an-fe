import React from "react";

export default function Image({ url, size }) {
  const styles = {
    background: ` url(${url}) 50% 50% no-repeat`,
    width: size,
    height: size,
    borderRadius: "10px",
    backgroundSize: "cover",
    border: "1px solid #f2f0f0",
  };
  return <div style={styles} className="imageBackground"></div>;
}
