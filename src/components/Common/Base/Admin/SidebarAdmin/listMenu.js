export const listMenu = [
  {
    text: "Tổng quan",
    path: "/admin/dashboard",
    exact: true,
    icon: "fa fa-home",
  },
  {
    text: "Quản lý tài khoản",
    path: "/admin/tin-tuyen-dung",
    exact: "",
    icon: "fa fa-newspaper",
    submenu: [
      { name: "Danh sách", path: "/admin/tai-khoan/danh-sach" },
    ],
  },
  {
    text: "Quản lý Danh mục việc làm",
    path: "/admin/ung-tuyen",
    exact: false,
    icon: "fa fa-columns",
    submenu: [
      { name: "Thêm mới", path: "/admin/danh-muc/them-moi" },
      { name: "Danh Sách", path: "/admin/danh-muc/danh-sach" },
    ],
  },
  {
    text: "Quản lý công ty",
    path: "/admin/ung-vien",
    exact: false,
    icon: "fa fa-search-plus",
    submenu: [
      // { name: "Thêm mới", path: "/admin/cong-ty/them-moi" },
      { name: "Danh Sách", path: "/admin/cong-ty/danh-sach" },
    ],
  },
  {
    text: "Quản lý tin tuyển dụng",
    path: "/admin/cong-ty",
    exact: false,
    icon: "fa fa-sliders-h",
    submenu: [
      // { name: "Thêm mới", path: "/admin/tin-tuyen-dung/them-moi" },
      { name: "Danh sách", path: "/admin/tin-tuyen-dung/danh-sach" },
    ],
  },
];
