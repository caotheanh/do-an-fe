import React, { useState } from "react";
import { Link, NavLink } from "react-router-dom";
import { listMenu } from "./listMenu.js";
import DropdownUser from "../../../DropDownUser";
const SidebarAdmin = () => {
  const [isShowDropdown, setIsShowDropdown] = useState(0);
  //   const x = location.pathname;
  return (
    <nav
      className="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-expand-xs-width-1 navbar-light bg-white"
      id="sidenav-main"
    >
      <div className="scrollbar-inner">
        <div className="sidenav-header  align-items-center">
          <Link to="/">
            <img
              src="../../../../../assets/img/brand/blue.png"
              className=""
              style={{ height: "80px" }}
              alt="images"
            />
          </Link>
        </div>
        <div className="d-flex  justify-content-center mb-3 mt-3">
          <DropdownUser />
        </div>
        <div className="navbar-inner">
          <div className="collapse navbar-collapse" id="sidenav-collapse-main">
            <ul className="navbar-nav">
              {listMenu &&
                listMenu.map((item, index) =>
                  item && !item.submenu ? (
                    <li className="nav-item" key={`menu_${item.text}`}>
                      <div className="row">
                        <div className="col-12">
                          <NavLink
                            className="nav-link-menu"
                            to={`${item.path}`}
                            activeClassName="active_employer"
                          >
                            <i className={item.icon}></i>
                            <span className="">{item.text}</span>
                          </NavLink>
                        </div>
                      </div>
                    </li>
                  ) : (
                    <li className="nav-item" key={`menu_${item.text}`}>
                      <label
                        className="row"
                        onClick={() =>
                          setIsShowDropdown(
                            isShowDropdown === index + 1 ? 0 : index + 1
                          )
                        }
                      >
                        <div className="col-9">
                          <div className="nav-link">
                            <i className={item.icon}></i>
                            <span className="nav-link-text">{item.text}</span>
                          </div>
                        </div>
                        <div className="col-3 pt-3">
                          {item.submenu && (
                            <i
                              className={`${
                                isShowDropdown === index + 1
                                  ? "fa fa-angle-down"
                                  : "fa  fa-angle-right"
                              } float-right`}
                            ></i>
                          )}
                        </div>
                      </label>

                      <div
                        className={`dropdown-sidebar-menu ${
                          isShowDropdown === index + 1 ? "show" : ""
                        }`}
                        aria-labelledby="dropdownMenuLink"
                      >
                        {item.submenu &&
                          item.submenu.length > 0 &&
                          item.submenu.map((sub) => (
                            <NavLink
                              to={sub.path}
                              key={`dropdown-item_${sub.name}`}
                              className="nav-link-menu dropdown-item-menu-employer"
                              activeClassName="active_employer"
                            >
                              {sub.name}
                            </NavLink>
                          ))}
                      </div>
                    </li>
                  )
                )}
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default SidebarAdmin;
