import React from "react";
import { Link, NavLink, useParams } from "react-router-dom";
import { listMenuEditCongTy } from "./listMenu";
const SidebarEditCongTy = () => {
  const {id }= useParams();
  return (
    <nav
      className="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-expand-xs-width-1 navbar-light bg-white"
      id="sidenav-main"
    >
      <div className="scrollbar-inner">
        <div className="sidenav-header  align-items-center">
          <Link to="/">
            <img
              src="../../../../../assets/img/brand/blue.png"
              className=""
              style={{ height: "80px" }}
              alt="images"
            />
          </Link>
        </div>
        <div className="navbar-inner">
          <div className="collapse navbar-collapse" id="sidenav-collapse-main">
            <ul className="navbar-nav">
              <li className="nav-item">
                <div className="row">
                  <div className="col-12">
                    <NavLink
                      className="nav-link-menu"
                      to={"/employer/cong-ty/danh-sach"}
                      activeClassName="active_employer"
                    >
                      <i className="ni ni-curved-next"></i>
                      <span className="">Quay lại</span>
                    </NavLink>
                  </div>
                </div>
              </li>

              {listMenuEditCongTy &&
                listMenuEditCongTy.map((item, index) => (
                  <li className="nav-item" key={`menu_${item.text}`}>
                    <div className="row">
                      <div className="col-12">
                        <NavLink
                          className="nav-link-menu"
                          to={`/employer/edit-cong-ty/${id}${item.path}`}
                          activeClassName="active_employer"
                        >
                          <i className={item.icon}></i>
                          <span className="">{item.text}</span>
                        </NavLink>
                      </div>
                    </div>
                  </li>
                ))}
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default SidebarEditCongTy;
