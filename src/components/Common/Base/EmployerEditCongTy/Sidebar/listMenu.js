export const listMenuEditCongTy = [
  {
    text: "Tổng quan",
    path: "/tong-quan",
    exact: true,
    icon: "fa fa-home",
  },{
    text: "Văn phòng",
    path: "/van-phong",
    exact: true,
    icon: "fa fa-briefcase",
  },{
    text: "Con người",
    path: "/cong-nguoi",
    exact: true,
    icon: "fa fa-users",
  },{
    text: "Hình ảnh",
    path: "/hinh-anh",
    exact: true,
    icon: "fa fa-images",
  },{
    text: "Phúc lợi",
    path: "/phuc-loi",
    exact: true,
    icon: "fa fa-dollar-sign",
  },{
    text: "Câu chuyện",
    path: "/cau-chuyen",
    exact: true,
    icon: "fa fa-marker",
  },];
