// import axios from "axios";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { DataAddress } from "../../../../../containers/Data/DataAddress";

function Banner() {
  const [keyWord, setKeyWord] = useState({ title: "", city: "", cate: "" });
  const handleEnterChangeKeyword = (e) => {
    const { name, value } = e.target;
    setKeyWord((prev) => {
      return { ...prev, [name]: value };
    });
  };
  const categories = useSelector((state) => state.categories);
  const dispatch = useDispatch();
  let history = useHistory();
  const setKeyWordSearch = () => {
    dispatch({ type: "SET_DATA_SEARCH", data_search: keyWord });
    localStorage.setItem("key_word_search", JSON.stringify(keyWord));
    history.push("/tim-viec-lam/search");
  };
  return (
    <div className="section section-hero section-shaped">
      <div className="shape shape-style-3 shape-default">
        <span className="span-150"></span>
        <span className="span-50"></span>
        <span className="span-50"></span>
        <span className="span-75"></span>
        <span className="span-100"></span>
        <span className="span-75"></span>
        <span className="span-50"></span>
        <span className="span-100"></span>
        <span className="span-50"></span>
        <span className="span-100"></span>
      </div>
      <div className="page-header">
        <div className="container shape-container d-flex align-items-center">
          <div className="col-12 ">
            <div className="row align-items-center justify-content-center">
              <div className="col-lg-8 text-center">
                <h3 className="text-white display-3">
                  TÌM VIỆC KHÓ - CÓ VIÊC LÀM POLY
                </h3>
                <h2 className="display-4 font-weight-normal text-white">
                  Việc làm siêu tốc - Đúng người, đúng việc, đúng giá trị
                </h2>
                <div className="btn-wrapper mt-4 d-flex justify-content-center ">
                  <div className="row">
                    <Link
                      className="btn btn-success  btn-lg text-uppercase mt-3 col-lg-auto"
                      to="/cv/mau-cv"
                    >
                      <span className="text-sm-center pl-4 pr-4"> Tạo CV</span>
                    </Link>
                    <Link
                      className="btn btn-warning btn-lg text-uppercase mt-3 col-lg-auto"
                      to="/cv"
                    >
                      <span className="text-sm-center pl-4 pr-4">
                        Tải CV lên
                      </span>
                    </Link>
                  </div>
                </div>
              </div>
              <div className="col-lg-4">
                <img
                  src="/assets/img/findJob.png"
                  alt="avatar"
                  style={{ width: "100%" }}
                />
              </div>
              <div className="mt-3">
                <div className="search-job ">
                  <h3 className="text-white">
                    Tìm việc làm nhanh, uy tín, mới nhất trên Mạng Tuyển dụng.
                  </h3>
                  <div className="row no-gutters">
                    <div className="col-lg-10 col-md-9  mr-md-2">
                      <div className="row">
                        <div className="form-group col-lg-12">
                          <div className="form-field">
                            <input
                              onChange={handleEnterChangeKeyword}
                              type="text"
                              name="title"
                              className="form-control"
                              placeholder=" Nhập công công việc tìm kiếm bạn mong muốn"
                            />
                          </div>
                        </div>
                        <div className="form-group col-lg-auto">
                          <div className="form-field">
                            <select
                              className="form-control "
                              name="city"
                              onChange={handleEnterChangeKeyword}
                            >
                              <option value="">Thành phố</option>
                              {DataAddress.map((thanhpho, index) => (
                                <option
                                  key={`thanh_phos_${index}`}
                                  checked={true}
                                  value={index}
                                >
                                  {thanhpho[1]}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>
                        <div className="form-group col-lg-auto">
                          <div className="form-field">
                            <select
                              className="form-control "
                              name="cate"
                              onChange={handleEnterChangeKeyword}
                            >
                              <option value="">Loại công việc</option>
                              {categories &&
                                Array.isArray(categories) &&
                                categories.map((cate) => (
                                  <option
                                    key={`category_${cate.id}`}
                                    value={cate.id}
                                  >
                                    {cate.name}
                                  </option>
                                ))}
                            </select>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div className="col-md col-lg-md col-lg-auto  mr-md-2">
                      <div className="form-group">
                        <div className="form-field">
                          <button
                            className="form-control btn btn-icon btn-secondary"
                            onClick={setKeyWordSearch}
                            type="button"
                          >
                            <i className="fa fa-search pr-2"></i>
                            Tìm việc ngay
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="separator separator-bottom separator-skew zindex-100">
        <svg
          x="0"
          y="0"
          viewBox="0 0 2560 100"
          preserveAspectRatio="none"
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
        >
          <polygon
            className="fill-white"
            points="2560 0 2560 100 0 100"
          ></polygon>
        </svg>
      </div>
    </div>
  );
}

export default Banner;
