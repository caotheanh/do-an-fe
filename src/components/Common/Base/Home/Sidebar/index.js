import axios from "axios";
import React, { useState } from "react";
import { Link, NavLink } from "react-router-dom";
import DropdownUser from "../../../DropDownUser";
import { listMenu } from "./listMenu";
import { api } from "../../../../../api";
import { useDispatch, useSelector } from "react-redux";
const Sidebar = ({ isChangeSidebar, setIsChangeSidebar }) => {
  const is_login = useSelector((state) => state.is_login);
  const info_login = useSelector((state) => state.info_login);
  const [isDropdown, setIsDropdown] = useState(false);
  const dispatch = useDispatch();
  const logout = (token_logout) => {
    axios.post(`${api}/logout/`, { token: token_logout }).then(() => {
      localStorage.removeItem("token");
      dispatch({ type: "SET_INFO_USER", info_user: {} });
      dispatch({ type: "SET_INFO_LOGIN", info_login: null });
      dispatch({ type: "SET_TOKEN", token: null });
      dispatch({ type: "SET_IS_LOGIN", is_login: false });
    });
  };
  return (
    <nav
      className="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-expand-xs-width-1 navbar-light bg-white"
      id="sidenav-main"
    >
      <div className="scrollbar-inner">
        <div className="row" style={{ width: "100%" }}>
          <div className="col-9 sidenav-header  align-items-center">
            <Link to="/">
              <img
                src="../../../../../assets/img/brand/blue.png"
                className=""
                style={{ height: "80px" }}
                alt="images"
              />
            </Link>
          </div>
          <div className="col-3">
            <i
              onClick={() => setIsChangeSidebar(!isChangeSidebar)}
              className="fa fa-align-right pt-4"
            ></i>
          </div>
        </div>
        <div
          className="d-flex  justify-content-center mb-3 mt-3"
          onClick={() => setIsDropdown(!isDropdown)}
        >
          {is_login ? (
            <DropdownUser
              isDropdown={isDropdown}
              setIsDropdown={setIsDropdown}
              logout={logout}
            />
          ) : (
            <Link
              exact="true"
              to="/login"
              className="btn btn-outline-primary pr-5 pl-5"
            >
              Đăng nhập
            </Link>
          )}
        </div>
        <div className="navbar-inner">
          <div className="collapse navbar-collapse" id="sidenav-collapse-main">
            <ul className="navbar-nav">
              {listMenu &&
                listMenu.map((item) => (
                  <li className="nav-item" key={`menu_${item.text}`}>
                    <NavLink
                      className="nav-link"
                      exact={true}
                      to={
                        item.path === "/cv"
                          ? is_login
                            ? "/cv"
                            : "/login"
                          : item.path
                      }
                      activeClassName="nav-link active active_h"
                    >
                      <i className={item.icon}></i>
                      <span className="nav-link-text">{item.text}</span>
                    </NavLink>
                  </li>
                ))}
             
              {info_login &&
                (+info_login.type === 1 || +info_login.type === 3) && (
                  <li className="nav-item">
                    <NavLink
                      className="nav-link"
                      exact={true}
                      to="/employer/dashboard"
                      activeClassName="nav-link active active_h"
                    >
                      <i className="fa fa-plus-square"></i>
                      <span className="nav-link-text">Đăng tin</span>
                    </NavLink>
                  </li>
                )}
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Sidebar;
