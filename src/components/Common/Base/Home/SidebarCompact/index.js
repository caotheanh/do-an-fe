import React from "react";
import { NavLink } from "react-router-dom";
import { Link } from "react-router-dom";
import ReactTooltip from "react-tooltip";
import { listMenu } from "../Sidebar/listMenu";

function SidebarCompact({
  isChangeSidebar,
  info_login,
  info_user,
  is_login,
  setIsChangeSidebar,
}) {
  const url = window.location.pathname;
  return (
    <nav
      className="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-expand-xs-width navbar-light bg-white"
      id="sidenav-main"
    >
      <div className="scrollbar-inner">
        <div className="sidenav-header  align-items-center">
          <a href="/#">
            <img
              src="../../../../../assets/img/brand/blue1.png"
              className=""
              style={{ height: "80px" }}
              alt="images"
            />
          </a>
        </div>
        <div className="d-flex  justify-content-center mb-3 mt-3">
          {is_login || info_login ? (
            <span className="avatar avatar-sm rounded-circle p--3">
              {(info_user &&
                info_user.name.length > 0 &&
                info_user.name.substr(0, 1)) ||
                (info_login &&
                  info_login.email.length > 0 &&
                  info_login.email.substr(0, 1))}
            </span>
          ) : (
            <Link exact="true" to="/login" className="btn btn-outline-primary ">
              <i className="fa fa-sign-in-alt"></i>
            </Link>
          )}
        </div>
        <div className="navbar-inner">
          <div className="collapse navbar-collapse" id="sidenav-collapse-main">
            <ul className="navbar-nav">
              {listMenu &&
                listMenu.map((item) => (
                  <li
                    className="nav-item"
                    key={`menu_${item.text}`}
                    data-tip={item.text}
                    data-for="submenu"
                  >
                    <NavLink
                      className="nav-link text-center"
                      exact={item.exact}
                      to={item.path}
                      activeClassName="nav-link active active_h"
                    >
                      <i className={item.icon}></i>
                    </NavLink>
                    <ReactTooltip
                      className="extraClass"
                      delayHide={1000}
                      effect="solid"
                      id="submenu"
                    />
                  </li>
                ))}
              {url !== "/cv/mau-cv" && (
                <li className="nav-item" data-for="submenu">
                  <div className="nav-link text-center">
                    <i
                      onClick={() => setIsChangeSidebar(!isChangeSidebar)}
                      className="la la-angle-double-right"
                    ></i>
                  </div>
                </li>
              )}
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default SidebarCompact;
