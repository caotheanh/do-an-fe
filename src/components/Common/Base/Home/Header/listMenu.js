export const listMenu = [
  {
    text: "Trang chủ",
    path: "/",
    exact: true,
    icon: "fa fa-home",
  },
  {
    text: "Tìm kiếm việc làm",
    path: "/tim-viec-lam",
    exact: false,
    icon: "fa fa-search-plus",
  },
  // {
  //   text: "Việc làm quanh bạn",
  //   path: "/viec-lam-xung-quanh",
  //   exact: false,
  //   icon: "ni ni-world  text-default",
  // },
  {
    text: "Công ty",
    path: "/cong-ty",
    exact: false,
    icon: "fa fa-sliders-h",
  },
  // {
  //   text: "Kiến thức",
  //   path: "/kien-thuc",
  //   exact: false,
  //   icon: "fa fa-passport",
  // },
  // {
  //   text: "Tin Tức",
  //   path: "/tin-tuc",
  //   exact: false,
  //   icon: "flaticon-notes text-default",
  // },
  {
    text: "CV của tôi",
    path: "/cv",
    exact: false,
    icon: "fa fa-address-card",
  },
  {
    text: "Đăng kí tuyển dụng",
    path: "/cv",
    exact: false,
    icon: "fa fa-address-card",
  },
  // {
  //   text: "Đăng tin",
  //   path: "/employer/dashboard",
  //   exact: false,
  //   icon: "fa fa-plus-square",
  // },
];
