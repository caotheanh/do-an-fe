import axios from "axios";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, NavLink } from "react-router-dom/cjs/react-router-dom.min";
import { api } from "../../../../../api";
import { gettoken } from "../../../../../utils/gettoken";
import BoxNotification from "../../../../BoxNotification";
import DropdownUser from "../../../DropDownUser";
import { listMenu } from "../Sidebar/listMenu";
const Header = () => {
  const is_login = useSelector((state) => state.is_login);
  const [isDropdown, setIsDropdown] = useState(false);
  const info_login = useSelector((state) => state.info_login);
  const dispatch = useDispatch();
  const token = gettoken();
  const logout = (token_logout) => {
    axios.post(`${api}/logout/`, { token: token }).then(() => {
      localStorage.removeItem("token");
      dispatch({ type: "SET_INFO_USER", info_user: {} });
      dispatch({ type: "SET_INFO_LOGIN", info_login: null });
      dispatch({ type: "SET_TOKEN", token: null });
      dispatch({ type: "SET_IS_LOGIN", is_login: false });
    });
  };
  return (
    <>
      <nav className="navbar navbar-expand-lg navbar-while text-default">
        <div className="container">
          <Link to="/">
            <img
              src="../../../../../assets/img/brand/blue.png"
              className=""
              style={{ height: "50px" }}
              alt="avt"
            />
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbar-primary"
            aria-controls="navbar-primary"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <i className="fa fa-bars"></i>
          </button>
          <div className="collapse navbar-collapse" id="navbar-primary">
            <div className="navbar-collapse-header">
              <div className="row">
                <div className="col-6 collapse-brand">
                  <Link to="/">
                    <img
                      src="../../../../../assets/img/brand/blue.png"
                      className=""
                      style={{ height: "50px" }}
                      alt="avt"
                    />
                  </Link>
                </div>
                <div className="col-6 collapse-close">
                  <button
                    type="button"
                    className="navbar-toggler"
                    data-toggle="collapse"
                    data-target="#navbar-primary"
                    aria-controls="navbar-primary"
                    aria-expanded="false"
                    aria-label="Toggle navigation"
                  >
                    <span></span>
                    <span></span>
                  </button>
                </div>
              </div>
            </div>
            <ul className="navbar-nav  d-flex justify-content-lg-center ml-lg-auto">
              {listMenu &&
                listMenu.map((item) => (
                  <li className="nav-item " key={item.text}>
                    <NavLink
                      key={`menu_${item.text}`}
                      className="nav-link"
                      to={
                        item.path === "/cv"
                          ? is_login
                            ? "/cv"
                            : "/login"
                          : item.path
                      }
                      exact={true}
                      activeClassName="nav-link active active_h"
                    >
                      <i className={`${item.icon} d-lg-none pr-2`}></i>
                      <span className="nav-link-inner--text">{item.text}</span>
                    </NavLink>
                  </li>
                ))}
              {(!info_login || (info_login && info_login.type === 2)) && (
                <li className="nav-item">
                  <NavLink
                    className="nav-link"
                    exact={true}
                    to="/register-employer"
                    activeClassName="nav-link active active_h"
                  >
                    <span className="nav-link-text">Đăng kí tuyển dụng</span>
                  </NavLink>
                </li>
              )}
            </ul>
            <ul className="navbar-nav align-items-lg-center ml-lg-auto  navbar-nav-hover ">
              {/* {is_login && <BoxNotification />} */}
              <li className="nav-item dropdown">
                <div
                  className="d-flex  justify-content-center"
                  onClick={() => setIsDropdown(!isDropdown)}
                >
                  {is_login ? (
                    <DropdownUser
                      isDropdown={isDropdown}
                      setIsDropdown={setIsDropdown}
                      logout={logout}
                    />
                  ) : (
                    <Link
                      exact="true"
                      to="/login"
                      className="btn text-uppercase pr-5 pl-5"
                    >
                      Đăng nhập
                    </Link>
                  )}
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </>
  );
};

export default Header;
