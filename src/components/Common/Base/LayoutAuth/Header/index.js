import React from "react";
import { Link } from "react-router-dom";
const Header = () => {
  return (
    <nav
      id="navbar-main"
      className="navbar navbar-horizontal navbar-transparent navbar-main navbar-expand-lg navbar-light"
    >
      <div className="container">
        <Link className="navbar-brand" to="/">
          <img
            alt="images"
            src="../../../../../assets/img/brand/blues.png"
            style={{ height: "50px" }}
          />
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbar-collapse"
          aria-controls="navbar-collapse"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div
          className="navbar-collapse navbar-custom-collapse collapse"
          id="navbar-collapse"
        >
          <div className="navbar-collapse-header">
            <div className="row">
              <div className="col-6 collapse-brand">
                <a href="dashboard.html">
                  <img src="../assets/img/brand/blue.png"  alt="images"/>
                </a>
              </div>
              <div className="col-6 collapse-close">
                <button
                  type="button"
                  className="navbar-toggler"
                  data-toggle="collapse"
                  data-target="#navbar-collapse"
                  aria-controls="navbar-collapse"
                  aria-expanded="false"
                  aria-label="Toggle navigation"
                >
                  <span></span>
                  <span></span>
                </button>
              </div>
            </div>
          </div>
          <ul className="navbar-nav mr-auto">
            <li className="nav-item">
              <Link to="/" className="nav-link">
                <span className="nav-link-inner--text">Trang chủ</span>
              </Link>
            </li>
            <li className="nav-item">
              <Link exact="true" to="/login" className="nav-link">
                <span className="nav-link-inner--text">Đăng nhập</span>
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/register" className="nav-link">
                <span className="nav-link-inner--text">Đăng kí</span>
              </Link>
            </li>
          </ul>
          <hr className="d-lg-none" />
        </div>
      </div>
    </nav>
  );
};

export default Header;
