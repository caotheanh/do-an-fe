import React, { useState } from "react";
import { Link, NavLink } from "react-router-dom";
import { listMenu } from "./listMenu";
import DropdownUser from "../../../DropDownUser";
import axios from "axios";
import { useDispatch } from "react-redux";
import { api } from "../../../../../api";
const Sidebar = () => {
  const [isShowDropdown, setIsShowDropdown] = useState(0);
  // eslint-disable-next-line no-restricted-globals
  const x = location.pathname;
  const dispatch = useDispatch();
  const logout = (token_logout) => {
    axios.post(`${api}/logout/`, { token: token_logout }).then(() => {
      localStorage.removeItem("token");
      dispatch({ type: "SET_INFO_USER", info_user: {} });
      dispatch({ type: "SET_INFO_LOGIN", info_login: null });
      dispatch({ type: "SET_TOKEN", token: null });
      dispatch({ type: "SET_IS_LOGIN", is_login: false });
    });
  };
  return (
    <nav
      className="sidenav navbar navbar-vertical  fixed-left  navbar-expand-xs navbar-expand-xs-width-1 navbar-light bg-white"
      id="sidenav-main"
    >
      <div className="scrollbar-inner">
        <div className="sidenav-header  align-items-center">
          <Link to="/">
            <img
              src="../../../../../assets/img/brand/blue.png"
              className=""
              style={{ height: "80px" }}
              alt="images"
            />
          </Link>
        </div>
        <div className="d-flex  justify-content-center mb-3 mt-3">
          <DropdownUser logout={logout}/>
        </div>
        <div className="navbar-inner">
          <div className="collapse navbar-collapse" id="sidenav-collapse-main">
            <ul className="navbar-nav">
              {listMenu &&
                listMenu.map((item, index) =>
                  item && !item.submenu ? (
                    <li className="nav-item" key={`menu_${item.text}`}>
                      <div className="row">
                        <div className="col-12">
                          <NavLink
                            className="nav-link-menu"
                            to={`${item.path}`}
                            activeClassName="active_employer"
                          >
                            <i className={item.icon}></i>
                            <span className="">{item.text}</span>
                          </NavLink>
                        </div>
                      </div>
                    </li>
                  ) : (
                    <li className="nav-item" key={`menu_${item.text}`}>
                      <label
                        className="row"
                        onClick={() =>
                          setIsShowDropdown(
                            isShowDropdown === index + 1 ? 0 : index + 1
                          )
                        }
                      >
                        <div className="col-9">
                          <div className="nav-link">
                            <i className={item.icon}></i>
                            <span className="nav-link-text">{item.text}</span>
                          </div>
                        </div>
                        <div className="col-3 pt-3">
                          {item.submenu && (
                            <i
                              className={`${
                                isShowDropdown === index + 1
                                  ? "fa fa-angle-down"
                                  : "fa  fa-angle-right"
                              } float-right`}
                            ></i>
                          )}
                        </div>
                      </label>

                      <div
                        className={`dropdown-sidebar-menu ${
                          isShowDropdown === index + 1 ||
                          x.substr(0, item.path.length) === item.path
                            ? "show"
                            : ""
                        }`}
                        aria-labelledby="dropdownMenuLink"
                      >
                        {item.submenu &&
                          item.submenu.length > 0 &&
                          item.submenu.map((sub) => (
                            // eslint-disable-next-line jsx-a11y/anchor-is-valid
                            <NavLink
                              to={sub.path}
                              key={`dropdown-item_${sub.name}`}
                              className="nav-link-menu dropdown-item-menu-employer"
                              activeClassName="active_employer"
                            >
                              {sub.name}
                            </NavLink>
                          ))}
                      </div>
                    </li>
                  )
                )}
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
};

export default Sidebar;
