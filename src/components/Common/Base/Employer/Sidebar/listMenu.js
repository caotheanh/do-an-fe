export const listMenu = [
  {
    text: "Tổng quan",
    path: "/employer/dashboard",
    exact: true,
    icon: "fa fa-home",
  },
  {
    text: "Tin tuyển dụng",
    path: "/employer/tin-tuyen-dung",
    exact: "",
    icon: "fa fa-newspaper",
    submenu: [
      { name: "Thêm mới", path: "/employer/tin-tuyen-dung/them-moi" },
      { name: "Đang hoạt động", path: "/employer/tin-tuyen-dung/hoat-dong" },
      { name: "Đã xóa", path: "/employer/tin-tuyen-dung/da-xoa" },
      { name: "Hết hạn", path: "/employer/tin-tuyen-dung/het-han" },

      
    ],
  },
  // {
  //   text: "Ứng tuyển",
  //   path: "/employer/ung-tuyen",
  //   exact: false,
  //   icon: "fa fa-columns",
  //   submenu: [
  //     { name: "Đang chờ", path: "/employer/ung-tuyen/dang-cho" },
  //     { name: "Đã duyệt", path: "/employer/ung-tuyen/da-duyet" },
  //   ],
  // },
  {
    text: "Tìm ứng viên",
    path: "/employer/ung-vien",
    exact: false,
    icon: "fa fa-search-plus",
    submenu: [
      { name: "Kho ứng viên", path: "/employer/tim-ung-vien/kho-ung-vien" },
      // { name: "Ứng viên đã xem", path: "/employer/ung-vien/ung-vien-da-xem" },
    ],
  },
  {
    text: "Công ty",
    path: "/employer/cong-ty",
    exact: false,
    icon: "fa fa-sliders-h",
    submenu: [
      { name: "Thêm công ty", path: "/employer/cong-ty/them-moi" },
      { name: "Danh sách công ty", path: "/employer/cong-ty/danh-sach" },
    ],
  },
];
