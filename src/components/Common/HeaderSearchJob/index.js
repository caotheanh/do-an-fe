import React from "react";

const HeaderSearchJob = () => {
  return (
    <>
    <div
        className="header bg-primary"
        style={{
          backgroundImage: `url(/assets/img/job-cover.jpeg)`,
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
          backgroundSize: "cover",
          position: "relative",
        }}
      >
        <div className="container-fluid">
          <div className="header-body">
            <div className=" align-items-center py-5  text-center">
            <div className="row justify-content-center">
                  <div className="col-xl-6 col-lg-6 col-md-8 px-5">
                    <h1 className="text-white">Việc làm Poly</h1>
                  </div>
                </div>
              <div className="justify-content-center">
                <div className="card">
                  <div className="card-header ">
                    <form action="#" className="search-job">
                      <div className="row no-gutters">
                        <div className="col-md-10 mr-md-2">
                          <div className="form-group">
                            <div className="form-field">
                              <input
                                type="text"
                                className="form-control"
                                placeholder=" Nhập công công việc tìm kiếm bạn mong muốn"
                              />
                            </div>
                          </div>
                        </div>
                        <div className="col-md mr-md-2">
                          <div className="form-group">
                            <div className="form-field">
                              <button
                                type="submit"
                                className="form-control btn btn-primary"
                              >
                                Tìm kiếm
                              </button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default HeaderSearchJob;
