import React from "react";
import { Link } from "react-router-dom";
import { api } from "../../../api";
import { DataAddress } from "../../../containers/Data/DataAddress";
import FormatDate from "../../../utils/FormatDate";
import TypeJob from "../../../utils/TypeJob";
function CardJobMobile({ handleLikeJob, isLikeJob, index, job }) {
  let typeJobDetail = "";
  if (job && job.type) {
    const types = TypeJob.find((item) => +job.type === +item.id);
    typeJobDetail = types ? types.name : "";
  }
  return (
    <div key={`job_${index}`} className="col-lg-6 col-md-6">
      <div className=" btn-card ">
          <div className="row">
            <div className="col-3">
              <img
                alt="images"
                src={`${api}/${job.img}`}
                className="img-fluid img-center"
                style={{ width: "5rem" }}
              />
            </div>
            <div className="col-7">
              <Link
                exact="true"
                to={`/viec-lam/${job.id}`}
                className="h4 font-weight-bold mt-0"
              >
                {job.title}
              </Link>
            </div>
            <div className="col-2">
              <p
                className="mt-0 mb-0 text-sm"
                onClick={() => handleLikeJob(job)}
              >
                <i
                  data-tip={
                    (isLikeJob || []).findIndex((item) => job.id === item.id) >
                    -1
                      ? "Bỏ Lưu"
                      : "Lưu"
                  }
                  data-for="like"
                  data-background-color="#5e72e5"
                  className={
                    (isLikeJob || []).findIndex((item) => job.id === item.id) >
                    -1
                      ? "la la-heart"
                      : "la la-heart-o"
                  }
                ></i>
              </p>
            </div>
          </div>
          <div className="d-flex flex-column text-left mt-3">
            <span className="mb-2 text-xs text-left">
              Hình thức làm việc:{" "}
              <span className="text-dark font-weight-bold ms-2">
                {typeJobDetail}
              </span>
            </span>
            <span className="mb-2 text-xs">
              Địa điểm làm việc:{" "}
              <span className="text-dark ms-2 font-weight-bold">
                {DataAddress[job.province_or_city][1]}
              </span>
            </span>
            <span className="text-xs">
              Hạn nộp hồ sơ:{" "}
              <span className="text-dark ms-2 font-weight-bold">
                {FormatDate(job.end)}
              </span>
            </span>
          </div>
          <div className="mt-3 justify-content-center">
            <Link to={`/viec-lam/${job.id}`}>
              <span
                className="h3 font-weight-bold"
                style={{ color: "#5e72e4" }}
              >
                Xem công việc
                <i className="la la-arrow-right ml-1"></i>
              </span>
            </Link>
          </div>
      </div>
    </div>
  );
}

export default CardJobMobile;
