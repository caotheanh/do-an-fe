import React from "react";

const Pagination = () => {
  return (
    <div aria-label="..." className="d-flex justify-content-center mt-5">
      <ul className="pagination">
        <li className="page-item disabled">
          <div className="page-link"  tabIndex="-1">
            <i className="fa fa-angle-left"></i>
            <span className="sr-only">Previous</span>
          </div>
        </li>
        <li className="page-item">
          <div className="page-link" >
            1
          </div>
        </li>
        <li className="page-item active">
          <div className="page-link" >
            2 <span className="sr-only">(current)</span>
          </div>
        </li>
        <li className="page-item">
          <div className="page-link" >
            3
          </div>
        </li>
        <li className="page-item">
          <div className="page-link" >
            <i className="fa fa-angle-right"></i>
            <span className="sr-only">Next</span>
          </div>
        </li>
      </ul>
    </div>
  );
};

export default Pagination;
