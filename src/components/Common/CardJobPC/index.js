import React from "react";
import NumberFormat from "react-number-format";
import { Link } from "react-router-dom";
import ReactTooltip from "react-tooltip";
import { api } from "../../../api";
import { DataAddress } from "../../../containers/Data/DataAddress";
import FormatDate from "../../../utils/FormatDate";
import TypeJob from "../../../utils/TypeJob";
import Image from "../Image";

function CardJobPC({ job, handleLikeJob, isLikeJob }) {
  let typeJobDetail = "";
  if (job && job.type) {
    const types = TypeJob.find((item) => +job.type === +item.id);
    typeJobDetail = types ? types.name : "";
  }
  return (
    <div className="col-lg-6">
      <li className="list-group-item border-0 d-flex m-1   bg-gray border-radius-lg p-3 row">
        <Link
          exact="true"
          to={`/viec-lam/${job.id}`}
          className="col-lg-3 text-center"
        >
          <Image url={`${api}/${job.img}`} size="6rem" />
        </Link>
        <div className="d-flex flex-column col-lg-6">
          <Link
            exact="true"
            to={`/viec-lam/${job.id}`}
            className="h6 mb-2 text-sm"
          >
            {job && job.title.length > 35
              ? job.title.substr(0, 35) + "..."
              : job.title}
          </Link>
          <span className="mb-1 text-xs">
            Hình thức:{" "}
            <span className="text-dark font-weight-bold ms-2">
              {typeJobDetail}
            </span>
          </span>
          <span className="mb-1 text-xs">
            Địa điểm:{" "}
            <span className="text-dark ms-2 font-weight-bold">
              {DataAddress[job.province_or_city][1]}
            </span>
          </span>
          <span className="text-xs">
            Hạn nộp hồ sơ:{" "}
            <span className="text-dark ms-2 font-weight-bold">
              {" "}
              {FormatDate(job.end)}
            </span>
          </span>
        </div>
        <div className="col-lg-3">
          <div className="text-lg-right">
            <div className="mt-0 mb-0" onClick={() => handleLikeJob(job)}>
              <i
                data-tip={
                  (isLikeJob || []).findIndex((item) => job.id === item.id) > -1
                    ? "Bỏ Lưu"
                    : "Lưu"
                }
                data-for="like"
                data-background-color="#5e72e5"
                className={
                  (isLikeJob || []).findIndex((item) => job.id === item.id) > -1
                    ? "la la-heart"
                    : "la la-heart-o"
                }
              ></i>
              <ReactTooltip
                className="extraClass"
                delayHide={0}
                effect="solid"
                id="like"
              />
            </div>
            <div className="mt-3 mb-0 text-sm">
              <span className="h6 font-weight-bold">
                <NumberFormat
                  value={job.salary}
                  className="foo"
                  displayType={"text"}
                  thousandSeparator={true}
                  renderText={(value, props) => (
                    <div {...props}>
                      {value ? value + " VNĐ" : "Thương lượng"}
                    </div>
                  )}
                />
              </span>
            </div>
            <div className="mt-3 mb-0 text-sm">
              <Link
                exact="true"
                to={`/viec-lam/${job.id}`}
                className="h3 font-weight-bold mt-0"
              >
                <span className="h6 font-weight-bold">
                  Xem công việc
                  <i className="la la-arrow-right"></i>
                </span>
              </Link>
            </div>
          </div>
        </div>
      </li>
    </div>
  );
}

export default CardJobPC;
