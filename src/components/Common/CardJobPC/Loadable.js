import Loadable from "react-loadable";
import ContentLoaderJob from "../../../containers/Page/PageHome/Home/ContentLoader/ContentLoaderJob";
export default Loadable({
  loader: () => import("./index"),
  loading() {
    return <ContentLoaderJob />;
  },
});
