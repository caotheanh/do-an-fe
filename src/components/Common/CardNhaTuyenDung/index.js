import React from "react";

function CardNhaTuyenDung({ index }) {
  return (
    <div key={`job_${index}`} className="col-lg-12">
      <button type="button" className="btn-icon-clipboard">
        <div className="row">
          <div className="col-3">
            <img
              alt="images"
              src="https://static.ketnoiviec.net/company_logo/ketnoiviec_default_logo.jpg"
              className="img-fluid img-right"
              style={{ width: "8rem" }}
            />
          </div>
          <div className="col-9 text-top">
            <div className="h3 font-weight-bold mt-0">
              Nhân viên vận hành game
            </div>
            <p className="mt-0 mb-0 text-sm">
              Hình thức làm việc: Toàn thời gian
            </p>
            <div></div>
          </div>
        </div>
      </button>
    </div>
  );
}

export default CardNhaTuyenDung;
