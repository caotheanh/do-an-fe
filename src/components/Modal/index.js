import React, { useState } from "react";

function ModalComponent({ title, component, text, buttonSubmit }) {
  const [isDisplay, setIsDisplay] = useState(false);
  return (
    <>
      <div onClick={() => setIsDisplay(!isDisplay)}>{text}</div>
      <div
        className="modal fade show "
        id="exampleModal"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
        style={isDisplay ? { display: "block" } : { display: "none" }}
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title" id="exampleModalLabel">
                {title}
              </h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true" onClick={() => setIsDisplay(false)}>
                  &times;
                </span>
              </button>
            </div>
            <div className="modal-body">{component}</div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
                onClick={() => setIsDisplay(false)}
              >
                Close
              </button>
              {buttonSubmit}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ModalComponent;
