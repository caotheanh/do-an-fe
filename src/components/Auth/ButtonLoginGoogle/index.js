import React from "react";
import { useGoogleLogin } from "react-google-login";

function ButtonLoginGoogle({ clientId, buttonText, onSuccess, onFailure }) {
  const { signIn } = useGoogleLogin({
    clientId,
    buttonText,
    onSuccess,
    onFailure,
  });
  return (
    <button onClick={signIn} className="btn btn-neutral btn-icon">
      <span className="btn-inner--icon">
        <img alt="images s" src="../assets/img/icons/common/google.svg" />
      </span>
      <span className="btn-inner--text">Google</span>
    </button>
  );
}

export default ButtonLoginGoogle;
