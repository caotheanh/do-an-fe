import React from "react";

function ThongTinThem({name, stt,handleChangInfoUser}) {
  return (
    <div >
      <div
        id="cv-more-infomation-content"
        contentEditable=""
        onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 1, "sở thích", null);
            }}
        cv-placeholder="Điền các thông tin khác nếu có"
      ></div>
    </div>
  );
}

export default ThongTinThem;
