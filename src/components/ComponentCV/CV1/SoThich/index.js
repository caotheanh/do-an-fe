import React from "react";

function SoThich({ name, stt,handleChangInfoUser}) {
  return (
    <div >
      <div
        id="cv-favorite-content"
        contentEditable=""
        onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 1, "sở thích", null);
            }}
        cv-placeholder="Nói ngắn gọn về sở thích của cá nhân"
      ></div>
    </div>
  );
}

export default SoThich;
