import React from "react";

function DuAn({name,stt,handleChangInfoUser}) {
  return (
    <div >
      <div className="cv-project-element cv-child-element">
        <div className="col-4 cv-element-time-duration">
          <div
            style={{ width: "100%" }}
            className="cv-project-name"
            contentEditable=""
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Tên dự án", null);
            }}
            cv-placeholder="Tên dự án"
          ></div>
          <div
            className="cv-project-start"
            contentEditable=""
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Bắt đầu", null);
            }}
            cv-placeholder="Bắt đầu"
          ></div>
          &nbsp;-&nbsp;
          <div
            className="cv-project-end"
            contentEditable=""
            cv-placeholder="Kết thúc"
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Kết thúc", null);
            }}
          ></div>
        </div>
        <div className="col-8 cv-element-content">
          <div
            className="cv-project-position"
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Vị trí tham gia", null);
            }}
            contentEditable=""
            cv-placeholder="Vị trí tham gia"
          ></div>
          <div
            className="cv-project-description"
            contentEditable=""
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "chi tiết", null);
            }}
            cv-placeholder="Mô tả chi tiết"
          ></div>
        </div>
      </div>
    </div>
  );
}

export default DuAn;
