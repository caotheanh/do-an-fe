import React, { useState } from "react";

function KiNang({ name, stt, handleChangInfoUser }) {
  const [pointSkill, setPointSkill] = useState(1);
  return (
    <div>
      <div className="cv-skill-element cv-child-element">
        <div className="col-4">
          <div
            className="cv-skill-name"
            contentEditable=""
            cv-placeholder="Name"
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Name", null);
            }}
          ></div>
        </div>
        <div className="col-8 cv-element-content">
          <div
            className="cv-skill-value"
            cv-placeholder="Kỹ năng"
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Kỹ năng", null);
            }}
            cv-type-skill="bar"
          >
            <div
              onClick={() => {
                setPointSkill(1);
                handleChangInfoUser(1, name, stt, 2, "point", null)
              }}
              className={`cv-skill-bar ${pointSkill >= 1 ? "cv-skill-bar-active" : ""
                }`}
            ></div>
            <div
              onClick={() => {
                setPointSkill(1);
                handleChangInfoUser(2, name, stt, 2, "point", null)
              }}
              className={`cv-skill-bar ${pointSkill >= 2 ? "cv-skill-bar-active" : ""
                }`}
            ></div>
            <div
              onClick={() => {
                setPointSkill(3);
                handleChangInfoUser(3, name, stt, 2, "point", null);
              }}
              className={`cv-skill-bar ${pointSkill >= 3 ? "cv-skill-bar-active" : ""
                }`}
            ></div>
            <div
              onClick={() => {
                setPointSkill(4);
                handleChangInfoUser(4, name, stt, 2, "point", null);
              }}
              className={`cv-skill-bar ${pointSkill >= 4 ? "cv-skill-bar-active" : ""
                }`}
            ></div>
            <div
              onClick={() => {
                setPointSkill(5);
                handleChangInfoUser(5, name, stt, 2, "point", null);
              }} className={`cv-skill-bar ${pointSkill >= 5 ? "cv-skill-bar-active" : ""
                }`}
            ></div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default KiNang;
