import React from "react";
const ButtonTool = () => {
  return (
    <div
      className="dropdown-menu "
      style={{ background: "initial", boxShadow: "none" }}
    >
      <div
        style={{
          top: "0",
          left: "0px",
          transform: "translate3d(0px, 0px)",
        }}
        role="group"
        aria-label="Button group with nested dropdown"
      >
        <button type="button" className="m-btn btn btn-secondary">
          <i className="la la-sort-asc"></i>
        </button>
        <button type="button" className="m-btn btn btn-secondary">
          <i className="la la-sort-desc"></i>
        </button>
        <button type="button" className="m-btn btn btn-secondary">
          <i className="flaticon-close"></i>
        </button>
        <button type="button" className="m-btn btn btn-secondary">
          <i className="flaticon-close"></i>
        </button>
      </div>
    </div>
  );
};

export default ButtonTool;
