import React from "react";

function HocVan({name, stt,handleChangInfoUser}) {
  return (
    <div>
      <div className="cv-education-element cv-child-element">
        <div className="col-4 cv-element-time-duration">
          <div style={{ width: "100%" }}>
            <div
              className="cv-education-school"
              contentEditable=""
              onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Tên trường học", null);
            }}
              cv-placeholder="Tên trường học"
            ></div>
          </div>
          <div
            className="cv-education-start"
            contentEditable=""
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Bắt đầu", null);
            }}
            cv-placeholder="Bắt đầu"
          ></div>
          &nbsp;-&nbsp;
          <div
            className="cv-education-end"
            contentEditable=""
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Kết thúc", null);
            }}
            cv-placeholder="Kết thúc"
          ></div>
        </div>
        <div className="col-8 cv-element-content">
          <div
            className="cv-education-subject"
            contentEditable=""
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Ngành học", null);
            }}
            cv-placeholder="Ngành học / Môn học"
          ></div>
          <div
            className="cv-education-description"
            contentEditable=""
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "chi tiết", null);
            }}
            cv-placeholder="Mô tả chi tiết"
          ></div>
        </div>
      </div>
    </div>
  );
}

export default HocVan;
