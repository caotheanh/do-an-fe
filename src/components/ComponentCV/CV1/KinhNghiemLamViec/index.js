import React from "react";

function KinhNghiemLamViec({name, stt,handleChangInfoUser}) {
  return (
    <div>
      <div className="cv-experience-element cv-child-element">
        <div className="col-4 cv-element-time-duration">
          <div style={{ width: "100%" }}>
            <div
              className="cv-experience-name"
              contentEditable=""
              onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Tên công ty", null);
            }}
              cv-placeholder="Tên công ty"
            ></div>
          </div>
          <div
            className="cv-experience-start"
            contentEditable=""
            cv-placeholder="Bắt đầu"
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Bắt đầu", null);
            }}
          ></div>
          &nbsp;-&nbsp;
          <div
            className="cv-experience-end"
            contentEditable=""
            cv-placeholder="Kết thúc"
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Kết thúc", null);
            }}
          ></div>
        </div>
        <div className="col-8 cv-element-content">
          <div
            className="cv-experience-position"
            contentEditable=""
            cv-placeholder="Vị trí công việc"
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Vị trí công việc", null);
            }}
          ></div>
          <div
            className="cv-experience-description"
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "chi tiết", null);
            }}
            contentEditable=""
            cv-placeholder="Mô tả chi tiết công việc, những gì đạt được trong quá trình làm việc"
          ></div>
        </div>
      </div>
    </div>
  );
}

export default KinhNghiemLamViec;
