import AnhDaiDien from "../AnhDaiDien";
import CaNhan from "../CaNhan";
import KinhNghiemLamViec from "../KinhNghiemLamViec";
import HocVan from "../HocVan";
import HoatDong from "../HoatDong";
import GiaiThuong from "../GiaiThuong";
import ThongTinThem from "../ThongTinThem";
import ChungChi from "../ChungChi";
import DuAn from "../DuAn";
import NguoiThamChieu from "../NguoiThamChieu";
import SoThich from "../SoThich"; 
import KiNang from "../KiNang";
import Info from "../Info";
import GioiThieu from "../GioiThieu";
export {
  AnhDaiDien,
  CaNhan,
  KinhNghiemLamViec,
  HocVan,
  HoatDong,
  GiaiThuong,
  ThongTinThem,
  ChungChi,
  DuAn,
  NguoiThamChieu,
  SoThich,
  KiNang,
  GioiThieu,
  Info,
};
