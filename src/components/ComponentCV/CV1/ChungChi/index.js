import React from "react";

function ChungChi({ name, stt,handleChangInfoUser }) {
  return (
    <div>
      <div className="cv-certificate-element cv-child-element">
        <div className="col-4">
          <div
            className="cv-certificate-time"
            contentEditable=""
            cv-placeholder="Năm"
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Năm", null);
            }}
          ></div>
        </div>
        <div className="col-8 cv-element-content">
          <div
            className="cv-certificate-name"
            contentEditable=""
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Tên chứng chỉ", null);
            }}
            cv-placeholder="Tên chứng chỉ"
          ></div>
        </div>
      </div>
    </div>
  );
}

export default ChungChi;
