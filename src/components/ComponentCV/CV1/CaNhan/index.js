import React from "react";
function CaNhan({ name, stt, handleChangInfoUser }) {
  return (
    <div id="cv-personal-informationblockControls dropdown">
      <div>
        <div id="cv-infomation">
          <div className="row-infomation">
            <div className="cv-infomation-text">Ngày sinh</div>
            <div
              style={{
                fontWeight: "500",
                marginRight: "12px",
              }}
            >
              :
            </div>
            <div
              className="infomation-content cv-infomation-text"
              contentEditable=""
              cv-form-field="true"
              cv-placeholder="Ngày sinh"
              onInput={function (e) {
                handleChangInfoUser(
                  e.target.innerHTML,
                  name,
                  stt,
                  1,
                  "Ngày sinh",
                  null
                );
              }}
              id="cv-infomation-birthday"
            ></div>
          </div>
          <div className="row-infomation">
            <div
              className="cv-infomation-text"
              style={{
                marginRight: "12px",
                width: "70px",
              }}
            >
              Giới tính
            </div>
            <div
              style={{
                fontWeight: "500",
                marginRight: "12px",
              }}
            >
              :
            </div>
            <div
              className="infomation-content cv-infomation-text"
              contentEditable=""
              cv-placeholder="Giới tính"
              id="cv-infomation-gender"
              onInput={function (e) {
                handleChangInfoUser(
                  e.target.innerHTML,
                  name,
                  stt,
                  1,
                  "Giới tính",
                  null
                );
              }}
              style={{ outline: "initial" }}
            ></div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CaNhan;
