import React from "react";

function GioiThieu({handleChangInfoUser,name,stt,qty}) {
  return (
    <div>
      <div
        onInput={function (e) {
          handleChangInfoUser(e.target.innerHTML,name,stt,1,2,null);
        }}
        id="cv-objectives-cothiệuntent"
        contentEditable
        cv-placeholder="Giới  bản thân, mục tiêu nghề nghiệp: ngắn hạn, dài hạn"
      ></div>
    </div>
  );
}

export default GioiThieu;
