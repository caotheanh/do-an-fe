import React from "react";

function NguoiThamChieu({name, stt, handleChangInfoUser}) {
  return (
    <div id="cv-reference" >
      <div className="cv-reference-element cv-child-element">
        <div className="cv-element-content">
          <div
            className="cv-reference-name"
            contentEditable=""
            cv-placeholder="Tên người tham chiếu"
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Tên người tham chiếu", null);
            }}
          ></div>
          <div
            className="cv-reference-position"
            contentEditable=""
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Vị trí", null);
            }}
            cv-placeholder="Vị trí hiện tại"
          ></div>
          <div
            className="cv-reference-email"
            contentEditable=""
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Địa chi email", null);
            }}
            cv-placeholder="Địa chi email"
          ></div>
          <div
            className="cv-reference-phonenumber"
            contentEditable=""
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Số điện thoại", null);
            }}
            cv-placeholder="Số điện thoại"
          ></div>
        </div>
      </div>
    </div>
  );
}

export default NguoiThamChieu;
