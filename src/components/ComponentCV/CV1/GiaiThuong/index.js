import React from "react";

function GiaiThuong({ handleChangInfoUser, name, stt, qty }) {
  return (
    <div>
      <div className="cv-prize-element cv-child-element">
        <div className="col-4 cv-element-time-duration">
          <div
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Ngày nhận giải", null);
            }}
            className="cv-prize-time"
            contentEditable=""
            cv-placeholder="Ngày nhận giải"
          ></div>
        </div>
        <div className="col-8 cv-element-content">
          <div
           onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Tên giải thưởng", null);
            }}
            className="cv-prize-description"
            contentEditable=""
            cv-placeholder="Tên giải thưởng"
          ></div>
        </div>
      </div>
    </div>
  );
}

export default GiaiThuong;
