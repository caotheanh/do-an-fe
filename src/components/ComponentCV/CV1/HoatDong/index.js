import React from "react";

function HoatDong({handleChangInfoUser,name,stt}) {
  
  return (
    <div >
      <div className="cv-activity-element cv-child-element">
        <div className="col-4 cv-element-time-duration">
          <div
           onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Tên tổ chức", null);
            }}
            style={{ width: "100%" }}
            className="cv-activity-name"
            contentEditable=""
            cv-placeholder="Tên tổ chức"
          ></div>
          <div
            className="cv-activity-start"
            contentEditable=""
            cv-placeholder="Bắt đầu"
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Bắt đầu", null);
            }}
          ></div>
          &nbsp;-&nbsp;
          <div
            className="cv-activity-end"
            contentEditable=""
            cv-placeholder="Kết thúc"
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Kết thúc", null);
            }}
          ></div>
        </div>
        <div className="col-8 cv-element-content">
          <div
            className="cv-activity-position"
            contentEditable=""
            cv-placeholder="Vị trí tham gia"
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Vị trí tham gia", null);
            }}
          ></div>
          <div
            className="cv-activity-description"
            contentEditable=""
            onInput={function (e) {
              handleChangInfoUser(e.target.innerHTML, name, stt, 2, "Chi tiết", null);
            }}
            cv-placeholder="Mô tả chi tiết các hoạt động đã tham gia"
          ></div>
        </div>
      </div>
    </div>
  );
}

export default HoatDong;
