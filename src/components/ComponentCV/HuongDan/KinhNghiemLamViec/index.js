import React from "react";

function KinhNghiemLamViec() {
  return (
    <div>
      <div className="cv-experience-element cv-child-element">
        <div className="col-4 cv-element-time-duration">
          <div style={{ width: "100%" }}>
            <div
              className="cv-experience-name"
              contentEditable=""
              cv-placeholder="Tên công ty"
            ></div>
          </div>
          <div
            className="cv-experience-start"
            contentEditable=""
            cv-placeholder="Bắt đầu"
          ></div>
          &nbsp;-&nbsp;
          <div
            className="cv-experience-end"
            contentEditable=""
            cv-placeholder="Kết thúc"
          ></div>
        </div>
        <div className="col-8 cv-element-content">
          <div
            className="cv-experience-position"
            contentEditable=""
            cv-placeholder="Vị trí công việc"
          ></div>
          <div
            className="cv-experience-description"
            contentEditable=""
            cv-placeholder="Mô tả chi tiết công việc, những gì đạt được trong quá trình làm việc"
          ></div>
        </div>
      </div>
    </div>
  );
}

export default KinhNghiemLamViec;
