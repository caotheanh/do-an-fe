import React from 'react'

function GiaiThuong() {

    return (
        <div>
        <div className="cv-prize-element cv-child-element">
          <div className="col-4 cv-element-time-duration">
            <div
              className="cv-prize-time"
              contentEditable=""
              cv-placeholder="Ngày nhận giải"
            ></div>
          </div>
          <div className="col-8 cv-element-content">
            <div
              className="cv-prize-description"
              contentEditable=""
              cv-placeholder="Tên giải thưởng"
            ></div>
          </div>
        </div>
      </div>
     
    )
}

export default GiaiThuong
