import React from "react";

function NguoiThamChieu() {
  return (
    <div id="cv-reference" >
      <div className="cv-reference-element cv-child-element">
        <div className="cv-element-content">
          <div
            className="cv-reference-name"
            contentEditable=""
            cv-placeholder="Tên người tham chiếu"
          ></div>
          <div
            className="cv-reference-position"
            contentEditable=""
            cv-placeholder="Vị trí hiện tại"
          ></div>
          <div
            className="cv-reference-email"
            contentEditable=""
            cv-placeholder="Địa chi email"
          ></div>
          <div
            className="cv-reference-phonenumber"
            contentEditable=""
            cv-placeholder="Số điện thoại"
          ></div>
        </div>
      </div>
    </div>
  );
}

export default NguoiThamChieu;
