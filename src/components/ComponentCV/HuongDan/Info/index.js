import React from "react";

const Info = () => {
  return (
    <div className="cv-header-left">
      <div
        id="cv-infomation-name"
        contentEditable=""
        cv-placeholder="Tên của bạn"
        style={{ outline: "rgb(214, 74, 64) dashed 1px" }}
        validate-error="Không được để trống tên"
      ></div>
      <div
        id="cv-infomation-position"
        contentEditable=""
        cv-placeholder="Công việc bạn mong muốn"
      ></div>

      <div
        style={{
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <div className="row-infomation">
          <div className="infomation-icon cv-infomation-text text-center">
            <i className="fa fa-phone "></i>
          </div>
          <div>
            <span>Số điện thoại</span>
            <div
              className="infomation-content cv-infomation-text"
              contentEditable=""
              cv-placeholder="Số điện thoại"
              id="cv-infomation-phonenumber"
              style={{ outline: "initial" }}
              validate-error="Không để trống"
            ></div>
          </div>
        </div>
        <div className="row-infomation">
          <div className="infomation-icon cv-infomation-text"></div>
          <div>
            <span>Email</span>
            <div
              className="infomation-content cv-infomation-text"
              contentEditable=""
              cv-placeholder="Địa chỉ email"
              id="cv-infomation-email"
              style={{ outline: "initial" }}
              validate-error="Không để trống"
            ></div>
          </div>
        </div>
        <div className="row-infomation">
          <div className="infomation-icon cv-infomation-text"></div>
          <div>
            <span>Địa chỉ</span>
            <div
              className="infomation-content cv-infomation-text"
              contentEditable=""
              cv-placeholder="Địa chỉ hiện tại"
              id="cv-infomation-address"
              style={{ outline: "initial" }}
              validate-error="Không để trống"
            ></div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Info;
