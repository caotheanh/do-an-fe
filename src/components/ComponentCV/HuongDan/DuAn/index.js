import React from "react";

function DuAn() {
  return (
    <div >
      <div className="cv-project-element cv-child-element">
        <div className="col-4 cv-element-time-duration">
          <div
            style={{ width: "100%" }}
            className="cv-project-name"
            contentEditable=""
            cv-placeholder="Tên dự án"
          ></div>
          <div
            className="cv-project-start"
            contentEditable=""
            cv-placeholder="Bắt đầu"
          ></div>
          &nbsp;-&nbsp;
          <div
            className="cv-project-end"
            contentEditable=""
            cv-placeholder="Kết thúc"
          ></div>
        </div>
        <div className="col-8 cv-element-content">
          <div
            className="cv-project-position"
            contentEditable=""
            cv-placeholder="Vị trí tham gia"
          ></div>
          <div
            className="cv-project-description"
            contentEditable=""
            cv-placeholder="Mô tả chi tiết"
          ></div>
        </div>
      </div>
    </div>
  );
}

export default DuAn;
