import React from "react";

function ChungChi({ name }) {
  return (
    <div>
      <div className="cv-certificate-element cv-child-element">
        <div className="col-4">
          <div
            className="cv-certificate-time"
            contentEditable=""
            cv-placeholder="Năm"
          ></div>
        </div>
        <div className="col-8 cv-element-content">
          <div
            className="cv-certificate-name"
            contentEditable=""
            cv-placeholder="Tên chứng chỉ"
          ></div>
        </div>
      </div>
    </div>
  );
}

export default ChungChi;
