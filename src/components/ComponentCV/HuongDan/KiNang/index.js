import React, { useState } from "react";

function KiNang() {
  const [pointSkill, setPointSkill] = useState(1);
  return (
    <div>
      <div className="cv-skill-element cv-child-element">
        <div className="col-4">
          <div
            className="cv-skill-name"
            contentEditable=""
            cv-placeholder="Name"
          ></div>
        </div>
        <div className="col-8 cv-element-content">
          <div
            className="cv-skill-value"
            cv-placeholder="Kỹ năng"
            cv-type-skill="bar"
          >
            <div
              onClick={() => setPointSkill(1)}
              className={`cv-skill-bar ${
                pointSkill >= 1 ? "cv-skill-bar-active" : ""
              }`}
            ></div>
            <div
              onClick={() => setPointSkill(2)}
              className={`cv-skill-bar ${
                pointSkill >= 2 ? "cv-skill-bar-active" : ""
              }`}
            ></div>
            <div
              onClick={() => setPointSkill(3)}
              className={`cv-skill-bar ${
                pointSkill >= 3 ? "cv-skill-bar-active" : ""
              }`}
            ></div>
            <div
              onClick={() => setPointSkill(4)}
              className={`cv-skill-bar ${
                pointSkill >= 4 ? "cv-skill-bar-active" : ""
              }`}
            ></div>
            <div
              onClick={() => setPointSkill(5)}
              className={`cv-skill-bar ${
                pointSkill >= 5 ? "cv-skill-bar-active" : ""
              }`}
            ></div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default KiNang;
