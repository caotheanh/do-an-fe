import React from "react";

function SoThich() {
  return (
    <div >
      <div
        id="cv-favorite-content"
        contentEditable=""
        cv-placeholder="Nói ngắn gọn về sở thích của cá nhân"
      ></div>
    </div>
  );
}

export default SoThich;
