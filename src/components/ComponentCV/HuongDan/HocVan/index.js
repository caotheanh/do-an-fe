import React from "react";

function HocVan() {
  return (
    <div>
      <div className="cv-education-element cv-child-element">
        <div className="col-4 cv-element-time-duration">
          <div style={{ width: "100%" }}>
            <div
              className="cv-education-school"
              contentEditable=""
              cv-placeholder="Tên trường học"
            ></div>
          </div>
          <div
            className="cv-education-start"
            contentEditable=""
            cv-placeholder="Bắt đầu"
          ></div>
          &nbsp;-&nbsp;
          <div
            className="cv-education-end"
            contentEditable=""
            cv-placeholder="Kết thúc"
          ></div>
        </div>
        <div className="col-8 cv-element-content">
          <div
            className="cv-education-subject"
            contentEditable=""
            cv-placeholder="Ngành học / Môn học"
          ></div>
          <div
            className="cv-education-description"
            contentEditable=""
            cv-placeholder="Mô tả chi tiết"
          ></div>
        </div>
      </div>
    </div>
  );
}

export default HocVan;
