import React from "react";
import ImageUploading from "react-images-uploading";

function AnhDaiDien() {
  const [images, setImages] = React.useState([]);
  const maxNumber = 69;
  const onChange = (imageList, addUpdateIndex) => {
    // data for submit
    setImages(imageList);
  };
  return (
    <div id="" data-toggle="modal" data-target="#avatar-modal">
      {/* <img
        style={{
          width: "120px",
          height: "120px",
        }}
        alt=""
        src="https://m.media-amazon.com/images/M/MV5BMjM2OTkyNTY3N15BMl5BanBnXkFtZTgwNzgzNDc2NjE@._V1_CR132,0,761,428_AL_UY268_CR82,0,477,268_AL_.jpg"
        value=""
      /> */}
      <ImageUploading
        multiple
        value={images}
        onChange={onChange}
        maxNumber={maxNumber}
        dataURLKey="data_url"
      >
        {({
          imageList,
          onImageUpload,
          onImageRemoveAll,
          onImageUpdate,
          onImageRemove,
          isDragging,
          dragProps,
        }) => (
          // write your building UI
          <div className="upload__image-wrapper " >
            {imageList.length === 0 && (
              <div
                style={{
                  width: "210px",
                  height: "210px",
                  backgroundImage: `url(https://cvlogin.com/images/no-avatar.jpg)`,
                  backgroundPosition: "center",
                  backgroundRepeat: "no-repeat",
                  backgroundSize: "cover",
                  position: "relative",
                }}
                onClick={onImageUpload}
              ></div>
            )}

            {imageList.map((image, index) => (
              <div
                onClick={() => onImageUpdate(index)}
                style={{
                  width: "210px",
                  height: "210px",
                  backgroundImage: `url(${image.data_url})`,
                  backgroundPosition: "center",
                  backgroundRepeat: "no-repeat",
                  backgroundSize: "cover",
                  position: "relative",
                }}
              ></div>
            ))}
          </div>
        )}
      </ImageUploading>
    </div>
  );
}

export default AnhDaiDien;
