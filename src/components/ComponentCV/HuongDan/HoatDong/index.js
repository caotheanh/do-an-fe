import React from "react";

function HoatDong() {
  
  return (
    <div >
      <div className="cv-activity-element cv-child-element">
        <div className="col-4 cv-element-time-duration">
          <div
            style={{ width: "100%" }}
            className="cv-activity-name"
            contentEditable=""
            cv-placeholder="Tên tổ chức"
          ></div>
          <div
            className="cv-activity-start"
            contentEditable=""
            cv-placeholder="Bắt đầu"
          ></div>
          &nbsp;-&nbsp;
          <div
            className="cv-activity-end"
            contentEditable=""
            cv-placeholder="Kết thúc"
          ></div>
        </div>
        <div className="col-8 cv-element-content">
          <div
            className="cv-activity-position"
            contentEditable=""
            cv-placeholder="Vị trí tham gia"
          ></div>
          <div
            className="cv-activity-description"
            contentEditable=""
            cv-placeholder="Mô tả chi tiết các hoạt động đã tham gia"
          ></div>
        </div>
      </div>
    </div>
  );
}

export default HoatDong;
