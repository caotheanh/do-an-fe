import React from "react";

function GioiThieu() {
  return (
    <div>
      <div
        id="cv-objectives-content"
        contentEditable=""
        cv-placeholder="Giới thiệu bản thân, mục tiêu nghề nghiệp: ngắn hạn, dài hạn"
      ></div>
    </div>
  );
}

export default GioiThieu;
