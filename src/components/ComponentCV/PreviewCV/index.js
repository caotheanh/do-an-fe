import React from "react";
import { Viewer, Worker } from "@react-pdf-viewer/core";
import "@react-pdf-viewer/core/lib/styles/index.css";
import { api } from "../../../api";
const PreviewCV = ({ cv }) => (
  <Worker workerUrl="https://unpkg.com/pdfjs-dist@2.6.347/build/pdf.worker.min.js">
    <Viewer onLoadError={console.error} fileUrl={`${api}/cv/${cv}`} />
  </Worker>
);

export default PreviewCV;
