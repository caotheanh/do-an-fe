import axios from "axios";
import React, { useEffect, useState } from "react";
import FormatDate from "../../../../utils/FormatDate";
import { api } from "../../../../api";
import { gettoken } from "../../../../utils/gettoken";
import { useForm } from "react-hook-form";
import NotificationManager from "react-notifications/lib/NotificationManager";
import StatusApply from "../../../../utils/StatusApply";
function ModalViewHistory({ id, item }) {
  const [noteApply, setnoteApply] = useState([]);
  useEffect(() => {
    axios
      .get(`${api}/note-apply/${id}`)
      .then(function (response) {
        setnoteApply(response.data);
      })
      .catch(function (error) {
        if (error.response) {
        }
      });
  }, [id]);
  const { handleSubmit, errors, register } = useForm();
  const token = gettoken();
  const date = new Date();
  const handleAgree = (data) => {
    axios.put(`${api}/apply/change-status`, {
      token: token,
      status: data.status,
      id: id,
    });
    axios
      .post(
        `${api}/note-apply`,
        Object.assign(data, { token: token, id_apply: id })
      )
      .then(() => {
        NotificationManager.success(
          "Gửi phản hồi từ chối thành công",
          "thành công",
          3000
        );
        const dataNew = Object.assign(data, { id_apply: id, created_at: date });
        const noteApplyTemp = [...noteApply];
        noteApplyTemp.push(dataNew);
        setnoteApply(noteApplyTemp);
      });
  };
  return (
    <div
      className="modal fade"
      id={`viewHistory${id}`}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div
        className="col-lg-8 modal-dialog modal-dialog-centered"
        role="document"
      >
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Chi tiết ứng tuyển
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div className="row">
              <div
                className={`card-body ${
                  ![0, 7].includes(item.status) ? "col-lg-6" : "col-lg-12"
                }`}
                style={{ overflow: "auto", maxHeight: "60vh" }}
              >
                <div className="timeline timeline-one-side">
                  <div className="timeline-block mb-3">
                    <span className="timeline-step">
                      <i className="ni ni-bell-55 text-success text-gradient"></i>
                    </span>
                    <div className="timeline-content">
                      <h6 className="text-dark text-sm font-weight-bold mb-0">
                        Nộp CV ứng tuyển
                      </h6>
                      <p className=" font-weight-bold text-xs mt-1 mb-0">
                        {FormatDate(item.created_at)}
                      </p>
                    </div>
                  </div>
                  {noteApply &&
                    Array.isArray(noteApply) &&
                    noteApply.map((apply) => (
                      <div className="timeline-block mb-3" key={apply.id}>
                        <span className="timeline-step">
                          <i
                            className={`${
                              StatusApply.find((gg) => gg.id === +apply.status)
                                .icon
                            } ${
                              StatusApply.find((gg) => gg.id === +apply.status)
                                .color
                            } text-gradient`}
                          ></i>
                        </span>
                        <div className="timeline-content">
                          <h3>
                            {
                              StatusApply.find((gg) => gg.id === +apply.status)
                                .textEmployer
                            }
                          </h3>
                          <h6 className="text-dark text-sm mb-0">
                            Chú thích: {apply.content}
                          </h6>
                          <p className=" font-weight-bold text-xs mt-1 mb-0">
                            {FormatDate(apply.created_at)}
                          </p>
                        </div>
                      </div>
                    ))}
                </div>
              </div>
              {![0, 7].includes(item.status) && (
                <div className="col-lg-6">
                  <form
                    onSubmit={handleSubmit(handleAgree)}
                    className="container"
                  >
                    <div className="form-group">
                      <label
                        htmlFor="example-text-input"
                        className="form-control-label"
                      >
                        Trạng thái
                      </label>
                      <select
                        className="form-control"
                        name="status"
                        ref={register({
                          required: "Vui lòng chọn trạng thái",
                        })}
                      >
                        <option value="">Chọn trạng thái</option>
                        {StatusApply.map(
                          (apply) =>
                            apply.isHidden &&
                            apply.id > item.status && (
                              <option value={apply.id} key={apply.id}>
                                {apply.textEmployer}
                              </option>
                            )
                        )}
                      </select>
                      {errors.status && (
                        <p className="text-danger">{errors.status.message}</p>
                      )}
                    </div>
                    <div className="form-group">
                      <label
                        htmlFor="example-text-input"
                        className="form-control-label"
                      >
                        Ghi chú
                      </label>
                      <textarea
                        name="content"
                        ref={register({
                          required: "Vui lòng nhập ghi chú",
                        })}
                        className="form-control"
                        rows="3"
                      ></textarea>{" "}
                      {errors.content && (
                        <p className="text-danger">{errors.content.message}</p>
                      )}
                    </div>
                    <button className="mt-3 btn btn-default" type="submit">
                      Gửi phản hồi
                    </button>
                  </form>
                </div>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModalViewHistory;
