import React from "react";

const ModalSetDefault = ({ handleSetDefault, cv }) => {
  const handleClick = () => {
    document.querySelector(`.closeSetDefault${cv}`).click();
    handleSetDefault(cv);
  };
  return (
    <div
      className="modal fade show"
      id={`modalDefaultCV${cv}`}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Đặt CV mặc định.
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">Bạn muốn đặt CV mặc định ?</div>
          <div className="modal-footer">
            <button
              type="button"
              className={`btn btn-secondary closeSetDefault${cv}`}
              data-dismiss="modal"
            >
              Huỷ
            </button>
            <button
              type="button"
              onClick={handleClick}
              className="btn btn-outline-default"
            >
              Đặt mặc định
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalSetDefault;
