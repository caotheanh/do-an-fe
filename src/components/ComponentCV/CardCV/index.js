import React from "react";
import ModalDelte from "./ModalDelete";
import ModalPreviewPDF from "./ModalPreviewPDF";
import { api } from "../../../api";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import ModalSetDefault from "./ModalSetDefault";
import NotificationManager from "react-notifications/lib/NotificationManager";
import FormatDate from "../../../utils/FormatDate";

const CardCV = ({ cv, deleteCV, token }) => {
  const handleDelete = () => {
    deleteCV(token, cv.id);
  };
  const info_user = useSelector((state) => state.info_user);
  const dispatch = useDispatch();
  const handleSetDefault = (cv) => {
    axios
      .put(`${api}/user-infomation/choose-cv-default`, {
        id: info_user.id,
        link_cv: cv,
      })
      .then(() => {
        dispatch({ type: "SET_DEFAULT_CV", cv: cv });
        NotificationManager.success(
          "Thay đổi cv mặc định thành công.",
          "Thành công",
          3000
        );
      })
      .catch(() =>
        NotificationManager.error(
          "Thay đổi cv mặc định không thành công.",
          "Thất bại",
          3000
        )
      );
  };

  return (
    <div className="row">
      <div className="col-auto">
        <img
          alt="photos cv"
          src="/assets/img/CV_PDF.png"
          className="img-fluid img-center m-3"
          style={{ width: "5rem" }}
        />
      </div>
      <div className="col-sm">
        <div className="row">
          <div className="col-6">
            <h3 className=" text-uppercase text-muted mb-0 pl-2">{cv.name}</h3>
          </div>
          <div className="col-6 text-right pr-5">
            {FormatDate(cv.updated_at)}
          </div>
        </div>
        <div className="p-2 bg-secondary">
          <input
            type="text"
            defaultValue={`${api}/cv/${cv.link}`}
            className="form-control form-control-alternative"
            placeholder="Alternative input"
          />
        </div>
        <ModalPreviewPDF linkCV={cv.link} id={cv.id} />
        <div className="btn-wrapper text-center mb-2 mt-2">
          <button
            type="button"
            data-toggle="modal"
            data-target={`#previewPDF${cv.id}`}
            className="btn btn-secondary btn-sm btn-icon"
          >
            <i className="fa fa-eye"></i>
            <span className="btn-inner--text">Xem</span>
          </button>
          <a
            href={`${api}/cv/${cv.link}`}
            className="btn btn-secondary btn-sm btn-icon"
          >
            <i className="fa fa-file-download"></i>
            <span className="btn-inner--text">Tải xuống</span>
          </a>

          {info_user && info_user.cv !== cv.link ? (
            <div
              className="btn btn-secondary btn-sm  btn-icon"
              data-toggle="modal"
              data-target={`#modalDefaultCV${cv.link}`}
            >
              <i className="fa fa-hand-point-up"></i>
              <span className="btn-inner--text">Đặt mặc định</span>
            </div>
          ) : (
            <div className="btn btn-secondary btn-sm text-success  btn-icon">
              <i className="fa fa-check-square"></i>
              <span className="btn-inner--text">Mặc định</span>
            </div>
          )}
          <div
            className="btn btn-secondary btn-sm text-danger btn-icon"
            data-toggle="modal"
            data-target={`#modalDeleteCV${cv.link}`}
          >
            <i className="flaticon-delete"></i>
            <span className="btn-inner--text">Xóa</span>
          </div>
          <ModalSetDefault handleSetDefault={handleSetDefault} cv={cv.link} />
          <ModalDelte cv={cv.link} handleDelete={handleDelete} />
        </div>
      </div>
    </div>
  );
};

export default CardCV;
