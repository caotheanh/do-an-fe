import React from "react";
import { Link } from "react-router-dom";
import { api } from "../../../api";
import Image from "../../Common/Image";
function CardCompany({ company }) {
  return (
    <Link to={`/xem-cong-ty/${company.id}`} className="btn-card">
      <div className="row">
        <div className="col-lg-3">
          <Image url={`${api}/companyImg/${company.avt}`} size="3rem" />
        </div>
        <div className="col-lg-9 ">
          <div className="h5 pl-2 text-top font-weight-bold mt-0">{company.name}</div>
        </div>
      </div>
    </Link>
  );
}

export default CardCompany;
