import React from "react";
import HeaderEditCongTy from "../../../components/Common/Base/EmployerEditCongTy/Header";
import SidebarEditCongTy from "../../../components/Common/Base/EmployerEditCongTy/Sidebar";
import Footer from "../../../components/Common/Base/Home/Footer";

const EditCongTyLayout = (props) => {
  return (
    <div className="g-sidenav-show g-sidenav-hidden">
      <SidebarEditCongTy />
      <div className="main-content" id="panel">
        <HeaderEditCongTy />
        <div>{props.children}</div>
        <Footer />
      </div>
    </div>
  );
};

export default EditCongTyLayout;
