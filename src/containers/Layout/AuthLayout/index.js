import React from "react";
import Footer from "../../../components/Common/Base/LayoutAuth/Footer";
import Header from "../../../components/Common/Base/LayoutAuth/Header";

function AuthLayout(props) {
  return (
    <div className="bg-default g-sidenav-show g-sidenav-pinned" style={{minHeight: "100vh"}}>
      <Header />
      {props.children}
      <Footer />
    </div>
  );
}

export default AuthLayout;
