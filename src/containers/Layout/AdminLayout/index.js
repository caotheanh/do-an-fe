import React, { useState } from "react";
import SidebarAdmin from "../../../components/Common/Base/Admin/SidebarAdmin";
// import Footer from "../../../components/Common/Base/Employer/Footer";
function AdminLayout(props) {
  const [isActiveSidebarMenu, setIsActiveSidebarMenu] = useState(false);
  return (
    <div
      className={
        isActiveSidebarMenu
          ? "g-sidenav-show nav-open g-sidenav-pinned"
          : "g-sidenav-show g-sidenav-hidden"
      }
    >
      <SidebarAdmin />
      <div className="main-content" id="panel">
        <div onClick={() => setIsActiveSidebarMenu(false)}>
          {props.children}
        </div>
      </div>
    </div>
  );
}

export default AdminLayout;

