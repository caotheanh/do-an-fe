import React, { useEffect } from "react";
import { Link } from "react-router-dom";

function EmployerAuthLayout(props) {
  function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }
  useEffect(() => {
    topFunction();
  }, []);
  return (
    <div style={{ minHeight: "100vh" }} className="row">
      <div
        className="bg-secondary col-lg-3"
      >
        <div className="ml-5">
          <Link to="/" className="text-left ">
            <img
              src="../assets/img/brand/blue.png"
              alt="images"
              style={{ width: "40%" }}
            />
          </Link>
          <div className="h1 mb-3 mt-3">
            Đăng ký để sử dụng đầy đủ chức năng tuyển dụng.
          </div>
          <div className="mt-5">
            Với tài khoản tại Việc Làm Poly, bạn có thể:
            <p className="mt-3 mb-0 text-sm">
              <span className="text-success mr-2">
                <i className="ni ni-check-bold"></i>{" "}
              </span>
              <span className="">Đăng tuyền dụng nhanh chóng và dễ dàng</span>
            </p>
            <p className="mt-3 mb-0 text-sm">
              <span className="text-success mr-2">
                <i className="ni ni-check-bold"></i>
              </span>
              <span className="">
                Tìm kiếm hồ sơ với công cụ tìm kiếm thông minh
              </span>
            </p>
            <p className="mt-3 mb-0 text-sm">
              <span className="text-success mr-2">
                <i className="ni ni-check-bold"></i>
              </span>
              <span className="">Có ngay nhưng hồ sơ phù hợp</span>
            </p>
            <p className="mt-3 mb-0 text-sm">
              <span className="text-success mr-2">
                <i className="ni ni-check-bold"></i>
              </span>
              <span className="">
                Nhận được hồ sơ mới tự động với chức năng “thông báo hồ sơ”
              </span>
            </p>
          </div>
        </div>
        <div className="mt-5" style={{ width: "100%" }}>
          <img
            alt="avt"
            src="/assets/img/employer-register.png"
            className="text-center"
            style={{ width: "100%" }}
          />
        </div>
      </div>
      <div className="col-lg-9">
        {props.children}
      </div>
    </div>
  );
}

export default EmployerAuthLayout;
