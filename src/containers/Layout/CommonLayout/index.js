import React, { useEffect, useState } from "react";
import Footer from "../../../components/Common/Base/Home/Footer";
import Header from "../../../components/Common/Base/Home/Header";
function CommonLayout(props) {
  const [isActiveSidebarMenu, setIsActiveSidebarMenu] = useState(false);
  return (
    <>
      <Header
        setIsActiveSidebarMenu={setIsActiveSidebarMenu}
        isActiveSidebarMenu={isActiveSidebarMenu}
      />
      <div id="panel" style={{ minHeight: "80vh" }}>
        {props.children}
      </div>
      <Footer />
    </>
  );
}

export default CommonLayout;
