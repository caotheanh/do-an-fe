import React, { useState } from "react";
import Footer from "../../../components/Common/Base/Home/Footer";
import SidebarCompact from "../../../components/Common/Base/Home/SidebarCompact";

function CreateCVLayout(props) {
  const [isActiveSidebarMenu, setIsActiveSidebarMenu] = useState(false);

  return (
    <div
      className={
        isActiveSidebarMenu
          ? "g-sidenav-show nav-open g-sidenav-pinned"
          : "g-sidenav-show g-sidenav-hidden"
      }
    >
      <SidebarCompact />
      <div className="main-content" style={{ marginLeft: "110px" }} id="panel">
        <div onClick={() => setIsActiveSidebarMenu(false)}>
          {props.children}
        </div>
        <Footer />
      </div>
    </div>
  );
}

export default CreateCVLayout;
