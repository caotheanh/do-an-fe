import React, { useState } from "react";
import Sidebar from "../../../components/Common/Base/Home/Sidebar";
import Footer from "../../../components/Common/Base/Home/Footer";
function PageDetailLayout(props) {
  const [isActiveSidebarMenu, setIsActiveSidebarMenu] = useState(false);
  return (
    <div
      className={
        isActiveSidebarMenu
          ? "g-sidenav-show nav-open g-sidenav-pinned"
          : "g-sidenav-show g-sidenav-hidden"
      }
    >
      <Sidebar />
      <div className="main-content" id="panel">
        <div onClick={() => setIsActiveSidebarMenu(false)}>
          {props.children}
        </div>
        <Footer />
      </div>
    </div>
  );
}

export default PageDetailLayout;
