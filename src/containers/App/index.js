import React, { useEffect, useState } from "react";
import { Link, Redirect, Route, Switch } from "react-router-dom";
import CommonRouter from "../Router/CommonRouter/Loadable";
import Home from "../Page/PageHome/Home/Loadable";
import TimViecLam from "../Page/PageHome/TimViecLam";
import ViecLam from "../Page/PageHome/ViecLam";
import AuthRouter from "../Router/AuthRouter";
import LoginPage from "../Page/PageHome/Auth/LoginPage";
import RegisterPage from "../Page/PageHome/Auth/RegisterPage";
import CreateCV from "../Page/PageHome/CV/CreateCV";
import EmployerRouter from "../Router/EmployerRouter";
import EditCongTyRouter from "../Router/EditCongTyRouter";
import DashboarEmployer from "../Page/PageEmployer/DashboarEmployer";
import ThemTinTuyenDung from "../Page/PageEmployer/TinTuyenDung/ThemTinTuyenDung/Loadable";
import SuaTinTuyenDung from "../Page/PageEmployer/TinTuyenDung/SuaTinTuyenDung";
import ViecXungQuanh from "../Page/PageHome/ViecXungQuanh";
import DangCho from "../Page/PageEmployer/UngTuyen/DangCho";
import DaDuyet from "../Page/PageEmployer/UngTuyen/DaDuyet";
import DaXoa from "../Page/PageEmployer/UngTuyen/DaXoa";
import CVHome from "../Page/PageHome/CV/CVHome";
import ListTemplateCV from "../Page/PageHome/CV/ListTemplateCV";
import VerifyPage from "../Page/PageHome/Auth/VerifyPage";
import ViecLamDaLuu from "../Page/PageHome/ViecLamDaLuu";
import DanhSachTinTuyenDung from "../Page/PageEmployer/TinTuyenDung/DanhSachTinTuyenDung";
import ThemMoiCongTy from "../Page/PageEmployer/CongTy/ThemMoiCongTy";
import DanhSachCongTy from "../Page/PageEmployer/CongTy/DanhSachCongTy";
import TongQuanCongTy from "../Page/PageEmployer/CongTy/EditCongTy/TongQuanCongTy";
import KhoUngVien from "../Page/PageEmployer/TimUngVien/KhoUngVien";
import TaiKhoan from "../Page/PageHome/TaiKhoan/Loadable";
import CongTy from "../Page/PageHome/CongTy";
import { useDispatch, useSelector } from "react-redux";
import axios from "axios";
import { api } from "../../api";
import { gettoken } from "../../utils/gettoken";
import ViecLamDaUngTuyen from "../Page/PageHome/ViecLamDaUngTuyen";
import AuthEmployerRouter from "../Router/AuthEmployerRouter";
import RegisterEmployer from "../Page/Auth/AuthEmployer/RegisterEmployer";
import UngTuyenTin from "../Page/PageEmployer/UngTuyen/UngTuyenTin";
import { NotificationContainer } from "react-notifications";
import AdminRouter from "../Router/AdminRouter";
import ChiTietUngVien from "../Page/PageEmployer/TimUngVien/ChiTietUngVien";
import TimUngVienPhuHopTin from "../Page/PageEmployer/TimUngVien/TimUngVienPhuHopTin";
import AdminDanhSachUser from "../Page/PageAdmin/QuanLyUser/AdminDanhSachUser";
import AdminDanhSachDanhMuc from "../Page/PageAdmin/QuanLyDanhMuc/AdminDanhSachDanhMuc";
import AdminDanhSachCongTy from "../Page/PageAdmin/QuanLyCongTy/AdminDanhSachCongTy";
import AdminDanhSachTin from "../Page/PageAdmin/QuanLyTinTuyenDung/AdminDanhSachTin";
import AdminDashboard from "../Page/PageAdmin/DashBoard";
import AdminThemMoiDanhMuc from "../Page/PageAdmin/QuanLyDanhMuc/AdminThemMoiDanhMuc";
import DanhSachTinHetHan from "../Page/PageEmployer/TinTuyenDung/DanhSachTinHetHan";
import ChiTietUngTuyen from "../Page/PageHome/ChiTietUngTuyen";
import LoiMoiUngTuyen from "../Page/PageHome/LoiMoiUngTuyen";
import SuaCongTy from "../Page/PageEmployer/CongTy/SuaCongTy";
import TinTuyenDungDaXoa from "../Page/PageEmployer/TinTuyenDung/TinTuyenDungDaXoa";
import ChiTietCongTy from "../Page/PageHome/ChiTietCongTy/Loadable";
import MoiUngTuyen from "../Page/PageEmployer/UngTuyen/MoiUngTuyen";
import LoaderCommonRouter from "../Router/CommonRouter/LoaderCommonRouter";
import ForgotPasswordPage from "../Page/PageHome/Auth/ForgotPasswordPage";

function App() {
  const token = gettoken();
  const isLogin = useSelector((state) => state.is_login);
  const dispatch = useDispatch();
  const [statusLogin, setStatusLogin] = useState(false);
  const [isCallData, setisCallData] = useState(false);
  useEffect(() => {
    if (token) {
      axios.get(`${api}/category`).then((res) => {
        dispatch({ type: "SET_CATEGORIES", categories: res.data });
      });
      axios
        .get(`${api}/user/${token}`)
        .then((res) => {
          if (res.data.data.status !== 401) {
            setStatusLogin(true);
            dispatch({ type: "SET_IS_LOGIN", is_login: true });
            dispatch({ type: "SET_INFO_LOGIN", info_login: res.data.data });
          }
        })
        .finally(() => setisCallData(true));
    } else {
      setisCallData(true);
      dispatch({ type: "SET_INFO_USER", info_user: {} });
      dispatch({ type: "SET_INFO_LOGIN", info_login: null });
      dispatch({ type: "SET_TOKEN", token: null });
      dispatch({ type: "SET_IS_LOGIN", is_login: false });
    }
    const getData = async () => {
      const study = await axios.get(`${api}/study/${token}`);
      const experience = await axios.get(`${api}/experience/${token}`);
      const work = await axios.get(`${api}/work/${token}`);
      const userInfomation = await axios
        .get(`${api}/user-infomation/${token}`)
        .then((res) =>
          dispatch({ type: "SET_INFO_USER", info_user: res.data.data })
        );
      const data = {
        study: study.data && study.data.length > 0 ? study.data : null,
        experience:
          experience.data && experience.data.length > 0
            ? experience.data
            : null,
        work: work.data && work.data.length > 0 ? work.data : null,
        userInfomation: userInfomation.info_user
          ? userInfomation.info_user
          : null,
      };

      dispatch({ type: "SET_DETAIL_INFO_USER", detail_info_user: data });
    };
    getData();
  }, [token, dispatch]);
  if (!isCallData) {
    return <LoaderCommonRouter />;
  }

  return (
    <div className="App">
      <Switch>
        <AuthRouter
          isLogin={isLogin}
          exact
          path="/login"
          component={LoginPage}
        />
        <AuthRouter
          isLogin={isLogin}
          exact
          path="/register"
          component={RegisterPage}
        />
        <AuthRouter
          isLogin={isLogin}
          exact
          path="/verify/:email"
          component={VerifyPage}
        />
        <AuthRouter exact path="/login-employer" component={LoginPage} />
        <AuthEmployerRouter
          exact
          path="/register-employer"
          component={RegisterEmployer}
        />
        <AuthRouter
          exact
          path="/forgot-password"
          component={ForgotPasswordPage}
        />

        <CommonRouter exact path="/viec-lam/:viec_lam_id" component={ViecLam} />
        <CommonRouter path="/" exact component={Home} />
        <CommonRouter exact path="/cong-ty" component={CongTy} />
        <CommonRouter exact path="/kien-thuc" component={Home} />
        <CommonRouter exact path="/tin-tuc" component={Home} />
        <CommonRouter exact path="/tim-viec-lam/" component={TimViecLam} />
        <CommonRouter
          exact
          path="/tim-viec-lam/danh-muc/:id_cate"
          component={TimViecLam}
        />
        <CommonRouter
          exact
          path="/tim-viec-lam/:search"
          component={TimViecLam}
        />

        <CommonRouter
          exact
          path="/viec-lam-xung-quanh"
          component={ViecXungQuanh}
        />
        <CommonRouter exact path="/tai-khoan" component={TaiKhoan} />
        <CommonRouter exact path="/viec-lam-da-luu" component={ViecLamDaLuu} />
        <CommonRouter
          exact
          path="/viec-lam-da-ung-tuyen"
          component={ViecLamDaUngTuyen}
        />
        <CommonRouter exact path="/xem-cong-ty/:id" component={ChiTietCongTy} />
        <CommonRouter
          exact
          path="/loi-moi-ung-tuyen"
          component={LoiMoiUngTuyen}
        />

        <CommonRouter
          exact
          path="/ung-tuyen/chi-tiet/:id"
          component={ChiTietUngTuyen}
        />

        <CommonRouter exact path="/bang-tin" component={Home} />
        <CommonRouter exact path="/cv" component={CVHome} />
        <CommonRouter exact path="/cv/crate-cv/:mau_cv" component={CreateCV} />
        <CommonRouter exact path="/cv/mau-cv" component={ListTemplateCV} />
        <CommonRouter exact path="/404" component={PageNotFound} />

        <Route path="/employer/">
          <EditCongTyRouter
            statusLogin={statusLogin}
            exact
            path="/employer/edit-cong-ty/:id/tong-quan"
            component={TongQuanCongTy}
          />
          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/dashboard"
            component={DashboarEmployer}
          />
          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/tin-tuyen-dung/them-moi"
            component={ThemTinTuyenDung}
          />
          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/tin-tuyen-dung/sua/:viec_lam_id"
            component={SuaTinTuyenDung}
          />
          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/ung-vien/chitiet/:id_ung_vien"
            component={ChiTietUngVien}
          />
          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/:tinTD/ung-vien/chitiet/:id_ung_vien"
            component={ChiTietUngVien}
          />
          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/tin-tuyen-dung/hoat-dong"
            component={DanhSachTinTuyenDung}
          />
          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/tin-tuyen-dung/da-xoa"
            component={TinTuyenDungDaXoa}
          />
          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/tin-tuyen-dung/het-han"
            component={DanhSachTinHetHan}
          />
          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/cong-ty/them-moi"
            component={ThemMoiCongTy}
          />

          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/cong-ty/sua/:id"
            component={SuaCongTy}
          />
          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/cong-ty/danh-sach"
            component={DanhSachCongTy}
          />

          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/tuyen-dung"
            component={DashboarEmployer}
          />
          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/ung-tuyen/dang-cho"
            component={DangCho}
          />
          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/tin-tuyen-dung/ung-tuyen/:id"
            component={UngTuyenTin}
          />
          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/tin-tuyen-dung/moi-ung-tuyen/:id"
            component={MoiUngTuyen}
          />

          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/tin-tuyen-dung/:id/tim-ung-vien"
            component={TimUngVienPhuHopTin}
          />
          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/ung-tuyen/da-duyet"
            component={DaDuyet}
          />
          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/tim-ung-vien/kho-ung-vien"
            component={KhoUngVien}
          />
          <EmployerRouter
            exact
            statusLogin={statusLogin}
            path="/employer/ung-tuyen/da-xoa"
            component={DaXoa}
          />
        </Route>
        <AdminRouter
          exact
          statusLogin={statusLogin}
          path="/admin/tai-khoan/danh-sach"
          component={AdminDanhSachUser}
        />
        <AdminRouter
          exact
          statusLogin={statusLogin}
          path="/admin/danh-muc/danh-sach"
          component={AdminDanhSachDanhMuc}
        />
        <AdminRouter
          exact
          statusLogin={statusLogin}
          path="/admin/danh-muc/them-moi"
          component={AdminThemMoiDanhMuc}
        />
        <AdminRouter
          exact
          statusLogin={statusLogin}
          path="/admin/cong-ty/danh-sach"
          component={AdminDanhSachCongTy}
        />
        <AdminRouter
          exact
          statusLogin={statusLogin}
          path="/admin/tin-tuyen-dung/danh-sach"
          component={AdminDanhSachTin}
        />
        <AdminRouter
          exact
          statusLogin={statusLogin}
          path="/admin/dashboard"
          component={AdminDashboard}
        />

        <Redirect from="*" to="/404" />
      </Switch>
      <NotificationContainer />
    </div>
  );
}

export const PageNotFound = () => {
  return (
    <div className="m-5">
      <div className="d-flex justify-content-center">
        <div style={{ textAlign: "center" }} className="col-lg-4">
          <img
            alt="not found"
            src="/assets/img/page_not_found.svg"
            width="70%"
          />
          <div className="text-center mt-5 h3">
            Trang không tồn tại. <Link to="/">Quay lại trang chủ</Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default App;
