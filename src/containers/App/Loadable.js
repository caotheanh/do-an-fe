import Loadable from 'react-loadable';
import LoaderCommonRouter from '../Router/CommonRouter/LoaderCommonRouter';
export default Loadable({
  loader: () => import('./index'),
  loading() {
    return  <LoaderCommonRouter />
  }
});