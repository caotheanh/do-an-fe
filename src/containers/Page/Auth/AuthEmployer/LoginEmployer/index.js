import React from "react";
import { useForm } from "react-hook-form";
import { Link } from "react-router-dom";

function LoginEmployer() {
  const { register, handleSubmit } = useForm();
  return (
    <div className="container mt-8 pb-5">
      <div className="row justify-content-center">
        <div className="col-lg-8 col-md-7">
          <div className="  border-0 mb-0">
            <div className="card-body px-lg-5 py-lg-5">
              <div className="text-center text-muted mb-4">
                <h1>Đăng nhập nhà tuyển dụng</h1>
              </div>
              <form  onSubmit={handleSubmit(registerEmployer)}>
                <div className="form-group mb-3">
                  <div className="input-group input-group-merge input-group-alternative">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="ni ni-email-83"></i>
                      </span>
                    </div>
                    <input
                      className="form-control p-3"
                      placeholder="Email"
                      type="email"
                      ref={register({ required: true })}
                    />
                  </div>
                </div>
                <div className="form-group">
                  <div className="input-group input-group-merge input-group-alternative">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="ni ni-lock-circle-open"></i>
                      </span>
                    </div>
                    <input
                      className="form-control p-3"
                      placeholder="Password"
                      ref={register({ required: true })}
                      type="password"
                    />
                  </div>
                </div>
                <div className="custom-control custom-control-alternative custom-checkbox">
                  <input
                    className="custom-control-input"
                    id=" customCheckLogin"
                    type="checkbox"
                  />
                  <label
                    className="custom-control-label"
                    htmlFor=" customCheckLogin"
                  >
                    <span className="text-muted">Remember me</span>
                  </label>
                </div>
                <div className="text-center">
                  <button type="submit" className="btn btn-primary my-4 col-12">
                    Đăng nhập
                  </button>
                </div>
                <div className="row mt-3">
                  <div className="col-6">
                    <Link to="">
                      <small>Quên mật khẩu ?</small>
                    </Link>
                  </div>
                  <div className="col-6 text-right">
                    <Link to="/register-employer">
                      <small>Đăng kí tài khoản</small>
                    </Link>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default LoginEmployer;
