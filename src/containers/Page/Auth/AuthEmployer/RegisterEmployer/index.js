import axios from "axios";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import NotificationManager from "react-notifications/lib/NotificationManager";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { api } from "../../../../../api";
const RegisterEmployer = () => {
  const { register, handleSubmit } = useForm();
  const [msg, setMsg] = useState("");
  const history = useHistory();
  const registerEmployer = async (data) => {
    setMsg("")
    axios
      .post(`${api}/user/td`, data)
      .then((res) => {
        if (res.data.status === 401) {
          NotificationManager.error(res.data.messages, "Thất bại", 30000);
          setMsg(res.data.messages);
        } else {
          NotificationManager.success(
            "Đăng kí trở thành nhà tuyển dụng thành công",
            "Thành công",
            30000
          );
          history.push("/login");
        }
      })
  };
  return (
    <div className="container mt-8 pb-5">
      <div className="row justify-content-center">
        <div className="col-lg-8 col-md-7">
          <div className="  border-0 mb-0">
            <div className="card-body px-lg-5 py-lg-5">
              <div className="text-center text-muted mb-4">
                <h1>Đăng kí miễn phí</h1>
                <h4 className="text-danger"> {msg && msg}</h4>
              </div>
              <form  onSubmit={handleSubmit(registerEmployer)}>
                <div className="form-group mb-3">
                  <div className="input-group input-group-merge input-group-alternative">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="ni ni-email-83"></i>
                      </span>
                    </div>
                    <input
                      className="form-control p-3"
                      placeholder="Email"
                      type="email"
                      ref={register({ required: true })}
                      name="email"
                    />
                  </div>
                </div>
                <div className="form-group">
                  <div className="input-group input-group-merge input-group-alternative">
                    <div className="input-group-prepend">
                      <span className="input-group-text">
                        <i className="ni ni-lock-circle-open"></i>
                      </span>
                    </div>
                    <input
                      className="form-control p-3"
                      placeholder="Password"
                      type="password"
                      ref={register({ required: true })}
                      name="password"
                    />
                  </div>
                </div>

                <div className="text-center">
                  <button type="submit" className="btn btn-primary my-4 col-12">
                    Đăng kí với email của bạn
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default RegisterEmployer;
