import React from "react";
import ContentLoader from "react-content-loader";

const LoaderChiTietCongTy = (props) => (
  <ContentLoader
   
    viewBox="0 0 600 560"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    {...props}
  >
    <rect x="250" y="4" rx="0" ry="0" width="86" height="81" />
    <rect x="237" y="88" rx="0" ry="0" width="115" height="14" />
    <rect x="7" y="111" rx="0" ry="0" width="287" height="26" />
    <rect x="303" y="112" rx="0" ry="0" width="278" height="25" />
    <rect x="53" y="352" rx="0" ry="0" width="0" height="1" />
    <rect x="8" y="144" rx="0" ry="0" width="572" height="272" />
  </ContentLoader>
);

export default LoaderChiTietCongTy;
