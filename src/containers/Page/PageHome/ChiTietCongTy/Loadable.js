import Loadable from 'react-loadable';
import LoaderChiTietCongTy from './LoaderChiTietCongTy';

export default Loadable({
  loader: () => import('./index'),
  loading() {
         return <LoaderChiTietCongTy />

  }
});