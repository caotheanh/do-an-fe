import axios from "axios";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useMediaQuery } from "react-responsive";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import { api } from "../../../../api";
import CardJobMobile from "../../../../components/Common/CardJobMobile";
import CardJobPC from "../../../../components/Common/CardJobPC";
import ContentLoaderJob from "../Home/ContentLoader/ContentLoaderJob";
import LoaderChiTietCongTy from "./LoaderChiTietCongTy";
export default function ChiTietCongTy() {
  const { id } = useParams();
  const [detailCompany, setdetailCompany] = useState("");
  const [job, setJob] = useState([]);
  const [loadingGetJob, setLoadingGetJob] = useState(true);
  const [isLikeJob, setIsLikeJob] = useState([]);
  const isMobile = useMediaQuery({ maxWidth: 991 });
  const handleLikeJob = (key_job) => {
    const checkJobIsset = (isLikeJob || []).findIndex(
      (item) => key_job.id === item.id
    );
    if (checkJobIsset === -1) {
      setIsLikeJob([...isLikeJob, key_job]);
      localStorage.setItem(
        "stored_jobs",
        JSON.stringify([...isLikeJob, key_job] || [])
      );
    } else {
      const filterJob = isLikeJob.filter((item) => item.id !== key_job.id);
      localStorage.setItem("stored_jobs", JSON.stringify(filterJob || []));
      setIsLikeJob(filterJob);
    }
  };
  const [loading, setLoading] = useState(true);
  useEffect(() => {
    axios
      .get(`${api}/company/${id}`)
      .then((res) => {
        if (res.data) {
          setdetailCompany(res.data);
        }
      })
      .finally(() => setLoading(false));
    axios
      .get(`${api}/recruitment/get-by-company/${id}`)
      .then((res) => {
        setJob(res.data);
      })
      .catch((error) => console.log(error))
      .finally(() => setLoadingGetJob(false));
  }, [id]);
  const categories = useSelector((state) => state.categories);
  if (loading) {
    return <LoaderChiTietCongTy />;
  }
  return detailCompany ? (
    <div className="container">
      <div className="card">
        <div className="card-body">
          <div
            style={{
              background: ` url(${api}/companyImg/${
                detailCompany && detailCompany.avt
              }) 50% 50% no-repeat`,
              width: "150px",
              height: "150px",
              borderRadius: "10px",
              backgroundSize: "cover",
              border: "4px solid #fff",
              margin: "auto",
            }}
            className="imageBackground"
          ></div>
          <div className=" text-center">
            <h2 className=" mb-0">{detailCompany && detailCompany.name}</h2>
          </div>
          <div className="nav-wrapper">
            <ul
              className="nav nav-pills nav-fill flex-column flex-md-row"
              id="tabs-icons-text"
              role="tablist"
            >
              <li className="nav-item">
                <a
                  className="nav-link mb-sm-3 mb-md-0 active"
                  id="tabs-icons-text-1-tab"
                  data-toggle="tab"
                  href="#tabs-icons-text-1"
                  role="tab"
                  aria-controls="tabs-icons-text-1"
                  aria-selected="true"
                >
                  Thông tin công ty
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link mb-sm-3 mb-md-0"
                  id="tabs-icons-text-2-tab"
                  data-toggle="tab"
                  href="#tabs-icons-text-2"
                  role="tab"
                  aria-controls="tabs-icons-text-2"
                  aria-selected="false"
                >
                  Tin tuyển dụng
                </a>
              </li>
            </ul>
          </div>
          <div className="card shadow">
            <div className="card-body">
              <div className="tab-content" id="myTabContent">
                <div
                  className="tab-pane fade show active"
                  id="tabs-icons-text-1"
                  role="tabpanel"
                  aria-labelledby="tabs-icons-text-1-tab"
                >
                  <div className="h-100">
                    <div className="card-header pb-0 p-3">
                      <div className="row">
                        <div className="col-md-8 d-flex align-items-center">
                          <h3 className="mb-0">Giới thiệu công ty</h3>
                        </div>
                      </div>
                    </div>
                    <div className="card-body p-3">
                      <p className="text-sm">
                        {detailCompany && detailCompany.details}
                      </p>
                      <hr className="horizontal gray-light my-4" />
                      <ul className="list-group">
                        <li className="list-group-item border-0 ps-0 pt-0 text-sm">
                          <strong className="text-dark">Email :</strong> &nbsp;
                          {detailCompany && detailCompany.email}
                        </li>
                        <li className="list-group-item border-0 ps-0 text-sm">
                          <strong className="text-dark">Số điện thoại :</strong>{" "}
                          &nbsp;
                          {detailCompany && detailCompany.phone}
                        </li>
                        <li className="list-group-item border-0 ps-0 text-sm">
                          <strong className="text-dark">
                            Loại hình công ty :
                          </strong>{" "}
                          &nbsp;
                          {detailCompany &&
                            categories &&
                            categories.length > 0 &&
                            Array.isArray(categories) &&
                            categories.find(
                              (cate) =>
                                cate.id === +detailCompany.fieldsOfOperation
                            ).name}
                        </li>
                        <li className="list-group-item border-0 ps-0 text-sm">
                          <strong className="text-dark">Địa chỉ :</strong>{" "}
                          &nbsp; Tầng 18, Tòa nhà Handico, Phạm Hùng, Nam Từ
                          Liêm, Hà Nội
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="tabs-icons-text-2"
                  role="tabpanel"
                  aria-labelledby="tabs-icons-text-2-tab"
                >
                  <div className="row">
                    {!loadingGetJob ? (
                      job &&
                      Array.isArray(job) &&
                      job.map(
                        (job, index) =>
                          index < 8 &&
                          (isMobile ? (
                            <CardJobMobile
                              job={job}
                              handleLikeJob={handleLikeJob}
                              isLikeJob={isLikeJob}
                              index={index}
                              key={job.id}
                            />
                          ) : (
                            <CardJobPC
                              handleLikeJob={handleLikeJob}
                              isLikeJob={isLikeJob}
                              index={index}
                              key={job.id}
                              job={job}
                            />
                          ))
                      )
                    ) : (
                      <ContentLoaderJob />
                    )}
                  </div>
                  {/* {job && job.length > 8 && (
                    <div className="d-flex justify-content-center">
                      <Link
                        to="/tim-viec-lam"
                        className="btn btn-outline-default"
                      >
                        Xem thêm
                      </Link>
                    </div>
                  )} */}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  ) : (
    <div className="m-5">
      <div className="d-flex justify-content-center">
        <div style={{ textAlign: "center" }} className="col-lg-4">
          <img
            alt="not found"
            src="/assets/img/page_not_found.svg"
            width="70%"
          />
          <div className="text-center mt-5 h3">Công ty không tồn tại.</div>
        </div>
      </div>
    </div>
  );
}
