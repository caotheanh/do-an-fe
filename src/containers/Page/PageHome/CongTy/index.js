import axios from "axios";
import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import { api } from "../../../../api";
import Image from "../../../../components/Common/Image";
const CongTy = () => {
  const [listCompany, setListCompany] = useState([]);
  useEffect(() => {
    axios({
      method: "get",
      url: `${api}/company`,
    }).then((res) => {
      setListCompany(res.data);
    });
  }, []);
  const { handleSubmit, register } = useForm();

  const handleSearchCompany = (company) => {
    axios.post(`${api}/company/search`, company).then((res) => {
      setListCompany(res.data[0]);
    });
  };
  return (
    <>
      <Helmet>
        <title>Công ty</title>
      </Helmet>
      <div
        className="header bg-primary "
        style={{
          backgroundImage: `url(/assets/img/background.jpg)`,
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
        }}
      >
        <div className="container-fluid">
          <div className="header-body">
            <div className="row align-items-center py-5 text-center">
              <div className="container  py-5">
                <div className="row justify-content-center">
                  <div className=" col ">
                    <div className=" bg-transparent">
                      <div className="ftco-search">
                        <div className="row">
                          <div className="col-md-12 tab-wrap">
                            <div
                              className="tab-content "
                              id="v-pills-tabContent"
                            >
                              <div
                                className="tab-pane fade show active"
                                id="v-pills-1"
                                role="tabpanel"
                                aria-labelledby="v-pills-nextgen-tab"
                              >
                                <form
                                  onSubmit={handleSubmit(handleSearchCompany)}
                                  className="search-job"
                                >
                                  <div className="row no-gutters">
                                    <div className="col-md mr-md-2">
                                      <div className="form-group">
                                        <div className="form-field">
                                          <input
                                            ref={register}
                                            type="text"
                                            name="name"
                                            placeholder="Nhập từ khóa tìm kiếm công ty"
                                            className="form-control"
                                          />
                                        </div>
                                      </div>
                                    </div>
                                    <div className="col-2">
                                      <div className="form-group">
                                        <div className="form-field">
                                          <button
                                            type="submit"
                                            className=" form-control btn btn-default btn-icon"
                                          >
                                            <i className="fa fa-search pr-3"></i>
                                            Tìm kiếm
                                          </button>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid mt--5">
        <div className="row">
          <div className="col-xl-12">
            <div className="card">
              <div className="card-header bg-transparent">
                <div className="row align-items-center">
                  <div className="col">
                    <h6 className=" text-uppercase ls-1 mb-1">
                      Nhà tuyển dụng
                    </h6>
                    <h5 className="h3  mb-0">Nhà tuyển dụng tiêu biểu</h5>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  {listCompany &&
                    Array.isArray(listCompany) &&
                    listCompany.length > 0 &&
                    listCompany.map((item) => (
                      <div className="col-xl-2 col-md-4">
                        <div className="card card-stats">
                          <Link
                            to={`/xem-cong-ty/${item.id}`}
                            key={item.id}
                            className="card-body text-center"
                          >
                            <Image
                              url={`${api}/companyImg/${item.avt}`}
                              size="10rem"
                              style={{ border: "1px solid #ccc" }}
                            />
                            <div
                              className="h5 font-weight-bold mt-3"
                              title={item.name}
                              style={{ height: 25 }}
                            >
                              {item && item.name && item.name.length > 35
                                ? item.name.substring(0, 35) + "..."
                                : item.name}
                            </div>
                          </Link>
                        </div>
                      </div>
                    ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CongTy;
