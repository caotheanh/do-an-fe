import React from "react";
import ContentLoader from "react-content-loader";

const LoaderLoMoiUngTuyen = (props) => (
  <div className="container">
    <ContentLoader
      speed={2}
      viewBox="0 0 600 400"
      backgroundColor="#f3f3f3"
      foregroundColor="#ecebeb"
      {...props}
    >
      <rect x="2" y="42" rx="0" ry="0" width="286" height="63" />
      <rect x="300" y="41" rx="0" ry="0" width="286" height="63" />
      <rect x="1" y="112" rx="0" ry="0" width="286" height="63" />
      <rect x="2" y="181" rx="0" ry="0" width="286" height="63" />
      <rect x="299" y="111" rx="0" ry="0" width="286" height="63" />
      <rect x="299" y="179" rx="0" ry="0" width="286" height="63" />
      <rect x="-4" y="7" rx="0" ry="0" width="99" height="19" />
    </ContentLoader>
  </div>
);

export default LoaderLoMoiUngTuyen;
