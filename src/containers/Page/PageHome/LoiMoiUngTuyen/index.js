import axios from "axios";
import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { gettoken } from "../../../../utils/gettoken";
import { api } from "../../../../api";
import { useSelector } from "react-redux";
import CardLoiMoiUngTuyen from "../../../../components/Common/CardLoiMoiUngTuyen";
import LoaderLoMoiUngTuyen from "./LoaderLoMoiUngTuyen";

function LoiMoiUngTuyen() {
  const token = gettoken();
  const [isLoading, setisLoading] = useState(true);
  const [listInvite, setListInvite] = useState([]);
  const getJobApply = (token) => {
    axios.get(`${api}/invite/get-by-user/${token}`).then(function (response) {
      setListInvite(response.data);
    });
  };
  const [listCV, setListCV] = useState([]);

  useEffect(() => {
    axios
      .get(`${api}/cv/get-cv/${token}`)
      .then(function (response) {
        setListCV(response.data);
      })
      .finally(() => setisLoading(false));
    getJobApply(token);
  }, [token]);
  const is_login = useSelector((state) => state.is_login);
  return (
    <>
      <Helmet>
        <title>Việc Làm Đã ứng tuyển</title>
      </Helmet>
      {isLoading ? (
        <LoaderLoMoiUngTuyen />
      ) : is_login ? (
        <div className="container">
          <div className=" m-3">
            <h3 className="font-weight-bold ml-1 header-cart">
              Lời mời ứng tuyển
            </h3>
            <div className="row">
              {listInvite &&
              Array.isArray(listInvite) &&
              listInvite.length > 0 ? (
                listInvite
                  .sort(function (a, b) {
                    return b.status - a.status;
                  })
                  .map((item) => (
                    <CardLoiMoiUngTuyen
                      key={item.id}
                      invite={item}
                      listCV={listCV}
                    />
                  ))
              ) : (
                <div className="d-flex justify-content-center m-5">
                  <div style={{ width: "20%" }} className="mt-5">
                    <img
                      alt="no data"
                      src="/assets/img/no_data.svg"
                      style={{ width: "100%" }}
                    />
                    <div className="text-center mt-5">
                      Bạn không có lời mời ứng tuyển nào.
                    </div>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      ) : (
        <div className="m-5">
          <div className="d-flex justify-content-center">
            <div style={{ width: "70%", textAlign: "center" }}>
              <img
                src="/assets/img/page_not_found.svg"
                alt="not found"
                width="70%"
              />
              <div className="text-center mt-5 display-3">
                Vui lòng đăng nhập.
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default LoiMoiUngTuyen;
