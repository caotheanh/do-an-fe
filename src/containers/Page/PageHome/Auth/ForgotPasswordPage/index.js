import axios from "axios";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { Link } from "react-router-dom";
import { api } from "../../../../../api";
function ForgotPasswordPage() {
  const { register, handleSubmit } = useForm();
  const [isForgotPassword, setIsForgotPassword] = useState(false);
  const submitForgotPassword = async (data) => {
    axios.put(`${api}/user/forgot-password`, data).then((res) => {
      console.log(res);
      setIsForgotPassword(true);
    });
  };
  return (
    <>
      <div className="main-content">
        <div className="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
          <div className="separator separator-bottom separator-skew zindex-100">
            <svg
              x="0"
              y="0"
              viewBox="0 0 2560 100"
              preserveAspectRatio="none"
              version="1.1"
              xmlns="http://www.w3.org/2000/svg"
            >
              <polygon
                className="fill-default"
                points="2560 0 2560 100 0 100"
              ></polygon>
            </svg>
          </div>
        </div>
        <div className="container mt--8 pb-5">
          <div className="row justify-content-center">
            <div className="col-lg-8 col-md-8">
              <div className="card bg-secondary border-0">
                <div className="text-center mt-3">
                  <h1>Quên mật khẩu</h1>
                  {isForgotPassword && (
                    <p>Chúng tôi đã gửi mật khẩu mới đến email của bạn.</p>
                  )}
                </div>
                <div className="card-body px-lg-5 py-lg-5">
                  <form onSubmit={handleSubmit(submitForgotPassword)}>
                    <div className="form-group">
                      <div className="input-group input-group-merge input-group-alternative">
                        <input
                          className="form-control text-center"
                          placeholder="email"
                          type="text"
                          ref={register({ required: true })}
                          name="email"
                        />
                      </div>
                    </div>
                    <div className="text-center">
                      <button
                        type="submit"
                        className="btn mt-4 btn-round btn-primary"
                      >
                        Quên mật khẩu
                      </button>
                      {isForgotPassword && (
                        <Link
                          to="/login"
                          className="btn mt-4  btn-outline-default"
                        >
                          Đăng nhập
                        </Link>
                      )}
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ForgotPasswordPage;
