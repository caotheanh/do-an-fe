import axios from "axios";
import React from "react";
import { useForm } from "react-hook-form";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import { api } from "../../../../../api";
function VerifyPage() {
  const { register, handleSubmit } = useForm();
  const { email } = useParams();
  const dispatch = useDispatch();
  const history = useHistory();

  const submitVerify = async (data) => {
    const isVerify = await axios.post(
      `${api}/verify`,
      Object.assign({ email: email, verify_code: +data.verify_code })
    );
    const { token } = isVerify.data;
    if (token) {
      getInfomation(token);
      getInfoUserLogin(token);
      localStorage.setItem("token", JSON.stringify(token));
      dispatch({ type: "SET_IS_LOGIN", is_login: true });
    }
  };
  const getInfomation = (token) => {
    axios({
      method: "get",
      url: `${api}/user-infomation/${token}`,
    }).then((res) => {
      dispatch({ type: "SET_INFO_USER", info_user: res.data.data });
      history.push("/");
    });
  };
  const getInfoUserLogin = (token) => {
    axios({
      method: "get",
      url: `${api}/user/${token}`,
    }).then((res) => {
      dispatch({ type: "SET_INFO_LOGIN", info_login: res.data.data });
    });
  };
  return (
    <>
      <div className="main-content">
        <div className="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
          <div className="separator separator-bottom separator-skew zindex-100">
            <svg
              x="0"
              y="0"
              viewBox="0 0 2560 100"
              preserveAspectRatio="none"
              version="1.1"
              xmlns="http://www.w3.org/2000/svg"
            >
              <polygon
                className="fill-default"
                points="2560 0 2560 100 0 100"
              ></polygon>
            </svg>
          </div>
        </div>
        <div className="container mt--8 pb-5">
          <div className="row justify-content-center">
            <div className="col-lg-8 col-md-8">
              <div className="card bg-secondary border-0">
                <div className="text-center mt-3">
                  <h1>Xác nhận email</h1>
                  <p>Chúng tôi đã gửi mã xác minh đến email của bạn.</p>
                </div>
                <div className="card-body px-lg-5 py-lg-5">
                  <form onSubmit={handleSubmit(submitVerify)}>
                    <div className="form-group">
                      <div className="input-group input-group-merge input-group-alternative">
                        <input
                          className="form-control text-center"
                          placeholder="Code..."
                          type="text"
                          ref={register({ required: true })}
                          maxLength="6"
                          name="verify_code"
                        />
                      </div>
                    </div>
                    <div className="text-center">
                      <button
                        type="submit"
                        className="btn mt-4 btn-round btn-primary"
                      >
                        Xác nhận
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default VerifyPage;
