import React from "react";
import { api } from "../../../../../api";
import axios from "axios";
import { useForm } from "react-hook-form";
import {
  NotificationContainer,
  NotificationManager,
} from "react-notifications";
import "react-notifications/lib/notifications.css";
import { useHistory } from "react-router-dom";
function RegisterPage() {
  const history = useHistory();
  const { register, handleSubmit, errors } = useForm();
  const register_user = async (data) => {
    const user = await axios.post(`${api}/user`, data);
    user.data.status === 200
      ? NotificationManager.success(user.data.messages, "", 3000)
      : NotificationManager.error(user.data.messages, "", 3000);
    if (user.data.status === 200) {
      setTimeout(() => {
        history.push("/login");
      }, 3000);
    }
  };
  return (
    <>
      <div className="main-content">
        <div className="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
          <div className="separator separator-bottom separator-skew zindex-100">
            <svg
              x="0"
              y="0"
              viewBox="0 0 2560 100"
              preserveAspectRatio="none"
              version="1.1"
              xmlns="http://www.w3.org/2000/svg"
            >
              <polygon
                className="fill-default"
                points="2560 0 2560 100 0 100"
              ></polygon>
            </svg>
          </div>
        </div>
        <div className="container mt--8 pb-5">
          <div className="row justify-content-center">
            <div className="col-lg-6 col-md-8">
              <div className="card bg-secondary border-0">
                <div className="card-body px-lg-5 py-lg-5">
                  <form onSubmit={handleSubmit(register_user)}>
                    <div className="form-group">
                      <div className="input-group input-group-merge input-group-alternative mb-3">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="ni ni-email-83"></i>
                          </span>
                        </div>
                        <input
                          className="form-control pl-3"
                          placeholder="Email"
                          type="email"
                          ref={register({
                            required: "Không được bỏ trống email",
                          })}
                          name="email"
                        />
                      </div>
                      {errors.email && (
                        <p className="text-danger">{errors.email.message}</p>
                      )}
                    </div>
                    <div className="form-group">
                      <div className="input-group input-group-merge input-group-alternative">
                        <div className="input-group-prepend">
                          <span className="input-group-text">
                            <i className="ni ni-lock-circle-open"></i>
                          </span>
                        </div>
                        <input
                          className="form-control pl-3"
                          placeholder="Password"
                          type="password"
                          ref={register({
                            required: "Không bỏ trống mật khẩu",
                            minLength: {
                              value: 6,
                              message: "Mật khẩu ít nhất 6 kí tự",
                            },
                          })}
                          name="password"
                        />
                      </div>
                      {errors.password && (
                        <p className="text-danger">{errors.password.message}</p>
                      )}
                    </div>
                    <div className="text-center">
                      <button type="submit" className="btn btn-primary mt-4">
                        Tạo tài khoản
                      </button>
                    </div>
                  </form>
                  <NotificationContainer />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default RegisterPage;
