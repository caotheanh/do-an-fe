import React, { useState } from "react";
import { api } from "../../../../../api";
import axios from "axios";
import { useForm } from "react-hook-form";
import { NotificationManager } from "react-notifications";
import "react-notifications/lib/notifications.css";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
function LoginPage() {
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();
  const { register, handleSubmit, errors } = useForm();
  let history = useHistory();
  const login = async (data) => {
    try {
      setLoading(true);
      const result_login = await axios.post(`${api}/login`, data);
      if (result_login.data.messages === "Please check email!") {
        history.push(`/verify/${data.email}`);
        return false;
      }
      if (result_login && result_login.data.token) {
        setLoading(false);
        NotificationManager.success("Đăng nhập thành công", "Thành công", 3000);
      } else {
        setLoading(false);
        NotificationManager.error("Đăng nhập thất bại", "Thất bại", 3000);
      }
      
      const { token } = result_login.data;
      if (token) {
        getInfomation(token);
        getInfoUserLogin(token);
        localStorage.setItem("token", JSON.stringify(token));
        dispatch({ type: "SET_IS_LOGIN", is_login: true });
      }
    } catch (error) {
      setLoading(false);
      NotificationManager.error("Đăng nhập thất bại", "Thất bại", 3000);
    }
  };
  const getInfomation = (token) => {
    axios({
      method: "get",
      url: `${api}/user-infomation/${token}`,
    }).then((res) => {
      dispatch({ type: "SET_INFO_USER", info_user: res.data.data });
      history.push("/");
    });
  };
  const getInfoUserLogin = (token) => {
    axios({
      method: "get",
      url: `${api}/user/${token}`,
    }).then((res) => {
      dispatch({ type: "SET_INFO_LOGIN", info_login: res.data.data });
    });
  };
  return (
    <div className="main-content">
      <div className="header bg-gradient-primary py-7 py-lg-8 pt-lg-9">
        <div className="separator separator-bottom separator-skew zindex-100">
          <svg
            x="0"
            y="0"
            l
            viewBox="0 0 2560 100"
            preserveAspectRatio="none"
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
          >
            <polygon
              className="fill-default"
              points="2560 0 2560 100 0 100"
            ></polygon>
          </svg>
        </div>
      </div>
      <div className="container mt--8 pb-5">
        <div className="row justify-content-center">
          <div className="col-lg-10 col-md-7">
            <div className="card bg-secondary border-0 mb-0">
              <div className="card-body px-lg-5 py-lg-5">
                <form onSubmit={handleSubmit(login)}>
                  <div className="form-group mb-3">
                    <div className="input-group input-group-merge input-group-alternative">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="ni ni-email-83"></i>
                        </span>
                      </div>
                      <input
                        className="form-control pl-3"
                        placeholder="Email"
                        type="email"
                        ref={register({
                          required: "Tài khoản không được để trống",
                        })}
                        name="email"
                      />
                    </div>
                    {errors.email && (
                      <p className="text-danger">{errors.email.message}</p>
                    )}
                  </div>
                  <div className="form-group">
                    <div className="input-group input-group-merge input-group-alternative">
                      <div className="input-group-prepend">
                        <span className="input-group-text">
                          <i className="ni ni-lock-circle-open"></i>
                        </span>
                      </div>
                      <input
                        className="form-control pl-3"
                        placeholder="Password"
                        type="password"
                        ref={register({
                          required: "Không bỏ trống mật khẩu",
                          minLength: {
                            value: 6,
                            message: "Mật khẩu ít nhất 6 kí tự",
                          },
                        })}
                        name="password"
                      />
                    </div>
                    {errors.password && (
                      <p className="text-danger">{errors.password.message}</p>
                    )}
                  </div>
                  <div className="text-right">
                    <Link to="/forgot-password" className="text-default">Quên mật khẩu</Link>
                  </div>
                  <div className="text-center">
                    <button type="submit" className="btn btn-primary my-4">
                      {loading && (
                        <span className="btn-inner--icon mr-3">
                          <i className="fa fa-spinner fa-spin"></i>
                        </span>
                      )}
                      Đăng nhập
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
export default LoginPage;
