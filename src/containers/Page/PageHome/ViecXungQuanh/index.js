import React from "react";
import { Helmet } from "react-helmet";
// import { GoogleMap, LoadScript, Marker } from "@react-google-maps/api";
// const containerStyle = {
//   width: "100%",
//   height: "800px",
// };
// const center = {
//   lat: 21.021183671604412,
//   lng: 105.79159208210125,
// };
// const data = [
//   {
//     lat: 21.038338072922073,
//     lng: 105.74677636914815,
//   },
//   {
//     lat: 21.021183671604412,
//     lng: 105.79159208210125,
//   },
// ];
// const clickMArket = () => {
//   alert("hehe");
// };
const ViecXungQuanh = () => {
  const TITLE = "Việc làm xung quanh bạn";
  return (
    <>
      <Helmet>
        <title>{TITLE}</title>
      </Helmet>
      <div
        className="header bg-primary "
        style={{
          backgroundImage: `url(https://ketnoiviec.net/static/media/slide-02.477a33b0.jpg)`,
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
        }}
      >
        <div className="container-fluid">
          <div className="header-body">
            <div className="row align-items-center py-5 text-center">
              <div className=" ">
                <div className="row justify-content-center">
                  <div className="col-xl-6 col-lg-6 col-md-8 px-5">
                    <h1 className="text-white">Việc làm Poly</h1>
                  </div>
                </div>
                <div className="row justify-content-center">
                  <div className=" col ">
                    <div className="card">
                      <div className="card-header bg-transparent">
                        <div className="ftco-search">
                          <div className="row">
                            <div className="col-md-12 tab-wrap">
                              <div
                                className="tab-content "
                                id="v-pills-tabContent"
                              >
                                <div
                                  className="tab-pane fade show active"
                                  id="v-pills-1"
                                  role="tabpanel"
                                  aria-labelledby="v-pills-nextgen-tab"
                                >
                                  <form action="#" className="search-job">
                                    <div className="row no-gutters">
                                      <div className="col-md mr-md-2">
                                        <div className="form-group">
                                          <div className="form-field">
                                            <span className="icon-briefcase">
                                              What job are you looking for?
                                            </span>
                                            <input
                                              type="text"
                                              className="form-control"
                                              placeholder="eg. Garphic. Web Developer"
                                            />
                                          </div>
                                        </div>
                                      </div>
                                      <div className="col-md mr-md-2">
                                        <div className="form-group">
                                          <div className="form-field">
                                            <div className="select-wrap">
                                              <span className="icon-briefcase">
                                                What job are you looking for?
                                              </span>
                                              <select
                                                name=""
                                                id=""
                                                className="form-control"
                                              >
                                                <option value="">
                                                  Category
                                                </option>
                                                <option value="">
                                                  Full Time
                                                </option>
                                                <option value="">
                                                  Part Time
                                                </option>
                                                <option value="">
                                                  Freelance
                                                </option>
                                                <option value="">
                                                  Internship
                                                </option>
                                                <option value="">
                                                  Temporary
                                                </option>
                                              </select>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div className="col-md mr-md-2">
                                        <div className="form-group">
                                          <div className="form-field">
                                            <span className="icon-briefcase">
                                              What job are you looking for?
                                            </span>
                                            <input
                                              type="text"
                                              className="form-control"
                                              placeholder="Location"
                                            />
                                          </div>
                                        </div>
                                      </div>
                                      <div className="col-md mr-md-2">
                                        <div className="form-group">
                                          <div className="form-field">
                                            <span className="icon-map-marker">
                                              <br />
                                            </span>
                                            <button
                                              type="submit"
                                              className="form-control btn btn-primary"
                                            >
                                              Search
                                            </button>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid mt--5">
        <div className="row">
          <div className="col-xl-12">
            <div className="card">
              <div className="card-header bg-transparent">
                <div className="row align-items-center">
                  <h5 className="h3 mb-0">Việc làm quanh đây</h5>
                </div>
              </div>
              <div className="card-body">
                <div id="map"></div>
                <div id="map_canvas"></div>
                <div id="current">Nothing yet...</div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default ViecXungQuanh;
