import React from "react";

function KhamPhaTienIch() {

  return (
    <>
      <h5 className="h3 mb-0">Khám phá tiện ích</h5>
      <div className="card">
        {Array(4)
          .fill(1)
          .map((item, index) => (
            <div key={`tien_ich_${index}`} className="col-12">
              <div className="card-body  text-center justify-content-center">
                <div className="row">
                  <img
                    alt="images"
                    src="https://ketnoiviec.net/static/media/ke-toan.cc0a61eb.svg"
                    className="img-fluid img-center"
                    style={{ width: "10rem" }}
                  />
                </div>
                <h5 className="mt-3 mb-0 card-title text-uppercase text-muted mb-0">
                  IT-Phần mềm
                </h5>
              </div>
            </div>
          ))}
      </div>
    </>
  );
}

export default KhamPhaTienIch;
