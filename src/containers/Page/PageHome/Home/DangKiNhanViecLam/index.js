import React from "react";

function DangKiNhanViecLam() {
  return (
    <>
      <h3 className="font-weight-bold">Đăng ký nhận việc làm</h3>
      <p>Đăng ký nhận việc làm siêu tốc từ Việc làm Poly</p>
      <div>
        <input
          type="text"
          className="form-control m-1"
          placeholder="Họ và tên"
        />
        <input
          type="text"
          className="form-control m-1"
          placeholder="Ngành nghề mong muốn"
        />
        <input
          type="text"
          className="form-control m-1"
          placeholder="Nơi làm việc mong muốn"
        />
        <input
          type="text"
          className="form-control m-1"
          placeholder="Số điện thoại liên hệ"
        />
        <input
          type="text"
          className="form-control m-1"
          placeholder="Email liên hệ"
        />
        <button type="button" className="btn btn-primary active m-1">
          Đăng kí
        </button>
      </div>
    </>
  );
}

export default DangKiNhanViecLam;
