import React from "react"
import ContentLoader from "react-content-loader"

const ContentLoaderJob = (props) => (
  <ContentLoader 
    speed={2}
    className="container"
    width="80%"
    viewBox="0 0 450 200"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    {...props}
  >
     <rect x="476" y="218" rx="0" ry="0" width="47" height="0" /> 
    <rect x="9" y="12" rx="0" ry="0" width="45" height="45" /> 
    <rect x="58" y="18" rx="0" ry="0" width="63" height="6" /> 
    <rect x="58" y="26" rx="0" ry="0" width="77" height="7" /> 
    <rect x="58" y="36" rx="0" ry="0" width="77" height="7" /> 
    <rect x="58" y="46" rx="0" ry="0" width="77" height="7" /> 
    <rect x="236" y="45" rx="0" ry="0" width="46" height="7" /> 
    <rect x="237" y="29" rx="0" ry="0" width="46" height="7" /> 
    <rect x="271" y="13" rx="0" ry="0" width="10" height="7" /> 
    <rect x="306" y="12" rx="0" ry="0" width="45" height="45" /> 
    <rect x="355" y="18" rx="0" ry="0" width="63" height="6" /> 
    <rect x="355" y="26" rx="0" ry="0" width="77" height="7" /> 
    <rect x="355" y="36" rx="0" ry="0" width="77" height="7" /> 
    <rect x="355" y="46" rx="0" ry="0" width="77" height="7" /> 
    <rect x="533" y="45" rx="0" ry="0" width="46" height="7" /> 
    <rect x="534" y="29" rx="0" ry="0" width="46" height="7" /> 
    <rect x="568" y="13" rx="0" ry="0" width="10" height="7" /> 
    <rect x="9" y="81" rx="0" ry="0" width="45" height="45" /> 
    <rect x="58" y="87" rx="0" ry="0" width="63" height="6" /> 
    <rect x="58" y="95" rx="0" ry="0" width="77" height="7" /> 
    <rect x="58" y="105" rx="0" ry="0" width="77" height="7" /> 
    <rect x="58" y="115" rx="0" ry="0" width="77" height="7" /> 
    <rect x="236" y="114" rx="0" ry="0" width="46" height="7" /> 
    <rect x="237" y="98" rx="0" ry="0" width="46" height="7" /> 
    <rect x="271" y="82" rx="0" ry="0" width="10" height="7" /> 
    <rect x="308" y="81" rx="0" ry="0" width="45" height="45" /> 
    <rect x="357" y="87" rx="0" ry="0" width="63" height="6" /> 
    <rect x="357" y="95" rx="0" ry="0" width="77" height="7" /> 
    <rect x="357" y="105" rx="0" ry="0" width="77" height="7" /> 
    <rect x="357" y="115" rx="0" ry="0" width="77" height="7" /> 
    <rect x="535" y="114" rx="0" ry="0" width="46" height="7" /> 
    <rect x="536" y="98" rx="0" ry="0" width="46" height="7" /> 
    <rect x="570" y="82" rx="0" ry="0" width="10" height="7" /> 
    <rect x="8" y="149" rx="0" ry="0" width="45" height="45" /> 
    <rect x="57" y="155" rx="0" ry="0" width="63" height="6" /> 
    <rect x="57" y="163" rx="0" ry="0" width="77" height="7" /> 
    <rect x="57" y="173" rx="0" ry="0" width="77" height="7" /> 
    <rect x="57" y="183" rx="0" ry="0" width="77" height="7" /> 
    <rect x="235" y="182" rx="0" ry="0" width="46" height="7" /> 
    <rect x="236" y="166" rx="0" ry="0" width="46" height="7" /> 
    <rect x="270" y="150" rx="0" ry="0" width="10" height="7" /> 
    <rect x="308" y="149" rx="0" ry="0" width="45" height="45" /> 
    <rect x="357" y="155" rx="0" ry="0" width="63" height="6" /> 
    <rect x="357" y="163" rx="0" ry="0" width="77" height="7" /> 
    <rect x="357" y="173" rx="0" ry="0" width="77" height="7" /> 
    <rect x="357" y="183" rx="0" ry="0" width="77" height="7" /> 
    <rect x="535" y="182" rx="0" ry="0" width="46" height="7" /> 
    <rect x="536" y="166" rx="0" ry="0" width="46" height="7" /> 
    <rect x="570" y="150" rx="0" ry="0" width="10" height="7" />
  </ContentLoader>
)

export default ContentLoaderJob