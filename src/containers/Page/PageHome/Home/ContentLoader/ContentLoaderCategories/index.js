import React from "react"
import ContentLoader from "react-content-loader"

const ContentLoaderCategories = (props) => (
  <ContentLoader 
    speed={2}
    width="100%"
    viewBox="0 0 580 200"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    {...props}
  >
    <rect x="476" y="218" rx="0" ry="0" width="47" height="0" /> 
    <rect x="26" y="10" rx="5" ry="5" width="82" height="69" /> 
    <rect x="36" y="84" rx="0" ry="0" width="60" height="5" /> 
    <rect x="28" y="102" rx="5" ry="5" width="82" height="69" /> 
    <rect x="38" y="176" rx="0" ry="0" width="60" height="5" /> 
    <rect x="175" y="102" rx="5" ry="5" width="82" height="69" /> 
    <rect x="185" y="176" rx="0" ry="0" width="60" height="5" /> 
    <rect x="175" y="10" rx="5" ry="5" width="82" height="69" /> 
    <rect x="185" y="84" rx="0" ry="0" width="60" height="5" /> 
    <rect x="324" y="10" rx="5" ry="5" width="82" height="69" /> 
    <rect x="334" y="84" rx="0" ry="0" width="60" height="5" /> 
    <rect x="324" y="100" rx="5" ry="5" width="82" height="69" /> 
    <rect x="334" y="174" rx="0" ry="0" width="60" height="5" /> 
    <rect x="477" y="11" rx="5" ry="5" width="82" height="69" /> 
    <rect x="491" y="85" rx="0" ry="0" width="60" height="5" /> 
    <rect x="480" y="99" rx="5" ry="5" width="82" height="69" /> 
    <rect x="494" y="173" rx="0" ry="0" width="60" height="5" />
  </ContentLoader>
)

export default ContentLoaderCategories
