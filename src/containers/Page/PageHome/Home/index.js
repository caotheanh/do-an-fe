import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { useMediaQuery } from "react-responsive";
import CardXuHuong from "../../../../components/Common/CardXuHuong";
import CardJobMobile from "../../../../components/Common/CardJobMobile";
import CardJobPC from "../../../../components/Common/CardJobPC/";
import axios from "axios";
import { api } from "../../../../api";
import { useDispatch, useSelector } from "react-redux";
import ContentLoaderCategories from "./ContentLoader/ContentLoaderCategories";
import ContentLoaderJob from "./ContentLoader/ContentLoaderJob";
import { Link } from "react-router-dom";
import Banner from "../../../../components/Common/Base/Home/Banner";
import CardDangKiNTD from "../../../../components/Common/CardDangKiNTD";
import CardCompany from "../../../../components/ComponentCV/CardCompany";

const Home = () => {
  const TITLE = "Trang chủ";
  const [isLikeJob, setIsLikeJob] = useState([]);
  const handleLikeJob = (key_job) => {
    const checkJobIsset = (isLikeJob || []).findIndex(
      (item) => key_job.id === item.id
    );
    if (checkJobIsset === -1) {
      setIsLikeJob([...isLikeJob, key_job]);
      localStorage.setItem(
        "stored_jobs",
        JSON.stringify([...isLikeJob, key_job] || [])
      );
    } else {
      const filterJob = isLikeJob.filter((item) => item.id !== key_job.id);
      localStorage.setItem("stored_jobs", JSON.stringify(filterJob || []));
      setIsLikeJob(filterJob);
    }
  };
  const [job, setJob] = useState([]);
  useEffect(() => {
    getJob();
    setIsLikeJob(JSON.parse(localStorage.getItem("stored_jobs")) || []);
  }, []);
  const [contentCategoryLoader, setContentCategoryLoader] = useState(false);
  const [contentJobLoader, setContentJobLoader] = useState(false);

  const isMobile = useMediaQuery({ maxWidth: 991 });
  const getJob = async () => {
    const data = await axios.get(`${api}/recruitment`);
    if (data && data.data) {
      setJob(data.data);
      setContentJobLoader(true);
    }
  };
  const [categories, setCategories] = useState([]);
  const categoriesStore = useSelector((state) => state.categories);
  useEffect(() => {
    setCategories(categoriesStore);
  }, [categoriesStore]);
  const dispatch = useDispatch();
  const [listCompany, setListCompany] = useState([]);
  useEffect(() => {
    axios({
      method: "get",
      url: `${api}/category`,
    }).then((res) => {
      setContentCategoryLoader(true);
      dispatch({ type: "SET_CATEGORIES", categories: res.data });
    });
    axios({
      method: "get",
      url: `${api}/company`,
    }).then((res) => {
      setListCompany(res.data);
    });
  }, [dispatch]);

  return (
    <>
      <Helmet>
        <title>{TITLE}</title>
      </Helmet>
      <Banner />
      <div className="container">
        <div className="m-3">
          <h3 className="font-weight-bold ml-1">Xu hướng nghề nghiệp</h3>
          <div className="row mt-3">
            {contentCategoryLoader ? (
              categories &&
              Array.isArray(categories) &&
              categories.map(
                (item, index) =>
                  index < 8 && <CardXuHuong key={item.id} item={item} />
              )
            ) : (
              <ContentLoaderCategories />
            )}
          </div>
        </div>
      </div>
      <div className="container">
        <div className="m-3">
          <h3 className="font-weight-bold ml-1">
            Tin tuyển dụng, việc làm tốt nhất
          </h3>
          <div className="row mt-3">
            {contentJobLoader ? (
              job && Array.isArray(job) ? (
                job.map(
                  (job, index) =>
                    index < 6 &&
                    (isMobile ? (
                      <CardJobMobile
                        job={job}
                        handleLikeJob={handleLikeJob}
                        isLikeJob={isLikeJob}
                        index={index}
                        key={job.id}
                      />
                    ) : (
                      <CardJobPC
                        handleLikeJob={handleLikeJob}
                        isLikeJob={isLikeJob}
                        index={index}
                        key={job.id}
                        job={job}
                      />
                    ))
                )
              ) : (
                ""
              )
            ) : (
              <ContentLoaderJob />
            )}
            {job && job.length > 8 && (
              <div className="d-flex justify-content-center">
                <Link to="/tim-viec-lam" className="btn btn-outline-default">
                  Xem thêm
                </Link>
              </div>
            )}
          </div>
        </div>
        <div className="m-3">
          <h3 className="font-weight-bold ml-1">Tin tuyển dụng mới</h3>
          <div className="row mt-3">
            {contentJobLoader ? (
              job && Array.isArray(job) ? (
                job.map(
                  (job, index) =>
                    index < 6 &&
                    (isMobile ? (
                      <CardJobMobile
                        job={job}
                        handleLikeJob={handleLikeJob}
                        isLikeJob={isLikeJob}
                        index={index}
                        key={job.id}
                      />
                    ) : (
                      <CardJobPC
                        handleLikeJob={handleLikeJob}
                        isLikeJob={isLikeJob}
                        index={index}
                        key={job.id}
                        job={job}
                      />
                    ))
                )
              ) : (
                ""
              )
            ) : (
              <ContentLoaderJob />
            )}
            {job && job.length > 8 && (
              <div className="d-flex justify-content-center">
                <Link to="/tim-viec-lam" className="btn btn-outline-default">
                  Xem thêm
                </Link>
              </div>
            )}
          </div>
        </div>
      </div>
      <div className="container">
        <div className="m-3">
          <div className="row">
            <div className="col-xl-12 order-xl-2">
              <h3 className="font-weight-bold">Công ty tiêu biểu</h3>
              <div className="row mt-3">
                <div className="col-lg-8">
                  <div className="row">
                    {listCompany && Array.isArray(listCompany)
                      ? listCompany &&
                        Array.isArray(listCompany) &&
                        listCompany.map((company, index) => (
                          <div className="col-lg-6" key={`job_${index}`}>
                            <CardCompany company={company} />
                          </div>
                        ))
                      : ""}
                  </div>
                </div>
                <div className="col-lg-4">
                  <CardDangKiNTD />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Home;
