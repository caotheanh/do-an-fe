import axios from "axios";
import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { gettoken } from "../../../../utils/gettoken";
import { api } from "../../../../api";
import { useSelector } from "react-redux";
import CardApplyJob from "../../../../components/Common/CardApplyJob";
import LoaderViecLamUngTuyen from "./LoaderViecLamUngTuyen";

function ViecLamDaUngTuyen() {
  const [jobApply, setJobApply] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const token = gettoken();
  const getJobApply = (token) => {
    setIsLoading(true);
    axios
      .get(`${api}/apply/get-by-user/${token}`)
      .then(function (response) {
        setJobApply(response.data);
      })
      .finally(() => setIsLoading(false));
  };
  useEffect(() => {
    getJobApply(token);
  }, [token]);
  const [typeList, setTypeList] = useState(1);
  const is_login = useSelector((state) => state.is_login);
  return (
    <>
      <Helmet>
        <title>Việc Làm Đã ứng tuyển</title>
      </Helmet>
      {isLoading ? (
        <LoaderViecLamUngTuyen />
      ) : is_login ? (
        <div className="container">
          <div className="d-flex justify-content-between">
            <h3 className="font-weight-bold ml-1 header-cart">
              Việc làm đã ứng tuyển
            </h3>
            <div className="btn-group btn-group-toggle" data-toggle="buttons">
              <label
                class={`btn btn-secondary ${typeList === 1 && "active"}`}
                onClick={() => setTypeList(1)}
              >
                <input type="radio" name="options" autoComplete="off" />{" "}
                <i className="fa fa-th-large"></i>
              </label>
              <label
                class={`btn btn-secondary ${typeList === 2 && "active"}`}
                onClick={() => setTypeList(2)}
              >
                <input type="radio" name="options" autoComplete="off" />{" "}
                <i className="fa fa-list"></i>
              </label>
            </div>
          </div>

          <div className="mt-3 row">
            {typeList === 1 ? (
              jobApply &&
              Array.isArray(jobApply) &&
              jobApply.length > 0 &&
              jobApply
                .sort(function (a, b) {
                  return b.status - a.status;
                })
                .map((job) => (
                  <CardApplyJob
                    key={job.id}
                    apply={job}
                    type={1}
                    job={job.recruitment}
                  />
                ))
            ) : (
              <table className="table align-items-center table-flush">
                <thead className="thead-light">
                  <tr>
                    <th scope="col" className="sort" data-sort="name">
                      Ảnh
                    </th>
                    <th scope="col" className="sort" data-sort="budget">
                      Tiêu đề
                    </th>
                    <th scope="col" className="sort" data-sort="status">
                      Hạn nộp
                    </th>
                    <th scope="col">Ngày ứng tuyển</th>
                    <th scope="col" className="sort" data-sort="completion">
                      Trạng thái
                    </th>
                    <th scope="col"></th>
                  </tr>
                </thead>
                <tbody className="list">
                  {jobApply &&
                    Array.isArray(jobApply) &&
                    jobApply.length > 0 &&
                    jobApply
                      .sort(function (a, b) {
                        return b.status - a.status;
                      })
                      .map((job, index) => (
                        <CardApplyJob
                          key={job.id}
                          apply={job}
                          type={2}
                          job={job.recruitment}
                        />
                      ))}
                </tbody>
              </table>
            )}
          </div>
        </div>
      ) : (
        <div className="m-5">
          <div className="d-flex justify-content-center">
            <div style={{ width: "70%", textAlign: "center" }}>
              <img
                src="/assets/img/page_not_found.svg"
                alt="not found"
                width="70%"
              />
              <div className="text-center mt-5 display-3">
                Vui lòng đăng nhập.
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

// <div className="d-flex justify-content-center m-5">
//   <div style={{ width: "20%" }} className="mt-5">
//     <img
//       alt="no data"
//       src="/assets/img/no_data.svg"
//       style={{ width: "100%" }}
//     />
//     <div className="text-center mt-5">Bạn chưa có ứng tuyển nào.</div>
//   </div>
// </div>;

export default ViecLamDaUngTuyen;
