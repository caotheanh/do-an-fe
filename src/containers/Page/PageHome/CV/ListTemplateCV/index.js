import React from "react";
import { Link } from "react-router-dom/cjs/react-router-dom.min";

function ListTemplateCV() {
  return (
    <div className="m-5"> 
      <div className="header pb-2 d-flex align-items-center">
        <span className="mask opacity-8"></span>
        <div className="container ">
          <div className="header-body text-center mb-7">
            <div className="row justify-content-center">
              <div className="col-xl-8 col-lg-6 col-md-8 px-5">
                <h1>Danh sách mẫu CV xin việc</h1>
                <p className="text-lead">
                  Các mẫu CV đuợc thiết kế theo chuẩn, theo các ngành nghề. Phù
                  hợp với sinh viên và người đi làm.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid mt--6">
        <div className="row">
          <div className="col-xl-3 col-lg-6">
            <Link to="/cv/crate-cv/luat-va-giao-duc">
              <div className="card card-profile">
                <img
                  src="../assets/img/mau-cv/cv1.png"
                  alt="images cv"
                  className="card-img-top"
                />
                <div className=" align-items-center">
                  <div className="text-center mt-2 mb-2">
                    <h2>Mẫu CV Luật - Giáo Dục</h2>
                  </div>
                </div>
              </div>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ListTemplateCV;
