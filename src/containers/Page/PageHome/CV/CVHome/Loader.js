import React from "react"
import ContentLoader from "react-content-loader"

const Loader = (props) => (
  <ContentLoader 
    speed={2}
    width={700}
    height={300}
    viewBox="0 0 700 300"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    {...props}
  >
    <rect x="5" y="38" rx="0" ry="0" width="186" height="67" /> 
    <rect x="200" y="36" rx="0" ry="0" width="186" height="67" /> 
    <rect x="398" y="36" rx="0" ry="0" width="186" height="67" /> 
    <rect x="14" y="128" rx="0" ry="0" width="44" height="13" /> 
    <rect x="483" y="126" rx="0" ry="0" width="88" height="20" /> 
    <rect x="15" y="155" rx="0" ry="0" width="562" height="56" /> 
    <rect x="13" y="234" rx="0" ry="0" width="562" height="56" /> 
    <rect x="11" y="222" rx="0" ry="0" width="560" height="2" />
  </ContentLoader>
)

export default Loader