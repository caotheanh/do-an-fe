import React, { useEffect, useState } from "react";

import { Link, useHistory } from "react-router-dom/cjs/react-router-dom.min";
import axios from "axios";
import { api } from "../../../../../api";
import { useForm } from "react-hook-form";
import CardCV from "../../../../../components/ComponentCV/CardCV";
import { useSelector } from "react-redux";
import NotificationManager from "react-notifications/lib/NotificationManager";
import { Helmet } from "react-helmet";
import LoaderCVHome from "./LoaderCVHome";
function CVHome() {
  const { register, handleSubmit, errors } = useForm();
  const [token, setToken] = useState("");
  const [listCV, setListCV] = useState([]);
  const is_login = useSelector((state) => state.is_login);
  const [isLoading, setIsLoading] = useState(true);
  const [loading, setLoading] = useState(false);
  const formData = new FormData();
  const [isStatusFindJob, setIsStatusFindJob] = useState();
  const [fullinfo, setFullInfo] = useState(false);
  const [pointInfo, setPointInfo] = useState(0);
  const info_user = useSelector((state) => state.info_user);

  const getListCV = (tokenLocalStorage) => {
    axios
      .get(`${api}/cv/get-cv/${tokenLocalStorage}`)
      .then(function (response) {
        setListCV(response.data);
      })
      .finally(() => setIsLoading(false));
  };
  useEffect(() => {
    const tokenLocalStorage = JSON.parse(localStorage.getItem("token"));
    setToken(tokenLocalStorage);
    getListCV(tokenLocalStorage);
  }, []);
  const [dataFormCV, setDataFormCV] = useState({ name: "", cv: "" });
  const handleChangeFormCV = (e) => {
    const { name, value } = e.target;
    setDataFormCV((prev) => {
      return { ...prev, [name]: value };
    });
  };

  const deleteCV = (tokenLocalStorage, id) => {
    axios
      .delete(`${api}/cv/${id}/${tokenLocalStorage}`)
      .then(() => {
        setListCV(listCV.filter((cv) => +cv.id !== +id));
        NotificationManager.success(
          "Đã xóa CV thành công.",
          "Thành công",
          3000
        );
      })
      .catch(() =>
        NotificationManager.error("Xóa CV không thành công.", "Thất bại", 3000)
      );
  };
  const onSubmit = async (data) => {
    setLoading(true);
    const { cv, name } = data;
    formData.append("cv", cv[0]);
    formData.append("token", token);
    formData.append("name", name);
    axios
      .post(`${api}/cv`, formData)
      .then(function () {
        document.querySelector(".huy222").click();
        getListCV(token);
        setDataFormCV({ name: "", cv: "" });
        NotificationManager.success("Thêm CV  thành công.", "Thành công", 3000);
      })
      .catch(() => {
        NotificationManager.error("Thêm CV thất bại.", "Thất bại", 3000);
      })
      .finally(() => setLoading(false));
  };

  useEffect(() => {
    if (info_user) {
      info_user.cv && pointInfo > 70 && setFullInfo(true);
      setIsStatusFindJob(+info_user.status_search_job === 1 ? true : false);
    }
  }, [info_user, pointInfo]);
  const changeFindJob = (e) => {
    const { checked } = e.target;
    const status = checked ? 0 : 1;
    axios
      .post(`${api}/user-infomation/change-status/update`, {
        token: token,
        status_search_job: status,
      })
      .then(function (response) {
        NotificationManager.success(
          "Thay đổi trạng thái thành công.",
          "Thành công",
          3000
        );
      });
  };
  const history = useHistory();
  if (!is_login) {
    history.push("/login");
  }
  const detail_info_user = useSelector((state) => state.detail_info_user);
  useEffect(() => {
    let point = 0;
    const pointArr = [10, 10, 30, 50];
    ["study", "experience", "work", "userInfomation"].forEach((item, index) => {
      if (detail_info_user && detail_info_user[item]) {
        point = point + pointArr[index];
      }
    });
    setPointInfo(point);
  }, [detail_info_user]);

  if (isLoading) {
    return <LoaderCVHome />;
  }
  return (
    <>
      <Helmet>
        <title>CV của tôi</title>
      </Helmet>
      <div className="container">
        <div className="row">
          <div className="col-xl-4 col-md-6">
            <div className="card card-stats" style={{ minHeight: "149px" }}>
              <div className="card-body">
                <div className="row">
                  <div className="col">
                    <h3 className="card-title text-uppercase text-muted mb-0">
                      Trạng thái tìm việc
                    </h3>
                    <span className="h2 font-weight-bold mb-0"></span>
                    <span className="h2 font-weight-bold mb-0">
                      {isStatusFindJob ? (
                        <span className="badge-dot mr-4">
                          <i className="bg-success"></i>
                          <span className="status h4 ">Đang tìm việc</span>
                        </span>
                      ) : (
                        <span className="badge-dot mr-4">
                          <i className="bg-danger"></i>
                          <span className="status h4">Ngừng tìm việc</span>
                        </span>
                      )}
                    </span>
                  </div>
                  <div className="col-auto">
                    <label className="custom-toggle">
                      <input
                        onChange={changeFindJob}
                        type={fullinfo ? `checkbox` : "disabled"}
                        checked={isStatusFindJob ? true : false}
                      />
                      {fullinfo ? (
                        <span
                          onClick={() => setIsStatusFindJob(!isStatusFindJob)}
                          className="custom-toggle-slider rounded-circle"
                        ></span>
                      ) : (
                        <span
                          type="button"
                          data-toggle="modal"
                          data-target="#modal-notification"
                          className="custom-toggle-slider "
                        ></span>
                      )}
                    </label>
                  </div>
                </div>
                <div className="col-md-4">
                  <div
                    className="modal fade"
                    id="modal-notification"
                    tabIndex="-1"
                    role="dialog"
                    aria-labelledby="modal-notification"
                    aria-hidden="true"
                  >
                    <div
                      className="modal-dialog modal-danger modal-dialog-centered modal-"
                      role="document"
                    >
                      <div className="modal-content ">
                        <div className="modal-header">
                          <h6
                            className="modal-title"
                            id="modal-title-notification"
                          >
                            Vui lòng cập nhật thông tin cá nhân
                          </h6>
                          <button
                            type="button"
                            className="close"
                            data-dismiss="modal"
                            aria-label="Close"
                          >
                            <span aria-hidden="true">×</span>
                          </button>
                        </div>

                        <div className="modal-body">
                          <div className="py-3 text-center ">
                            <i className="ni ni-bell-55 ni-3x"></i>
                            <h4 className="heading mt-4 ">
                              Cập nhật đầy đủ thông tin để sử dụng chức năng tìm
                              kiếm việc làm.
                            </h4>
                          </div>
                          <div>
                            <p className="mt-3 mb-0 text-sm">
                              <span className="h1 text-success mr-2">
                                {pointInfo > 70 ? (
                                  <i className="la la-check"></i>
                                ) : (
                                  <i className="la la-close"></i>
                                )}
                              </span>
                              <span className="">
                                Hoàn thiện trên
                                <span className="text-success mr-2"> 70%</span>
                                thông tin cá nhân để bắt đầu tiếp cận với nhà
                                tuyển dụng
                              </span>
                            </p>
                            <p className="mt-3 mb-0 text-sm">
                              <span className="h1 text-success mr-2">
                                {info_user && info_user.cv ? (
                                  <i className="la la-check"></i>
                                ) : (
                                  <i className="la la-close"></i>
                                )}
                              </span>
                              <span className="">Chọn 1 CV mặc định.</span>
                            </p>
                          </div>
                        </div>

                        <div className="modal-footer">
                          <Link to="/tai-khoan" className="btn btn-white">
                            Cập nhật
                          </Link>
                          <button
                            type="button"
                            className="btn btn-link text-white ml-auto"
                            data-dismiss="modal"
                          >
                            Đóng
                          </button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <p className="mt-3 mb-0 text-sm">
                  <span className="">
                    Bật tìm việc để nhận được nhiều cơ hội việc làm tốt nhất từ
                    Việc làm Poly.
                  </span>
                </p>
              </div>
            </div>
          </div>
          <div className="col-xl-4 col-md-6">
            <div className="card card-stats" style={{ minHeight: "149px" }}>
              <div className="card-body">
                <div className="row">
                  <div className="col">
                    <h3 className="card-title text-uppercase text-muted mb-0">
                      Cập nhật thông tin
                    </h3>
                    <span className="badge-dot mr-4">
                      <span className="status h4">
                        Đã hoàn thành {pointInfo}%
                      </span>
                    </span>
                  </div>
                  <div className="col-auto">
                    <Link to="/tai-khoan">
                      <div className="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                        <i className="fa flaticon-edit-1"></i>
                      </div>
                    </Link>
                  </div>
                </div>
                <p className="mt-3 mb-0 text-sm">
                  <span className="">
                    Hoàn thiện trên
                    <span className="text-success mr-2"> 70%</span>
                    thông tin cá nhân để bắt đầu tiếp cận với nhà tuyển dụng
                  </span>
                </p>
              </div>
            </div>
          </div>
          <div className="col-xl-4 col-md-6">
            <div className="card card-stats" style={{ minHeight: "149px" }}>
              <div className="card-body">
                <div className="row">
                  <div className="col">
                    <h3 className="card-title text-uppercase text-muted mb-0">
                      Tạo CV mới
                    </h3>
                  </div>
                  <div className="col-auto">
                    <Link
                      to="/cv/mau-cv"
                      className="icon icon-shape  bg-gradient-info  text-white rounded-circle shadow"
                    >
                      <i className="fa flaticon-add"></i>
                    </Link>
                  </div>
                </div>
                <p className="mt-3 mb-0 text-sm">
                  <span className="">
                    Tạo CV chuyên nghiệp, nhận ngay việc làm ưng ý
                  </span>
                </p>
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-xl-12">
            <div className="card">
              <div className="card-header border-0">
                <div className="row align-items-center">
                  <div className="col">
                    <h3 className="mb-0">CV của bạn</h3>
                  </div>
                  <div className="col">
                    <ul className="nav nav-pills justify-content-end">
                      <button
                        className="btn btn-outline-default"
                        data-toggle="modal"
                        data-target="#exampleModal"
                        type="button"
                      >
                        <span className="btn-inner--icon pr-3">
                          <i className="fa 	fa-file-upload "></i>
                        </span>
                        <span className="btn-inner--text">
                          {" "}
                          Tải lên CV của bạn
                        </span>
                      </button>
                      <div
                        className="modal fade"
                        id="exampleModal"
                        tabIndex="-1"
                        role="dialog"
                        aria-labelledby="exampleModalLabel"
                        aria-hidden="true"
                      >
                        <div
                          className="modal-dialog modal-dialog-centered"
                          role="document"
                        >
                          <form
                            encType="multipart/form-data"
                            onSubmit={handleSubmit(onSubmit)}
                            className="modal-content"
                          >
                            <div className="modal-header">
                              <h5
                                className="modal-title"
                                id="exampleModalLabel"
                              >
                                Tải lên CV
                              </h5>
                              <button
                                type="button"
                                className="close"
                                data-dismiss="modal"
                                aria-label="Close"
                              >
                                <span aria-hidden="true">&times;</span>
                              </button>
                            </div>
                            <div className="modal-body">
                              <div className="form-group">
                                <label htmlFor="exampleFormControlInput1">
                                  Tên CV
                                </label>
                                <input
                                  onChange={handleChangeFormCV}
                                  type="text"
                                  autoComplete="off"
                                  name="name"
                                  ref={register({ required: true })}
                                  maxLength={60}
                                  className="form-control"
                                  value={dataFormCV.name}
                                />
                                {errors.name && (
                                  <span>Vui lòng nhập tên CV</span>
                                )}
                              </div>
                              <div className="form-group">
                                <label
                                  htmlFor="upload-photo"
                                  className="btn btn-outline-default btn-icon"
                                  style={{ width: "100%" }}
                                >
                                  <span className="btn-inner--icon">
                                    <i className="fa fa-upload"></i>
                                  </span>

                                  <span className="btn-inner--text">
                                    {dataFormCV.cv
                                      ? "Chọn file khác"
                                      : "Tải cv lên"}
                                  </span>
                                </label>
                                <input
                                  style={{
                                    opacity: "0",
                                    position: "absolute",
                                    zIndex: "-1",
                                  }}
                                  accept="application/pdf, application/vnd.ms-excel"
                                  type="file"
                                  onChange={handleChangeFormCV}
                                  onBlur={handleChangeFormCV}
                                  ref={register({ required: true })}
                                  name="cv"
                                  value={dataFormCV.cv}
                                  id="upload-photo"
                                />

                                {errors.cv && (
                                  <span className="text-danger pt-2">
                                    Vui lòng nhập CV
                                  </span>
                                )}
                              </div>
                              <div>{dataFormCV.cv}</div>
                            </div>
                            <div className="modal-footer">
                              <button
                                type="button"
                                className="btn btn-secondary huy222"
                                data-dismiss="modal"
                              >
                                Đóng
                              </button>

                              <button
                                type={loading ? "button" : "submit"}
                                className={
                                  dataFormCV.name && dataFormCV.cv
                                    ? "btn btn-primary"
                                    : "btn btn-primary disabled"
                                }
                              >
                                {loading && (
                                  <span className="btn-inner--icon mr-3">
                                    <i className="fa fa-spinner fa-spin"></i>
                                  </span>
                                )}
                                Thêm CV mới
                              </button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </ul>
                  </div>
                </div>
              </div>
              <div className="container">
                {listCV && Array.isArray(listCV) && listCV.length > 0 ? (
                  listCV.map((cv, index) => (
                    <div key={cv.id}>
                      <CardCV cv={cv} deleteCV={deleteCV} token={token} />
                      {index < listCV.length - 1 && <hr className="my-4" />}
                    </div>
                  ))
                ) : (
                  <div className="container shape-container d-flex align-items-center ">
                    <div className="col px-0">
                      <div className="row align-items-center justify-content-center">
                        <div className="col-lg-6 text-center">
                          <img
                            className=" img-fluid m-3 pb-2"
                            src="/assets/img/no_data.svg"
                            alt="no data"
                            style={{ margin: "auto", width: "7rem" }}
                          />
                          <p className=" h3">Bạn chưa tải lên CV nào.</p>
                        </div>
                      </div>
                    </div>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default CVHome;
