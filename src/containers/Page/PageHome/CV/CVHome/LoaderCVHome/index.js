import React from "react";
import ContentLoader from "react-content-loader";

const LoaderCVHome = (props) => (
  <div className="container">
    <ContentLoader 
    speed={2}
    
    viewBox="0 0 600 300"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    {...props}
  >
    <rect x="4" y="4" rx="0" ry="0" width="186" height="67" /> 
    <rect x="199" y="2" rx="0" ry="0" width="186" height="67" /> 
    <rect x="397" y="2" rx="0" ry="0" width="186" height="67" /> 
    <rect x="13" y="94" rx="0" ry="0" width="44" height="13" /> 
    <rect x="482" y="92" rx="0" ry="0" width="88" height="20" /> 
    <rect x="14" y="121" rx="0" ry="0" width="562" height="56" /> 
    <rect x="12" y="200" rx="0" ry="0" width="562" height="56" /> 
    <rect x="10" y="188" rx="0" ry="0" width="560" height="2" />
  </ContentLoader>
  </div>
);

export default LoaderCVHome;
