import React from "react";
function Tool({ exportPDFWithMethod, setStyleCustom, stateStyle }) {
  const handleChangeStyle = (e) => {
    const { name, value } = e.target;
    setStyleCustom({ stateStyle, [name]: value });
  };
  return (
    <div id="cv-layout-container">
      <div id="toolbar-layout">
        <div className="toolbar-block">
          <div className="toolbar-block-row">
            <div id="toolbar-block-font">
              <div className="toolbar-block-title">Font chữ</div>
              <div className="select-box" id="toolbar-select-font">
                <div className="select-box__current">
                  <div className="select-box__value">
                    <input
                      className="select-box__input"
                      type="radio"
                      id="font-0"
                      value="Arial"
                      name="font"
                      checked="checked"
                    />
                    <p className="select-box__input-text">Arial</p>
                  </div>
                  <div className="select-box__value">
                    <input
                      className="select-box__input"
                      type="radio"
                      id="font-1"
                      value="Times News Roman"
                      name="font"
                    />
                    <p className="select-box__input-text">Times News Roman</p>
                  </div>
                  <div className="select-box__value">
                    <input
                      className="select-box__input"
                      type="radio"
                      id="font-2"
                      value="Sans-serif"
                      name="font"
                    />
                    <p className="select-box__input-text">Sans-serif</p>
                  </div>
                  <div className="select-box__value">
                    <input
                      className="select-box__input"
                      type="radio"
                      id="font-3"
                      value="Tahoma"
                      name="font"
                    />
                    <p className="select-box__input-text">Tahoma</p>
                  </div>

                  {/* <img
                        className="select-box__icon"
                        src="./dsadas_files/arrow_down.svg"
                        alt="Arrow Icon"
                        aria-hidden="true"
                      /> */}
                </div>
                <ul className="select-box__list">
                  <li>
                    <label className="select-box__option" htmlFor="font-0">
                      Arial
                    </label>
                  </li>
                  <li>
                    <label className="select-box__option" htmlFor="font-1">
                      Times News Roman
                    </label>
                  </li>
                  <li>
                    <label className="select-box__option" htmlFor="font-2">
                      Sans-serif
                    </label>
                  </li>
                  <li>
                    <label className="select-box__option" htmlFor="font-3">
                      Tahoma
                    </label>
                  </li>
                </ul>
              </div>
            </div>
            <div id="toolbar-block-font-size">
              <div className="toolbar-block-title">Cỡ chữ</div>
              <div className="select-box" id="toolbar-select-font-size">
                <div className="select-box__current">
                  <div className="select-box__value">
                    <input
                      className="select-box__input"
                      type="radio"
                      id="font-size-small"
                      value="0.9"
                      name="font-size"
                    />
                    <p className="select-box__input-text">Nhỏ</p>
                  </div>
                  <div className="select-box__value">
                    <input
                      className="select-box__input"
                      type="radio"
                      id="font-size-medium"
                      value="1"
                      name="font-size"
                      checked="checked"
                    />
                    <p className="select-box__input-text">Vừa</p>
                  </div>
                  <div className="select-box__value">
                    <input
                      className="select-box__input"
                      type="radio"
                      id="font-size-big"
                      value="1.1"
                      name="font-size"
                    />
                    <p className="select-box__input-text">To</p>
                  </div>
                  {/* <img
                        className="select-box__icon"
                        src="./dsadas_files/arrow_down.svg"
                        alt="Arrow Icon"
                        aria-hidden="true"
                      /> */}
                </div>
                <ul className="select-box__list">
                  <li>
                    <label
                      className="select-box__option"
                      htmlFor="font-size-small"
                    >
                      Nhỏ
                    </label>
                  </li>
                  <li>
                    <label
                      className="select-box__option"
                      htmlFor="font-size-medium"
                    >
                      Vừa
                    </label>
                  </li>
                  <li>
                    <label
                      className="select-box__option"
                      htmlFor="font-size-big"
                    >
                      To
                    </label>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="toolbar-block-row">
            <button
              className="toolbar-text-style"
              id="toolbar-text-bold"
              style={{ cursor: "not-allowed" }}
              disabled="disabled"
            >
              B
            </button>
            <button
              className="toolbar-text-style"
              id="toolbar-text-italic"
              style={{ cursor: "not-allowed" }}
              disabled="disabled"
            >
              I
            </button>
            <button
              className="toolbar-text-style"
              id="toolbar-text-underline"
              style={{ cursor: "not-allowed" }}
              disabled="disabled"
            >
              U
            </button>
            <button
              className="toolbar-text-align-style"
              id="toolbar-text-left"
              style={{ cursor: "not-allowed" }}
              disabled="disabled"
            >
              <i className="fa fa-align-left" aria-hidden="true"></i>
            </button>
            <button
              className="toolbar-text-align-style"
              id="toolbar-text-center"
              style={{ cursor: "not-allowed" }}
              disabled="disabled"
            >
              <i className="fa fa-align-center" aria-hidden="true"></i>
            </button>
            <button
              className="toolbar-text-align-style"
              id="toolbar-text-right"
              style={{ cursor: "not-allowed" }}
              disabled="disabled"
            >
              <i className="fa fa-align-right" aria-hidden="true"></i>
            </button>
            <button
              className="toolbar-text-align-style"
              id="toolbar-text-justify"
              style={{ cursor: "not-allowed" }}
              disabled="disabled"
            >
              <i className="fa fa-align-justify" aria-hidden="true"></i>
            </button>
          </div>
        </div>
        <div className="tool-line"></div>

        <div className="toolbar-block">
          
          <div id="toolbar-color-picker">
            <input
              onChange={handleChangeStyle}
              type="color"
              name="color"
              style={{ width: "50px" }}
              value={stateStyle.color}
              id="example-color-input"
            />
            <div id="toolbar-color-picker-val">
              <div className="toolbar-block-title">
                <label
                  htmlFor="example-color-input"
                  className="form-control-label"
                >
                  Màu sắc chữ
                </label>
              </div>
              <div id="toolbar-color-picker-val-text"></div>
            </div>
          </div>
          <div id="toolbar-color-picker">
            <input
              onChange={handleChangeStyle}
              type="color"
              name="background"
              style={{ width: "50px" }}
              value={stateStyle.background}
              id="example-color-input"
            />
            <div id="toolbar-color-picker-val">
              <div className="toolbar-block-title">
                <label
                  htmlFor="example-color-input"
                  className="form-control-label"
                >
                  Màu sắc nền
                </label>
              </div>
              <div id="toolbar-color-picker-val-text"></div>
            </div>
          </div>
        </div>
        <div className="tool-line"></div>


        <div className="toolbar-block">
        <div id="toolbar-block-line-hight">
            <div className="toolbar-block-title">Cách dòng</div>
            <div
              className="toolbar-block-row row-align-center row-justify-center"
              style={{ marginBottom: "6px" }}
            >
              <div
                onClick={() =>
                  setStyleCustom({ stateStyle, lineHeight: "15px" })
                }
                id="toolbar-line-hight-small"
                className={`toolbar-line-hight-item ${
                  stateStyle.lineHeight === "15px"
                    ? "toolbar-line-hight-item--active"
                    : ""
                }`}
              >
                <i className="fa fa-arrows-alt-v" aria-hidden="true"></i>
              </div>
              <div
                onClick={() =>
                  setStyleCustom({ stateStyle, lineHeight: "25px" })
                }
                id="toolbar-line-hight-medium"
                className={`toolbar-line-hight-item ${
                  stateStyle.lineHeight === "25px"
                    ? "toolbar-line-hight-item--active"
                    : ""
                }`}
              >
                <i className="fa fa-arrows-alt-v" aria-hidden="true"></i>
              </div>
              <div
                onClick={() =>
                  setStyleCustom({ stateStyle, lineHeight: "35px" })
                }
                id="toolbar-line-hight-large"
                className={`toolbar-line-hight-item ${
                  stateStyle.lineHeight === "35px"
                    ? "toolbar-line-hight-item--active"
                    : ""
                }`}
              >
                <i className="fa fa-arrows-alt-v" aria-hidden="true"></i>
              </div>
            </div>
          </div>
        </div>
        <div className="tool-line"></div>
        <div className="toolbar-block">
          <div id="toolbar-change-theme">
            <div className="toolbar-block-title">Đổi mẫu CV</div>
            <i className="fa fa-files-o" aria-hidden="true"></i>
          </div>
        </div>
        <div className="tool-line"></div>
        <div
          className="toolbar-block"
          id="toolbar-save-cv"
          onClick={exportPDFWithMethod}
        >
          <div className="toolbar-block-title">Lưu CV</div>
          <i className="flaticon-download-1" aria-hidden="true"></i>
        </div>
      </div>
    </div>
  );
}

export default Tool;
