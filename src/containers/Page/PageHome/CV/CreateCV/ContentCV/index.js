import React, { useEffect, useState } from "react";
import AnhDaiDien from "../../../../../../components/ComponentCV/CV1/AnhDaiDien";
import {
  GioiThieu,
  KinhNghiemLamViec,
  HocVan,
  HoatDong,
  GiaiThuong,
  ThongTinThem,
  ChungChi,
  DuAn,
  CaNhan,
  NguoiThamChieu,
  KiNang,
  SoThich,
  Info,
} from "../../../../../../components/ComponentCV/CV1/GroupComponentCV1";
import { useMediaQuery } from "react-responsive";
const componentsAll = [
  { name: "Giới thiệu", Component: GioiThieu },
  { name: "Kinh nghiệm làm việc", Component: KinhNghiemLamViec },
  { name: "Học vấn", Component: HocVan },
  { name: "Hoạt động", Component: HoatDong },
  { name: "Giải thưởng", Component: GiaiThuong },
  { name: "Thông tin thêm", Component: ThongTinThem },
  { name: "Chứng chỉ", Component: ChungChi },
  { name: "Dự án", Component: DuAn },
  { name: "Cá nhân", Component: CaNhan },
  { name: "Người tham chiếu", Component: NguoiThamChieu },
  { name: "Kĩ năng", Component: KiNang },
  { name: "Sở thích", Component: SoThich },
];
const ContentCV = ({ stateTool, responsePC, setIsResponsive }) => {
  const [isStyleCustom, setisStyleCustom] = useState({});
  const [itemCv, setItemCv] = useState([]);
  const [compTemp, setCompTemp] = useState([]);
  const isMobile = useMediaQuery({ maxWidth: 700 });
  const [dataUser, setDataUser] = useState({});
  useEffect(() => {
    setCompTemp(componentsAll);
    const ComponentAllTemp = [...componentsAll];
    ComponentAllTemp.push(
      { name: "Thông tin", Component: Info },
      { name: "Ảnh đại diện", Component: AnhDaiDien }
    );
    ComponentAllTemp.map((component) =>
      setDataUser(
        Object.assign(dataUser, { [component.name]: { content: {} } })
      )
    );
  }, [dataUser]);
  const handleChangInfoUser = (
    value2,
    name,
    stt,
    category,
    subName,
    value1
  ) => {
    if (category === 1) {
      if (value1) {
        setDataUser((prev) => {
          return { ...prev, [name]: { ...prev[name], title: value1 } };
        });
      } else {
        setDataUser((prev) => {
          return {
            ...prev,
            [name]: {
              ...prev[name],
              content: { ...prev[name].content, [stt]: value2 },
            },
          };
        });
      }
    } else if (category === 2) {
      if (value1) {
        setDataUser((prev) => {
          return { ...prev, [name]: { ...prev[name], title: value1 } };
        });
      } else {
        setDataUser((prev) => {
          return {
            ...prev,
            [name]: {
              ...prev[name],
              content: {
                ...prev[name].content,
                [stt]: { ...prev[name].content[stt], [subName]: value2 },
              },
            },
          };
        });
      }
    } else {
    }
  };
  useEffect(() => {
    if (isMobile) {
      setIsResponsive({ widthPC: false });
    } else {
      setIsResponsive({ widthPC: true });
    }
  }, [isMobile, setIsResponsive]);
  const handleSetTop = (indexComp, components) => {
    const CompTemp = [...components];
    CompTemp.splice(indexComp, 0, CompTemp.splice(indexComp - 1, 1)[0]);
    setCompTemp(CompTemp);
  };
  const handleSetDown = (indexComp, components) => {
    const CompTemp = [...components];
    CompTemp.length > indexComp + 1 &&
      CompTemp.splice(indexComp, 0, CompTemp.splice(indexComp + 1, 1)[0]);
    setCompTemp(CompTemp);
  };

  const addItemCV = (name, status) => {
    const arrayItemClone = [...itemCv];
    const index = itemCv.findIndex((item) => item.cate === name);
    arrayItemClone[index].qtyItem = arrayItemClone[index].qtyItem + status || 1;
    setItemCv(arrayItemClone);
  };
  const deleteComponent = (name) => {
    const compClone = [...compTemp];
    setCompTemp(compClone.filter((item) => item.name !== name));
  };

  useEffect(() => {
    setisStyleCustom({
      color: stateTool.color,
      lineHeight: stateTool.lineHeight,
    });
  }, [stateTool]);

  useEffect(() => {
    const array = [];
    compTemp.length > 0 &&
      compTemp.map((component, index) =>
        array.push({
          cate: component.name,
          name: component.name,
          Component: component.Component,
          content: [],
          qtyItem: 1,
          index: index < 8 ? "l" : "r",
        })
      );
    setItemCv(array);
  }, [compTemp]);

  return (
    <>
      <div style={{ margin: "auto" }} className="ui-sortable">
        <div id="cv-container" lang="vi">
          <div className="cv-header" style={stateTool}>
            <Info
              stt={1}
              name="Info"
              handleChangInfoUser={handleChangInfoUser}
            />
            <AnhDaiDien
              stt={1}
              name="AnhDaiDien"
              handleChangInfoUser={handleChangInfoUser}
            />
          </div>
          <div
            style={{
              position: "relative",
              display: "flex",
              height: " 100%",
              width: " 100%",
            }}
          >
            <div
              style={{
                padding: "0px 0px",
                width: "70%",
                left: "0",
              }}
            >
              <div
                style={{
                  padding: "0px 0px",
                  borderRight: " 1px solid rgb(0, 102, 255)",
                }}
              >
                {itemCv.length > 1 &&
                  itemCv.map(
                    (Component, key) =>
                      Component.index === "l" && (
                        <div key={key} id="cv-project" className="cv-element">
                          <Dropdown
                            indexs={key}
                            compTemp={compTemp}
                            names={Component.name}
                            addItemCV={addItemCV}
                            deleteComponent={deleteComponent}
                            handleSetTop={handleSetTop}
                            handleSetDown={handleSetDown}
                          />
                          <>
                            <div
                              className="cv-element-title "
                              contentEditable=""
                              style={isStyleCustom}
                              cv-placeholder="TIÊU ĐỀ MỤC LỚN"
                              onInput={function (e) {
                                handleChangInfoUser(
                                  null,
                                  Component.name,
                                  null,
                                  1,
                                  null,
                                  e.target.innerHTML
                                );
                              }}
                              id="cv-skill-title"
                            >
                              {Component.name}
                            </div>
                            {Array(Component.qtyItem || 1)
                              .fill(1)
                              .map((item, index) => (
                                <Component.Component
                                  key={index}
                                  stt={index}
                                  name={Component.name}
                                  handleChangInfoUser={handleChangInfoUser}
                                />
                              ))}
                          </>
                        </div>
                      )
                  )}
              </div>
            </div>
            <div
              style={{
                width: "30%",
                right: "0",
              }}
              className="cv-block-drag-drop ui-sortable"
            >
              {itemCv &&
                itemCv.map(
                  (Component, key) =>
                    Component.index === "r" && (
                      <div key={key} id="cv-project" className="cv-element">
                        <Dropdown
                          indexs={key}
                          deleteComponent={deleteComponent}
                          compTemp={compTemp}
                          names={Component.name}
                          addItemCV={addItemCV}
                          handleSetTop={handleSetTop}
                          handleSetDown={handleSetDown}
                        />

                        <div
                          className="cv-element-title"
                          contentEditable=""
                          onInput={function (e) {
                            handleChangInfoUser(
                              null,
                              Component.name,
                              null,
                              1,
                              null,
                              e.target.innerHTML
                            );
                          }}
                          cv-placeholder="TIÊU ĐỀ MỤC LỚN"
                          id="cv-skill-title"
                          style={isStyleCustom}
                        >
                          {Component.name}
                        </div>
                        {Array(Component.qtyItem || 1)
                          .fill(1)
                          .map((item, index) => (
                            <Component.Component
                              key={index}
                              stt={index}
                              name={Component.name}
                              handleChangInfoUser={handleChangInfoUser}
                            />
                          ))}
                      </div>
                    )
                )}
            </div>
          </div>
        </div>
      </div>
      <div id="cv-preview-save-detect"></div>
    </>
  );
};
const Dropdown = ({
  deleteComponent,
  handleSetTop,
  handleSetDown,
  addItemCV,
  names,
  compTemp,
  indexs,
}) => (
  <div
    className="dropdown-menu "
    style={{
      background: "initial",
      boxShadow: "none",
    }}
  >
    <div
      style={{
        top: "0",
        left: "0px",
        transform: "translate3d(0px, 0px)",
      }}
      role="group"
      aria-label="Button group with nested "
    >
      <button
        type="button"
        className="btn btn-default"
        onClick={() => handleSetTop(indexs, compTemp)}
      >
        <i className="la la-sort-asc"></i>
      </button>
      <button
        type="button"
        className="btn btn-default"
        onClick={() => handleSetDown(indexs, compTemp)}
      >
        <i className="la la-sort-desc"></i>
      </button>
      <button
        onClick={() => addItemCV(names, +1)}
        type="button"
        className="btn btn-default"
      >
        <i className="flaticon-add-circular-button"></i>
      </button>
      <button
        onClick={() => addItemCV(names, -1)}
        type="button"
        className="btn btn-default"
      >
        <i className="flaticon-cancel"></i>
      </button>
      <button
        onClick={() => deleteComponent(names)}
        type="button"
        className="btn btn-danger"
      >
        <i className="flaticon-circle"></i> Loại bỏ
      </button>
    </div>
  </div>
);

export default ContentCV;
