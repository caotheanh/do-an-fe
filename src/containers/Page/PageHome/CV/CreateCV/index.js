import * as React from "react";
import { PDFExport } from "@progress/kendo-react-pdf";
import "./css.css";

import Tool from "./Tool";
import ContentCV from "./ContentCV";
class CreateCV extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      color: "#000000",
      fontFamily: "",
      background: "#72656f",
      lineHeight: "25px",
      widthPC: true,
    };
    this.backHome = this.backHome.bind(this);
  }
  backHome() {
    this.props.history.push("/");
  }
  exportPDFWithComponent = () => {
    this.pdfExportComponent.save();
  };
  setStyleCustom = (e) => {
    this.setState((state) => {
      return e;
    });
  };
  setIsResponsive = (e) => {
    this.setState((state) => {
      return e;
    });
  };
  render() {
    if (!this.state.widthPC) {
      return (
        <div className="">
          <div
            className="modal fade show"
            style={{ display: "block" }}
            id="modal-notification"
            role="dialog"
            aria-labelledby="modal-notification"
            aria-hidden="true"
          >
            <div
              className="modal-dialog modal-danger modal-dialog-centered modal-"
              role="document"
            >
              <div
                className="modal-content bg-gradient-danger"
                style={{ width: "80%", margin: "auto" }}
              >
                <div className="modal-header"></div>

                <div className="modal-body">
                  <div className="py-3 text-center">
                    <i className="ni ni-bell-55 ni-3x"></i>
                    <h1 className="heading mt-4">Thông báo</h1>
                    <p>
                      Tính năng tạo CV không hỗ trợ trên thiết bị di động. Vui
                      lòng sử dụng máy tính hoặc máy tính bảng để thao tác dễ
                      dàng và tạo CV chuyên nghiệp nhất.
                    </p>
                  </div>
                </div>
                <div className="d-flex justify-content-center">
                  <button
                    type="button"
                    className="btn btn-white mb-3"
                    onClick={this.backHome}
                  >
                    Ok, Quay lại trang chủ
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    }
    return (
      <div className="container">
        <Tool
          stateStyle={this.state}
          exportPDFWithMethod={this.exportPDFWithComponent}
          setStyleCustom={this.setStyleCustom}
        />
        <div className="row m-1">
          <div className=" mb-5 col-lg-8">
            <div
              ref={(container) => (this.container = container)}
              id="cv-layout-container  "
              className="border rounded"
            >
              <div
                className="mt-3"
                id="cv-title"
                contentEditable=""
                cv-placeholder="Tiêu đề CV"
              >
                Mẫu CV Luật - Giáo Dục
              </div>
              <div id="cv-layout ">
                <PDFExport
                  paperSize="A4"
                  ref={(component) => (this.pdfExportComponent = component)}
                  scale={0.6}
                  margin={0}
                  fileName={`CV`}
                  author="KendoReact Team"
                  className="cv-document"
                >
                  <ContentCV
                    setIsResponsive={this.setIsResponsive}
                    responsePC={this.responsive}
                    stateTool={this.state}
                  />
                </PDFExport>
              </div>
            </div>
          </div>
          <div className="col-lg-4">
            <div className="card">
              <div className="card-header bg-transparent">
                <div className="row align-items-center">
                  <div className="col">
                    <h5 className="h3 mb-0">Hướng dẫn viết cv</h5>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <div className="chart"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CreateCV;
