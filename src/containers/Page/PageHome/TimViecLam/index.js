import axios from "axios";
import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import { api } from "../../../../api";
import CardJobPC from "../../../../components/Common/CardJobPC";
import { getKeyWordSearch } from "../../../../utils/getKeyWordSearch";
import { DataAddress } from "../../../Data/DataAddress";
import ContentLoaderJob from "../Home/ContentLoader/ContentLoaderJob";
const TimViecLam = () => {
  const TITLE = "Tìm kiếm việc làm";
  const [job, setJob] = useState([]);
  const [isLikeJob, setIsLikeJob] = useState([]);
  const [contentJobLoader, setContentJobLoader] = useState(false);
  const [isSearch, setIsSearch] = useState("");
  const data_search = useSelector((state) => state.data_search);
  const categories = useSelector((state) => state.categories);
  const [keyWord, setKeyWord] = useState({ title: "", city: "", cate: "" });
  const KeyWordSearchLocalStorage = getKeyWordSearch();
  useEffect(() => {
    setIsLikeJob(JSON.parse(localStorage.getItem("stored_jobs")) || []);
  }, []);

  const { search, id_cate } = useParams();
  useEffect(() => {
    if (search === "search") {
      const keyWords = KeyWordSearchLocalStorage || data_search;
      setKeyWord(keyWords);
      setIsSearch(true);
      axios.post(`${api}/recruitment/search`, keyWords).then((data) => {
        if (data && data.data) {
          setJob(data.data);
          setContentJobLoader(true);
        }
      });
    }
  }, []);
  useEffect(() => {
    if (id_cate) {
      setIsSearch(true);
      axios
        .get(`${api}/recruitment/get-by-category/${id_cate}`)
        .then((data) => {
          if (data && data.data) {
            setJob(data.data);
            setContentJobLoader(true);
          }
        });
    }
    if (!id_cate && !search) {
      getJob();
    }
  }, []);
  const handleLikeJob = (key_job) => {
    const checkJobIsset = (isLikeJob || []).findIndex(
      (item) => key_job.id === item.id
    );
    if (checkJobIsset === -1) {
      setIsLikeJob([...isLikeJob, key_job]);
      localStorage.setItem(
        "stored_jobs",
        JSON.stringify([...isLikeJob, key_job] || [])
      );
    } else {
      const filterJob = isLikeJob.filter((item) => item.id !== key_job.id);
      localStorage.setItem("stored_jobs", JSON.stringify(filterJob || []));
      setIsLikeJob(filterJob);
    }
  };
  const getJob = async () => {
    const data = await axios.get(`${api}/recruitment`);
    if (data && data.data) {
      setJob(data.data);
      setContentJobLoader(true);
    }
  };

  const handleEnterChangeKeyword = (e) => {
    const { name, value } = e.target;
    setKeyWord((prev) => {
      return { ...prev, [name]: value };
    });
  };
  const handleClickSearchJob = async () => {
    setIsSearch(true);
    const data = await axios.post(`${api}/recruitment/search`, keyWord);
    if (data && data.data) {
      setJob(data.data);
      setContentJobLoader(true);
    }
  };
  return (
    <>
      <Helmet>
        <title>{TITLE}</title>
      </Helmet>
      <div
        className="header bg-primary "
        style={{
          backgroundImage: `url(/assets/img/job-cover.jpeg)`,
          backgroundRepeat: "no-repeat",
          backgroundPosition: "center",
          backgroundSize: "cover",
          position: "relative",
        }}
      >
        <span className="mask bg-gradient-dark opacity-6"></span>
        <div className="container-fluid">
          <div className="header-body  align-items-center">
            <div className=" py-5 text-center ">
              <div className="d-flex justify-content-center ">
                <div className="container">
                  <div className="">
                    <form className="search-job ">
                      <div className="row ">
                        <div className="col-lg-10 ">
                          <div className="row">
                            <div className="col-lg-12">
                              <div className="form-group">
                                <div className="form-field">
                                  <input
                                    onChange={handleEnterChangeKeyword}
                                    type="text"
                                    name="title"
                                    value={keyWord.title}
                                    className="form-control"
                                    placeholder=" Nhập công công việc tìm kiếm bạn mong muốn"
                                  />
                                </div>
                              </div>
                            </div>
                            <div className="form-group col-lg-auto">
                              <select
                                className="form-control"
                                name="city"
                                value={keyWord.city}
                                onChange={handleEnterChangeKeyword}
                              >
                                <option value="">Thành phố</option>
                                {DataAddress.map((thanhpho, index) => (
                                  <option
                                    key={`thanh_pho_${index}`}
                                    checked={true}
                                    value={index}
                                  >
                                    {thanhpho[1]}
                                  </option>
                                ))}
                              </select>
                            </div>
                            <div className="form-group col-lg-auto ">
                              <select
                                className="form-control"
                                name="cate"
                                value={keyWord.cate}
                                onChange={handleEnterChangeKeyword}
                              >
                                <option value="">Loại công việc</option>
                                {categories &&
                                  Array.isArray(categories) &&
                                  categories.length > 0 &&
                                  categories.map((cate) => (
                                    <option
                                      key={`category_${cate.id}`}
                                      value={cate.id}
                                    >
                                      {cate.name}
                                    </option>
                                  ))}
                              </select>
                            </div>
                          </div>
                        </div>
                        <div className="col-lg-2">
                          <div className="form-group">
                            <button
                              type="button"
                              onClick={handleClickSearchJob}
                              className="form-control btn btn-primary "
                            >
                              Tìm kiếm
                            </button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="row">
          <div className="col-xl-12">
            <div className="">
              <div className="card-header bg-transparent">
                <div className="row align-items-center">
                  <div className="col">
                    <h4 className=" text-uppercase ">
                      {isSearch
                        ? `Kết quả tìm kiếm : Có ${job.length} việc làm phù hợp `
                        : "Việc làm mới nhất"}
                    </h4>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <div className="row">
                  {contentJobLoader ? (
                    job &&
                    Array.isArray(job) &&
                    job.map((job, index) => (
                      <CardJobPC
                        handleLikeJob={handleLikeJob}
                        isLikeJob={isLikeJob}
                        index={index}
                        key={job.id}
                        job={job}
                      />
                    ))
                  ) : (
                    <ContentLoaderJob />
                  )}
                  {job && job.length < 5 && (
                    <div className="col-lg-6 col-md-6 mt-1">
                      <div className=" alert alert-default fade show row">
                        <div className="col-xl-8 ">
                          <h3 className="text-white" style={{ color: "#fff" }}>
                            Bạn là nhà tuyển dụng?
                          </h3>
                          <p className="text-sm">
                            Đăng tin miễn phí ngay trên Kết Nối Việc
                          </p>
                          <p className="text-sm">
                            Tuyển dụng trực tiếp trên website
                          </p>
                        </div>
                        <div className="col-xl-4 text-right">
                          <Link
                            target="_blank"
                            to="/register-employer"
                            className="btn btn-warning btn-sm"
                          >
                            Đăng kí ngay
                          </Link>
                          <div className="mt-3 pr-3">Hoàn toàn miễn phí</div>
                        </div>
                      </div>
                    </div>
                  )}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default TimViecLam;
