import axios from "axios";
import React, { useEffect, useState } from "react";
import ModalThemCVMM from "./ModalThemCVMM";
import { api } from "../../../../../api";
import { gettoken } from "../../../../../utils/gettoken";
import { useDispatch, useSelector } from "react-redux";
import ModalDeleteCVMM from "./ModalDeleteCVMM";
import ModalEditCVMM from "./ModalEditCVMM";
import NotificationManager from "react-notifications/lib/NotificationManager";
import FormatNumber from "../../../../../utils/FormatNumber/index";
const CongViecMongMuonTaiKhoan = () => {
  const token = gettoken();
  const [listWork, setListWork] = useState([]);
  useEffect(() => {
    axios.get(`${api}/work/${token}`).then(function (response) {
      setListWork(response.data);
    });
  }, [token]);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({
      type: "SET_POINT_FULL_INFO",
      point: {
        name: "work",
        point: listWork && listWork.length > 0 ? 1 : 0,
      },
    });
  }, [listWork, dispatch]);
  const getCVMM = () => {
    axios.get(`${api}/work/${token}`).then(function (response) {
      setListWork(response.data);
    });
  };
  const deleteWork = (id) => {
    axios.delete(`${api}/work/delete/${id}`).then(function () {
      getCVMM();
      NotificationManager.success(
        "Đã xóa công việc mong muốn thành công.",
        "Thành công",
        3000
      );
      setListWork(listWork.filter((work) => work.id !== id));
    });
  };
  const categories = useSelector((state) => state.categories);
  return (
    <div>
      <button
        type="button"
        className="btn btn-outline-primary"
        data-toggle="modal"
        data-target="#ModalThemCongViecMongMuon"
      >
        <i className="fa fa-plus mr-3"></i> Thêm công việc mong muốn
      </button>
      <ModalThemCVMM getCVMM={getCVMM} />
      {listWork &&
        Array.isArray(listWork) &&
        listWork.map((work) => (
          <div key={work.id} className="card-body">
            <div className="row">
              <div className="col">
                <span className="h2 font-weight-bold mb-0">
                  {work.name_job}
                </span>
                <h5 className="card-title text-uppercase text-muted mb-0">
                  {categories &&
                    Array.isArray(categories) &&
                    categories.find((cate) => cate.id === +work.id_category)
                      .name}
                </h5>
              </div>
              <div className="col-auto">
                <button
                  data-toggle="modal"
                  data-target={`#ModalEditCongViecMongMuon${work.id}`}
                  type="button"
                  className="btn btn-outline-default btn-icon  btn-sm"
                >
                  <i className="fa fa-edit"></i>
                </button>
                <button
                  data-toggle="modal"
                  data-target={`#modalDeleteWork${work.id}`}
                  type="button"
                  className="btn btn-outline-default btn-icon  btn-sm"
                >
                  <i className="fa fa-trash-alt"></i>
                </button>
              </div>
            </div>
            <ModalDeleteCVMM id={work.id} deleteWork={deleteWork} />
            <ModalEditCVMM id={work.id} getCVMM={getCVMM} />
            <p className="mb-0 text-sm">
              <span className="text-success mr-2 pr-2">
                {FormatNumber(work.salary_1)} - {FormatNumber(work.salary_2)}
              </span>
            </p>
          </div>
        ))}
    </div>
  );
};

export default CongViecMongMuonTaiKhoan;
