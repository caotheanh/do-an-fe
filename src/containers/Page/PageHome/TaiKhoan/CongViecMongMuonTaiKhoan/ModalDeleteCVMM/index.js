const ModalDeleteCVMM = ({ id, deleteWork }) => {
  const handleDeleteWork = () => {
    deleteWork(id);
    document.querySelector(".huy").click();
  };
  return (
    <div
      className="modal fade show"
      id={`modalDeleteWork${id}`}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Xóa học vấn
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            Bạn có muốn xóa thông tin học vấn này ?
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary huy"
              data-dismiss="modal"
            >
              Huỷ
            </button>
            <button
              type="button"
              onClick={() => handleDeleteWork(id)}
              className="btn btn-primary"
            >
              Xóa
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalDeleteCVMM;
