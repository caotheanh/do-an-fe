import axios from "axios";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { gettoken } from "../../../../../../utils/gettoken";
import { api } from "../../../../../../api";
import NotificationManager from "react-notifications/lib/NotificationManager";
import { useSelector } from "react-redux";
import TypeJob from "../../../../../../utils/TypeJob";
function ModalEditCVMM({ id, getCVMM }) {
  const { handleSubmit, register } = useForm();
  const token = gettoken();
  const [workDetail, setworkDetail] = useState();
  const handleChangeDetailWork = (e) => {
    const { name, value } = e.target;
    setworkDetail((prev) => {
      return { ...prev, [name]: value };
    });
  };
  const categories = useSelector((state) => state.categories);
  useEffect(() => {
    axios.get(`${api}/work/details/${id}/${token}`).then((res) => {
      setworkDetail(res.data);
    });
  }, [id, token]);
  const onsubmit = (data) => {
    document.querySelector(`.close_${id}`).click();
    axios
      .put(`${api}/work/update`, Object.assign(data, { token: token, id: id }))
      .then(() => {
        getCVMM();
        NotificationManager.success(
          "Sửa công việc mong muốn thành công.",
          "Thành công",
          3000
        );
      })
      .catch(() =>
        NotificationManager.success("Thêm học vấn thành công.", "Khong", 3000)
      );
  };
  return (
    <form
      onSubmit={handleSubmit(onsubmit)}
      className="modal fade"
      id={`ModalEditCongViecMongMuon${id}`}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="modal_knlv"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="modal_knlv">
              Sửa mong muốn việc làm
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Tên công việc
              </label>
              <input
                name="name_job"
                type="text"
                value={workDetail && workDetail.name_job}
                onChange={handleChangeDetailWork}
                ref={register({ required: true })}
                
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Hình thức làm việc
              </label>
              <select
                name="type"
                onChange={handleChangeDetailWork}
                value={workDetail && workDetail.type}
                className="form-control"
                ref={register({ required: true })}
              >
                <option value="">Chọn hình thức làm việc</option>
                {TypeJob.map((item) => (
                  <option key={item.id} value={item.id}>
                    {item.name}
                  </option>
                ))}
              </select>
            </div>
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Ngành nghề
              </label>
              <select
                className="form-control"
                onChange={handleChangeDetailWork}
                value={workDetail && workDetail.id_category}
                name="id_category"
                ref={register({ required: true })}
              >
                {categories &&
                  Array.isArray(categories) &&
                  categories.map((cate) => (
                    <option key={cate.id} value={cate.id}>
                      {cate.name}
                    </option>
                  ))}
              </select>
            </div>
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Mức lương (Tối thiểu)
              </label>
              <input
                value={workDetail && workDetail.salary_1}
                type="number"
                ref={register({ required: true })}
                
                onChange={handleChangeDetailWork}
                name="salary_1"
                className="form-control"
              />
            </div>{" "}
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Mức lương (Tối đa)
              </label>
              <input
                ref={register({ required: true })}
                type="number"
                onChange={handleChangeDetailWork}
                value={workDetail && workDetail.salary_2}
                
                name="salary_2"
                className="form-control"
              />
            </div>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className={`btn btn-secondary  close_${id}`}
              data-dismiss="modal"
            >
              Đóng
            </button>
            <button
              type="submit"
              //   onClick={addExperience}
              className="btn btn-outline-default"
            >
              Sửa
            </button>
          </div>
        </div>
      </div>
    </form>
  );
}

export default ModalEditCVMM;
