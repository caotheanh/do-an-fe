import axios from "axios";
import React from "react";
import { useForm } from "react-hook-form";
import { gettoken } from "../../../../../../utils/gettoken";
import { api } from "../../../../../../api";
import NotificationManager from "react-notifications/lib/NotificationManager";
import { useSelector } from "react-redux";
import TypeJob from "../../../../../../utils/TypeJob/index";
function ModalThemCVMM({ getCVMM }) {
  const { handleSubmit, register } = useForm();
  const token = gettoken();
  const categories = useSelector((state) => state.categories);
  const onsubmit = (data) => {
    document.querySelector(".huy122").click();
    axios
      .post(`${api}/work`, Object.assign(data, { token: token }))
      .then(() => {
        getCVMM();
        NotificationManager.success(
          "Thêm học vấn thành công.",
          "Thành công",
          3000
        );
      })
      .catch(() =>
        NotificationManager.error("Thêm học vấn không thành công.", "Thất bại", 3000)
      );
  };
  return (
    <form
      onSubmit={handleSubmit(onsubmit)}
      className="modal fade"
      id="ModalThemCongViecMongMuon"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="modal_knlv"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="modal_knlv">
              Thêm mong muốn việc làm
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Tên công việc
              </label>
              <input
                name="name_job"
                type="text"
                ref={register({ required: true })}
                
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Hình thức làm việc
              </label>
              <select
                name="type"
                className="form-control"
                ref={register({ required: true })}
              >
                <option value="">Chọn hình thức làm việc</option>
                {TypeJob.map((item) => (
                  <option key={item.id} value={item.id}>
                    {item.name}
                  </option>
                ))}
              </select>
            </div>
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Ngành nghê
              </label>
              <select
                className="form-control"
                name="id_category"
                ref={register({ required: true })}
              >
                {categories &&
                  Array.isArray(categories) &&
                  categories.length > 0 &&
                  categories.map((cate) => (
                    <option key={cate.id} value={cate.id}>
                      {cate.name}
                    </option>
                  ))}
              </select>
            </div>
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Mức lương (Tối thiểu)
              </label>
              <input
                type="number"
                ref={register({ required: true })}
                
                name="salary_1"
                className="form-control"
              />
            </div>{" "}
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Mức lương (Tối đa)
              </label>
              <input
                ref={register({ required: true })}
                type="number"
                
                name="salary_2"
                className="form-control"
              />
            </div>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary huy122"
              data-dismiss="modal"
            >
              Đóng
            </button>
            <button
              type="submit"
              //   onClick={addExperience}
              className="btn btn-outline-default"
            >
              Thêm mới
            </button>
          </div>
        </div>
      </div>
    </form>
  );
}

export default ModalThemCVMM;
