import React from "react";
const ModalDeleteKNLV = ({ id, deleteKNLV }) => {
  const handleDeleteKNLV = () => {
    deleteKNLV(id);
    document.querySelector(".huy2").click();
  };
  return (
    <div
      className="modal fade show"
      id={`modalDeleteKNLV${id}`}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Xóa kinh nghiệm làm việc
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            Bạn có muốn xóa kinh nghiệm làm việc này ?
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="huy2 btn btn-secondary "
              data-dismiss="modal"
            >
              Huỷ
            </button>
            <button
              type="button"
              onClick={handleDeleteKNLV}
              className="btn btn-primary"
            >
              Xóa
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalDeleteKNLV;
