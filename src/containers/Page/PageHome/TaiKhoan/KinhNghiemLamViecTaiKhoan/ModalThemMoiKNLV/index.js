import axios from "axios";
import React from "react";
import { useForm } from "react-hook-form";
import NotificationManager from "react-notifications/lib/NotificationManager";
import { api } from "../../../../../../api";
import { gettoken } from "../../../../../../utils/gettoken";
const ModalThemMoiKNLV = ({ getExperience }) => {
  const { register, handleSubmit, errors, watch } = useForm();
  const token = gettoken();
  const addExperience = (data) => {
    document.querySelector(".closeModal").click();
    axios
      .post(`${api}/experience/`, Object.assign(data, { token: token }))
      .then(function () {
        getExperience();
        NotificationManager.success(
          "Thêm kinh nghiệm làm việc thành công.",
          "Thành công",
          3000
        );
      });
  };
  return (
    <form
      onSubmit={handleSubmit(addExperience)}
      className="modal fade"
      id="modal_knlv"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="modal_knlv"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="modal_knlv">
              Thêm mới kinh nghiệm làm việc của bạn
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Tên công ty/tổ chức
              </label>
              <input
                ref={register({ required: true })}
                name="company"
                type="text"
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Ngày bắt đầu
              </label>
              <input
                ref={register}
                name="start"
                type="date"
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Ngày kết thúc
              </label>
              <input
                type="date"
                ref={register}
                name="end"
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Vị trí công việc
              </label>
              <input
                type="text"
                ref={register({ required: true })}
                name="position"
                className="form-control"
              />
            </div>{" "}
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Mô tả công việc
              </label>
              <textarea
                name="description"
                ref={register}
                className="form-control"
                rows="3"
              ></textarea>
            </div>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary closeModal"
              data-dismiss="modal"
            >
              Đóng
            </button>
            <button className="btn btn-outline-default">Thêm mới</button>
          </div>
        </div>
      </div>
    </form>
  );
};

export default ModalThemMoiKNLV;
