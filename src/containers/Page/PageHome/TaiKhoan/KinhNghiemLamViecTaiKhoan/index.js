import axios from "axios";
import React, { useEffect, useState } from "react";
import { gettoken } from "../../../../../utils/gettoken";
import { api } from "../../../../../api";
import ModalThemMoiKNLV from "./ModalThemMoiKNLV";
import ModalEditKNLV from "./ModalEditKNLV";
import ModalDeleteKNLV from "./ModalDeleteKNLV";
import NotificationManager from "react-notifications/lib/NotificationManager";
import { useDispatch } from "react-redux";
import FormatDate from "../../../../../utils/FormatDate";

const KinhNghiemLamViecTaiKhoan = () => {
  const token = gettoken();
  const [listExperience, setListExperience] = useState([]);

  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({
      type: "SET_POINT_FULL_INFO",
      point: {
        name: "experience",
        point: listExperience.length > 0 ? 1 : 0,
      },
    });
  }, [listExperience, dispatch]);

  const getExperience = () => {
    axios.get(`${api}/experience/${token}`).then(function (response) {
      setListExperience(response.data);
    });
  };

  const deleteKNLV = (id) => {
    axios.delete(`${api}/experience/delete/${id}`).then(() => {
      getExperience();
      NotificationManager.success(
        "Xóa kinh nghiệm làm việc thành công.",
        "Thành công",
        3000
      );
    });
  };
  const editExperience = (experience) => {
    axios
      .put(
        `${api}/experience/update`,
        Object.assign(experience, { token: token })
      )
      .then(function () {
        getExperience();
        NotificationManager.success(
          "Sửa kinh nghiệm làm việc thành công.",
          "Thành công",
          3000
        );
      });
  };

  useEffect(() => {
    axios.get(`${api}/experience/${token}`).then(function (response) {
      setListExperience(response.data);
    });
  }, [token]);
  return (
    <div>
      <div>
        <button
          type="button"
          className="btn btn-outline-primary"
          data-toggle="modal"
          data-target="#modal_knlv"
        >
          <i className="fa fa-plus mr-3"></i> Thêm kinh nghiệm làm việc
        </button>
        <ModalThemMoiKNLV getExperience={getExperience} />
        <div className="">
          {listExperience.length > 0 &&
            listExperience.map((experience) => (
              <div className="card-body" key={experience.id}>
                <div className="row">
                  <div className="col">
                    <span className="h2 font-weight-bold mb-0">
                      {experience.company}
                    </span>
                    <h5 className="card-title text-uppercase text-muted mb-0">
                      {experience.position}
                    </h5>
                    <p className="mb-0">{experience.description}</p>
                  </div>
                  <div className="col-auto">
                    <button
                      data-toggle="modal"
                      data-target={`#modalEdit_experience_${experience.id}`}
                      type="button"
                      className="btn btn-outline-default btn-icon  btn-sm"
                    >
                      <i className="fa fa-edit"></i>
                    </button>
                    <button
                      data-toggle="modal"
                      data-target={`#modalDeleteKNLV${experience.id}`}
                      type="button"
                      className="btn btn-outline-default btn-icon btn-sm"
                    >
                      <i className="fa fa-trash-alt"></i>
                    </button>
                  </div>
                </div>
                <ModalEditKNLV
                  editExperience={editExperience}
                  experience={experience}
                />
                <ModalDeleteKNLV deleteKNLV={deleteKNLV} id={experience.id} />
                <p className="mb-0 text-sm">
                  <span className="text-success mr-2">
                    {FormatDate(experience.start)}-{FormatDate(experience.end)}
                  </span>
                </p>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
};

export default KinhNghiemLamViecTaiKhoan;
