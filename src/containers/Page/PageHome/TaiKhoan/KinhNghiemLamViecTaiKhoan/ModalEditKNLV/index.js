import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";

const ModalEditKNLV = ({ experience, editExperience }) => {
  const onSubmit = (data) => {
    document.querySelector(".huy222").click();
    editExperience(Object.assign(data, { id: experience.id }));
  };
  const [detailExperience, setDetailExperience] = useState({
    start: "",
    end: "",
    company: "",
    position: "",
    description: "",
  });
  const handleChangeDetailExperience = (e) => {
    const { name, value } = e.target;
    setDetailExperience((prev) => {
      return { ...prev, [name]: value };
    });
  };
  useEffect(() => {
    setDetailExperience(experience);
  }, [experience]);
  const { register, handleSubmit } = useForm();

  return (
    <form
      className="modal fade"
      id={`modalEdit_experience_${experience.id}`}
      tabIndex="-1"
      onSubmit={handleSubmit(onSubmit)}
      role="dialog"
      aria-labelledby="modal_knlv"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="modal_knlv">
              Sửa kinh nghiệm làm việc
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Ngày bắt đầu
              </label>
              <input
                onChange={handleChangeDetailExperience}
                name="start"
                ref={register}
                type="date"
                value={detailExperience.start}
                
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Ngày tốt nghiệp
              </label>
              <input
                type="date"
                onChange={handleChangeDetailExperience}
                name="end"
                value={detailExperience.end}
                ref={register}
                
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Tên công ty/tổ chức
              </label>
              <input
                onChange={handleChangeDetailExperience}
                name="company"
                type="text"
                
                ref={register({ required: true })}
                value={detailExperience.company}
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Vị trí công việc
              </label>
              <input
                type="text"
                
                onChange={handleChangeDetailExperience}
                name="position"
                value={detailExperience.position}
                ref={register({ required: true })}
                className="form-control"
              />
            </div>{" "}
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Mô tả công việc
              </label>
              <textarea
                name="description"
                ref={register}
                onChange={handleChangeDetailExperience}
                className="form-control"
                
                rows="3"
                value={detailExperience.description}
              ></textarea>
            </div>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary huy222"
              data-dismiss="modal"
            >
              Đóng
            </button>
            <button type="submit" className="btn btn-outline-default">
              Sửa
            </button>
          </div>
        </div>
      </div>
    </form>
  );
};

export default ModalEditKNLV;
