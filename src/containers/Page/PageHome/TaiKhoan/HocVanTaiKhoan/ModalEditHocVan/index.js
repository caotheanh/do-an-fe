import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";

const ModalEdiHocVan = ({ study, editStudy }) => {
  const onSubmit = (data) => {
    document.querySelector(`.huy${study.id}`).click();
    editStudy(Object.assign(data, { id: study.id }));
  };
  const [detailStudy, setDetailStudy] = useState({
    start: "",
    end: "",
    school: "",
    description: "",
  });

  const handleChangeDetailStudy = (e) => {
    const { name, value } = e.target;
    setDetailStudy((prev) => {
      return { ...prev, [name]: value };
    });
  };
  useEffect(() => {
    setDetailStudy(study);
  }, [study]);
  const { register, handleSubmit } = useForm();
  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      className="modal fade"
      id={`modalEdit_${study.id}`}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Sửa học vấn của bạn
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Trường/Đơn vị đào tạo
              </label>
              <input
                onChange={handleChangeDetailStudy}
                name="school"
                type="text"
                value={detailStudy.school}
                ref={register({ required: true })}
                
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Ngày bắt đầu
              </label>
              <input
                onChange={handleChangeDetailStudy}
                name="start"
                type="date"
                ref={register}
                value={detailStudy.start}
                
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Ngày tốt nghiệp
              </label>
              <input
                onChange={handleChangeDetailStudy}
                name="end"
                type="date"
                ref={register}
                value={study.end}
                
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Mô tả quá trình học
              </label>
              <textarea
                onChange={handleChangeDetailStudy}
                name="description"
                value={detailStudy.description}
                ref={register}
                className="form-control"
                
                rows="3"
              ></textarea>
            </div>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className={`btn btn-secondary huy${study.id}`}
              data-dismiss="modal"
            >
              Đóng
            </button>
            <button
              type="submit"
              onClick={onSubmit}
              className="btn btn-outline-primary"
            >
              Sửa
            </button>
          </div>
        </div>
      </div>
    </form>
  );
};

export default ModalEdiHocVan;
