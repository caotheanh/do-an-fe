const ModalDeleteHocVan = ({ id, DeleteStudy }) => {
  const handleDeleteStudy = () => {
    DeleteStudy(id);
    document.querySelector(`.delete${id}`).click();
  };
  return (
    <div
      className="modal fade show"
      id={`modalDelete${id}`}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Xóa học vấn
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            Bạn có muốn xóa thông tin học vấn này ?
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className={`btn btn-secondary delete${id}`}
              data-dismiss="modal"
            >
              Huỷ
            </button>
            <button
              type="button"
              onClick={() => handleDeleteStudy(id)}
              className="btn btn-primary"
            >
              Xóa
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalDeleteHocVan;
