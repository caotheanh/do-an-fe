import axios from "axios";
import React, { useEffect, useState } from "react";
import { gettoken } from "../../../../../utils/gettoken";
import { api } from "../../../../../api";
import ModalDeleteHocVan from "./ModalDeleteHocVan";
import ModalThemMoiHocVan from "./ModalThemMoiHocVan";
import ModalEdiHocVan from "./ModalEditHocVan";
import NotificationManager from "react-notifications/lib/NotificationManager";
import { useDispatch } from "react-redux";
import FormatDate from "../../../../../utils/FormatDate";
function HocVanTaiKhoan() {
  const token = gettoken();
  const [listStudy, setListStudy] = useState([]);
  const [detailStudy, setDetailStudy] = useState({
    token: token,
    start: "",
    end: "",
    school: "",
    description: "",
  });

  const handleChangeDetailStudy = (e) => {
    const { name, value } = e.target;
    setDetailStudy((prev) => {
      return { ...prev, [name]: value };
    });
  };
  const getStudy = () => {
    axios.get(`${api}/study/${token}`).then(function (response) {
      setListStudy(response.data);
    });
  };
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch({
      type: "SET_POINT_FULL_INFO",
      point: {
        name: "study",
        point: listStudy.length > 0 ? 1 : 0,
      },
    });
  }, [listStudy, dispatch]);
  const addStudy = () => {
    axios
      .post(`${api}/study/`, Object.assign(detailStudy, { token: token }))
      .then(function () {
        NotificationManager.success(
          "Thêm học vấn thành công.",
          "Thành công",
          3000
        );
        getStudy();
      });
  };

  const editStudy = (dataStudy) => {
    axios
      .put(`${api}/study/update`, Object.assign(dataStudy, { token: token }))
      .then(function () {
        NotificationManager.success(
          "Sửa học vấn thành công.",
          "Thành công",
          3000
        );
        getStudy();
      });
  };
  const DeleteStudy = (id) => {
    axios.delete(`${api}/study/delete/${id}`).then(function () {
      NotificationManager.success(
        "Đã xóa học vấn thành công.",
        "Thành công",
        3000
      );
      getStudy();
    });
  };

  useEffect(() => {
    axios.get(`${api}/study/${token}`).then(function (response) {
      setListStudy(response.data);
    });
  }, [token]);

  return (
    <div>
      <button
        type="button"
        className="btn btn-outline-primary"
        data-toggle="modal"
        data-target="#exampleModal"
      >
        <i className="fa fa-plus mr-3"></i> Thêm học vấn
      </button>
      <ModalThemMoiHocVan
        addStudy={addStudy}
        handleChangeDetailStudy={handleChangeDetailStudy}
      />

      <div className="">
        {listStudy &&
          Array.isArray(listStudy) &&
          listStudy.map((study) => (
            <div key={study.id} className="card-body">
              <div className="row">
                <div className="col">
                  <span className="h3 font-weight-bold mb-0">
                    {study.school}
                  </span>
                  <h5 className="card-title text-uppercase text-muted mb-0">
                    {study.description}
                  </h5>
                </div>
                <div className="col-auto">
                  <button
                    data-toggle="modal"
                    data-target={`#modalEdit_${study.id}`}
                    type="button"
                    className="btn btn-outline-default btn-icon  btn-sm"
                  >
                    <i className="fa fa-edit"></i>
                  </button>
                  <button
                    data-toggle="modal"
                    data-target={`#modalDelete${study.id}`}
                    type="button"
                    className="btn btn-outline-default btn-icon  btn-sm"
                  >
                    <i className="fa fa-trash-alt"></i>
                  </button>
                </div>
              </div>
              <ModalEdiHocVan study={study} editStudy={editStudy} />
              <ModalDeleteHocVan id={study.id} DeleteStudy={DeleteStudy} />
              <p className="mb-0 text-sm">
                <span className="text-success mr-2">
                  {FormatDate(study.start)} - {FormatDate(study.end)}
                </span>
              </p>
            </div>
          ))}
      </div>
    </div>
  );
}

export default HocVanTaiKhoan;
