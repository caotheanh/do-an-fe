import React from "react";

const ModalThemMoiHocVan = ({ addStudy, handleChangeDetailStudy }) => {
  const handleClickAddStudy = () => {
    addStudy();
    document.querySelector(".closeAdd123").click();
  };
  return (
    <div
      className="modal fade"
      id="exampleModal"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Thêm học vấn của bạn
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Trường/Đơn vị đào tạo
              </label>
              <input
                onChange={handleChangeDetailStudy}
                name="school"
                type="text"
                
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Ngày bắt đầu
              </label>
              <input
                onChange={handleChangeDetailStudy}
                name="start"
                type="date"
                
                className="form-control"
              />
            </div>
            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Ngày tốt nghiệp
              </label>
              <input
                onChange={handleChangeDetailStudy}
                name="end"
                type="date"
                
                className="form-control"
              />
            </div>

            <div className="form-group">
              <label className="form-control-label" htmlFor="input-fullname">
                Mô tả quá trình học
              </label>
              <textarea
                onChange={handleChangeDetailStudy}
                name="description"
                className="form-control"
                
                rows="3"
              ></textarea>
            </div>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary closeAdd123"
              data-dismiss="modal"
            >
              Đóng
            </button>
            <button
              type="button"
              onClick={handleClickAddStudy}
              className="btn btn-outline-primary"
            >
              Thêm mới
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalThemMoiHocVan;
