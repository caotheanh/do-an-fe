import Loadable from "react-loadable";
import LoaderTaiKhoan from "./LoaderTaiKhoan";
export default Loadable({
  loader: () => import("./index"),
  loading() {
    return <LoaderTaiKhoan />;
  },
});
