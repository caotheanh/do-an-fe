import React from "react";

const NavItemTaiKhoan = ({ text, indexItem, icon, active ,iconStatus}) => {
  return (
    <li className="nav-item">
      <a
        className={`nav-link mb-sm-4 mb-md-0 ${active ? "active" : ""}`}
        id={`tabs-icons-text-${indexItem}-tab`}
        data-toggle="tab"
        href={`#tabs-icons-text-${indexItem}`}
        role="tab"
        aria-controls={`tabs-icons-text-${indexItem}`}
        aria-selected="true"
      >
        <span>{text}</span>
        <i
        className={`${iconStatus}`}
        style={{ fontSize: "20px" }}
      ></i>
      </a>
      
    </li>
  );
};

export default NavItemTaiKhoan;
