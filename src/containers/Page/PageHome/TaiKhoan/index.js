import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { gettoken } from "../../../../utils/gettoken";
import HocVanTaiKhoan from "./HocVanTaiKhoan";
import KinhNghiemLamViecTaiKhoan from "./KinhNghiemLamViecTaiKhoan";
import NavItemTaiKhoan from "./NavItemTaiKhoan";
import ThongTinCoBanTaiKhoan from "./ThongTinCoBanTaiKhoan";
import LoaderTaiKhoan from "./LoaderTaiKhoan";
import { api } from "../../../../api";
import axios from "axios";
import {
  NotificationContainer,
  NotificationManager,
} from "react-notifications";
import { useForm } from "react-hook-form";
import CongViecMongMuonTaiKhoan from "./CongViecMongMuonTaiKhoan";
import ModalDoiMatKhau from "./ModalDoiMatKhau";
const TaiKhoan = () => {
  const [image, setImage] = useState();
  const info_login = useSelector((state) => state.info_login);
  const info_user = useSelector((state) => state.info_user);
  const dispatch = useDispatch();
  const is_login = useSelector((state) => state.is_login);
  const { register, handleSubmit } = useForm();
  const [isChangeAvatar, setisChangeAvatar] = useState(false);
  const styleTabContent = { minHeight: "400px" };
  const token = gettoken();
  const formData = new FormData();
  const [storeDataUser, setStoreDataUser] = useState({
    study: null,
    experience: null,
    work: null,
    userInfomation: null,
  });

  const onsubmit = (data) => {
    const { photo } = data;
    formData.append("photo", photo[0]);
    axios.post(`${api}/user/avatar/${token}`, formData).then(function () {
      setisChangeAvatar(false);
      NotificationManager.success(
        "Thay đổi ảnh đại diện thành công.",
        "Thành công",
        3000
      );
    });
  };
  const handleClose = () => {
    setisChangeAvatar(false);
    setImage(
      (info_login &&
        info_login.images &&
        `${api}/avatar/${info_login.images}`) ||
        null
    );
  };
  const handleChangeImage = (event) => {
    setisChangeAvatar(true);
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      setImage(URL.createObjectURL(img));
    }
  };
  useEffect(() => {
    if (info_login && info_login.images) {
      setImage(`${api}/avatar/${info_login.images}`);
    }
  }, [info_login]);
  let textImage = "";
  if (is_login && info_user && info_user.email) {
    textImage = info_user.name.substr(0, 1);
  } else if (is_login && info_login && info_login.email) {
    textImage = info_login.email.substr(0, 1);
  }
  const styleDefaultNoneAvatar = {
    background: `#adb5bd`,
    width: "120px",
    textTransform: "uppercase",
    height: "120px",
    fontSize: "80px",
    textAlign: "center",
    borderRadius: "10px",
  };

  const success = `la la-check-circle-o pl-3 text-success`;
  const warning = "la la-exclamation-circle pl-3 text-danger";

  const listNav = [
    {
      text: "Thông tin tài khoản",
      icon: "ni ni-cloud-upload-96 mr-2",
      active: true,
      name: "userInfomation",
    },
    {
      text: "Hoc vấn",
      icon: "ni ni-cloud-upload-96 mr-2",
      active: false,
      name: "study",
    },
    {
      text: "Kinh nghiệm làm việc",
      icon: "ni ni-cloud-upload-96 mr-2",
      active: false,
      name: "experience",
    },
    {
      text: "Công việc muốn tìm",
      icon: "ni ni-cloud-upload-96 mr-2",
      active: false,
      name: "work",
    },
  ];
  const detail_info_user = useSelector((state) => state.detail_info_user);
  useEffect(() => {
    if (detail_info_user) {
      setStoreDataUser(detail_info_user);
    }
  }, [detail_info_user]);
  useEffect(() => {
    const getData = async () => {
      const study = await axios.get(`${api}/study/${token}`);
      const experience = await axios.get(`${api}/experience/${token}`);
      const work = await axios.get(`${api}/work/${token}`);
      const userInfomation = await axios.get(`${api}/user-infomation/${token}`);
      const data = {
        study: study.data && study.data.length > 0 ? study.data : null,
        experience:
          experience.data && experience.data.length > 0
            ? experience.data
            : null,
        work: work.data && work.data.length > 0 ? work.data : null,
        userInfomation:
          userInfomation.data &&
          userInfomation.data.data &&
          userInfomation.data.data !== {}
            ? userInfomation.data.data
            : null,
      };

      dispatch({ type: "SET_DETAIL_INFO_USER", detail_info_user: data });
      setStoreDataUser(data);
    };
    getData();
  }, [token, dispatch]);
  const [statusInfo, setstatusInfo] = useState(0);
  const [isLoading, setIsLoading] = useState(true);
  const point_full_info = useSelector((state) => state.point_full_info);
  useEffect(() => {
    let point = 0;
    const pointArr = [10, 10, 30, 50];
    ["study", "experience", "work", "userInfomation"].forEach((item, index) => {
      if (point_full_info[item]) {
        point = point + pointArr[index];
      }
    });
    setstatusInfo(point);
    point_full_info && setIsLoading(false);
  }, [point_full_info]);
  useEffect(() => {
    if (storeDataUser) {
      Object.keys(storeDataUser).forEach(function (key, index) {
        dispatch({
          type: "SET_POINT_FULL_INFO",
          point: { name: key, point: storeDataUser[key] ? 1 : 0 },
        });
      });
    }
  }, [storeDataUser, dispatch]);

  if (!is_login) {
    return (
      <div className="m-5">
        <div className="d-flex justify-content-center">
          <div style={{ width: "70%", textAlign: "center" }}>
            <img
              alt="not found"
              src="/assets/img/page_not_found.svg"
              width="70%"
            />
            <div className="text-center mt-5 display-3">
              Vui lòng đăng nhập.
            </div>
          </div>
        </div>
      </div>
    );
  }

  if (isLoading) {
    return <LoaderTaiKhoan />;
  }
  return (
    <>
      <div className="header bg-gradient-success pb-6">
        <div className="container-fluid">
          <div className="header-body">
            <div className="row align-items-center py-4">
              <div className="col-lg-6 col-7"></div>
              <div className="col-lg-6 col-5 text-right"></div>
            </div>
          </div>
        </div>
      </div>
      <div className="container mt--6">
        <div className="row">
          <div className="col">
            <div className="card">
              <div className="card-header border-0">
                <div className="row">
                  <form
                    onSubmit={handleSubmit(onsubmit)}
                    encType="multipart/form-data"
                    className="col-lg-auto"
                  >
                    <>
                      <div
                        style={
                          image
                            ? {
                                background: ` url(${image}) 50% 50% no-repeat`,
                                width: "120px",
                                height: "120px",
                                borderRadius: "10px",
                                backgroundSize: "cover",
                                border: "4px solid #fff",
                                margin: "auto",
                                // backgroundPosition: "top center",
                              }
                            : styleDefaultNoneAvatar
                        }
                        className="imageBackground card-profile-image"
                      >
                        {!image && textImage}
                        <div className="d-flex justify-content-center">
                          {isChangeAvatar ? (
                            <div>
                              <button
                                style={{
                                  width: "40px",
                                  color: "red",
                                  height: "40px",
                                }}
                                onClick={handleClose}
                                type="button"
                                className="btn icon icon-lg icon-shape icon-shape-primary shadow rounded-circle avatar-ss"
                              >
                                <i
                                  className="fa fa-times-circle"
                                  style={{ fontSize: "15px" }}
                                ></i>
                              </button>
                              <button
                                style={{
                                  width: "40px",
                                  height: "40px",
                                  color: "green",
                                }}
                                className="btn icon icon-lg icon-shape icon-shape-primary shadow rounded-circle avatar-ss"
                              >
                                <i
                                  className="fa fa-check"
                                  style={{ fontSize: "15px" }}
                                ></i>
                              </button>
                            </div>
                          ) : (
                            <label htmlFor="upload-photo" className="mt--3">
                              <div
                                style={{
                                  width: "40px",
                                  height: "40px",
                                  color: "#fff",
                                }}
                                type="button"
                                className=" icon icon-lg icon-shape icon-shape-primary shadow rounded-circle avatar-ss"
                              >
                                <i
                                  className="fa fa-camera"
                                  style={{ fontSize: "15px" }}
                                ></i>
                              </div>
                            </label>
                          )}
                          <input
                            onChange={handleChangeImage}
                            style={{
                              opacity: "0",
                              position: "absolute",
                              zIndex: "-1",
                            }}
                            accept="image/png, image/jpeg, image/jpg"
                            ref={register}
                            type="file"
                            name="photo"
                            id="upload-photo"
                            className="form-control"
                          />
                        </div>
                      </div>
                    </>
                  </form>
                  <div className="col-auto">
                    <div className="col-auto my-auto">
                      <div className="h-100">
                        <h1 className="mb-1">
                          {(info_user && info_user.name) ||
                            "(Chưa cập nhật tên)"}
                        </h1>
                        <h3>{info_login && info_login.email}</h3>
                        <p className="mb-0 font-weight-bold text-sm">
                          {
                            [
                              "Nhà tuyển dụng",
                              "Người tìm việc",
                              "Quản trị viên",
                            ][info_login && info_login.type - 1]
                          }
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="col-sm-4  col-lg-3 my-sm-auto ms-sm-auto me-sm-0 mt-3 d-flex justify-content-center">
                    <button
                      className="btn btn-outline-default btn-icon"
                      data-toggle="modal"
                      data-target="#changePassword"
                    >
                      <i className="fa fa-edit mr-1"></i> Đổi mật khẩu
                    </button>
                    <ModalDoiMatKhau />
                  </div>
                </div>
                <div className="progress-wrapper">
                  <div className="progress-info">
                    <div className="progress-label">
                      <span>Mức độ hoàn thành hồ sơ</span>
                    </div>
                    <div className="progress-percentage">
                      <span>{statusInfo}%</span>
                    </div>
                  </div>
                  <div className="progress">
                    <div
                      className="progress-bar bg-primary"
                      role="progressbar"
                      aria-valuenow={statusInfo}
                      aria-valuemin="0"
                      aria-valuemax="100"
                      style={{ width: `${statusInfo}%` }}
                    ></div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="nav-wrapper ">
          <ul
            className="nav nav-pills nav-fill flex-column flex-md-row"
            id="tabs-icons-text"
            role="tablist"
          >
            {listNav &&
              listNav.map((nav, index) => (
                <NavItemTaiKhoan
                  iconStatus={
                    point_full_info[nav.name] === 1 ? success : warning
                  }
                  key={index}
                  icon={nav.icon}
                  active={nav.active}
                  indexItem={index + 1}
                  text={nav.text}
                />
              ))}
          </ul>
        </div>
        <div className=" shadow">
          <div className="card-body">
            <div className="tab-content" id="myTabContent">
              {[
                ThongTinCoBanTaiKhoan,
                HocVanTaiKhoan,
                KinhNghiemLamViecTaiKhoan,
                CongViecMongMuonTaiKhoan,
              ].map((Component, index) => (
                <div
                  style={styleTabContent}
                  key={`taikhoan___${index}`}
                  className={`tab-pane fade show ${index < 1 ? "active" : ""}`}
                  id={`tabs-icons-text-${index + 1}`}
                  role="tabpanel"
                  aria-labelledby={`"tabs-icons-text-${index + 1}-tab `}
                >
                  <Component storeDataUser={storeDataUser} />
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
      <NotificationContainer />
    </>
  );
};

export default TaiKhoan;
