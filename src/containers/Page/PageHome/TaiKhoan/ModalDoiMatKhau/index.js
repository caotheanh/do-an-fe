import React, { useRef } from "react";
import { useForm } from "react-hook-form";
import { gettoken } from "../../../../../utils/gettoken";
import { api } from "../../../../../api";
import axios from "axios";
import NotificationManager from "react-notifications/lib/NotificationManager";
function ModalDoiMatKhau() {
  const token = gettoken();
  const { handleSubmit, errors, register, watch } = useForm();
  const changePassword = (data) => {
    const dataChangePassword = {
      token: token,
      currentPassword: data.currentPassword,
      newPassword: data.newPassword,
    };
    axios.put(`${api}/user/change-password`, dataChangePassword).then((res) => {
      if (res.data.data.messages === "Change password success!") {
        document.querySelector(".closeModalChangePassword").click();
        NotificationManager.success(
          "Thay đổi ảnh đại diện thành công.",
          "Thành công",
          3000
        );
      }
    });
  };
  const password = useRef({});
  password.current = watch("newPassword", "");
  return (
    <form
      onSubmit={handleSubmit(changePassword)}
      className="modal fade"
      id="changePassword"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Thay đổi mật khẩu
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              autoComplete="off"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div className="form-group">
              <label
                htmlFor="example-password-input"
                className="form-control-label"
              >
                Mật khẩu cũ
              </label>
              <input
                className="form-control"
                ref={register({
                  required: "Vui lòng nhập mật khẩu",
                  minLength: {
                    value: 6,
                    message: "Mật khẩu ít nhất 6 kí tự",
                  },
                })}
                type="password"
                autoComplete="off"
                name="currentPassword"
                
              />
              {errors.currentPassword && (
                <p className="text-danger">{errors.currentPassword.message}</p>
              )}
            </div>
            <div className="form-group">
              <label
                htmlFor="example-password-input"
                className="form-control-label"
              >
                Mật khẩu mới
              </label>
              <input
                autoComplete="off"
                className="form-control"
                type="password"
                name="newPassword"
                ref={register({
                  required: "Vui lòng nhập mật khẩu mới",
                  minLength: {
                    value: 6,
                    message: "Mật khẩu ít nhất 6 kí tự",
                  },
                })}
                
              />
              {errors.newPassword && (
                <p className="text-danger">{errors.newPassword.message}</p>
              )}
            </div>
            <div
              className={`form-group ${
                errors.cf_newPassword ? "has-danger" : "has-success"
              } `}
            >
              <label
                htmlFor="example-password-input"
                className="form-control-label"
              >
                Nhập lại mật khẩu mới
              </label>
              <input
                className={`form-control ${
                  errors.cf_newPassword && "is-invalid"
                }`}
                ref={register({
                  validate: (value) =>
                    value === password.current || "Nhập lại mật khẩu không trùng khớp",
                })}
                type="password"
                name="cf_newPassword"
                
              />
              {errors.cf_newPassword && (
                <p className="text-danger">{errors.cf_newPassword.message}</p>
              )}
            </div>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary closeModalChangePassword"
              data-dismiss="modal"
            >
              Hủy
            </button>
            <button type="submit" className="btn btn-primary">
              Đổi mật khẩu
            </button>
          </div>
        </div>
      </div>
    </form>
  );
}

export default ModalDoiMatKhau;
