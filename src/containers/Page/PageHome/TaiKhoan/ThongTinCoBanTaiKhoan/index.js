import axios from "axios";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { gettoken } from "../../../../../utils/gettoken";
import { DataAddress } from "../../../../Data/DataAddress";
import { api } from "../../../../../api";
import { useDispatch, useSelector } from "react-redux";
import NotificationManager from "react-notifications/lib/NotificationManager";
const ThongTinCoBanTaiKhoan = ({ storeDataUser, point_full_info }) => {
  const token = gettoken();
  const info_login = useSelector((state) => state.info_login);
  const info_user = useSelector((state) => state.info_user);
  const [dataChangeInfoUser, setdataChangeInfoUser] = useState({});
  const { register, handleSubmit, errors } = useForm();
  const dispatch = useDispatch();
  const [addressDetail, setAddressDetail] = useState("");
  const handleChangeInfoUser = (e) => {
    const { name, value } = e.target;
    setdataChangeInfoUser((prev) => {
      return { ...prev, [name]: value };
    });
  };
  useEffect(() => {
    setdataChangeInfoUser(
      storeDataUser.userInfomation ? storeDataUser.userInfomation : []
    );
  }, [storeDataUser.userInfomation]);
  const date = new Date();
  const getDataUser = () => {
    axios({
      method: "get",
      url: `${api}/user-infomation/${token}`,
    }).then((res) => {
      if (res.data) {
        dispatch({
          type: "SET_POINT_FULL_INFO",
          point: {
            name: "userInfomation",
            point: res.data.data ? 1 : 0,
          },
        });
        dispatch({ type: "SET_IS_LOGIN", is_login: true });
        dispatch({ type: "SET_INFO_USER", info_user: res.data.data });
      }
    });
  };
  const onsubmit = async (data) => {
    await (info_user
      ? axios
          .put(
            `${api}/user-infomation`,
            Object.assign(data, { token: token, id_user: info_login.id })
          )
          .then(() => {
            NotificationManager.success(
              "Cập nhật thông tin thành công.",
              "Thành công",
              3000
            );
            getDataUser();
          })
      : axios
          .post(
            `${api}/user-infomation`,
            Object.assign(data, { token: token, id_user: info_login.id })
          )
          .then(() => {
            NotificationManager.success(
              "Cập nhật thông tin thành công.",
              "Thành công",
              3000
            );
            getDataUser();
          }));
  };
  useEffect(() => {
    let province_or_city = "",
      district = "",
      ward = "";
    if (dataChangeInfoUser.province_or_city) {
      province_or_city = DataAddress[dataChangeInfoUser.province_or_city][1];
    }
    if (dataChangeInfoUser.district) {
      district =
        DataAddress[dataChangeInfoUser.province_or_city][4][
          dataChangeInfoUser.district
        ][1];
    }
    if (dataChangeInfoUser.ward) {
      ward =
        DataAddress[dataChangeInfoUser.province_or_city][4][
          dataChangeInfoUser.district
        ][4][dataChangeInfoUser.ward][1];
    }
    setAddressDetail(
      [
        dataChangeInfoUser.details_address,
        province_or_city,
        district,
        ward,
      ].join(" - ")
    );
  }, [dataChangeInfoUser]);

  let list_district = [];
  let list_ward = [];
  if (dataChangeInfoUser) {
    if (
      dataChangeInfoUser.province_or_city ||
      dataChangeInfoUser.province_or_city === 0
    ) {
      list_district = DataAddress[dataChangeInfoUser.province_or_city][4];
    }
    if (dataChangeInfoUser.district) {
      list_ward =
        DataAddress[dataChangeInfoUser.province_or_city][4][
          dataChangeInfoUser.district
        ][4];
    }
  }
  return (
    <>
      <form onSubmit={handleSubmit(onsubmit)}>
        <h3 className="heading-small text-muted mb-4">
          Cài đặt thông tin cơ bản
        </h3>
        <div className="pl-lg-6">
          <div className="row">
            <div className="col-lg-6">
              <div className="form-group">
                <label className="form-control-label" >
                  Họ và tên
                </label>
                <input
                  name="name"
                  onChange={handleChangeInfoUser}
                  type="text"
                  ref={register({
                    required: "Vui lòng nhập họ tên",
                  })}
                  value={(dataChangeInfoUser && dataChangeInfoUser.name) || ""}
                  className="form-control"
                  placeholder="Username"
                />
                {errors.name && (
                  <p className="text-danger">{errors.name.message}</p>
                )}
              </div>
            </div>
            <div className="col-lg-6">
              <div className="form-group">
                <label className="form-control-label" htmlFor="input-date">
                  Ngày sinh
                </label>
                <input
                  type="date"
                  name="birth_date"
                  onChange={handleChangeInfoUser}
                  ref={register({
                    required: "Vui lòng nhập ngày sinh",
                    max: {
                      value: `${date.getFullYear()-17}-01-01`,
                      message: "Ngày sinh không hợp lệ",
                    },
                  })}
                  value={dataChangeInfoUser && dataChangeInfoUser.birth_date}
                  className="form-control"
                />
                {errors.birth_date && (
                  <p className="text-danger">{errors.birth_date.message}</p>
                )}
              </div>
            </div>
            <div className="col-lg-6">
              <div className="form-group">
                <label className="form-control-label" htmlFor="input-phone">
                  Số điện thoại
                </label>
                <input
                  name="phone"
                  onChange={handleChangeInfoUser}
                  type="number"
                  ref={register({
                    required: "Vui lòng nhập số điện thoại",
                  })}
                  value={dataChangeInfoUser && dataChangeInfoUser.phone}
                  id="input-date"
                  className="form-control"
                />
                {errors.phone && (
                  <p className="text-danger">{errors.phone.message}</p>
                )}
              </div>
            </div>
            <div className="col-lg-6">
              <div className="form-group">
                <label className="form-control-label" htmlFor="input-phone">
                  Giới tính
                </label>
                <div className="form-control">
                  <div className="custom-control custom-radio custom-control-inline">
                    <input
                      type="radio"
                      ref={register({ required: true })}
                      onChange={handleChangeInfoUser}
                      id="customRadioInline1"
                      value="1"
                      name="gender"
                      checked={
                        dataChangeInfoUser && +dataChangeInfoUser.gender === 1
                      }
                      className="custom-control-input"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customRadioInline1"
                    >
                      Nam
                    </label>
                  </div>
                  <div className="custom-control custom-radio custom-control-inline">
                    <input
                      type="radio"
                      ref={register({ required: true })}
                      onChange={handleChangeInfoUser}
                      value="2"
                      checked={
                        dataChangeInfoUser && +dataChangeInfoUser.gender === 2
                      }
                      id="customRadioInline2"
                      name="gender"
                      className="custom-control-input"
                    />
                    <label
                      className="custom-control-label"
                      htmlFor="customRadioInline2"
                    >
                      Nữ
                    </label>
                  </div>
                </div>
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-4">
              <div className="form-group">
                <label className="form-control-label" >
                  Thành phố
                </label>
                <select
                  onChange={handleChangeInfoUser}
                  name="province_or_city"
                  ref={register({ required: true })}
                  className="form-control"
                  value={
                    dataChangeInfoUser && dataChangeInfoUser.province_or_city
                  }
                  
                >
                  {dataChangeInfoUser.province_or_city === "" && (
                    <option value="">Chọn thành phố</option>
                  )}
                  {DataAddress.map((thanhpho, index) => (
                    <option
                      key={`thanh_pho_${index}`}
                      checked={true}
                      value={index}
                    >
                      {thanhpho[1]}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="form-group">
                <label className="form-control-label" >
                  Quận/Huyện
                </label>
                <select
                  onChange={handleChangeInfoUser}
                  name="district"
                  disabled={
                    dataChangeInfoUser.province_or_city ||
                    dataChangeInfoUser.province_or_city === 0
                      ? false
                      : true
                  }
                  ref={register({ required: true })}
                  className="form-control"
                  value={dataChangeInfoUser && dataChangeInfoUser.district}
                  
                >
                  {!dataChangeInfoUser.province_or_city && (
                    <option value="">Quận/Huyện</option>
                  )}
                  {list_district.map((huyen, index) => (
                    <option key={`huyen_${huyen[0]}`} value={index}>
                      {huyen[1]}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="col-lg-4">
              <div className="form-group">
                <label className="form-control-label" >
                  Xã/Phường
                </label>
                <select
                  name="ward"
                  disabled={dataChangeInfoUser.district ? false : true}
                  onChange={handleChangeInfoUser}
                  ref={register({ required: true })}
                  className="form-control"
                  defaultChecked="3"
                  value={dataChangeInfoUser && dataChangeInfoUser.ward}
                  
                >
                  {!dataChangeInfoUser.province_or_city && (
                    <option value="">Xã/Phường</option>
                  )}
                  {list_ward.map((xa, index) => (
                    <option
                      key={`xa_${xa[0]}`}
                      value={index}
                      defaultChecked={+dataChangeInfoUser.ward === index}
                    >
                      {xa[1]}
                    </option>
                  ))}
                </select>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <div className="form-group">
                <input
                  value={
                    dataChangeInfoUser && dataChangeInfoUser.details_address
                  }
                  type="text"
                  ref={register}
                  name="details_address"
                  onChange={handleChangeInfoUser}
                  className="form-control"
                  
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-12">
              <div className="form-group">
                <input
                  value={
                    addressDetail && dataChangeInfoUser.ward
                      ? addressDetail
                      : ""
                  }
                  type="text"
                  disabled
                  className="form-control"
                  
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-6">
              <div className="form-group">
                <label className="form-control-label" >
                  Website cá nhân
                </label>
                <input
                  type="text"
                  value={dataChangeInfoUser && dataChangeInfoUser.website}
                  name="website"
                  ref={register}
                  onChange={handleChangeInfoUser}
                  className="form-control"
                  placeholder="Username"
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-lg-6">
              <div className="form-group">
                <label className="form-control-label" >
                  Giới thiệu
                </label>
                <textarea
                  name="detail_user"
                  onChange={handleChangeInfoUser}
                  value={dataChangeInfoUser && dataChangeInfoUser.detail_user}
                  className="form-control"
                  ref={register}
                  
                  rows="3"
                ></textarea>
              </div>
            </div>
          </div>
        </div>
        <hr className="my-4" />
        <div className=" justify-content-center">
          <div>
            <button type="submit" className=" btn btn-primary">
              Lưu thông tin
            </button>
          </div>
        </div>
      </form>
    </>
  );
};

export default ThongTinCoBanTaiKhoan;
