import React from "react";
import ContentLoader from "react-content-loader";

const LoaderTaiKhoan = (props) => (
  <div className="container">
    <ContentLoader
      speed={2}
      viewBox="0 0 600 400"
      backgroundColor="#f3f3f3"
      foregroundColor="#ecebeb"
      {...props}
    >
      <rect x="0" y="22" rx="0" ry="0" width="591" height="111" />
      <rect x="1" y="156" rx="0" ry="0" width="147" height="26" />
      <rect x="152" y="157" rx="0" ry="0" width="121" height="26" />
      <rect x="280" y="155" rx="0" ry="0" width="156" height="26" />
      <rect x="439" y="154" rx="0" ry="0" width="150" height="26" />
      <rect x="0" y="191" rx="0" ry="0" width="589" height="176" />
    </ContentLoader>
  </div>
);

export default LoaderTaiKhoan;
