import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import CardJobPC from "../../../../components/Common/CardJobPC";
import LoaderViecLamDaLuu from "./LoaderViecLamDaLuu";

function ViecLamDaLuu() {
  const [isLikeJob, setIsLikeJob] = useState([]);
  const [store, setStore] = useState([]);
  const [isLoading, setisLoading] = useState(true);
  useEffect(() => {
    const jobStorage = JSON.parse(localStorage.getItem("stored_jobs")) || [];
    setIsLikeJob(
      (jobStorage &&
        jobStorage.length > 0 &&
        Array.isArray(jobStorage) &&
        jobStorage) ||
        []
    );
    const data =
      (jobStorage &&
        jobStorage.length > 0 &&
        Array.isArray(jobStorage) &&
        jobStorage) ||
      [];
    setStore(data);
    setisLoading(false);
  }, []);
  const handleLikeJob = (key_job) => {
    const checkJob = (isLikeJob || []).findIndex(
      (item) => key_job.id === item.id
    );

    if (checkJob === -1) {
      setIsLikeJob([...isLikeJob, key_job]);
      localStorage.setItem(
        "stored_jobs",
        JSON.stringify([...isLikeJob, key_job] || [])
      );
    } else {
      const filterJob = isLikeJob.filter((item) => item !== key_job);
      localStorage.setItem("stored_jobs", JSON.stringify(filterJob || []));
      setIsLikeJob(filterJob);
    }
  };
  return isLoading ? (
    <LoaderViecLamDaLuu />
  ) : (
    <>
      <Helmet>
        <title>Việc Làm Đã Lưu</title>
      </Helmet>
      <div className="container">
        <div className="">
          <div className="card-header">
            <div className="row align-items-center">
              <div className="col-8">
                <h3 className="mb-0">Việc Làm Đã Lưu</h3>
              </div>
            </div>
          </div>
          <div className="row mt-3">
            {store && Array.isArray(store) && store.length > 0 ? (
              store.map((job, index) => (
                <CardJobPC
                  handleLikeJob={handleLikeJob}
                  isLikeJob={isLikeJob}
                  index={index}
                  key={job.id}
                  job={job}
                />
              ))
            ) : (
              <div className="header-body text-center mb-7">
                <div className="row justify-content-center">
                  <div className="col-xl-5 col-lg-6 col-md-8 px-5">
                    <img
                      alt="no data"
                      className="text-center m-3"
                      src="/assets/img/khong-co.svg"
                      style={{ width: "40%" }}
                    />
                    <h2 className="text-lead text-center mt-5 ">
                      Bạn chưa lưu việc làm nào !
                    </h2>
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </div>
    </>
  );
}

export default ViecLamDaLuu;
