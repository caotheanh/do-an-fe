import axios from "axios";
import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { useParams } from "react-router";
import { api } from "../../../../api";
import { gettoken } from "../../../../utils/gettoken";
import { useSelector } from "react-redux";
import ModalCV from "./ModalCV";
import { getAddress } from "../../../../utils/getAddress";
import { Link, useHistory } from "react-router-dom/cjs/react-router-dom.min";
import LoaderPageJob from "./LoaderPageJob";
import NumberFormat from "react-number-format";
import {
  NotificationContainer,
  NotificationManager,
} from "react-notifications";
import { DataAddress } from "../../../Data/DataAddress";
import FormatDate from "../../../../utils/FormatDate";
import TypeJob from "../../../../utils/TypeJob";

const ViecLam = () => {
  const TITLE = "Chi tiết việc làm";
  const { viec_lam_id } = useParams();
  const [detailWork, setWork] = useState("");
  const [category, setCategory] = useState("");
  const [token, setToken] = useState("");
  const [listCV, setListCV] = useState([]);
  const [cvActive, setCvActive] = useState();
  const [address_company, setAddress_company] = useState("");
  const [isContentloader, setIsContentloader] = useState(false);
  const [loading, setLoading] = useState(false);
  const is_login = useSelector((state) => state.is_login);
  const tokenLocalStorage = gettoken();

  function topFunction() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  }

  const getListCV = (tokenLocalStorage) => {
    axios
      .get(`${api}/cv/get-cv/${tokenLocalStorage}`)
      .then(function (response) {
        setListCV(response.data || []);
      });
  };
  const history = useHistory();
  const applyJob = () => {
    setLoading(true);
    const data = {
      token: token,
      id_recruitment: viec_lam_id,
      link_cv: cvActive,
    };
    axios
      .post(`${api}/apply/`, data)
      .then((res) => {
        if (res.data.status === 401) {
          NotificationManager.error(
            "Trước đó bạn đã ứng tuyển việc làm này",
            "Thất bại",
            3000
          );
        } else {
          NotificationManager.success(
            "Ứng tuyển thành công",
            "Thành công",
            3000
          );
          history.push("/viec-lam-da-ung-tuyen");
        }
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  };

  useEffect(() => {
    if (tokenLocalStorage) {
      setToken(tokenLocalStorage);
      getListCV(tokenLocalStorage);
    }
    topFunction();
    getWork(viec_lam_id);
  }, [tokenLocalStorage, viec_lam_id]);
  const getWork = async (viec_lam_id) => {
    const { data } = await axios
      .get(`${api}/recruitment/${viec_lam_id}`)
      .finally(() => setIsContentloader(true));
    if (data && data.length > 0) {
      const { province_or_city, district, ward, details_address } = data[0];
      setAddress_company(
        getAddress(province_or_city, district, ward, details_address)
      );
      setWork(data[0]);
      setCategory(data[0].category.name);
    }
  };
  let typeJobDetail = "";
  if (detailWork && detailWork.type) {
    const types = TypeJob.find((item) => +detailWork.type === +item.id);
    typeJobDetail = types ? types.name : "";
  }
  const info_login = useSelector((state) => state.info_login);
  return (
    <div>
      <Helmet>
        <title>{TITLE}</title>
      </Helmet>
      {isContentloader ? (
        detailWork ? (
          <div className="container">
            <div className="container-fluid ">
              <div className="row">
                <div className="col-xl-9 order-xl-1">
                  <div className="card">
                    <div className="row">
                      <div className="col-12 mx-auto">
                        <div className="row m-3">
                          <div className="col-lg-3 col-md-5 position-relative my-auto">
                            <div
                              style={{
                                background: ` url(${api}/${detailWork.img}) 50% 50% no-repeat`,
                                width: "150px",
                                height: "150px",
                                borderRadius: "10px",
                                backgroundSize: "cover",
                                border: "4px solid #fff",
                                // backgroundPosition: "top center",
                              }}
                              className="imageBackground"
                            ></div>
                          </div>
                          <div className="col-lg-9 z-index-2 position-relative px-md-2 px-sm-5 mt-sm-0 mt-4">
                            <div className="d-flex justify-content-between align-items-center mb-2">
                              <div className="col-lg-10">
                                <h2 className=" text-left mb-0">
                                  {detailWork.title}
                                </h2>
                                <Link
                                  to={`/xem-cong-ty/${detailWork.company_id}`}
                                  className=" mb-0"
                                >
                                  {detailWork.company}
                                </Link>
                              </div>
                              {/* <div className="d-block">
                                <button
                                  type="button"
                                  className="btn btn-sm btn-outline-info text-nowrap mb-0"
                                >
                                  Theo dõi
                                </button>
                              </div> */}
                            </div>
                            {/* <div className="row mb-4 ml-1">
                              <div className="col-auto">
                                <span className="h3 pr-2">323</span>
                                <span>Lượt xem</span>
                              </div>
                            </div> */}
                          </div>
                        </div>
                      </div>
                    </div>

                    <div className="card-body">
                      <>
                        <div className="row">
                          <div className="row">
                            <div className="col-6 col-md-4 mt-3">
                              <div className="">
                                <div className="  text-center justify-content-center">
                                  <div className="row">
                                    <div className="col-2">
                                      <i className="fa fa-money-bill-alt"></i>
                                    </div>
                                    <div className="col text-left">
                                      <h5 className="card-title  text-muted mb-0">
                                        Mức lương
                                      </h5>
                                      <span className="h4 font-weight-bold mb-0">
                                        <NumberFormat
                                          value={detailWork.salary}
                                          displayType={"text"}
                                          thousandSeparator={true}
                                          renderText={(value, props) => (
                                            <div {...props}>
                                              {value
                                                ? value + " VNĐ"
                                                : "Thương lượng"}
                                            </div>
                                          )}
                                        />
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6 col-md-4 mt-3">
                              <div className="">
                                <div className="  text-center justify-content-center">
                                  <div className="row">
                                    <div className="col-2">
                                      <i className="fa 	fa-map-marked-alt"></i>
                                    </div>
                                    <div className="col text-left">
                                      <h5 className="card-title  text-muted mb-0">
                                        Nơi làm việc
                                      </h5>
                                      <span className="h4 font-weight-bold mb-0">
                                        {
                                          DataAddress[
                                            detailWork.province_or_city
                                          ][1]
                                        }
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6 col-md-4 mt-3">
                              <div className="">
                                <div className="  text-center justify-content-center">
                                  <div className="row">
                                    <div className="col-2">
                                      <i className="fa fa-receipt"></i>
                                    </div>
                                    <div className="col text-left">
                                      <h5 className="card-title  text-muted mb-0">
                                        Hình thức làm việc
                                      </h5>
                                      <span className="h4 font-weight-bold mb-0">
                                        {typeJobDetail}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6 col-md-4 mt-3">
                              <div className="">
                                <div className="  text-center justify-content-center">
                                  <div className="row">
                                    <div className="col-2">
                                      <i className="fa fa-rocket"></i>
                                    </div>
                                    <div className="col text-left">
                                      <h5 className="card-title  text-muted mb-0">
                                        Lĩnh vực
                                      </h5>
                                      <span className="h4 font-weight-bold mb-0">
                                        {category}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6 col-md-4 mt-3">
                              <div className="">
                                <div className="  text-center justify-content-center">
                                  <div className="row">
                                    <div className="col-2">
                                      <i className="fa fa-users"></i>
                                    </div>
                                    <div className="col text-left">
                                      <h5 className="card-title  text-muted mb-0">
                                        Số lượng
                                      </h5>
                                      <span className="h4 font-weight-bold mb-0">
                                        {detailWork.total}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div className="col-6 col-md-4 mt-3">
                              <div className="">
                                <div className="  text-center justify-content-center">
                                  <div className="row">
                                    <div className="col-2">
                                      <i className="fa fa-hourglass-end"></i>
                                    </div>
                                    <div className="col text-left">
                                      <h5 className="card-title  text-muted mb-0">
                                        Hạn nộp hồ sơ
                                      </h5>
                                      <span className="h4 font-weight-bold mb-0">
                                        {FormatDate(detailWork.end)}
                                      </span>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </>
                    </div>
                    <div className="card-header">
                      <div className="row align-items-center">
                        <div className="col-8">
                          <h3 className="mb-0"> Phúc lợi </h3>
                        </div>
                      </div>
                    </div>
                    <div className="card-body">
                      <p className="description-text">{detailWork.benefit}</p>
                    </div>
                    <div className="card-header">
                      <div className="row align-items-center">
                        <div className="col-8">
                          <h3 className="mb-0">Mô tả công việc </h3>
                        </div>
                      </div>
                    </div>
                    <div className="card-body">
                      <p className="description-text">
                        {detailWork.description}
                      </p>
                    </div>
                    <div className="card-header">
                      <div className="row align-items-center">
                        <div className="col-8">
                          <h3 className="mb-0">Yêu cầu công việc </h3>
                        </div>
                      </div>
                    </div>
                    <div className="card-body">
                      <p className="description-text">{detailWork.request}</p>
                    </div>
                  </div>
                </div>
                <div className="col-xl-3 order-xl-2">
                  <div className="card">
                    <div className="card-header">
                      <div className="row align-items-center">
                        <div className="col-8">
                          <h3 className="mb-0">Liên Hệ </h3>
                        </div>
                      </div>
                    </div>
                    <div className="card-body pt-2">
                      {/* <ul className="list-group">
                        <li className="list-group-item border-0 ps-0 pt-0 text-sm">
                          <strong className="text-dark">Người liên hệ:</strong>{" "}
                          {detailWork.name_contact}
                        </li>
                        <li className="list-group-item border-0 ps-0 text-sm">
                          <strong className="text-dark"> Số điện thoại:</strong>{" "}
                          {detailWork.phone_contact}
                        </li>
                        <li className="list-group-item border-0 ps-0 text-sm">
                          <strong className="text-dark">Email:</strong>
                          {detailWork.email_contact}
                        </li>

                        <li className="list-group-item border-0 ps-0 text-sm">
                          <strong className="text-dark">
                            Địa chỉ làm việc:
                          </strong>{" "}
                          {address_company}
                        </li>
                      </ul> */}
                      <div class="d-flex align-items-start flex-column justify-content-center mb-2">
                        <h5 class="mb-0 text-sm">Người liên hệ:</h5>
                        <p class="mb-0 text-sm">{detailWork.name_contact}</p>
                      </div>
                      <div class="d-flex align-items-start flex-column justify-content-center  mb-2">
                        <h5 class="mb-0 text-sm">Số điện thoại:</h5>
                        <p class="mb-0 text-sm">{detailWork.phone_contact}</p>
                      </div>
                      <div class="d-flex align-items-start flex-column justify-content-center  mb-2">
                        <h5 class="mb-0 text-sm">Email:</h5>
                        <p class="mb-0 text-sm">{detailWork.email_contact}</p>
                      </div>
                      <div class="d-flex align-items-start flex-column justify-content-center">
                        <h5 class="mb-0 text-sm">Địa chỉ làm việc:</h5>
                        <p class="mb-0 text-sm">{address_company}</p>
                      </div>
                      {(!info_login ||
                        (info_login && info_login.type === 2)) && (
                        <div className="d-flex justify-content-center mt-3">
                          <ModalCV
                            loading={loading}
                            setLoading={setLoading}
                            getListCV={getListCV}
                            is_login={is_login}
                            listCV={listCV}
                            applyJob={applyJob}
                            setCvActive={setCvActive}
                          />
                        </div>
                      )}
                    </div>
                  </div>
                </div>
              </div>
              <NotificationContainer />
            </div>
          </div>
        ) : (
          <div className="m-5">
            <div className="d-flex justify-content-center">
              <div style={{ width: "70%", textAlign: "center" }}>
                <img
                  src="/assets/img/page_not_found.svg"
                  alt="not found"
                  width="70%"
                />
                <div className="text-center mt-5 display-4">
                  Việc làm không tồn tại! <br></br>
                </div>
                <div className="text-center mt-5">
                  <Link to="/" className="display-2">
                    Quay lại trang chủ.
                  </Link>
                </div>
              </div>
            </div>
          </div>
        )
      ) : (
        <LoaderPageJob />
      )}
    </div>
  );
};
export default ViecLam;
