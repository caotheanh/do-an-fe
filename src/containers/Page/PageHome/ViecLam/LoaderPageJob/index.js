import React from "react";
import ContentLoader from "react-content-loader";

const LoaderPageJob = (props) => (
  <div className="container">
    <ContentLoader
      speed={2}
      width={"100%"}
      viewBox="0 0 502 200"
      backgroundColor="#f3f3f3"
      foregroundColor="#ecebeb"
      {...props}
    >
      <rect x="8" y="7" rx="0" ry="0" width="50" height="50" />
      <rect x="74" y="8" rx="0" ry="0" width="26" height="5" />
      <rect x="74" y="16" rx="0" ry="0" width="67" height="5" />
      <rect x="9" y="95" rx="0" ry="0" width="380" height="260" />
      <rect x="476" y="218" rx="0" ry="0" width="47" height="0" />
      <rect x="397" y="94" rx="0" ry="0" width="187" height="218" />
      <rect x="518" y="79" rx="0" ry="0" width="56" height="9" />
    </ContentLoader>
  </div>
);

export default LoaderPageJob;
