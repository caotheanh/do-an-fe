import axios from "axios";
import React from "react";
import { useForm } from "react-hook-form";
import { NotificationManager } from "react-notifications";
import { useDispatch } from "react-redux";
import { api } from "../../../../../api";
const ModalLogin = ({ getListCV }) => {
  const { register, handleSubmit } = useForm();
  const dispatch = useDispatch();
  const login = async (data) => {
    try {
      const result_login = await axios.post(`${api}/login`, data);
      result_login.data.token
        ? NotificationManager.success(result_login.data.messages, "", 3000)
        : NotificationManager.error(result_login.data.messages, "", 3000);
      const { token } = result_login.data;

      if (token) {
        const userInfo = await axios({
          method: "get",
          url: `${api}/user-infomation/${token}`,
        });
        localStorage.setItem("token", JSON.stringify(token));
        getListCV(token);
        dispatch({ type: "SET_IS_LOGIN", is_login: true });
        dispatch({ type: "SET_INFO_USER", info_user: userInfo.data.data });
      }
    } catch (error) {}
  };
  return (
    <div className="modal-content">
      <div className="modal-header">
        <h5 className="modal-title" id="exampleModalLabel">
          Đăng nhập để tiếp tục ứng tuyển.
        </h5>
        <button
          type="button"
          className="close"
          data-dismiss="modal"
          aria-label="Close"
        >
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div className="modal-body">
        <form onSubmit={handleSubmit(login)}>
          <div className="form-group mb-3">
            <div className="input-group input-group-merge input-group-alternative">
              <div className="input-group-prepend">
                <span className="input-group-text">
                  <i className="ni ni-email-83"></i>
                </span>
              </div>
              <input
                name="email"
                ref={register({ required: true })}
                className="form-control"
                placeholder="Email"
                type="email"
              />
            </div>
          </div>
          <div className="form-group">
            <div className="input-group input-group-merge input-group-alternative">
              <div className="input-group-prepend">
                <span className="input-group-text">
                  <i className="ni ni-lock-circle-open"></i>
                </span>
              </div>
              <input
                className="form-control"
                placeholder="Password"
                type="password"
                name="password"
                ref={register({ required: true })}
              />
            </div>
          </div>
          <div className="text-center">
            <button
              type="button"
              data-dismiss="modal"
              className="btn btn-outline-default my-4"
            >
              Hủy
            </button>
            <button type="submit" className="btn btn-primary my-4">
              Đăng nhập
            </button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default ModalLogin;
