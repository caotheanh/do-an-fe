import React from "react";
import CardCV from "../CardCV";
import { api } from "../../../../../api";
import { useSelector } from "react-redux";
import ModalLogin from "../ModalLogin";
import { Link } from "react-router-dom";
const ModalCV = ({
  applyJob,
  listCV,
  getListCV,
  setCvActive,
  loading,
  setLoading,
}) => {
  const is_login = useSelector((state) => state.is_login);
  return (
    <>
      <div>
        <button
          type="button"
          className="btn btn-outline-primary"
          data-toggle="modal"
          data-target="#aplly"
        >
          Ứng tuyển ngay
        </button>
      </div>

      <div
        className="modal fade show"
        id="aplly"
        role="dialog"
        aria-labelledby="exampleModalLabel"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-dialog-centered" role="document">
          {is_login ? (
            <div className="modal-content">
              <div className="modal-header">
                <h3>Chọn CV ứng tuyển</h3>
                <button
                  type="button"
                  className="close"
                  data-dismiss="modal"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="ml-3 mr-3">
                {listCV && Array.isArray(listCV) && listCV.length > 0
                  ? listCV.map((cv) => (
                      <CardCV cv={cv} api={api} setCvActive={setCvActive} />
                    ))
                  : "Bạn đang không có CV nào. Hãy thêm CV để tiếp tục ứng tuyển"}
              </div>
              <div className="modal-footer">
                <button
                  type="button"
                  className="btn btn-secondary huy"
                  data-dismiss="modal"
                >
                  Hủy
                </button>
                {listCV && listCV.length > 0 ? (
                  <button
                    onClick={() => {
                      applyJob();
                      setLoading(true);
                    }}
                    type="button"
                    className="btn btn-outline-default pl-5 pr-5"
                  >
                    {!loading ? (
                      "Ứng tuyển"
                    ) : (
                      <>
                        <span className="btn-inner--icon">
                          <i className="fa fa-spinner fa-spin"></i>
                        </span>
                        <span className="btn-inner--text pl-3">Đang gửi</span>
                      </>
                    )}
                  </button>
                ) : (
                  <Link
                    to="/cv"
                    className="btn btn-secondary huy"
                    data-dismiss="modal"
                  >
                    Tải lên CV
                  </Link>
                )}
              </div>
            </div>
          ) : (
            <ModalLogin getListCV={getListCV} />
          )}
        </div>
      </div>
    </>
  );
};

export default ModalCV;
