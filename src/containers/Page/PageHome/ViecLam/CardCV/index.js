import React from "react";
import ModalPreviewPDF from "../../../../../components/ComponentCV/CardCV/ModalPreviewPDF";
import FormatDate from "../../../../../utils/FormatDate";

const CardCV = ({ cv, api, setCvActive }) => {
  return (
    <>
      <label
        className="btn-icon-clipboard"
        onClick={() => setCvActive(cv.link)}
        htmlFor={`customRadio1_${cv.id}`}
      >
        <div className="row">
          <div className="col-auto">
            <div className="custom-control custom-radio mb-3">
              <input
                type="radio"
                id={`customRadio1_${cv.id}`}
                name="customRadio"
                className="custom-control-input ml-3"
              />
              <label className="custom-control-label"></label>
            </div>
          </div>
          <div className="col-lg">
            <div className="row  mb-1">
              <div className="col-lg-6">
                <h4 className="mb-0">{cv.name}</h4>
              </div>
              <div className="col-lg-6 text-right  ml-lg-auto">
                {FormatDate(cv.updated_at)}
              </div>
            </div>
            <div className="btn-wrapper text-center row">
              <div className="input-group">
                <input
                  className="form-control col-lg-auto"
                  value={api + "/cv/" + cv.link}
                  type="text"
                  name="linkCV"
                />{" "}
                <div className="input-group-append">
                  <button
                    id="basic-addon2"
                    className="input-group-text btn btn-sm btn-icon"
                    data-toggle="modal"
                    data-target={`#previewPDF${cv.id}`}
                  >
                    <i className="fa fa-eye" style={{ fontSize: "15px" }}></i>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </label>
      <ModalPreviewPDF linkCV={cv.link} id={cv.id} />
    </>
  );
};

export default CardCV;
