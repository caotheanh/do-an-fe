import axios from "axios";
import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { gettoken } from "../../../../utils/gettoken";
import { api } from "../../../../api";
import { useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom/cjs/react-router-dom.min";
import FormatNumber from "../../../../utils/FormatNumber";
import FormatDate from "../../../../utils/FormatDate";
import StatusApply from "../../../../utils/StatusApply";

function ChiTietUngTuyen() {
  const [detailApply, setdetailApply] = useState({});
  const [noteApply, setnoteApply] = useState([]);
  // const { isLoading, setIsLoading } = useState(true);
  const token = gettoken();
  const { id } = useParams();
  useEffect(() => {
    axios
      .get(`${api}/apply/${id}/${token}`)
      .then(function (response) {
        setdetailApply(response.data[0]);
      })
      .catch(function (error) {
        if (error.response) {
        }
      });
    axios
      .get(`${api}/note-apply/${id}`)
      .then(function (response) {
        setnoteApply(response.data);
        // setIsLoading(false);
      })
      .catch(function (error) {
        if (error.response) {
        }
      });
  }, [id, token]);
  // const getJobApply = (token) => {
  //   axios
  //     .get(`${api}/apply/get-by-user/${token}`)
  //     .then(function (response) {
  //       setJobApply(response.data);
  //     })
  //
  // };
  const is_login = useSelector((state) => state.is_login);
  return (
    <>
      <Helmet>
        <title>Việc Làm Đã ứng tuyển</title>
      </Helmet>
      {is_login ? (
        <div className="container">
          <div className="m-3">
            <div className="row">
              <div className="col-xl-8 order-xl-1">
                <div className="card">
                  <div className="card-header">
                    <div className="row align-items-center">
                      <div className="col-8">
                        <h3 className="mb-0">Chi tiết việc làm đã ứng tuyển</h3>
                      </div>
                    </div>
                  </div>
                  <div
                    className="card-body"
                    style={{ overflow: "auto", height: "70vh" }}
                  >
                    <div className="timeline timeline-one-side">
                      <div className="timeline-block mb-3">
                        <span className="timeline-step">
                          <i className="ni ni-bell-55 text-success text-gradient"></i>
                        </span>
                        <div className="timeline-content">
                          <h6 className="text-dark text-sm font-weight-bold mb-0">
                            Nộp CV ứng tuyển
                          </h6>
                          <p className=" font-weight-bold text-xs mt-1 mb-0">
                            {FormatDate(detailApply.created_at)}
                          </p>
                        </div>
                      </div>
                      {noteApply &&
                        Array.isArray(noteApply) &&
                        noteApply.map((item) => (
                          <div className="timeline-block mb-3" key={item.id}>
                            <span className="timeline-step">
                              <i
                                className={`${
                                  StatusApply.find(
                                    (status) => status.id === item.status
                                  ).icon
                                } ${
                                  StatusApply.find(
                                    (status) => status.id === item.status
                                  ).color
                                } text-gradient`}
                              ></i>
                            </span>
                            <div className="timeline-content">
                              <h3>
                                {
                                  StatusApply.find(
                                    (status) => status.id === item.status
                                  ).textUser
                                }
                              </h3>
                              <h6 className="text-dark text-sm mb-0">
                                Chú thích: {item.content}
                              </h6>
                              <p className=" font-weight-bold text-xs mt-1 mb-0">
                                {FormatDate(item.created_at)}
                              </p>
                            </div>
                          </div>
                        ))}
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xl-4 order-xl-2">
                <div className="card">
                  <div className="card-header">
                    <div className="row align-items-center">
                      <div className="col-8">
                        <h3 className="mb-0">Việc làm </h3>
                      </div>
                    </div>
                  </div>
                  <div className="card-body pt-2">
                    <div
                      style={{
                        background: ` url(${api}/${
                          detailApply &&
                          detailApply.recruitment &&
                          detailApply.recruitment.img
                        }) 50% 50% no-repeat`,
                        width: "100%",
                        height: "150px",
                        borderRadius: "10px",
                        backgroundSize: "cover",
                        border: "4px solid #fff",
                        // backgroundPosition: "top center",
                      }}
                      className="imageBackground"
                    ></div>
                    <ul className="list-group">
                      <li className="list-group-item border-0 ps-0 pt-0 text-sm">
                        <Link
                          target="_blank"

                          to={`/viec-lam/${
                            detailApply &&
                            detailApply.recruitment &&
                            detailApply.recruitment.id
                          }`}
                        className=" text-center"
                        >
                          <p className="h3">
                            {detailApply &&
                              detailApply.recruitment &&
                              detailApply.recruitment.title}
                          </p>
                        </Link>
                      </li>
                      <li className="list-group-item border-0 ps-0 text-sm">
                        <strong className="text-dark">Lương :</strong> &nbsp;{" "}
                        {isNaN(
                          detailApply &&
                            detailApply.recruitment &&
                            detailApply.recruitment.salary
                        )
                          ? "Thương lượng"
                          : FormatNumber(
                              Number(
                                detailApply &&
                                  detailApply.recruitment &&
                                  detailApply.recruitment.salary
                              )
                            )}
                      </li>
                      <li className="list-group-item border-0 ps-0 text-sm">
                        <strong className="text-dark">Số lượng :</strong> &nbsp;{" "}
                        {detailApply &&
                          detailApply.recruitment &&
                          detailApply.recruitment.total}{" "}
                        Người
                      </li>
                    </ul>
                  </div>
                </div>
                <div className="card">
                  <div className="card-header">
                    <div className="row align-items-center">
                      <div className="col-8">
                        <h3 className="mb-0">Thông tin liên hệ</h3>
                      </div>
                    </div>
                  </div>
                  <div className="card-body pt-2">
                    <ul className="list-group">
                      <li className="list-group-item border-0 ps-0 text-sm">
                        <strong className="text-dark">
                          Tên người liên hệ:
                        </strong>{" "}
                        &nbsp;
                        {detailApply &&
                          detailApply.recruitment &&
                          detailApply.recruitment.name_contact}
                      </li>
                      <li className="list-group-item border-0 ps-0 text-sm">
                        <strong className="text-dark">Email:</strong> &nbsp;
                        {detailApply &&
                          detailApply.recruitment &&
                          detailApply.recruitment.email_contact}
                      </li>
                      <li className="list-group-item border-0 ps-0 text-sm">
                        <strong className="text-dark">Điện thoại:</strong>{" "}
                        &nbsp;
                        {detailApply &&
                          detailApply.recruitment &&
                          detailApply.recruitment.phone_contact}
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      ) : (
        <div className="m-5">
          <div className="d-flex justify-content-center">
            <div style={{ width: "70%", textAlign: "center" }}>
              <img
                src="/assets/img/page_not_found.svg"
                alt="not found"
                width="70%"
              />
              <div className="text-center mt-5 display-3">
                Vui lòng đăng nhập.
              </div>
            </div>
          </div>
        </div>
      )}
    </>
  );
}

export default ChiTietUngTuyen;
