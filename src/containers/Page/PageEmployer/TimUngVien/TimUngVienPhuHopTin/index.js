import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link, useParams } from "react-router-dom";
import { api } from "../../../../../api";
import FormatNumber from "../../../../../utils/FormatNumber";
function TimUngVienPhuHopTin() {
  const { id } = useParams();
  const [dataUngVien, setDataUngVien] = useState();
  const [detailRecruiment, setdDetailRecruiment] = useState("");
  const [image, setImage] = useState("");

  useEffect(() => {
    axios.get(`${api}/recruitment/${id}`).then((res) => {
      setdDetailRecruiment(res.data[0]);
      setImage(`${api}/${res.data[0].img}`);
      const { id_category, type } = res.data[0];
      getUserFinJob(id_category, type);
    });
  }, [id]);
  const getUserFinJob = async (id_category, type) => {
    const data = { id_category: id_category, type: type };
    axios
      .post(`${api}/user-infomation/search-user`, data)
      .then((res) => setDataUngVien(res.data));
  };
  return (
    <div className="m-3">
      <div className="row">
        <div className="col-xl-4 order-xl-1">
          <div className="card">
            <div className="card-header">
              <div className="row align-items-center">
                <div className="col-8">
                  <h3 className="mb-0">Tìm ứng viên phù hợp</h3>
                </div>
              </div>
            </div>
            <div className="card-body">
              <div className="col-xl-12">
                <div className="card card-blog card-plain">
                  <div className="position-relative">
                    <div className="d-block shadow-xl border-radius-xl">
                      <img
                        src={image}
                        style={{ width: "100%" }}
                        alt="img-blur-shadow"
                        className="img-fluid shadow border-radius-xl"
                      />
                    </div>
                  </div>
                  <div className="card-body px-1 pb-0">
                    <p className="text-gradient text-dark mb-2 text-sm">
                      {detailRecruiment.company}
                    </p>
                    <p >
                      <h3>{detailRecruiment.title}</h3>
                    </p>
                    {/* <p className="mb-4 text-sm">
                      As Uber works through a huge amount of internal management
                      turmoil.
                    </p> */}
                    <div className="d-flex align-items-center justify-content-between mt-5">
                      <Link
                        target="_blank"
                        to={`/viec-lam/${detailRecruiment.id}`}
                        className="btn btn-outline-primary "
                      >
                        Xem bài viết
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="col-xl-8 order-xl-2">
          <div className="card">
            <div className="card-header">
              <div className="row align-items-center">
                <div className="col-8">
                  <h3 className="mb-0">
                    Kết quả tìm kiếm :{" "}
                    {dataUngVien && dataUngVien.length > 0
                      ? `Tìm thấy ${dataUngVien.length} ứng viên phù hợp`
                      : "Không tìm thấy ứng viên nào phù hợp"}
                  </h3>
                </div>
              </div>
            </div>
            {dataUngVien && dataUngVien.length === 0 && (
              <div className="card-body d-flex justify-content-center">
                <img
                  alt="khong co"
                  src="/assets/img/khong-co.svg"
                  className="text-center"
                  style={{ width: "20rem", margin: "auto" }}
                />
              </div>
            )}
            <ul className="list-group">
              {dataUngVien &&
                dataUngVien.length > 0 &&
                dataUngVien.map((item) => (
                  <li
                    key={item.id}
                    className="list-group-item border-0 d-flex p-4 mb-2 bg-gray-100 border-radius-lg"
                  >
                    <div>
                      <div
                        style={
                          item &&
                          item.email &&
                          item.email.img && {
                            background: ` url(${api}/avatar/${
                              item && item.email && item.email.img
                            }) 50% 50% no-repeat`,
                            width: "120px",
                            height: "120px",
                            borderRadius: "10px",
                            backgroundSize: "cover",
                            border: "4px solid #fff",
                            // backgroundPosition: "top center",
                          }
                        }
                        className="imageBackground"
                      ></div>
                    </div>
                    <div className="d-flex flex-column pl-3">
                      <h2>{item && item.email && item.email.name}</h2>
                      <Link
                        to={`/employer/${id}/ung-vien/chitiet/${item.id_user}`}
                        className="h6 text-sm"
                      >
                        {item.name}
                      </Link>
                      <span className="mb-2 text-xs">
                        Email:{" "}
                        <span className="text-dark ms-2 font-weight-bold">
                          {item && item.email && item.email.email}
                        </span>
                      </span>
                      <span className="mb-2 text-xs">
                        Luơng mong muốn:
                        <span className="text-dark font-weight-bold ms-2">
                          {FormatNumber(item.salary_1) +
                            " - " +
                            FormatNumber(item.salary_2)}
                        </span>
                      </span>
                      <span className="mb-2 text-xs">
                        Công việc mong muốn:{" "}
                        <span className="text-dark ms-2 font-weight-bold">
                          {item.name_job}
                        </span>
                      </span>

                      {/* <span className="text-xs">
                        VAT Number:{" "}
                        <span className="text-dark ms-2 font-weight-bold">
                          FRB1235476
                        </span>
                      </span> */}
                    </div>
                    <div className="ms-auto">
                      <Link
                        className="btn  btn-outline-default px-3 mb-0"
                        to={`/employer/${id}/ung-vien/chitiet/${item.id_user}`}
                      >
                        <i className="far fa-eye me-2"></i>Xem
                      </Link>
                    </div>
                  </li>
                ))}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

export default TimUngVienPhuHopTin;
