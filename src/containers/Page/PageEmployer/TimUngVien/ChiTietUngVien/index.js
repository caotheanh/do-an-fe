import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import PreviewCV from "../../../../../components/ComponentCV/PreviewCV";
import { api } from "../../../../../api";
import { getAddress } from "../../../../../utils/getAddress";
import { useForm } from "react-hook-form";
import NotificationManager from "react-notifications/lib/NotificationManager";
import FormatDate from "../../../../../utils/FormatDate";
import FormatNumber from "../../../../../utils/FormatNumber";
import { gettoken } from "../../../../../utils/gettoken";
import { Link } from "react-router-dom";
function ChiTietUngVien() {
  const { id_ung_vien, tinTD } = useParams();
  const [dataDefaultUser, setdataDefaultUser] = useState({});
  const [address, setAddress] = useState("");
  const [job, setJob] = useState([]);
  const token = gettoken();
  useEffect(() => {
    axios
      .get(`${api}/user-infomation/view-result-user/${id_ung_vien}`)
      .then((res) => {
        setdataDefaultUser(res.data);
        if (res.data.infomation) {
          const {
            province_or_city,
            district,
            ward,
            details_address,
          } = res.data.infomation;
          setAddress(
            getAddress(province_or_city, district, ward, details_address)
          );
        }
      });
    if (!tinTD) {
      axios
        .get(`${api}/recruitment/get-by-user/${token}`)
        .then((res) => setJob(res.data));
    }
  }, [id_ung_vien, tinTD, token]);
  const { handleSubmit, register } = useForm();
  const MoiUngTuyen = (data) => {
    const data1 = {
      id_user: id_ung_vien,
      id_recruitment: tinTD || data.id_recruitment,
      text: data.text,
    };
    axios.post(`${api}/invite`, data1).then(() => {
      document.querySelector(".closeModalMoiUngTuyen").click();
      NotificationManager.success(
        "Gửi yêu cầu liên hệ thành công.",
        "Thành công",
        3000
      );
    });
  };
  const [isShow, setIsShow] = useState({
    study: false,
    work: false,
    exp: false,
  });
  return (
    <div className="row m-3">
      <div className="col-xl-3 order-xl-2">
        <div className="card card-profile">
          <div className="card-header">
            <Link
              className="h6 heading-small text-muted  mb--2"
              onClick={() => setIsShow({ ...isShow, study: !isShow.study })}
            >
              Học vấn
              <i
                className={`fa ${!isShow.study ? "fa-eye" : "fa-eye-slash"}`}
              ></i>
            </Link>
          </div>
          {isShow.study && (
            <div className="timeline timeline-one-side m-3">
              {dataDefaultUser &&
                dataDefaultUser.study &&
                Array.isArray(dataDefaultUser.study) &&
                dataDefaultUser.study.map((item) => (
                  <div className="timeline-block mb-3">
                    <span className="timeline-step">
                      <i className="fa fa-graduation-cap text-success text-gradient"></i>
                    </span>
                    <div className="timeline-content">
                      <h6 className="text-dark text-sm font-weight-bold mb-0">
                        {item.school}
                      </h6>
                      <p className="text-drank font-weight-bold text-xs mt-1 mb-0">
                        {FormatDate(item.start) + " - " + FormatDate(item.end)}
                      </p>
                    </div>
                  </div>
                ))}
            </div>
          )}
          <div className="card-header">
            <Link
              className="h6 heading-small text-muted  mb--2"
              onClick={() => setIsShow({ ...isShow, exp: !isShow.exp })}
            >
              Kinh nghiệm làm việc
              <i className={`fa ${!isShow.exp ? "fa-eye" : "fa-eye-slash"}`}>
                {" "}
              </i>
            </Link>
          </div>
          {isShow.exp && (
            <div className="timeline timeline-one-side m-3">
              {dataDefaultUser &&
                dataDefaultUser.exp &&
                Array.isArray(dataDefaultUser.exp) &&
                dataDefaultUser.exp.map((item) => (
                  <div className="timeline-block mb-3">
                    <span className="timeline-step">
                      <i className="fa fa-search-plus text-success text-gradient"></i>
                    </span>
                    <div className="timeline-content">
                      <h6 className="text-dark text-sm font-weight-bold mb-0">
                        {item.company}
                      </h6>
                      <p className="text-drank font-weight-bold text-xs mt-1 mb-0">
                        {FormatDate(item.start) + " - " + FormatDate(item.end)}
                      </p>
                    </div>
                  </div>
                ))}
            </div>
          )}
          <div className="card-header">
            <Link
              className="heading-small text-muted  mb--2"
              onClick={() => setIsShow({ ...isShow, work: !isShow.work })}
            >
              Công việc mong muốn{" "}
              <i
                className={`fa ${!isShow.work ? "fa-eye" : "fa-eye-slash"}`}
              ></i>
            </Link>
          </div>
          {isShow.work && (
            <div className="timeline timeline-one-side m-3">
              {dataDefaultUser &&
                dataDefaultUser.work &&
                dataDefaultUser.work.length > 0 &&
                dataDefaultUser.work.map((item) => (
                  <div className="timeline-block mb-3">
                    <span className="timeline-step">
                      <i className="fa fa-search-plus text-success text-gradient"></i>
                    </span>
                    <div className="timeline-content">
                      <h6 className="text-dark text-sm font-weight-bold mb-0">
                        {item.name_job}
                      </h6>
                      <p className="text-drank font-weight-bold text-xs mt-1 mb-0">
                        {FormatNumber(item.salary_1) +
                          " - " +
                          FormatNumber(item.salary_2)}
                      </p>
                    </div>
                  </div>
                ))}
            </div>
          )}
          <div className="card-header">
            <h6 className="heading-small text-muted">Hoạt động</h6>
          </div>
          <div className="card-body pt-0">
            <div className="d-flex justify-content-lg-center">
              <button
                className="btn btn-outline-default m-3"
                data-toggle="modal"
                data-target="#exampleModal"
              >
                Gửi yêu cầu liên hệ
              </button>
              <div
                className="modal fade"
                id="exampleModal"
                tabIndex="-1"
                role="dialog"
                aria-labelledby="exampleModalLabel"
                aria-hidden="true"
              >
                <div
                  className="modal-dialog modal-dialog-centered"
                  role="document"
                >
                  <form
                    className="modal-content"
                    onSubmit={handleSubmit(MoiUngTuyen)}
                  >
                    <div className="modal-header">
                      <h5 className="modal-title" id="exampleModalLabel">
                        Gửi yêu cầu liên hệ
                      </h5>
                      <button
                        type="button"
                        className="close"
                        data-dismiss="modal"
                        aria-label="Close"
                      >
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div className="modal-body">
                      {!tinTD && (
                        <div className="form-group">
                          <label
                            htmlFor="example-search-input"
                            className="form-control-label"
                          >
                            Chọn tin tuyển dụng
                          </label>
                          <select
                            name="id_recruitment"
                            ref={register({ required: true })}
                            className="form-control"
                          >
                            <option value="">Chọn tin tuyển dụng</option>
                            {job &&
                              Array.isArray(job) &&
                              job.map((item) => (
                                <option value={item.id}>{item.title}</option>
                              ))}
                          </select>
                        </div>
                      )}
                      <div className="form-group">
                        <label
                          htmlFor="example-search-input"
                          className="form-control-label"
                        >
                          Lời Mời ứng tuyển
                        </label>
                        <textarea
                          name="text"
                          ref={register({ required: true })}
                          className="form-control"
                          type="search"
                          id="example-search-input"
                        ></textarea>
                      </div>
                    </div>
                    <div className="modal-footer">
                      <button
                        type="button"
                        className="btn btn-secondary closeModalMoiUngTuyen"
                        data-dismiss="modal"
                      >
                        Hủy
                      </button>
                      <button type="submit" className="btn btn-primary">
                        Gửi
                      </button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-xl-9 order-xl-1">
        <div className="card">
          <div className="card-header">
            <div className="row align-items-center">
              <div className="col-8">
                <h3 className="mb-0">Chi tiết ứng viên </h3>
              </div>
            </div>
          </div>
          <div className="card-body">
            <h6 className="heading-small text-muted mb-4">Thông tin cơ bản</h6>
            <div className="row">
              <div className="col-lg-6">
                <p className="mb-0">
                  <span className="h3 ">Họ và tên </span> :
                  {dataDefaultUser &&
                    dataDefaultUser.infomation &&
                    dataDefaultUser.infomation.name}
                </p>
                <p className="mb-0">
                  <span className="h3">Ngày sinh</span> :
                  {(dataDefaultUser &&
                    dataDefaultUser.infomation &&
                    FormatDate(dataDefaultUser.infomation.birth_date)) ||
                    ""}
                </p>
                <p className="mb-0">
                  <span className="h3">Website </span> :
                  <a
                    target="_blank"
                    rel="noreferrer"
                    href={`${
                      dataDefaultUser &&
                      dataDefaultUser.infomation &&
                      dataDefaultUser.infomation.website
                    }`}
                  >
                    {(dataDefaultUser &&
                      dataDefaultUser.infomation &&
                      dataDefaultUser.infomation.website) ||
                      ""}
                  </a>
                </p>
              </div>
              <div className="col-lg-6">
                <p className="mb-0">
                  <span className="h3 ">Số điện thoại </span> :
                  {dataDefaultUser &&
                    dataDefaultUser.infomation &&
                    dataDefaultUser.infomation.phone}
                </p>
                <p className="mb-0">
                  <span className="h3">Địa chỉ </span> :{address}
                </p>
              </div>
            </div>
          </div>

          <div
            className="card-body"
            style={{
              height: "100vh",
            }}
          >
            <h6 className="heading-small text-muted mb-4">Xem CV</h6>
            <PreviewCV
              cv={
                dataDefaultUser &&
                dataDefaultUser.infomation &&
                dataDefaultUser.infomation.cv
              }
            />
          </div>
        </div>
      </div>
    </div>
  );
}

export default ChiTietUngVien;
