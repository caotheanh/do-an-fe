import axios from "axios";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import { Link } from "react-router-dom";
import { api } from "../../../../../api";
import FormatDate from "../../../../../utils/FormatDate";
import TypeJob from "../../../../../utils/TypeJob";
function KhoUngVien() {
  const { handleSubmit, register } = useForm();
  const [dataUngVien, setDataUngVien] = useState();
  useEffect(() => {
    axios
      .get(`${api}/user-infomation/search-user/all`)
      .then((res) => setDataUngVien(res.data));
  }, []);
  const handleSearchUser = (data) => {
    console.log(data);
    axios
      .post(`${api}/user-infomation/search-user/`, {
        id_category: +data.id_category,
        type: +data.type,
      })
      .then((res) => setDataUngVien(res.data));
  };
  const categories = useSelector((state) => state.categories);
  return (
    <div className="m-3">
      <div className="row">
        <div className="col-xl-4 order-xl-1">
          <div className="card">
            <div className="card-header">
              <div className="row align-items-center">
                <div className="col-8">
                  <h3 className="mb-0">Tìm kiếm ứng viên </h3>
                </div>
              </div>
            </div>
            <div className="card-body">
              <form onSubmit={handleSubmit(handleSearchUser)}>
                <div className="form-group">
                  <label
                    className="form-control-label"
                    htmlFor="input-username"
                  >
                    Loại công việc
                  </label>
                  <select
                    ref={register({ required: true })}
                    name="id_category"
                    className="form-control"
                  >
                    <option value="">Chọn loại công việc</option>
                    {categories &&
                      categories &&
                      Array.isArray(categories) &&
                      categories.length > 0 &&
                      categories.map((cate) => (
                        <option key={cate.id} value={cate.id}>
                          {cate.name}
                        </option>
                      ))}
                  </select>
                </div>
                <div className="form-group">
                  <label
                    className="form-control-label"
                    htmlFor="input-username"
                  >
                    Hình thức làm việc
                  </label>
                  <select
                    ref={register({ required: true })}
                    name="type"
                    className="form-control"
                  >
                    <option value="">Chọn hình thức làm việc</option>
                    {TypeJob &&
                      TypeJob.map((type) => (
                        <option key={type.id} value={type.id}>
                          {type.name}
                        </option>
                      ))}
                  </select>
                </div>
                <button className="btn btn-default">Tìm ứng viên</button>
              </form>
            </div>
          </div>
        </div>
        <div className="col-xl-8 order-xl-2">
          <div className="card">
            <div className="card-header">
              <div className="row align-items-center">
                <div className="col-8">
                  <h3 className="mb-0">
                    Kết quả tìm kiếm :{" "}
                    {dataUngVien &&
                    Array.isArray(dataUngVien) &&
                    dataUngVien.length > 0
                      ? `Tìm thấy ${dataUngVien.length} ứng viên phù hợp`
                      : "Không tìm thấy ứng viên nào phù hợp"}
                  </h3>
                </div>
              </div>
            </div>
            <div className="card-body d-flex justify-content-center">
              {dataUngVien && dataUngVien.length === 0 && (
                <img
                  alt="khong co"
                  src="/assets/img/khong-co.svg"
                  className="text-center"
                  style={{ width: "20rem", margin: "auto" }}
                />
              )}
            </div>
            <ul className="list-group">
              {dataUngVien &&
                Array.isArray(dataUngVien) &&
                dataUngVien.length > 0 &&
                dataUngVien.map((item) => (
                  <li
                    className="list-group-item border-0 d-flex p-2 pl-4 bg-gray-100 border-radius-lg m-2"
                    style={{ background: "#f8fafa", borderRadius: 5 }}
                  >
                    <div className="d-flex flex-column">
                      <Link
                        to={`/employer/ung-vien/chitiet/${item.id_user}`}
                        className="h6 text-sm"
                      >
                        {item.name}
                      </Link>
                      <span className="mb-2 text-xs">
                        website:
                        <span className="text-dark font-weight-bold ms-2">
                          {item.website}
                        </span>
                      </span>
                      <span className="mb-2 text-xs">
                        Ngày sinh:
                        <span className="text-dark ms-2 font-weight-bold">
                          {FormatDate(item.birth_date)}
                        </span>
                      </span>
                      {/* <span className="text-xs">
                        VAT Number:{" "}
                        <span className="text-dark ms-2 font-weight-bold">
                          FRB1235476
                        </span>
                      </span> */}
                    </div>
                    <div className="ms-auto">
                      <Link
                        className="btn btn-outline-default text-gradient px-3 mb-0"
                        to={`/employer/ung-vien/chitiet/${item.id_user}`}
                      >
                        <i className="far fa-eye pr-2"></i>Xem
                      </Link>
                      {/* <a className="btn  btn-outline-info btn-link text-dark px-3 mb-0" href="/#">
                        <i
                          className="fa fa-save  text-dark me-2"
                          aria-hidden="true"
                        ></i>
                        Lưu
                      </a> */}
                    </div>
                  </li>
                ))}
            </ul>
          </div>
        </div>
      </div>
    </div>
  );
}

export default KhoUngVien;
