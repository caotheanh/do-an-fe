import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { api } from "../../../../../api";
import FormatDate from "../../../../../utils/FormatDate";
const MoiUngTuyen = () => {
  const { id } = useParams();
  const [infoRecruiment, setinfoRecruiment] = useState([]);
  useEffect(() => {
    axios.get(`${api}/invite/get-by-recruiment/${id}`).then((response) => {
      setinfoRecruiment(response.data);
    });
  }, [id]);
  return (
    <>
      <div>
        <div className="header bg-primary pb-6">
          <div className="container-fluid">
            <div className="header-body">
              <div className="row align-items-center py-4">
                <div className="col-lg-6 col-7"></div>
                <div className="col-lg-6 col-5 text-right"></div>
              </div>
            </div>
          </div>
        </div>
        <div className="container-fluid mt--6">
          <div className="row">
            <div className="col">
              <div className="card">
                <div className="card-header border-0">
                  <h3 className="mb-0">Danh sách ứng viên ứng tuyển </h3>
                </div>
                <div className="table-responsive">
                  <table className="table align-items-center table-flush">
                    <thead className="thead-light">
                      <tr>
                        <th scope="col">Ngày ứng tuyển</th>
                        <th scope="col" className="sort" data-sort="completion">
                          Trạng thái
                        </th>
                      </tr>
                    </thead>
                    <tbody className="list" style={{minHeight:"60vh"}}>
                      {infoRecruiment &&
                        Array.isArray(infoRecruiment) &&
                        infoRecruiment.map((item, index) => (
                          <tr key={index}>
                            <td>
                              <div className="d-flex align-items-center">
                                <span className="completion mr-2">
                                  {FormatDate(item.created_at)}
                                </span>
                              </div>
                            </td>
                            <td>
                              <span className=" badge-dot mr-4">
                                {
                                  [
                                    <i className="bg-danger"></i>,
                                    <i className="bg-info"></i>,
                                    <i className="bg-success"></i>,
                                  ][item && item.status]
                                }
                                <span className="status">
                                  {
                                    ["Đã từ chối", "Đang chờ", "Đã đồng ý"][
                                      item && item.status
                                    ]
                                  }
                                </span>
                              </span>
                            </td>
                          </tr>
                        ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default MoiUngTuyen;
