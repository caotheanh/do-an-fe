import React from "react";

function DaXoa() {
  return (
    <div>
      <div className="header bg-primary pb-6">
        <div className="container-fluid">
          <div className="header-body">
            <div className="row align-items-center py-4">
              <div className="col-lg-6 col-7"></div>
              <div className="col-lg-6 col-5 text-right"></div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid mt--6">
        <div className="row">
          <div className="col">
            <div className="card">
              <div className="card-header border-0">
                <h3 className="mb-0">Danh sách ứng viên đã xóa</h3>
              </div>
              <div className="table-responsive">
                <table className="table align-items-center table-flush">
                  <thead className="thead-light">
                    <tr>
                      <th scope="col" className="sort" data-sort="name">
                        Ảnh - Tên ứng viên
                      </th>
                      <th scope="col" className="sort" data-sort="budget">
                        Luơng
                      </th>
                      <th scope="col" className="sort" data-sort="status">
                        Công việc ứng tuyển
                      </th>
                      <th scope="col">Ngày ứng tuyển</th>
                      <th scope="col" className="sort" data-sort="completion">
                        Trạng thái
                      </th>
                      <th scope="col">Hành động</th>
                    </tr>
                  </thead>
                  <tbody className="list">
                    <tr>
                      <th scope="row">
                        <div className="media align-items-center">
                          <div
                            className="image-job  mr-3"
                            style={{
                              backgroundImage:
                                "url(https://www.glassdoor.com/blog/app/uploads/sites/2/iStock-585051592-1-750x450.jpg)",
                            }}
                          ></div>
                          <div className="media-body">
                            <span className="name mb-0 text-sm">
                              Argon Design System
                            </span>
                          </div>
                        </div>
                      </th>
                      <td className="budget">$2500 USD</td>
                      <td>React JS</td>

                      <td>
                        <div className="d-flex align-items-center">
                          <span className="completion mr-2">60%</span>
                        </div>
                      </td>
                      <td>
                        <span className=" badge-dot mr-4">
                          <i className="bg-warning"></i>
                          <span className="status">pending</span>
                        </span>
                      </td>
                      <td className="text-right">
                        <button
                          className="btn btn-icon btn-primary"
                          type="button"
                        >
                          <span className="btn-inner--icon">
                            <i className="ni ni-atom"></i>
                          </span>
                        </button>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DaXoa;
