import axios from "axios";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router";
import { api } from "../../../../../api";

import ModalViewHistory from "../../../../../components/ComponentCV/CardCV/ModalViewHistory";
import ModalPreviewPDF from "../../../../../components/ComponentCV/CardCV/ModalPreviewPDF";
import FormatDate from "../../../../../utils/FormatDate";
import ModalPhanHoiUngTuyen from "./ModalPhanHoiUngTuyen";
import { Fragment } from "react";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import StatusApply from "../../../../../utils/StatusApply";
const UngTuyenTin = () => {
  const { id } = useParams();
  const [infoRecruiment, setinfoRecruiment] = useState([]);
  useEffect(() => {
    axios.get(`${api}/recruitment/get-apply/${id}`).then((response) => {
      setinfoRecruiment(response.data);
    });
  }, [id]);
  const [isSearch, setisSearch] = useState(false);
  const [resDataSearch, setResDataSearch] = useState([]);
  const [dataRender, setDataRender] = useState([]);

  const handleSearch = (e) => {
    const { value } = e.target;
    if (value) {
      setisSearch(true);
    }
    const dataFilter = infoRecruiment.filter((item) => +item.status === +value);
    setResDataSearch(dataFilter);
  };

  useEffect(() => {
    if (isSearch) {
      setDataRender(resDataSearch);
    } else {
      setDataRender(infoRecruiment);
    }
  }, [resDataSearch, isSearch, infoRecruiment]);
  return (
    <>
      <div>
        <div
          className="header bg-primary pb-6"
          style={{ backgroundImage: "url(/assets/img/background.jpg)" }}
        >
          <div className="container-fluid">
            <div className="header-body">
              <div className="row align-items-center py-4">
                <div className="col-lg-6 col-7"></div>
                <div className="col-lg-6 col-5 text-right"></div>
              </div>
            </div>
          </div>
        </div>
        <div className="container-fluid mt--6">
          <div className="row">
            <div className="col">
              <div className="card">
                <div className="card-header border-0">
                  <div className="row align-items-center">
                    <div className="col">
                      <h6 className=" text-uppercase ls-1 mb-1">ứng tuyển</h6>
                      <h5 className="h3 mb-0">Danh sách ứng viên ứng tuyển</h5>
                    </div>
                    <div className="col-lg-auto">
                      <select
                        onChange={handleSearch}
                        className="form-select"
                        aria-label="Default select example"
                      >
                        <option selected value="">
                          Lọc tin theo trạng thái
                        </option>
                        {StatusApply.map((status) => (
                          <option key={status.id} value={status.id}>
                            {status.textEmployer}
                          </option>
                        ))}
                      </select>
                    </div>
                  </div>
                </div>
                <div className="table-responsive">
                  <table className="table align-items-center table-flush">
                    <thead className="thead-light">
                      <tr>
                        <th scope="col">Ngày ứng tuyển</th>
                        <th scope="col" className="sort" data-sort="completion">
                          Trạng thái
                        </th>
                        <th scope="col" className=" text-center">
                          Hành động
                        </th>
                      </tr>
                    </thead>
                    <tbody className="list"  style={{minHeight:"80vh"}}>
                      {dataRender &&
                        Array.isArray(dataRender) &&
                        dataRender.map((item, index) => (
                          <Fragment key={item.id}>
                            <tr>
                              <td>
                                <div className="d-flex align-items-center">
                                  <span className="completion mr-2">
                                    {FormatDate(item.created_at)}
                                  </span>
                                </div>
                              </td>
                              <td>
                                <span className=" badge-dot mr-4">
                                  <i
                                    className={
                                      StatusApply.find(
                                        (status) => status.id === +item.status
                                      ).background
                                    }
                                  ></i>
                                  <span className="status">
                                    {
                                      StatusApply.find(
                                        (status) => status.id === +item.status
                                      ).textEmployer
                                    }
                                  </span>
                                </span>
                              </td>
                              <td className="text-center">
                                <div className="dropdown">
                                  <Link
                                    className="btn btn-sm btn-outline-default "
                                    role="button"
                                    data-toggle="dropdown"
                                    aria-haspopup="true"
                                    aria-expanded="false"
                                  >
                                  <i className="fa fa-sliders-h pr-1" ></i>
                                  Hành động
                                  </Link>
                                  <div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                    <button
                                      data-toggle="modal"
                                      type="button"
                                      data-target={`#previewPDF${index}`}
                                      className="dropdown-item"
                                    >
                                      <i className="fa fa-file-pdf pr-2"></i>
                                      <span>Xem CV</span>
                                    </button>
                                    <a
                                      href={`${api}/cv/${item.link_cv}`}
                                      className="dropdown-item"
                                    >
                                      <i className="fa fa-file-download pr-2"></i>
                                      <span>Tải CV</span>
                                    </a>
                                    {item &&
                                    item.status &&
                                    item.status === 1 ? (
                                      <button
                                        className="dropdown-item"
                                        data-toggle="modal"
                                        type="button"
                                        data-target={`#action${item.id}`}
                                      >
                                        <i className="fa fa-reply  pr-2"></i>
                                        <span> Phản hồi</span>
                                      </button>
                                    ) : (
                                      <button
                                        className="dropdown-item"
                                        data-toggle="modal"
                                        type="button"
                                        data-target={`#viewHistory${item.id}`}
                                      >
                                        <i className="fas fa-eye   pr-2"></i>
                                        <span> Xem phản hồi</span>
                                      </button>
                                    )}
                                  </div>
                                </div>
                              </td>
                            </tr>
                            <ModalViewHistory id={item.id} item={item} />
                            <ModalPreviewPDF linkCV={item.link_cv} id={index} />
                            <ModalPhanHoiUngTuyen id={item.id} />
                          </Fragment>
                        ))}
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default UngTuyenTin;
