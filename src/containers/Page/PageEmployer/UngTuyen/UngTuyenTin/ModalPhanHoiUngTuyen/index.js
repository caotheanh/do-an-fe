import axios from "axios";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { gettoken } from "../../../../../../utils/gettoken";
import StatusApply from "../../../../../../utils/StatusApply";
import { api } from "../../../../../../api";
import NotificationManager from "react-notifications/lib/NotificationManager";
function ModalPhanHoiUngTuyen({ id }) {
  const [isClick, setisClick] = useState(false);
  const [type, setType] = useState(0);
  const hanleClick = (numbers) => {
    setisClick(true);
    setType(numbers);
  };
  const token = gettoken();
  const { handleSubmit, errors, register } = useForm();
  const handleRefuse = (data) => {
    document.querySelector(".huy2321").click();
    axios.put(`${api}/apply/change-status`, {
      token: token,
      status: 0,
      id: id,
    });
    axios
      .post(
        `${api}/note-apply`,
        Object.assign(data, { token: token, status: 0, id_apply: id })
      )
      .then(() =>
        NotificationManager.success(
          "Gửi phản hồi từ chối thành công",
          "thành công",
          3000
        )
      );
  };
  const handleAgree = (data) => {
    document.querySelector(".huy2321").click();
    axios.put(`${api}/apply/change-status`, {
      token: token,
      status: 2,
      id: id,
    });
    axios
      .post(
        `${api}/note-apply`,
        Object.assign(data, { token: token, id_apply: id })
      )
      .then(() =>
        NotificationManager.success(
          "Gửi phản hồi từ chối thành công",
          "thành công",
          3000
        )
      );
  };
  return (
    <div
      className="modal fade"
      id={`action${id}`}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div
        className={`${isClick ? "col-8" : ""} modal-dialog modal-dialog-centered`}
        role="document"
      >
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Phản hồi ứng viên ứng tuyển
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            {!isClick ? (
              <div className="mt-3 mb-3 d-flex justify-content-md-center">
                <button
                  className="btn btn-outline-danger"
                  onClick={() => hanleClick(1)}
                >
                  Từ chối
                </button>
                <button
                  className="btn btn-outline-default  ml-3"
                  onClick={() => hanleClick(2)}
                >
                  Đồng ý
                </button>
              </div>
            ) : type === 1 ? (
              <form onSubmit={handleSubmit(handleRefuse)} className="container">
                <div className="form-group">
                  <label htmlFor="example-text-input" className="form-control-label">
                    Ghi chú từ chối
                  </label>
                  <textarea
                    name="content"
                    ref={register({
                      required: "Vui lòng nhập tiêu đề tin tuyển dụng",
                    })}
                    className="form-control"
                    rows="3"
                  ></textarea>
                  {errors.content && (
                    <p className="text-danger">{errors.content.message}</p>
                  )}
                </div>
                <button
                  className=" btn btn-danger btn-icon"
                  onClick={() => setisClick(false)}
                >
                  <i className="fa fa-reply"></i> Quay lại
                </button>
                <button className="btn btn-default" type="submit">
                  Gửi phản hồi
                </button>
                <button type="button" className="btn btn-secondary huy2321 " data-dismiss="modal">Hủy</button>
              </form>
            ) : (
              <form onSubmit={handleSubmit(handleAgree)} className="container">
                <div className="form-group">
                  <label htmlFor="example-text-input" className="form-control-label">
                    Trạng thái
                  </label>
                  <select
                    className="form-control"
                    name="status"
                    ref={register({
                      required: "Vui lòng chọn trạng thái",
                    })}
                  >
                    <option value="">Chọn trạng thái</option>
                    {StatusApply.map((item) => (
                      item.isHidden&&<option value={item.id}>{item.textEmployer}</option>
                    ))}
                  </select>
                  {errors.status && (
                    <p className="text-danger">{errors.status.message}</p>
                  )}
                </div>
                <div className="form-group">
                  <label htmlFor="example-text-input" className="form-control-label">
                    Ghi chú
                  </label>
                  <textarea
                    name="content"
                    ref={register({
                      required: "Vui lòng nhập ghi chú",
                    })}
                    className="form-control"
                    rows="3"
                  ></textarea>{" "}
                  {errors.content && (
                    <p className="text-danger">{errors.content.message}</p>
                  )}
                </div>
                <button
                  className="mt-3 btn btn-danger btn-icon"
                  onClick={() => setisClick(false)}
                >
                  <i className="fa fa-reply"></i> Quay lại
                </button>
                <button className="mt-3 btn btn-default" type="submit">
                  Gửi phản hồi
                </button>
                <button type="button" className="btn btn-secondary huy2321" data-dismiss="modal">Hủy</button>
              </form>
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModalPhanHoiUngTuyen;
