import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import { gettoken } from "../../../../../utils/gettoken";
import { api } from "../../../../../api";
import axios from "axios";
import FormatDate from "../../../../../utils/FormatDate";
import NotificationManager from "react-notifications/lib/NotificationManager";
import ToolCongTy from "./Tool";

const DanhSachCongTy = () => {
  const [listCompany, setListCompany] = useState([]);
  const token = gettoken();
  useEffect(() => {
    axios({
      method: "get",
      url: `${api}/company/user/${token}`,
    }).then((res) => {
      setListCompany(res.data);
    });
  }, [token]);
  const getdata = () => {
    axios({
      method: "get",
      url: `${api}/company/user/${token}`,
    }).then((res) => {
      setListCompany(res.data);
    });
  };
  const handleDelete = (id) => {
    axios
      .delete(`${api}/company/${id}`)
      .then(() => {
        document.querySelector(`.closeModalDelete${id}`).click();
        NotificationManager.success(
          "Xóa công ty thành công",
          "Thành công",
          3000
        );
        getdata();
      })
      .catch(() => {
        document.querySelector(".closeModalDelete").click();
        NotificationManager.error(
          "Xóa công ty không thành công",
          "Thất bại",
          3000
        );
      });
  };
  return (
    <div className="m-3">
      <Helmet>
        <title>Thêm tin tuyển dụng</title>
      </Helmet>
      <div className=" order-xl-1">
        <div className="card">
          <div className="card-header">
            <div className="row align-items-center">
              <div className="col-6 ">
                <input
                  type="text"
                  className="form-control"
                  placeholder="Username"
                  value="reactjs"
                />
              </div>
              <div className="col-6 text-right">
                <Link
                  to="/employer/cong-ty/them-moi"
                  className="btn  btn-primary"
                >
                  Thêm công ty mới
                </Link>
              </div>
            </div>
          </div>
          {listCompany &&
            Array.isArray(listCompany) &&
            listCompany.map((company) => (
              <>
                <div className="card-body">
                  <div className="row">
                    <div className="col-lg-3">
                      <img
                        alt="img"
                        src={`${api}/companyImg/${company.avt}`}
                        style={{ width: "10rem" }}
                      />
                    </div>
                    <div className="col-lg-9">
                      <div className="row">
                        <div className="col-lg-10">
                          <h1>{company.name}</h1>
                        </div>
                        <div className="col-lg-2 item-right">
                          <Link
                            to={`/employer/cong-ty/sua/${company.id}`}
                            className="btn btn-icon btn-outline-primary  btn-sm"
                          >
                              <i className="fa fa-edit"></i>
                          </Link>
                          <button
                            className="btn btn-icon btn-outline-default btn-sm"
                            data-toggle="modal"
                            data-target={`#modalDelete${company.id}`}
                          >
                              <i className="fa fa-trash-alt"></i>
                          </button>
                          <div
                            className="modal fade"
                            id={`modalDelete${company.id}`}
                            tabIndex="-1"
                            role="dialog"
                            aria-labelledby="exampleModalLabel"
                            aria-hidden="true"
                          >
                            <div
                              className="modal-dialog modal-dialog-centered"
                              role="document"
                            >
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5
                                    className="modal-title"
                                    id="exampleModalLabel"
                                  >
                                    Xóa công ty
                                  </h5>
                                  <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                  >
                                    <span aria-hidden="true">&times;</span>
                                  </button>
                                </div>
                                <div className="modal-body d-flex justify-content-center">
                                  <button
                                    type="button"
                                    className={`btn btn-secondary closeModalDelete${company.id}`}
                                    data-dismiss="modal"
                                  >
                                    Hủy
                                  </button>
                                  <button
                                    type="button"
                                    className="btn btn-danger"
                                    onClick={() => handleDelete(company.id)}
                                  >
                                    Xóa
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <p>Chưa có địa chỉ văn phòng</p>
                      <div className="row">
                        <div className="col-6">
                          <div>Lĩnh vực: Chưa cập nhật</div>
                          <div>Ngày tạo: {FormatDate(company.created_at)}</div>
                        </div>
                        <div className="col-6">
                          <div>Quy mô: {company.numberMember}</div>
                          <div>
                            Ngày cập nhật: {FormatDate(company.updated_at)}
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <ToolCongTy />
              </>
            ))}
        </div>
      </div>
    </div>
  );
};

export default DanhSachCongTy;
