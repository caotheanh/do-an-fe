import React from 'react'

const TongQuanCongTy = () => {
    const cssCover = "https://i.ytimg.com/vi/xTL0Za8_8KY/maxresdefault.jpg"
    return (
        <>
            <div className="header pb-6 d-flex align-items-center" style={{ minHeight: "350px", backgroundImage: `url(${cssCover})`, backgroundSize: "cover", backgroundPosition: "center top" }}>
                <span className="mask bg-gradient-default opacity-4"></span>
                <div className="container-fluid d-flex align-items-center">
                </div>
            </div>
            <div className="m-3">
                <div className="card card-profile">
                    <div className="row justify-content-center">
                        <div className="col-lg-3 order-lg-2">
                            <div className="card-profile-image">
                                <div style={{
                                    background: ` url(https://cdn.thukyluat.vn/nhch-images//CauHoi_Hinh/99a3a229-20ff-4bce-a1a3-673ea456d33e.jpg) 50% 50% no-repeat`,
                                    width: "200px",
                                    margin: "auto",
                                    height: "200px",
                                    borderRadius: "50%",
                                    border: "4px solid #f6f9fc"
                                }} className="rounded-circle mt--9"></div>
                            </div>
                            <div>
                                <h1 className="text-center">
                                    Sotatek
                                </h1>
                                <h5 className="h3 text-center">
                                    27<span className="font-weight-light"> Người dõi </span>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <div className="card">

                        <div className="card-body">
                            <form>
                                <h6 className="heading-small text-muted mb-4">Cài đặt thông tin cơ bản </h6>
                                <div className="pl-lg-4">
                                    <div className="row">
                                        <div className="col-lg-6">
                                            <div className="form-group">
                                                <label className="form-control-label" htmlFor="input-username">Username</label>
                                                <input type="text" id="input-username" className="form-control" placeholder="Username" value="lucky.jesse" />
                                            </div>
                                        </div>
                                        <div className="col-lg-6">
                                            <div className="form-group">
                                                <label className="form-control-label" htmlFor="input-email">Email address</label>
                                                <input type="email" id="input-email" className="form-control" placeholder="jesse@example.com" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-lg-6">
                                            <div className="form-group">
                                                <label className="form-control-label" htmlFor="input-first-name">First name</label>
                                                <input type="text" id="input-first-name" className="form-control" placeholder="First name" value="Lucky" />
                                            </div>
                                        </div>
                                        <div className="col-lg-6">
                                            <div className="form-group">
                                                <label className="form-control-label" htmlFor="input-last-name">Last name</label>
                                                <input type="text" id="input-last-name" className="form-control" placeholder="Last name" value="Jesse" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr className="my-4" />
                                <h6 className="heading-small text-muted mb-4">Lĩnh vực hoạt động</h6>
                                <div className="pl-lg-4">
                                    <div className="form-group">
                                        <label className="form-control-label">About Me</label>
                                        <textarea rows="4" className="form-control" placeholder="A few words about you ...">A beautiful Dashboard for Bootstrap 4. It is Free and Open Source.</textarea>
                                    </div>
                                </div>
                                <div className=" justify-content-center">
                                    <div>
                                        <button type="button" className=" btn btn-primary">Lưu thông tin</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export default TongQuanCongTy
