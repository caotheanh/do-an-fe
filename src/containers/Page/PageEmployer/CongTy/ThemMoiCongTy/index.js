import axios from "axios";
import React, { useState } from "react";
import { useForm } from "react-hook-form";
import { gettoken } from "../../../../../utils/gettoken";
import { api } from "../../../../../api";
import { useSelector } from "react-redux";
import NotificationManager from "react-notifications/lib/NotificationManager";
const ThemMoiCongTy = () => {
  const { register, handleSubmit } = useForm();
  const [image, setImage] = useState("");
  const onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      setImage(URL.createObjectURL(img));
    }
  };
  const formData = new FormData();
  const token = gettoken();
  const addCompany = async (data) => {
    formData.append("token", token);
    formData.append("email", data.email);
    formData.append("numberMember", data.numberMember);
    formData.append("name", data.name);
    formData.append("fieldsOfOperation", data.fieldsOfOperation);
    formData.append("phone", data.phone);
    formData.append("details", data.details);
    formData.append("photo", data.photo[0]);
    axios
      .post(`${api}/company`, formData)
      .then((res) => {
        NotificationManager.success(
          "Thêm công ty thành công",
          "Thành công",
          3000
        );
      })
      .catch(() =>
        NotificationManager.success(
          "Thêm công ty không thành công",
          "Thất bại",
          3000
        )
      );
  };
  const categories = useSelector((state) => state.categories);
  return (
    <div>
      <div className="col-xl-12 order-xl-1">
        <div className="card">
          <div className="card-header">
            <div className="row align-items-center">
              <div className="col-8">
                <h3 className="mb-0">
                  Thêm công ty của bạn vào hệ thống Kết Nối Việc
                </h3>
              </div>
            </div>
          </div>
          <div className="card-body">
            <form onSubmit={handleSubmit(addCompany)}>
              <div className="row">
                <div className="col-lg-6">
                  <div className="form-group">
                    <label className="form-control-label" htmlFor="input-username">
                      Tên công ty
                    </label>
                    <input
                      type="text"
                      ref={register({ required: true })}
                      id="input-username"
                      name="name"
                      className="form-control"
                    />
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group">
                    <label className="form-control-label" htmlFor="input-email">
                      Email
                    </label>
                    <input
                      type="email"
                      ref={register({ required: true })}
                      id="input-email"
                      name="email"
                      className="form-control"
                    />
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-6">
                  <div className="form-group">
                    <label className="form-control-label" htmlFor="input-username">
                      Số điện thoại
                    </label>
                    <input
                      type="text"
                      name="phone"
                      ref={register({ required: true })}
                      id="input-username"
                      className="form-control"
                    />
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group">
                    <label className="form-control-label" htmlFor="input-username">
                      Lĩnh vực hoạt động
                    </label>
                    <select
                      name="fieldsOfOperation"
                      ref={register({ required: true })}
                      className="form-control"
                    >
                      {categories &&
                        categories.map((cate) => (
                          <option value={cate.id}>{cate.name}</option>
                        ))}
                    </select>
                  </div>
                </div>
              </div>
              <div className="row">
                <div className="col-lg-6">
                  <div className="form-group">
                    <label className="form-control-label" htmlFor="input-username">
                      Quy mô thành viên
                    </label>
                    <input
                      type="text"
                      name="numberMember"
                      ref={register({ required: true })}
                      id="input-username"
                      className="form-control"
                    />
                  </div>
                  <div className="form-group">
                    <label className="form-control-label" htmlFor="input-username">
                      Mô tả
                    </label>
                    <textarea
                      rows="4"
                      className="form-control"
                      name="details"
                      ref={register({ required: true })}
                    ></textarea>
                  </div>
                </div>
                <div className="col-lg-6">
                  <div className="form-group">
                    <label
                      htmlFor="upload-photo"
                      className="btn btn-neutral btn-icon"
                    >
                      <span className="btn-inner--icon">
                        <i className="fa fa-upload"></i>
                      </span>

                      <span className="btn-inner--text">Thêm ảnh</span>
                    </label>
                    <input
                      style={{
                        opacity: "0",
                        position: "absolute",
                        zIndex: "-1",
                      }}
                      accept="image/png, image/jpeg, image/jpg" 
                      type="file"
                      name="photo"
                      ref={register({ required: true })}
                      onChange={onImageChange}
                      id="upload-photo"
                      className="form-control"
                    />
                  </div>
                  <img
                    alt="preview"
                    src={image ? image : "/assets/img/image-none.png"}
                    style={{
                      maxHeight: "30rem",
                      maxWidth: "30rem",
                      border: "1px solid red",
                      borderRadius: "5px",
                    }}
                  />
                </div>
              </div>
              <button
                className="btn btn-icon btn-outline-primary"
                type="submit"
              >
                <span className="btn-inner--icon">
                  <i className="ni ni-bag-17"></i>
                </span>
                <span className="btn-inner--text">Thêm công ty</span>
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ThemMoiCongTy;
