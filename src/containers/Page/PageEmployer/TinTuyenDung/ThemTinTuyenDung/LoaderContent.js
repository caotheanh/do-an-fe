import React from "react";
import ContentLoader from "react-content-loader";

const LoaderContent = (props) => {
  return (
    <div className="mt-3">
      <ContentLoader
        speed={2}
        width="100%"
        height={500}
        backgroundColor="#f3f3f3"
        foregroundColor="#ecebeb"
        {...props}
      >
        <rect x="8" y="-1" rx="0" ry="0" width="20%" height="17" />
        <rect x="9" y="21" rx="0" ry="0" width="245" height="26" />
        <rect x="12" y="77" rx="0" ry="0" width="94" height="20" />
        <rect x="12" y="99" rx="0" ry="0" width="190" height="21" />
        <rect x="11" y="57" rx="0" ry="0" width="564" height="4" />
        <rect x="13" y="123" rx="0" ry="0" width="568" height="58" />
      </ContentLoader>
    </div>
  );
};

export default LoaderContent;
