import React, { useEffect, useState } from "react";

import { Helmet } from "react-helmet";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import { DataAddress } from "../../../../Data/DataAddress";
import LoaderContent from "./LoaderContent";
import { api } from "../../../../../api";
import { gettoken } from "../../../../../utils/gettoken";
import axios from "axios";
import { NotificationManager } from "react-notifications";
import { ListPosition } from "../../../../../utils/ListPosition";
import TypeJob from "../../../../../utils/TypeJob";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
function ThemTinTuyenDung() {
  const info_user = useSelector((state) => state.info_user);
  const info_login = useSelector((state) => state.info_login);
  console.log(info_login, info_user);
  const [listCompany, setListCompany] = useState([]);
  const categories = useSelector((state) => state.categories);
  const token = gettoken();
  const [address, setAddress] = useState({
    province_or_city: "",
    district: "",
    ward: "",
    details_address: "",
  });
  const [addressDetail, setAddressDetail] = useState("");
  ////

  const [image, setImage] = useState("");
  const onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      setImage(URL.createObjectURL(img));
    }
  };
  ///
  const formData = new FormData();
  const handleChangeAddress = (e) => {
    const { name, value } = e.target;
    setAddress({ ...address, [name]: value });
  };
  const [wage, setWage] = useState(0);
  let dem = 0;
  const history = useHistory();
  const { register, handleSubmit, errors, setValue } = useForm();
  const [isLoading, setIsLoading] = useState(false);
  const onSubmit = (data) => {
    setIsLoading(true);
    formData.append("token", token);
    formData.append("photo", data.image[0]);
    formData.append("benefit", data.benefit);
    formData.append("company", data.company ? data.company : null);
    formData.append("company_id", data.company_id ? data.company_id : null);
    formData.append("description", data.description);
    formData.append("details_address", data.details_address);
    formData.append("district", data.district);
    formData.append("email_contact", data.email_contact);
    formData.append("end", data.end);
    formData.append("id_category", data.id_category);
    formData.append("name_contact", data.name_contact);
    formData.append("phone_contact", data.phone_contact);
    formData.append("province_or_city", data.province_or_city);
    formData.append("request", data.request);
    formData.append("salary", +data.show === 1 ? data.salary : null);
    formData.append("total", data.total);
    formData.append("title", data.title);
    formData.append("type", data.type);
    formData.append("lat_lng", "23123");
    formData.append("ward", data.ward);
    axios
      .post(`${api}/recruitment/insert`, formData)
      .then(() => {
        NotificationManager.success(
          "Thêm tin tuyển dụng thành công",
          "Thành công",
          3000
        );
        history.push("/employer/tin-tuyen-dung/hoat-dong");
      })
      .finally(() => {
        setIsLoading(false);
      });
  };
  useEffect(() => {
    axios({
      method: "get",
      url: `${api}/company/user/${token}`,
    }).then((res) => {
      setListCompany(res.data);
    });
    setValue("email_contact", (info_login && info_login.email) || "");
    setValue("name_contact", (info_user && info_user.name) || "");
    setValue("phone_contact", (info_user && info_user.phone) || "");
  }, [token, info_user, info_login, setValue]);
  useEffect(() => {
    let province_or_city = "",
      district = "",
      ward = "";
    if (address.province_or_city) {
      province_or_city = DataAddress[address.province_or_city][1];
    }
    if (address.district) {
      district = DataAddress[address.province_or_city][4][address.district][1];
    }
    if (address.ward) {
      ward =
        DataAddress[address.province_or_city][4][address.district][4][
          address.ward
        ][1];
    }
    if (address.details_address) {
      setAddressDetail(
        address.details_address +
          " - " +
          ward +
          " - " +
          district +
          " - " +
          province_or_city
      );
    }
  }, [address]);

  return (
    <>
      <Helmet>
        <title>Thêm tin tuyển dụng</title>
      </Helmet>
      <div className="navbar navbar-top navbar-expand navbar-dark bg-default border-bottom">
        <div className="container-fluid">
          <div className="header-body">
            <div className="row align-items-center py-6 text-center">
              <div className="container ">
                <div className="row justify-content-center">
                  <div className="col-xl-6 col-lg-6 col-md-8 px-5"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid mt--5">
        <div className="row">
          <div className="col-xl-12">
            <div className="card">
              {dem === 0 ? (
                <form
                  onSubmit={handleSubmit(onSubmit)}
                  encType="multipart/form-data"
                >
                  <div className="card-header bg-transparent">
                    <div className="row align-items-center">
                      <div className="col">
                        <h6 className="text-uppercase ls-1 mb-1">
                          Tin tuyển dụng
                        </h6>
                        <h5 className="h3 mb-0 text-uppercase">
                          Thêm mới tin tuyển dụng
                        </h5>
                      </div>
                    </div>
                  </div>
                  <div className="card-body">
                    <div className="pl-lg-4">
                      <div className="row">
                        <div className="col-md-12">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                              Tiêu đề tin tuyển dụng
                              <span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                              Tiêu đề tin tuyển dụng nên chứa tên vị trí, chức
                              danh cần tuyển.
                            </div>
                            <input
                              id="input-address"
                              className="form-control"
                              type="text"
                              name="title"
                              ref={register({
                                required:
                                  "Vui lòng nhập tiêu đề tin tuyển dụng",
                                minLength: {
                                  value: 6,
                                  message: "Tin tuyển dụng ít nhất 6 kí tự",
                                },
                              })}
                            />
                            {errors.title && (
                              <p className="text-danger">
                                {errors.title.message}
                              </p>
                            )}
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-city"
                            >
                              Ngành nghề<span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                              Lựa chọn ngành nghề để tiếp cận đúng ứng viên.
                            </div>
                            <select
                              name="id_category"
                              type="text"
                              id="input-city"
                              ref={register({ required: true })}
                              className="form-control"
                            >
                              {categories &&
                                Array.isArray(categories) &&
                                categories.map((item) => (
                                  <option value={item.id} key={item.id}>
                                    {item.name}
                                  </option>
                                ))}
                            </select>
                            {errors.id_category && (
                              <p className="text-danger">
                                {errors.id_category.message}
                              </p>
                            )}
                          </div>
                        </div>
                        <div className="col-lg-6">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                              Vị trí<span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2  icon-aler-required"></i>
                              Cấp bậc cần tuyển.
                            </div>
                            <select
                              name="dasdasdasdas"
                              ref={register({ required: true })}
                              id="input-postal-code"
                              className="form-control"
                            >
                              <option value="">Chọn cấp bậc</option>
                              {ListPosition &&
                                ListPosition.map((item) => (
                                  <option value={item.id} key={item.id}>
                                    {item.text}
                                  </option>
                                ))}
                            </select>
                            {errors.type && (
                              <p className="text-danger">
                                {errors.type.message}
                              </p>
                            )}
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-city"
                            >
                              Số lượng<span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2  icon-aler-required"></i>
                              Số lượng cần tuyển.
                            </div>
                            <input
                              type="number"
                              name="total"
                              ref={register({
                                required: "Vui lòng nhập Số lượng",
                              })}
                              id="input-city"
                              className="form-control"
                            />
                            {errors.total && (
                              <p className="text-danger">
                                {errors.total.message}
                              </p>
                            )}
                          </div>
                        </div>
                        <div className="col-lg-6">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                              Hình thức làm việc
                              <span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                              Chọn hình thức làm việc để xác định đúng đối tượng
                              cần tuyển dụng.
                            </div>
                            <select
                              name="type"
                              ref={register({
                                required: "Vui lòng chọn hình thức làm việc",
                              })}
                              className="form-control"
                            >
                              <option value="">Chọn hình thức làm việc</option>
                              {TypeJob.map((item) => (
                                <option value={item.id} key={item.id}>
                                  {item.name}
                                </option>
                              ))}
                            </select>
                            {errors.description && (
                              <p className="text-danger">
                                {errors.description.message}
                              </p>
                            )}
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-12">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                              Mức lương<span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                              Thêm mức lương để ứng viên có thể lọc và tìm thấy
                              tin tuyển dụng.
                            </div>

                            <select
                              onChange={(e) => setWage(e.target.value)}
                              className="form-control"
                              name="show"
                              ref={register({ required: true })}
                            >
                              <option>----------</option>
                              <option value="1">
                                Hiển thị mức lương cụ thể
                              </option>
                              <option value="2">
                                Hiển thị mức lương thương lượng
                              </option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div
                        className="row"
                        style={
                          +wage === 1
                            ? { display: "block" }
                            : { display: "none" }
                        }
                      >
                        <div className="col-lg-12">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-city"
                            >
                              Mức lương (triệu đồng)
                              <span className="text-orange">*</span>
                            </label>
                            <div className="input-group input-group-merge">
                              <input
                                type="number"
                                name="salary"
                                ref={register({
                                  required: +wage === 1 ? true : false,
                                })}
                                className="form-control"
                                aria-describedby="basic-addon2"
                              />
                              <div className="input-group-append">
                                <span
                                  className="input-group-text"
                                  id="basic-addon2"
                                >
                                  Triệu đồng
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-12">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                              Yêu cầu công việc
                              <span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                              Các yêu cầu đối với ứng viên.
                            </div>
                            <textarea
                              name="request"
                              ref={register({ required: true })}
                              className="form-control"
                              rows="3"
                            ></textarea>
                          </div>
                        </div>
                        <div className="col-lg-12">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                              Mô tả công việc
                              <span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                              Chọn hình thức làm việc để xác định đúng đối tượng
                              cần tuyển dụng.
                            </div>
                            <textarea
                              name="description"
                              ref={register({ required: true })}
                              className="form-control"
                              rows="3"
                            ></textarea>
                            {errors.description && (
                              <p className="text-danger">
                                {errors.description.message}
                              </p>
                            )}
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="form-group">
                          <label
                            className="form-control-label"
                            htmlFor="input-city"
                          >
                            Địa chỉ làm việc
                            <span className="text-orange">*</span>
                          </label>
                          <div className="alert_1 alert-required" role="alert">
                            <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                            Chọn địa chỉ làm việc. Nhập địa chỉ càng cụ thể
                            chính xác thì tin của bạn càng uy tín và có nhiều
                            ứng viên.
                          </div>
                        </div>
                        <div className="col-lg-4">
                          <div className="form-group">
                            <select
                              onChange={handleChangeAddress}
                              name="province_or_city"
                              ref={register({ required: true })}
                              className="form-control"
                            >
                              {!address.province_or_city && (
                                <option>Chọn thành phố</option>
                              )}
                              {DataAddress.map((thanhpho, index) => (
                                <option
                                  key={`thanh_pho_${index}`}
                                  value={index}
                                >
                                  {thanhpho[1]}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>
                        <div className="col-lg-4">
                          <div className="form-group">
                            <select
                              onChange={handleChangeAddress}
                              name="district"
                              ref={register({ required: true })}
                              className="form-control"
                              disabled={address.province_or_city ? false : true}
                            >
                              {!address.district && <option>Quận/Huyện</option>}
                              {address.province_or_city &&
                                DataAddress[address.province_or_city][4].map(
                                  (huyen, index) => (
                                    <option
                                      key={`tinh_thanh_${huyen[0]}`}
                                      value={index}
                                    >
                                      {huyen[1]}
                                    </option>
                                  )
                                )}
                            </select>
                          </div>
                        </div>
                        <div className="col-lg-4">
                          <div className="form-group">
                            <select
                              onChange={handleChangeAddress}
                              name="ward"
                              ref={register({ required: true })}
                              className="form-control"
                              disabled={address.district ? false : true}
                              id="exampleFormControlSelect2 "
                            >
                              {!address.ward && <option>Xã/Phường</option>}
                              {address.district &&
                                DataAddress[address.province_or_city][4][
                                  address.district
                                ][4].map((xa, index) => (
                                  <option key={`xa_${xa[0]}`} value={index}>
                                    {xa[1]}
                                  </option>
                                ))}
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <input
                          onChange={handleChangeAddress}
                          name="details_address"
                          ref={register({ required: true })}
                          type="text"
                          className="form-control"
                        />
                      </div>

                      <div className="form-group">
                        <input
                          value={
                            addressDetail && address.details_address
                              ? addressDetail
                              : ""
                          }
                          type="text"
                          disabled
                          className="form-control"
                        />
                      </div>
                      <div className="row">
                        <div className="col-lg-6">
                          <div className="form-group">
                            <label className="form-control-label">
                              Doanh nghiệp<span className="text-orange">*</span>
                            </label>
                            <select
                              className="form-control mt-2"
                              name="company_id"
                              ref={register({ required: true })}
                            >
                              <option value="">Chọn công ty của bạn</option>
                              {listCompany &&
                                Array.isArray(listCompany) &&
                                listCompany.map((item) => (
                                  <option key={item.id} value={item.id}>
                                    {item.name}
                                  </option>
                                ))}
                            </select>
                          </div>

                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                              Thêm liên hệ
                              <span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2  icon-aler-required"></i>
                              Bạn có thể thêm liên hệ của nhà tuyển dụng.
                            </div>
                            <div className="form-group">
                              <div className="input-group">
                                <div className="input-group-prepend">
                                  <span
                                    className="input-group-text"
                                    id="basic-addon3"
                                  >
                                    Họ và tên
                                  </span>
                                </div>
                                <input
                                  type="text"
                                  className="form-control"
                                  id="basic-url"
                                  ref={register({ required: true })}
                                  name="name_contact"
                                  aria-describedby="basic-addon3"
                                />
                              </div>
                            </div>
                            <div className="form-group">
                              <div className="input-group">
                                <div className="input-group-prepend">
                                  <span
                                    className="input-group-text"
                                    id="basic-addon3"
                                  >
                                    Email
                                  </span>
                                </div>
                                <input
                                  type="text"
                                  className="form-control"
                                  id="basic-url"
                                  ref={register({ required: true })}
                                  name="email_contact"
                                  aria-describedby="basic-addon3"
                                />
                              </div>
                            </div>

                            <div className="form-group">
                              <div className="input-group">
                                <div className="input-group-prepend">
                                  <span
                                    className="input-group-text"
                                    id="basic-addon3"
                                  >
                                    Số điện thoại
                                  </span>
                                </div>

                                <input
                                  type="text"
                                  className="form-control"
                                  id="basic-url"
                                  ref={register({ required: true })}
                                  name="phone_contact"
                                  aria-describedby="basic-addon3"
                                />
                              </div>
                            </div>
                          </div>

                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                              Phúc lợi
                              <span className="text-orange">*</span>
                            </label>

                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                              Thêm mô tả các phúc lợi mà ứng viên nhận được nếu
                              trúng tuyển.
                            </div>
                            <textarea
                              name="benefit"
                              ref={register({ required: true })}
                              className="form-control"
                              rows="3"
                            ></textarea>
                          </div>
                        </div>

                        <div className="col-lg-6">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                              Ảnh đại diện tin tuyển dụng
                              <span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2  icon-aler-required"></i>
                              Thêm hình ảnh đại diện cho tin tuyển dụng của bạn.
                            </div>
                            <label
                              htmlFor="upload-photo"
                              className="btn btn-neutral btn-icon"
                            >
                              <span className="btn-inner--icon">
                                <i className="fa fa-upload"></i>
                              </span>

                              <span className="btn-inner--text">Thêm ảnh</span>
                            </label>
                            <input
                              style={{
                                opacity: "0",
                                position: "absolute",
                                zIndex: "-1",
                              }}
                              accept="image/png, image/jpeg, image/jpg"
                              type="file"
                              name="image"
                              ref={register({ required: true })}
                              onChange={onImageChange}
                              id="upload-photo"
                              className="form-control"
                            />
                          </div>
                          <div className="form-group">
                            <img
                              alt="preview"
                              src={image ? image : "/assets/img/image-none.png"}
                              style={{
                                maxHeight: "30rem",
                                maxWidth: "30rem",
                                border: "1px solid red",
                                borderRadius: "5px",
                              }}
                            />
                          </div>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-md-12">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                              Hạn nộp hồ sơ
                              <span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                              Thêm hạn nộp hồ sơ để ứng viên nhanh chóng ứng
                              tuyển.
                            </div>
                            <input
                              type="date"
                              name="end"
                              ref={register({ required: true })}
                              className="form-control mb-2"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr className="my-1 "></hr>
                  <div className="card-header bg-transparent">
                    <div className="row align-items-center">
                      <div className="col">
                        {!isLoading ? (
                          <button
                            className="btn btn-icon btn-primary"
                            type="submit"
                          >
                            <span className="btn-inner--icon">
                              <i className="fa fa-plus"></i>
                            </span>
                            <span className="btn-inner--text">
                              Tạo tin tuyển dụng
                            </span>
                          </button>
                        ) : (
                          <button className="btn btn-icon btn-primary disabled">
                            <span className="btn-inner--icon">
                              <i className="fa fa-spinner fa-spin"></i>
                            </span>

                            <span className="btn-inner--text pl-3">
                              Tạo tin tuyển dụng
                            </span>
                          </button>
                        )}
                      </div>
                    </div>
                  </div>
                </form>
              ) : (
                <LoaderContent />
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default ThemTinTuyenDung;
