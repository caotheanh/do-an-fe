import axios from "axios";
import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";
import { gettoken } from "../../../../../utils/gettoken";
import ToolTinTuyenDung from "./Tool";
import { api } from "../../../../../api";
import ModalDeleteTin from "./ModalDeleteTin";
import { NotificationContainer, NotificationManager } from "react-notifications";
import FormatDateDetail from "../../../../../utils/FormatDateDetail";
import FormatDate from "../../../../../utils/FormatDate";
import TypeJob from "../../../../../utils/TypeJob";
const TinTuyenDungDaXoa = () => {
  const token = gettoken();
  const [job, setJob] = useState();
  useEffect(() => {
    axios
      .get(`${api}/recruitment/delete/${token}`)
      .then((res) => setJob(res.data));
  }, [token]);
  const deleteTinTuyenDung = (id) => {
    axios.post(`${api}/recruitment/delete/${id}/${token}`).then((res) => {
      NotificationManager.success("Xóa thành công", "", 3000);
      axios
        .get(`${api}/recruitment/get-by-user/${token}`)
        .then((res) => setJob(res.data));
    });
  };
  return (
    <div className="m-3">
      <Helmet>
        <title>Thêm tin tuyển dụng</title>
      </Helmet>
      <div className=" order-xl-1">
        <div className="card">
          <div className="card-header">
            <div className="row align-items-center">
              <div className="col-6 ">
                
              </div>
              <div className="col-6 text-right">
                <Link
                  to="/employer/tin-tuyen-dung/them-moi"
                  className="btn  btn-primary"
                >
                  <i className="fa fa-plus mr-2"></i>
                  Thêm tin tuyển dụng mới
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
      {job && Array.isArray(job) ? (
        job.map((recruitment) => (
          <div className=" order-xl-1" key={recruitment.id}>
            <div className="card">
              <div className="card-header">
                <>
                  <div className="card-body">
                    <div className="row">
                      <div className="col-3">
                        <img
                          alt="img"
                          src={`${api}/${recruitment.img}`}
                          style={{ width: "10rem" }}
                        />
                      </div>
                      <div className="col-9">
                        <div className="row">
                          <div className="col-8 mb-3">
                            <span className="h2 font-weight-bold mb-0">
                              {recruitment.title}
                            </span>
                            <h5 className="card-title text-uppercase text-muted mb-0">
                              {recruitment.company}
                            </h5>
                          </div>
                          <ModalDeleteTin
                            deleteTinTuyenDung={deleteTinTuyenDung}
                            id={recruitment.id}
                          />
                        </div>

                        <div className="row">
                          <div className="col-6">
                            <div>Ngành nghề: </div>
                            <div>Số lượng: {recruitment.total}</div>
                            <div>Ngày hết hạn: {FormatDate(recruitment.end)}</div>
                          </div>
                          <div className="col-6">
                            <div>Cấp bậc: Phó giám đốc</div>
                            <div>Hình thức làm việc: {TypeJob[recruitment.type].name}</div>
                            <div>Ngày cập nhật:  {FormatDateDetail(recruitment.updated_at)}</div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <ToolTinTuyenDung id={recruitment.id} />
                </>
              </div>
            </div>
            <NotificationContainer />
          </div>
        ))
      ) : (
        <div className="d-flex justify-content-center m-5">
          <div style={{ width: "20%" }} className="mt-5">
            <img src="/assets/img/no_data.svg" alt="no data" style={{ width: "100%" }} />
            <div className="text-center mt-5">Không có tin tuyển dụng nào.</div>
          </div>
        </div>
      )}
    </div>
  );
};

export default TinTuyenDungDaXoa;
