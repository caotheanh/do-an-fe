import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { api } from "../../../../../../api";
const ToolTinTuyenDung = ({ id }) => {
  const [infoRecruiment, setinfoRecruiment] = useState([]);
  useEffect(() => {
    axios
      .get(`${api}/recruitment/get-apply/${id}`)
      .then((response) => setinfoRecruiment(response.data))
     
  }, [id]);
  return (
    <div className="row mt-2 m-3">
      <div className="col-xl-3 col-md-6">
        <div className="card card-stats">
          <div className="card-body">
            <div className="row">
              <div className="col">
                <h5 className="card-title text-uppercase text-muted mb-0">
                  Ứng viên ứng tuyển
                </h5>
                <span className="h2 font-weight-bold mb-0">
                  {infoRecruiment.length}
                </span>
              </div>
              <Link
                to={`/employer/tin-tuyen-dung/ung-tuyen/${id}`}
                className="col-auto"
              >
                <div className="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                  <i className="fa fa-inbox"></i>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </div>
      <div className="col-xl-3 col-md-6">
        <div className="card card-stats">
          <div className="card-body">
            <div className="row">
              <div className="col">
                <h5 className="card-title text-uppercase text-muted mb-0">
                  Thống kê
                </h5>
                <span className="h2 font-weight-bold mb-0"></span>
              </div>
              <div className="col-auto">
                <div className="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                  <i className="fa fa-chart-area"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-xl-3 col-md-6">
        <div className="card card-stats">
          <div className="card-body">
            <div className="row">
              <div className="col">
                <h5 className="card-title text-uppercase text-muted mb-0">
                  Lượt mời ứng tuyển
                </h5>
                <span className="h2 font-weight-bold mb-0"></span>
              </div>
              <div className="col-auto">
                <div className="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                  <i className="fa fa-envelope"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="col-xl-3 col-md-6">
        <div className="card card-stats">
          <div className="card-body">
            <div className="row">
              <div className="col">
                <h5 className="card-title text-uppercase text-muted mb-0">
                  Tìm kiếm ứng viên
                </h5>
              </div>
              <Link to={`/employer/tin-tuyen-dung/${id}/tim-ung-vien`} className="col-auto">
                <div className="icon icon-shape bg-gradient-info text-white rounded-circle shadow">
                  <i className="fa fa-search"></i>
                </div>
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ToolTinTuyenDung;
