import React from "react";

const ModalDeleteTin = ({ id,deleteTinTuyenDung }) => {
  const handleClickDelete = () => {
    deleteTinTuyenDung(id);
    document.querySelector(".huy").click();
  };
  return (
    <div
      className="modal fade show"
      id="modalDeleteTinTuyenDung"
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Xóa tin tuyển dụng này
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">Bạn có muốn xóa tin tuyển dụng này ?</div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn btn-secondary huy"
              data-dismiss="modal"
            >
              Huỷ
            </button>
            <button
              type="button"
              onClick={() => {
                handleClickDelete();
              }}
              className="btn btn-primary"
            >
              Xóa
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ModalDeleteTin;
