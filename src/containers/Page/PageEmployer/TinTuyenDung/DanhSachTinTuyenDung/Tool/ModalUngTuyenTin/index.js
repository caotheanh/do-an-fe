import axios from "axios";
import React, { useEffect, useState } from "react";
import { api } from "../../../../../../../api";
import ModalPreviewPDF from "../../../../../../../components/ComponentCV/CardCV/ModalPreviewPDF";
function ModalUngTuyenTin({ id }) {
  const [infoRecruiment, setinfoRecruiment] = useState([]);
  useEffect(() => {
    axios
      .get(`${api}/recruitment/get-apply/${id}`)
      .then((response) => setinfoRecruiment(response.data))
  }, []);
  return (
    <div
      className="modal fade"
      id={`ModalUngTuyenTin${id}`}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered col-6" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Danh sách ứng tuyển gần đây.
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div className="card-body p-3 pb-0">
              <ul className="list-group">
                {infoRecruiment &&
                  Array.isArray(infoRecruiment) &&
                  infoRecruiment
                    .sort(function (a, b) {
                      return b.id - a.id;
                    })
                    .map(
                      (user, index) =>
                        index < 6 && (
                          <li className="list-group-item border-0 d-flex justify-content-between ps-0 mb-2 border-radius-lg">
                            <div className="d-flex flex-column">
                              <h6 className="mb-1 text-dark font-weight-bold text-sm">
                                {user.created_at}
                              </h6>
                              <span className=" badge-dot mr-4">
                                {
                                  [
                                    <i className="bg-danger"></i>,
                                    <i className="bg-info"></i>,
                                    <i className="bg-success"></i>,
                                  ][user && user.status]
                                }
                                <span className="status">
                                  {
                                    ["Đã từ chối", "Đang chờ", "Đã đồng ý"][
                                      user && user.status
                                    ]
                                  }
                                </span>
                              </span>
                            </div>
                            <div className="d-flex align-items-center text-sm">
                              <button
                                className="btn btn-outline-default btn-icon text-dark text-uppercase"
                                data-toggle="modal"
                                type="button"
                                data-target={`#previewPDF${user.link_cv}`}
                              >
                                <i className="fas fa-file-pdf text-lg me-1"></i>{" "}
                                Xem CV
                              </button>
                              <button
                                className="btn btn-outline-info btn-icon text-dark text-uppercase"
                                data-toggle="modal"
                                type="button"
                                data-target={`#action${user.id}`}
                              >
                                <i className="fas fa-reply text-lg me-1"></i>{" "}
                                Phản hồi
                              </button>
                            </div>
                            <ModalPreviewPDF
                              linkCV={user.link_cv}
                              id={user.link_cv}
                            />
                          </li>
                        )
                    )}
              </ul>
            </div>
          </div>
          <div className="modal-footer d-flex justify-content-center">
            <button
              type="button"
              className="btn btn-secondary"
              data-dismiss="modal"
            >
              Đóng
            </button>
            <a
              href={`/employer/tin-tuyen-dung/ung-tuyen/${id}`}
              className="btn btn-outline-primary"
              target="_blank"
            >
              Xem tất cả
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModalUngTuyenTin;
