import React, { useEffect, useState } from "react";
import { Helmet } from "react-helmet";
import { useForm } from "react-hook-form";
import { useSelector } from "react-redux";
import { DataAddress } from "../../../../Data/DataAddress";
import { api } from "../../../../../api";
import { gettoken } from "../../../../../utils/gettoken";
import axios from "axios";
import { useParams } from "react-router-dom/cjs/react-router-dom.min";
import NotificationManager from "react-notifications/lib/NotificationManager";
function SuaTinTuyenDung() {
  const categories = useSelector((state) => state.categories);
  const token = gettoken();
  const { viec_lam_id } = useParams();
  const [address, setAddress] = useState({
    province_or_city: "",
    district: "",
    ward: "",
    details_address: "",
  });
  const topFunction = () => {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
  };
  const [addressDetail, setAddressDetail] = useState("");
  ////
  const [image, setImage] = useState("");
  const onImageChange = (event) => {
    if (event.target.files && event.target.files[0]) {
      let img = event.target.files[0];
      setImage(URL.createObjectURL(img));
    }
  };

  ///
  const formData = new FormData();
  const handleChangeAddress = (e) => {
    const { name, value } = e.target;
    setAddress({ ...address, [name]: value });
  };
  // const [isContentloader, setIsContentloader] = useState(false);
  const [detailRecruiment, setdDetailRecruiment] = useState({});
  const handleChangeRecruiment = (e) => {
    const { name, value } = e.target;
    setdDetailRecruiment((prev) => {
      return { ...prev, [name]: value };
    });
  };
  useEffect(() => {
    topFunction();
    axios.get(`${api}/recruitment/${viec_lam_id}`).then((res) => {
      setdDetailRecruiment(res.data[0]);
      setImage(`${api}/${res.data[0].img}`);
    });
    // .finally(() => setIsContentloader(true));
  }, [viec_lam_id]);
  const [wage, setWage] = useState(0);
  let dem = 0;
  const { register, handleSubmit, setValue } = useForm();
  const onSubmit = (data) => {
    const dataTemp = { ...data };
    delete dataTemp.image;

    formData.append("id", viec_lam_id);
    formData.append("token", token);
    formData.append("photo", data.image[0]);
    formData.append("benefit", data.benefit);
    formData.append("company", data.company);
    formData.append("description", data.description);
    formData.append("details_address", data.details_address);
    formData.append("district", data.district);
    formData.append("email_contact", data.email_contact);
    formData.append("end", data.end);
    formData.append("id_category", data.id_category);
    formData.append("name_contact", data.name_contact);
    formData.append("phone_contact", data.phone_contact);
    formData.append("province_or_city", data.province_or_city);
    formData.append("request", data.request);
    formData.append("salary", +data.show === 1 ? data.salary : null);
    formData.append("total", data.total);
    formData.append("title", data.title);
    formData.append("type", data.type);
    formData.append("lat_lng", data.lat_lng);
    formData.append("ward", data.ward);
    delete dataTemp.image;
    axios
      .post(`${api}/recruitment/update`, formData)
      .then((res) =>
        NotificationManager.success(
          "Sửa tin tuyển dụng thành công.",
          "Thành công",
          3000
        )
      );
  };
  useEffect(() => {
    setValue("province_or_city", detailRecruiment.province_or_city);
    setValue("district", detailRecruiment.district);
    setValue("ward", detailRecruiment.ward);
  }, [detailRecruiment, setValue]);
  useEffect(() => {
    let province_or_city = "",
      district = "",
      ward = "";
    if (detailRecruiment.province_or_city) {
      province_or_city = DataAddress[detailRecruiment.province_or_city][1];
    }
    if (detailRecruiment.district) {
      district =
        DataAddress[detailRecruiment.province_or_city][4][
          detailRecruiment.district
        ][1];
    }
    if (detailRecruiment.ward) {
      ward =
        DataAddress[detailRecruiment.province_or_city][4][
          detailRecruiment.district
        ][4][detailRecruiment.ward][1];
    }
    if (detailRecruiment.details_address) {
      setAddressDetail(
        detailRecruiment.details_address +
          " - " +
          ward +
          " - " +
          district +
          " - " +
          province_or_city
      );
    }
  }, [detailRecruiment]);
  return (
    <>
      <Helmet>
        <title>Thêm tin tuyển dụng</title>
      </Helmet>
      <div className="navbar navbar-top navbar-expand navbar-dark bg-default border-bottom">
        <div className="container-fluid">
          <div className="header-body">
            <div className="row align-items-center py-6 text-center">
              <div className="container ">
                <div className="row justify-content-center">
                  <div className="col-xl-6 col-lg-6 col-md-8 px-5"></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid mt--5">
        <div className="row">
          <div className="col-xl-12">
            <div className="card">
              {dem === 0 ? (
                <form
                  onSubmit={handleSubmit(onSubmit)}
                  encType="multipart/form-data"
                >
                  <div className="card-header bg-transparent">
                    <div className="row align-items-center">
                      <div className="col">
                        <h6 className="text-uppercase ls-1 mb-1">
                          Tin tuyển dụng
                        </h6>
                        <h5 className="h3 mb-0 text-uppercase">
                          Thêm mới tin tuyển dụng
                        </h5>
                      </div>
                    </div>
                  </div>
                  <div className="card-body">
                    <div className="pl-lg-4">
                      <div className="row">
                        <div className="col-md-12">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                              Tiêu đề tin tuyển dụng
                              <span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                              Tiêu đề tin tuyển dụng nên chứa tên vị trí, chức
                              danh cần tuyển.
                            </div>
                            <input
                              id="input-address"
                              className="form-control"
                              type="text"
                              name="title"
                              onChange={handleChangeRecruiment}
                              value={detailRecruiment.title}
                              ref={register({ required: true })}
                            />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-city"
                            >
                              Ngành nghề<span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                              Lựa chọn ngành nghề để tiếp cận đúng ứng viên.
                            </div>
                            <select
                              name="id_category"
                              type="text"
                              onChange={handleChangeRecruiment}
                              value={detailRecruiment.id_category}
                              id="input-city"
                              ref={register({ required: true })}
                              className="form-control"
                            >
                              {categories &&
                                Array.isArray(categories) &&
                                categories.map((item) => (
                                  <option value={item.id}>{item.name}</option>
                                ))}
                            </select>
                          </div>
                        </div>
                        <div className="col-lg-6">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                              Vị trí<span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2  icon-aler-required"></i>
                              Cấp bậc cần tuyển.
                            </div>
                            <input
                              type="text"
                              name="type"
                              onChange={handleChangeRecruiment}
                              value={detailRecruiment.type}
                              ref={register({ required: true })}
                              id="input-postal-code"
                              className="form-control"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-lg-6">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-city"
                            >
                              Số lượng<span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2  icon-aler-required"></i>
                              Số lượng cần tuyển.
                            </div>
                            <input
                              type="text"
                              onChange={handleChangeRecruiment}
                              value={detailRecruiment.total}
                              name="total"
                              ref={register({ required: true })}
                              id="input-city"
                              className="form-control"
                            />
                          </div>
                        </div>
                        <div className="col-lg-6">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                              Hình thức làm việc
                              <span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                              Chọn hình thức làm việc để xác định đúng đối tượng
                              cần tuyển dụng.
                            </div>
                            <input
                              name="description"
                              onChange={handleChangeRecruiment}
                              value={detailRecruiment.description}
                              ref={register({ required: true })}
                              type="text"
                              id="input-postal-code"
                              className="form-control"
                            />
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-12">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                              Mức lương<span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                              Thêm mức lương để ứng viên có thể lọc và tìm thấy
                              tin tuyển dụng.
                            </div>

                            <select
                              onChange={(e) => setWage(e.target.value)}
                              className="form-control"
                              name="show"
                              ref={register({ required: true })}
                              
                            >
                              <option>----------</option>
                              <option value="1">
                                Hiển thị mức lương cụ thể
                              </option>
                              <option value="2">
                                Hiển thị mức lương thương lượng
                              </option>
                            </select>
                          </div>
                        </div>
                      </div>
                      <div
                        className="row"
                        style={
                          detailRecruiment.salary
                            ? { display: "block" }
                            : { display: "none" }
                        }
                      >
                        <div className="col-lg-12">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-city"
                            >
                              Mức lương (triệu đồng)
                              <span className="text-orange">*</span>
                            </label>
                            <div className="input-group input-group-merge">
                              <input
                                type="number"
                                name="salary"
                                value={detailRecruiment.salary}
                                ref={register({
                                  required: +wage === 1 ? true : false,
                                })}
                                className="form-control"
                                aria-describedby="basic-addon2"
                              />
                              <div className="input-group-append">
                                <span
                                  className="input-group-text"
                                  id="basic-addon2"
                                >
                                  Triệu đồng
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col-md-12">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                              Yêu cầu công việc
                              <span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                              Các yêu cầu đối với ứng viên.
                            </div>
                            <textarea
                              onChange={handleChangeRecruiment}
                              value={detailRecruiment.request}
                              name="request"
                              ref={register({ required: true })}
                              className="form-control"
                              
                              rows="3"
                            ></textarea>
                          </div>
                        </div>
                      </div>
                      <div className="row">
                        <div className="form-group">
                          <label
                            className="form-control-label"
                            htmlFor="input-city"
                          >
                            Địa chỉ làm việc
                            <span className="text-orange">*</span>
                          </label>
                          <div className="alert_1 alert-required" role="alert">
                            <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                            Chọn địa chỉ làm việc. Nhập địa chỉ càng cụ thể
                            chính xác thì tin của bạn càng uy tín và có nhiều
                            ứng viên.
                          </div>
                        </div>
                        <div className="col-lg-4">
                          <div className="form-group">
                            <select
                              onChange={() => {
                                handleChangeRecruiment();
                                handleChangeAddress();
                              }}
                              name="province_or_city"
                              ref={register({ required: true })}
                              className="form-control"
                            >
                              <option value="">Chọn thành phố</option>
                              {DataAddress.map((thanhpho, index) => (
                                <option
                                  key={`thanh_pho_${thanhpho[1]}`}
                                  value={index}
                                >
                                  {thanhpho[1]}
                                </option>
                              ))}
                            </select>
                          </div>
                        </div>
                        <div className="col-lg-4">
                          <div className="form-group">
                            <select
                              disabled={
                                detailRecruiment.province_or_city ||
                                detailRecruiment.province_or_city === 0
                                  ? false
                                  : true
                              }
                              onChange={() => {
                                handleChangeRecruiment();
                                handleChangeAddress();
                              }}
                              name="district"
                              ref={register({ required: true })}
                              className="form-control"
                              
                            >
                              <option value="">Quận/Huyện</option>
                              {detailRecruiment.province_or_city ||
                                (detailRecruiment.province_or_city === 0 &&
                                  DataAddress[
                                    detailRecruiment.province_or_city
                                  ][4].map((huyen, index) => (
                                    <option
                                      key={`tinh_thanh${huyen[0]}`}
                                      value={index}
                                    >
                                      {huyen[1]}
                                    </option>
                                  )))}
                            </select>
                          </div>
                        </div>
                        <div className="col-lg-4">
                          <div className="form-group">
                            <select
                              onChange={() => {
                                handleChangeRecruiment();
                                handleChangeAddress();
                              }}
                              name="ward"
                              disabled={
                                detailRecruiment.district ? false : true
                              }
                              ref={register({ required: true })}
                              className="form-control"
                              
                            >
                              <option value="">Xã/Phường</option>
                              {detailRecruiment.district &&
                                DataAddress[
                                  detailRecruiment.province_or_city
                                ][4][detailRecruiment.district][4].map(
                                  (xa, index) => (
                                    <option key={`xa_${xa[0]}`} value={index}>
                                      {xa[1]}
                                    </option>
                                  )
                                )}
                            </select>
                          </div>
                        </div>
                      </div>
                      <div className="form-group">
                        <input
                          onChange={() => {
                            handleChangeRecruiment();
                            handleChangeAddress();
                          }}
                          name="details_address"
                          ref={register({ required: true })}
                          type="text"
                          value={detailRecruiment.details_address}
                          className="form-control"
                          
                        />
                      </div>
                      <div className="form-group">
                        <label className="form-control-label">
                          Tọa độ trên Googlemap
                          <span className="text-orange">*</span>
                          <span>
                            {" "}
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              width="16"
                              height="16"
                              fill="currentColor"
                              className="bi bi-geo-alt"
                              viewBox="0 0 16 16"
                            >
                              <path d="M12.166 8.94c-.524 1.062-1.234 2.12-1.96 3.07A31.493 31.493 0 0 1 8 14.58a31.481 31.481 0 0 1-2.206-2.57c-.726-.95-1.436-2.008-1.96-3.07C3.304 7.867 3 6.862 3 6a5 5 0 0 1 10 0c0 .862-.305 1.867-.834 2.94zM8 16s6-5.686 6-10A6 6 0 0 0 2 6c0 4.314 6 10 6 10z" />
                              <path d="M8 8a2 2 0 1 1 0-4 2 2 0 0 1 0 4zm0 1a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
                            </svg>
                          </span>
                        </label>
                        <div className="alert_1 alert-required" role="alert">
                          <a
                            rel="noreferrer"
                            data-toggle="modal"
                            data-target="#exampleModalLong"
                            href="/#"
                            className="flaticon-exclamation-1 pr-2 icon-aler-required"
                          >
                            {" "}
                          </a>
                          Click icon để xem hướng dẫn lấy tọa độ giúp tiếp cận
                          ứng viên quanh khu vực
                        </div>
                        <div>
                          <div
                            className="modal fade"
                            id="exampleModalLong"
                            tabIndex={-1}
                            role="dialog"
                            aria-labelledby="exampleModalLongTitle"
                            aria-hidden="true"
                          >
                            <div
                              className="modal-dialog"
                              style={{
                                maxWidth: "65%",
                                overflowY: "scroll",
                                height: "100%",
                              }}
                              role="document"
                            >
                              <div className="modal-content">
                                <div className="modal-header">
                                  <h5
                                    className="modal-title"
                                    id="exampleModalLongTitle"
                                  >
                                    Hướng dẫn lấy tọa độ trên Googlemap
                                  </h5>
                                  <button
                                    type="button"
                                    className="close"
                                    data-dismiss="modal"
                                    aria-label="Close"
                                  >
                                    <span aria-hidden="true">×</span>
                                  </button>
                                </div>
                                <div className="modal-body">
                                  <p>
                                    1. Truy cập{" "}
                                    <a
                                      rel="noreferrer"
                                      href="https://www.google.com/maps/"
                                      target="_blank"
                                    >
                                      https://www.google.com/maps/
                                    </a>{" "}
                                    và tìm kiếm vị trí của công ty bạn.
                                  </p>
                                  <img
                                    src="/assets/map_one.png"
                                    alt="img"
                                    style={{ width: "100%", height: "100%" }}
                                  />
                                  <p className="my-4">
                                    2. Click chuột vào vị trí có màu đỏ trên bản
                                    đồ và click chuột phải để coppy tọa đồ.{" "}
                                  </p>
                                  <img
                                    src="/assets/map_two.png"
                                    alt="img"
                                    style={{ width: "100%", height: "100%" }}
                                  />
                                </div>
                                <div className="modal-footer">
                                  <button
                                    type="button"
                                    className="btn btn-secondary"
                                    data-dismiss="modal"
                                  >
                                    Close
                                  </button>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <input
                          name="lat_lng"
                          ref={register({ required: true })}
                          type="text"
                          id="input-city"
                          onChange={handleChangeRecruiment}
                          value={detailRecruiment.lat_lng}
                          className="form-control"
                        />
                      </div>
                      <div className="form-group">
                        <input
                          value={
                            addressDetail && address.details_address
                              ? addressDetail
                              : ""
                          }
                          type="text"
                          disabled
                          className="form-control"
                          
                        />
                      </div>
                      <div className="row">
                        <div className="col-lg-6">
                          <div className="form-group">
                            <label className="form-control-label">
                              Doanh nghiệp<span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                              Lựa chọn ngành nghề để tiếp cận đúng ứng viên.
                            </div>
                            <input
                              onChange={handleChangeRecruiment}
                              value={detailRecruiment.company}
                              type="text"
                              name="company"
                              ref={register({ required: true })}
                              id="input-city"
                              className="form-control"
                            />
                          </div>

                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                              Thêm liên hệ
                              <span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2  icon-aler-required"></i>
                              Bạn có thể thêm liên hệ của nhà tuyển dụng.
                            </div>
                            <div className="form-group">
                              <div className="input-group">
                                <div className="input-group-prepend">
                                  <span
                                    className="input-group-text"
                                    id="basic-addon3"
                                  >
                                    Họ và tên
                                  </span>
                                </div>
                                <input
                                  type="text"
                                  onChange={handleChangeRecruiment}
                                  value={detailRecruiment.name_contact}
                                  className="form-control"
                                  id="basic-url"
                                  ref={register({ required: true })}
                                  name="name_contact"
                                  aria-describedby="basic-addon3"
                                />
                              </div>
                            </div>
                            <div className="form-group">
                              <div className="input-group">
                                <div className="input-group-prepend">
                                  <span
                                    className="input-group-text"
                                    id="basic-addon3"
                                  >
                                    Email
                                  </span>
                                </div>
                                <input
                                  type="text"
                                  onChange={handleChangeRecruiment}
                                  value={detailRecruiment.email_contact}
                                  className="form-control"
                                  id="basic-url"
                                  ref={register({ required: true })}
                                  name="email_contact"
                                  aria-describedby="basic-addon3"
                                />
                              </div>
                            </div>

                            <div className="form-group">
                              <div className="input-group">
                                <div className="input-group-prepend">
                                  <span
                                    className="input-group-text"
                                    id="basic-addon3"
                                  >
                                    Số điện thoại
                                  </span>
                                </div>
                                <input
                                  onChange={handleChangeRecruiment}
                                  value={detailRecruiment.phone_contact}
                                  type="text"
                                  className="form-control"
                                  id="basic-url"
                                  ref={register({ required: true })}
                                  name="phone_contact"
                                  aria-describedby="basic-addon3"
                                />
                              </div>
                            </div>
                          </div>
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                              Phúc lợi
                              <span className="text-orange">*</span>
                            </label>

                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                              Thêm mô tả các phúc lợi mà ứng viên nhận được nếu
                              trúng tuyển.
                            </div>
                            <textarea
                              onChange={handleChangeRecruiment}
                              value={detailRecruiment.benefit}
                              name="benefit"
                              ref={register({ required: true })}
                              className="form-control"
                              
                              rows="3"
                            ></textarea>
                          </div>
                        </div>

                        <div className="col-lg-6">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-country"
                            >
                              Ảnh đại diện tin tuyển dụng
                              <span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2  icon-aler-required"></i>
                              Thêm hình ảnh đại diện cho tin tuyển dụng của bạn.
                            </div>
                            <label
                              htmlFor="upload-photo"
                              className="btn btn-neutral btn-icon"
                            >
                              <span className="btn-inner--icon">
                                <i className="fa fa-upload"></i>
                              </span>

                              <span className="btn-inner--text">Thêm ảnh</span>
                            </label>
                            <input
                              style={{
                                opacity: "0",
                                position: "absolute",
                                zIndex: "-1",
                              }}
                              type="file"
                              accept="image/png, image/jpeg, image/jpg"
                              name="image"
                              ref={register({ required: false })}
                              onChange={onImageChange}
                              id="upload-photo"
                              className="form-control"
                            />
                          </div>
                          <div className="form-group">
                            <img
                              alt="khai dep trai"
                              src={image}
                              style={{
                                maxHeight: "30rem",
                                maxWidth: "30rem",
                                border: "1px solid red",
                                borderRadius: "5px",
                              }}
                            />
                          </div>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col-md-12">
                          <div className="form-group">
                            <label
                              className="form-control-label"
                              htmlFor="input-address"
                            >
                              Hạn nộp hồ sơ
                              <span className="text-orange">*</span>
                            </label>
                            <div
                              className="alert_1 alert-required"
                              role="alert"
                            >
                              <i className="flaticon-exclamation-1 pr-2 icon-aler-required"></i>
                              Thêm hạn nộp hồ sơ để ứng viên nhanh chóng ứng
                              tuyển.
                            </div>
                            <input
                              type="date"
                              name="end"
                              onChange={handleChangeRecruiment}
                              value={detailRecruiment.end}
                              ref={register({ required: true })}
                              className="form-control mb-2"
                              
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <hr className="my-1 "></hr>
                  <div className="card-header bg-transparent">
                    <div className="row align-items-center">
                      <div className="col">
                        <button
                          className="btn btn-icon btn-primary"
                          type="submit"
                        >
                          <span className="btn-inner--icon">
                            <i className="fa fa-plus"></i>
                          </span>
                          <span className="btn-inner--text">Lưu lại</span>
                        </button>
                      </div>
                    </div>
                  </div>
                </form>
              ) : (
                ""
              )}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default SuaTinTuyenDung;
