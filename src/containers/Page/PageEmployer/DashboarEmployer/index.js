import React from "react";
import { Helmet } from "react-helmet";
import { Link } from "react-router-dom";

function DashboarEmployer() {
  return (
    <div className="m-3">
      <Helmet>
        <title>Tổng quan</title>
      </Helmet>
      <div className="row">
        {/* <div className="col-xl-3 col-md-6">
          <div className="card card-stats">
            <Link className="card-body" to="/employer/ung-tuyen/dang-cho">
              <div className="row">
                <img
                  alt="images"
                  src="/assets/img/quan-ly-ung-vien.svg"
                  className="img-fluid img-center"
                  style={{ height: "10rem" }}
                />
              </div>
              <h5 className="mt-3 mb-0 text-uppercase text-center mb-0">
                Quản lý ứng viên
              </h5>
            </Link>
          </div>
        </div> */}
        <div className="col-xl-3 col-md-6">
          <div className="card card-stats">
            <Link to="/employer/tin-tuyen-dung/hoat-dong" className="card-body">
              <div className="row">
                <img
                  alt="images"
                  src="/assets/img/quan-ly-bai-viet.svg"
                  className="img-fluid img-center"
                  style={{ height: "10rem" }}
                />
              </div>
              <h5 className="mt-3 mb-0 text-uppercase text-center mb-0">
                Quản lý Bài viết
              </h5>
            </Link>
          </div>
        </div>
        <div className="col-xl-3 col-md-6">
          <div className="card card-stats">
            <Link to="/employer/tim-ung-vien/kho-ung-vien" className="card-body">
              <div className="row">
                <img
                  alt="images"
                  src="/assets/img/tim-kiem-ung-vien.svg"
                  className="img-fluid img-center"
                  style={{ height: "10rem" }}
                />
              </div>
              <h5 className="mt-3 mb-0 text-uppercase text-center mb-0">
                Tìm kiếm ứng viên
              </h5>
            </Link>
          </div>
        </div>
        <div className="col-xl-3 col-md-6">
          <div className="card card-stats">
            <Link to="/employer/cong-ty/danh-sach" className="card-body">
              <div className="row">
                <img
                  alt="images"
                  src="/assets/img/them-cong-ty.svg"
                  className="img-fluid img-center"
                  style={{ height: "10rem" }}
                />
              </div>
              <h5 className="mt-3 mb-0 text-uppercase text-center mb-0">
                Tạo và quản lý công ty
              </h5>
            </Link>
          </div>
        </div>
      </div>
    </div>
  );
}

export default DashboarEmployer;
