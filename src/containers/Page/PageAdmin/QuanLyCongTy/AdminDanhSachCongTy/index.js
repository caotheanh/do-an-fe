import axios from "axios";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { api } from "../../../../../api";
import Image from "../../../../../components/Common/Image";
import FormatDate from "../../../../../utils/FormatDate";
function AdminDanhSachCongTy() {
  const [listCompany, setListCompany] = useState([]);
  useEffect(() => {
    axios({
      method: "get",
      url: `${api}/company`,
    }).then((res) => {
      setListCompany(res.data);
    });
  }, []);
  return (
    <div>
      <div className="header bg-primary pb-6">
        <div className="container-fluid">
          <div className="header-body">
            <div className="row align-items-center py-4">
              <div className="col-lg-6 col-7"></div>
              <div className="col-lg-6 col-5 text-right"></div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid mt--6">
        <div className="row">
          <div className="col">
            <div className="card">
              <div className="card-header border-0">
                <h3 className="mb-0">Danh sách công ty</h3>
              </div>
              <div className="table-responsive">
                <table className="table align-items-center table-flush">
                  <thead className="thead-light">
                    <tr>
                      <th scope="col" className="sort" data-sort="name">
                        Ảnh
                      </th>
                      <th scope="col" className="sort" data-sort="name">
                        Tên Công ty
                      </th>
                      <th scope="col" className="sort" data-sort="budget">
                        Lĩnh vực họat động
                      </th>
                      <th scope="col" className="sort" data-sort="status">
                        Email
                      </th>
                      <th scope="col">Điện thoại</th>
                      <th scope="col" className="sort" data-sort="completion">
                        Quy mô (Người)
                      </th>
                      <th scope="col">Ngày tạo</th>

                      <th scope="col">Hành động</th>
                    </tr>
                  </thead>
                  <tbody className="list">
                    {listCompany &&
                      Array.isArray(listCompany) &&
                      listCompany.length > 0 &&
                      listCompany.map((item) => (
                        <tr>
                          <th scope="row">
                            <Image
                              url={`${api}/companyImg/${item.avt}`}
                              size="3rem"
                            />
                          </th>
                          <th scope="row">
                            <Link
                            target="_blank"
                              to={`/xem-cong-ty/${item.id}`}
                              className="media-body"
                            >
                              <span className="name mb-0 text-sm">
                                {item.name}
                              </span>
                            </Link>
                          </th>
                          <td className="budget"> IT- Phần cứng</td>
                          <td>{item.email}</td>

                          <td>
                            <div className="d-flex align-items-center">
                              <span className="completion mr-2">
                                {item.phone}
                              </span>
                            </div>
                          </td>
                          <td>
                            <span className="status">{item.numberMember}</span>
                          </td>
                          <td>
                            <span className="status">
                              {FormatDate(item.created_at)}
                            </span>
                          </td>
                          <td className="text-right">
                            <button
                              className="btn btn-icon btn-primary"
                              type="button"
                            >
                              <span className="btn-inner--icon">
                                <i className="ni ni-atom"></i>
                              </span>
                            </button>
                          </td>
                        </tr>
                      ))}
                  </tbody>
                </table>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AdminDanhSachCongTy;
