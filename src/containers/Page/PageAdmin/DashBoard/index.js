import { Helmet } from "react-helmet";
import CardThongKeUi from "../../../../components/DashBoard/CardThongKeUI";
import ThongkeUngTuyenTin from "./ThongKeUngTuyenTin";
import { api } from "../../../../api";
import { useEffect, useState } from "react";
import axios from "axios";
function AdminDashboard() {
  const [statisticsUser, setStatisticsUser] = useState({});
  const [statisticsRecruitment, setStatisticsRecruitment] = useState(0);
  useEffect(() => {
    axios.get(`${api}/admin/statistics-user/4`).then((res) => {
      setStatisticsUser(res.data);
    });
    axios
      .get(`${api}/admin/get-recruitment-month/4`)
      .then((res) => setStatisticsRecruitment(res.data));
  }, []);
  console.log(statisticsUser, statisticsRecruitment);
  const dataThongke = [
    {
      title: "Tài khoản kích hoạt tháng này",
      total: statisticsUser.userActiveInMonth || 0,
      icon: "fa fa-door-open",
    },
    {
      title: "Tài khoản đăng kí tháng này",
      total: statisticsUser.userActiveInMonth || 0,
      icon: "fa fa-newspaper",
    },
    {
      title: "Tổng tài khoản trên hệ thống",
      total: statisticsUser.totalUser || 0,
      icon: "ni ni-money-coins",
    },
    {
      title: "Số tin tuyển dụng tháng này",
      total: statisticsRecruitment || 0,
      icon: "fa fa-user-ninja",
    },
  ];
  return (
    <>
      <Helmet>
        <title>Tổng quan</title>
      </Helmet>
      <div
        className="header pb-6"
        style={{
          backgroundImage: `url(/assets/img/background.jpg)`,
          backgroundPosition: "center",
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
        }}
      >
        <div className="container-fluid">
          <div className="header-body">
            <div className="row align-items-center py-4"></div>
            <div className="row">
              {dataThongke.map((item) => (
                <CardThongKeUi key={item.title} data={item} />
              ))}
            </div>
          </div>
        </div>
      </div>
      <div className="row m-3 mt--6">
        {/* <ThongkeUngTuyenTin /> */}
      </div>
    </>
  );
}

export default AdminDashboard;
