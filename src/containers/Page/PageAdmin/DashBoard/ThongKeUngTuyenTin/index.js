import React from "react";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import { tinTuyenDung } from "./dataFake";

function ThongkeUngTuyenTin() {
  return (
    <div className="col-xl-6">
      <div className="card">
        <div className="card-header border-0">
          <div className="row align-items-center">
            <div className="col">
              <h3 className="mb-0">Thống kê lượt ứng tuyển tin tuyển dụng</h3>
            </div>
            <div className="col text-right">
              <a href="#!" className="btn btn-sm btn-outline-warning">
                Xem tất cả
              </a>
            </div>
          </div>
        </div>
        <div className="table-responsive">
          <table className="table align-items-center table-flush">
            <thead className="thead-light">
              <tr>
                <th scope="col">Tin tuyển dụng</th>
                <th scope="col">Ứng tuyển</th>
                <th scope="col"></th>
              </tr>
            </thead>
            <tbody>
              {tinTuyenDung &&
                tinTuyenDung.map((item) => (
                  <tr>
                    <th scope="row">
                      <Link>
                        {item.title.length > 50
                          ? item.title.substr(0, 50) + "..."
                          : item.title.substr(0, 30)}
                      </Link>
                    </th>
                    <td>{item.apply}</td>
                    <td>
                      <div className="d-flex align-items-center">
                        <span className="mr-2">{tinTuyenDung[0].apply}</span>
                        <div>
                          <div className="progress">
                            <div
                              className="progress-bar bg-gradient-danger"
                              role="progressbar"
                              aria-valuenow="60"
                              aria-valuemin="0"
                              aria-valuemax="100"
                              style={{ width: "60%" }}
                            ></div>
                          </div>
                        </div>
                      </div>
                    </td>
                  </tr>
                ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

export default ThongkeUngTuyenTin;
