import axios from "axios";
import React from "react";
import { useForm } from "react-hook-form";
import NotificationManager from "react-notifications/lib/NotificationManager";
import { api } from "../../../../../api";
function AdminThemMoiDanhMuc() {
  const { handleSubmit, register, errors } = useForm();
  const addCategory = (data) => {
    axios
      .post(`${api}/category`, data)
      .then(() =>
        NotificationManager.success(
          "Thêm danh mục thành công.",
          "Thành công",
          3000
        )
      );
  };
  return (
    <div>
      <div className="col-xl-12">
        <div className="card">
          <div className="card-header">
            <div className="row align-items-center">
              <div className="col-8">
                <h3 className="mb-0">Thêm công danh mục</h3>
              </div>
            </div>
          </div>
          <div className="card-body">
            <form onSubmit={handleSubmit(addCategory)}>
              <div className="row">
                <div className="col-lg-12">
                  <div className="form-group">
                    <label className="form-control-label" htmlFor="input-username">
                      Tên danh mục
                    </label>
                    <input
                      type="text"
                      ref={register({ required: true })}
                      id="input-username"
                      name="name"
                      className="form-control"
                    />
                    {errors.name && <small className="text-danger">Vui lòng nhập tên danh mục</small>}
                  </div>
                </div>
              </div>
              <button
                className="btn btn-icon btn-outline-primary"
                type="submit"
              >
                <span className="btn-inner--icon">
                  <i className="ni ni-bag-17"></i>
                </span>
                <span className="btn-inner--text">Thêm danh mục</span>
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AdminThemMoiDanhMuc;
