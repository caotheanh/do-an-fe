import axios from "axios";
import React, { useEffect, useState } from "react";
import NotificationManager from "react-notifications/lib/NotificationManager";
import { api } from "../../../../../api";
import FormatDate from "../../../../../utils/FormatDate";
function AdminDanhSachDanhMuc() {
  const [categories, setCategories] = useState([]);
  useEffect(() => {
    axios.get(`${api}/category`).then((res) => setCategories(res.data));
  }, []);
  const handleDeleteCategory = (id) => {
    axios.delete(`${api}/category/${id}`).then((res) => {
      setCategories(categories.filter((cate) => cate.id !== id));
      NotificationManager.success(
        "Xóa danh mục thành công.",
        "Thành công",
        3000
      );
    });
  };
  return (
    <div>
      <div className="header bg-primary pb-6">
        <div className="container-fluid">
          <div className="header-body">
            <div className="row align-items-center py-4">
              <div className="col-lg-6 col-7"></div>
              <div className="col-lg-6 col-5 text-right"></div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid mt--6">
        <div className="row">
          <div className="col">
            <div className="card">
              <div className="card-header border-0">
                <h3 className="mb-0">Danh sách danh mục</h3>
              </div>
              <div className="table-responsive">
                <table className="table align-items-center table-flush">
                  <thead className="thead-light">
                    <tr>
                      <th scope="col" className="sort" data-sort="name">
                        ID
                      </th>
                      <th scope="col" className="sort" data-sort="name">
                        Tên danh mục
                      </th>
                      <th scope="col" className="sort" data-sort="budget">
                        Ngày tạo
                      </th>
                      <th scope="col" className="text-right">
                        Hành động
                      </th>
                    </tr>
                  </thead>
                  <tbody className="list">
                    {categories &&
                      categories.length > 0 &&
                      categories.map((category) => (
                        <tr>
                          <td>{category.id}</td>
                          <td>
                            <span className="completion mr-2">
                              {category.name}
                            </span>
                          </td>
                          <td>{FormatDate(category.created_at)} </td>
                          <td className="text-right">
                            <button
                              onClick={() => handleDeleteCategory(category.id)}
                              className="btn btn-icon btn-outline-primary"
                              type="button"
                            >
                              <span className="btn-inner--icon">
                                <i className="fa fa-trash-alt"></i>
                              </span>
                            </button>
                          </td>
                        </tr>
                      ))}
                  </tbody>
                </table>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AdminDanhSachDanhMuc;
