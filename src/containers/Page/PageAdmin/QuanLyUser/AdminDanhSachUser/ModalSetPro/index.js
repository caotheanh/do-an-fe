import React from "react";

function ModalSetPro({ id, email, setProUser }) {
  const handleSetPro = (id, email, status) => {
    document.querySelector(`.closeModalSetPro${id}`).click();
    setProUser(email, status);
  };
  return (
    <div
      className="modal fade"
      id={`set_pro_${id}`}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Nâng cấp tài khoản
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div className="d-flex justify-content-lg-center">
              <button
                type="button"
                className={`btn btn-secondary closeModalSetPro${id}`}
                data-dismiss="modal"
              >
                Hủy
              </button>
              <button
                type="button"
                onClick={() => handleSetPro(id, email, 1)}
                className={`btn btn-outline-success`}
              >
                Nâng cấp lên pro
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModalSetPro;
