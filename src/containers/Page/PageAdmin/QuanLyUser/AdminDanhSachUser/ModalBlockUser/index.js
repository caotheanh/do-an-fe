import React from "react";

function ModalBlockUser({ id, blockUser, isBlock }) {
  const handleBlockUser = (id, status) => {
    document.querySelector(`.closeModalBlok${id}`).click();
    blockUser(id, status);
  };
  return (
    <div
      className="modal fade"
      id={`block_user_${id}`}
      tabIndex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div className="modal-dialog modal-dialog-centered" role="document">
        <div className="modal-content">
          <div className="modal-header">
            <h5 className="modal-title" id="exampleModalLabel">
              Chặn người này ?
            </h5>
            <button
              type="button"
              className="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div className="modal-body">
            <div className="d-flex justify-content-lg-center">
              <button
                type="button"
                className={`btn btn-secondary closeModalBlok${id}`}
                data-dismiss="modal"
              >
                Hủy
              </button>
              <button
                type="button"
                onClick={() => handleBlockUser(id, !isBlock)}
                className={`btn ${
                  !isBlock ? "btn-outline-danger" : "btn-outline-success"
                }`}
              >
                {isBlock ? "Bỏ chặn" : "Chặn"}
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModalBlockUser;
