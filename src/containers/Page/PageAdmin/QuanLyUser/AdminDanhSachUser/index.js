import axios from "axios";
import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import NotificationManager from "react-notifications/lib/NotificationManager";
import { Link } from "react-router-dom/cjs/react-router-dom.min";
import { api } from "../../../../../api";
import FormatDate from "../../../../../utils/FormatDate/index";
import ModalBlockUser from "./ModalBlockUser";
import ModalSetPro from "./ModalSetPro";
function AdminDanhSachUser() {
  const [listUser, setlistUser] = useState([]);
  const { handleSubmit, register } = useForm();
  useEffect(() => {
    axios.get(`${api}/admin/get-all-user`).then((res) => setlistUser(res.data));
  }, []);
  const handleSearchUser = (data) => {
    axios
      .post(`${api}/admin/search-user`, { email: data.email })
      .then((res) => setlistUser([res.data]));
  };
  const blockUser = (id, status) => {
    axios
      .put(`${api}/admin/block-user`, { id: id, isBlock: status })
      .then(() =>
        NotificationManager.success(
          status ? "Chặn thành công." : "Bỏ chặn thành công",
          "Thành công",
          3000
        )
      );
  };
  const setProUser = (email, status) => {
    axios
      .post(`${api}/admin/set-pro`, { email: email, ispro: status })
      .then(() =>
        NotificationManager.success(
          status
            ? "Nâng cấp tài khoản lên cao cấp thành công."
            : "Xóa tài khoản cao cấp thành công",
          "Thành công",
          3000
        )
      );
  };
  return (
    <div>
      <div className="header bg-primary pb-6">
        <div className="container-fluid">
          <div className="header-body">
            <div className="row align-items-center py-4">
              <div className="col-lg-6 col-7"></div>
              <div className="col-lg-6 col-5 text-right"></div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid mt--6">
        <div className="row">
          <div className="col">
            <div className="card">
              <div className="card-header border-0 row">
                <h3 className="mb-0  col-lg-6">Danh sách tài khoản</h3>
                <form
                  onSubmit={handleSubmit(handleSearchUser)}
                  className="input-group mb-3 col-lg-6"
                >
                  <input
                    type="text"
                    name="email"
                    ref={register({ required: true })}
                    className="form-control"
                    placeholder="Email user"
                  />
                  <button className="btn btn-outline-primary">Tìm kiếm</button>
                </form>
              </div>
              <div className="table-responsive">
                <table className="table align-items-center table-flush">
                  <thead className="thead-light">
                    <tr>
                      {/* <th scope="col" className="sort" data-sort="name">
                        Ảnh
                      </th>
                      <th scope="col" className="sort" data-sort="name">
                        Tên ứng viên
                      </th> */}
                      <th scope="col" className="sort" data-sort="budget">
                        Email
                      </th>
                      <th scope="col" className="sort" data-sort="status">
                        Tìm viẹc
                      </th>
                      <th scope="col">Chức vụ</th>
                      <th scope="col">Ngày tạo</th>
                      <th scope="col" className="sort" data-sort="completion">
                        Trạng thái
                      </th>
                      <th scope="col">Tài khoản cao cấp</th>
                      <th scope="col">Hành động</th>
                    </tr>
                  </thead>
                  <tbody className="list">
                    {listUser &&
                      Array.isArray(listUser) &&
                      listUser.map((item) => (
                        item.type!==3&&<tr key={item.id}>
                          {/* <th scope="row">
                            <div
                              className="image-job  mr-3"
                              style={{
                                backgroundImage:
                                  "url(https://www.glassdoor.com/blog/app/uploads/sites/2/iStock-585051592-1-750x450.jpg)",
                              }}
                            ></div>
                          </th>
                          <th scope="row">
                            <div className="media-body">
                              <span className="name mb-0 text-sm">
                                Phạm văn khải
                              </span>
                            </div>
                          </th> */}
                          <td className="budget">{item.email}</td>
                          <td>Bật </td>
                          <td>
                            <span className="completion mr-2">
                              {
                                ["Tuyển dụng", "Người tìm việc", "Quản trị"][
                                  item.type - 1
                                ]
                              }
                            </span>
                          </td>
                          <td>{FormatDate(item.create_at)}</td>
                          <td>
                            {item.is_block ? (
                              <span className=" badge-dot mr-4">
                                <i className="bg-warning"></i>
                                <span className="status">Đã khóa</span>
                              </span>
                            ) : (
                              <span className=" badge-dot mr-4">
                                <i className="bg-success"></i>
                                <span className="status">Đang hoạt động</span>
                              </span>
                            )}
                          </td>
                          <td>
                            <input
                              type="checkbox"
                              class="custom-control-input"
                              id="customCheck1"
                              checked={item.isPro === 1 ? true : false}
                            />
                            <div class="custom-control custom-checkbox">
                              <input
                                type="checkbox"
                                class="custom-control-input"
                                id="customCheck1"
                                checked={item.isPro === 1 ? true : false}
                              />
                              <label
                                class="custom-control-label"
                                for="customCheck1"
                              >
                                Pro
                              </label>
                            </div>
                          </td>

                          <td className="text-right">
                            <div className="dropdown">
                              <Link
                                className="btn btn-sm btn-outline-default "
                                role="button"
                                data-toggle="dropdown"
                                aria-haspopup="true"
                                aria-expanded="false"
                              >
                                <i className="fa fa-sliders-h pr-1"></i>
                                Hành động
                              </Link>
                              <div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                {item.type === 1 && (
                                  <Link
                                    className="dropdown-item"
                                    data-toggle="modal"
                                    data-target={`#set_pro_${item.id}`}
                                  >
                                    Nâng cấp tài khoản
                                  </Link>
                                )}
                                <Link
                                  className={`dropdown-item ${
                                    !item.is_block
                                      ? "text-danger"
                                      : "text-success"
                                  }`}
                                  data-toggle="modal"
                                  data-target={`#block_user_${item.id}`}
                                >
                                  {!item.is_block ? "Chặn" : "Bỏ chặn"}
                                </Link>
                              </div>
                            </div>
                          </td>
                          <ModalBlockUser
                            id={item.id}
                            blockUser={blockUser}
                            isBlock={item.is_block}
                          />
                          <ModalSetPro
                            id={item.id}
                            email={item.email}
                            setProUser={setProUser}
                          />
                        </tr>
                      ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AdminDanhSachUser;
