import axios from "axios";
import React, { useEffect, useState } from "react";
import { Fragment } from "react";
import { Link } from "react-router-dom";
import { api } from "../../../../../api";
import Image from "../../../../../components/Common/Image";
import FormatDate from "../../../../../utils/FormatDate";
import FormatNumber from "../../../../../utils/FormatNumber";
import ModalDeleteRecruiment from "./ModalDeleteRecruiment";
function AdminDanhSachTin() {
  const [job, setJob] = useState([]);
  useEffect(() => {
    axios.get(`${api}/recruitment`).then((res) => {
      setJob(res.data);
    });
  }, []);
  const handleDeleteRecruitment = () => {
    axios.post(`${api}/admin/delete-recruitment`).then((res) => {
      setJob(res.data);
    });
  };
  return (
    <div>
      <div className="header bg-primary pb-6">
        <div className="container-fluid">
          <div className="header-body">
            <div className="row align-items-center py-4">
              <div className="col-lg-6 col-7"></div>
              <div className="col-lg-6 col-5 text-right"></div>
            </div>
          </div>
        </div>
      </div>
      <div className="container-fluid mt--6">
        <div className="row">
          <div className="col">
            <div className="card">
              <div className="card-header border-0">
                <h3 className="mb-0">Danh sách sách tin tuyển dụng</h3>
              </div>
              <div className="table-responsive">
                <table className="table align-items-center table-flush">
                  <thead className="thead-light">
                    <tr>
                      <th scope="col" className="sort" data-sort="name">
                        Ảnh
                      </th>
                      <th scope="col" className="sort" data-sort="name">
                        Tiêu đề tin
                      </th>
                      <th scope="col" className="sort" data-sort="budget">
                        Công ty
                      </th>
                      <th scope="col" className="sort" data-sort="status">
                        Số lượng
                      </th>
                      <th scope="col">Ngày hết hạn</th>
                      <th scope="col" className="sort" data-sort="completion">
                        Lương
                      </th>
                      <th scope="col">Ngày tạo</th>
                      <th scope="col">Trạng thái</th>
                      <th scope="col">Hành động</th>
                    </tr>
                  </thead>
                  <tbody className="list">
                    {job &&
                      Array.isArray(job) &&
                      job.length > 0 &&
                      job.map((item) => (
                        <Fragment key={item.id}>
                          <tr>
                            <th scope="row">
                              <Image url={`${api}/${item.img}`} size={"4rem"} />
                            </th>
                            <td>{item.title}</td>
                            <th scope="row">
                              <div className="media-body">
                                <span className="name mb-0 text-sm">
                                  {item.company}
                                </span>
                              </div>
                            </th>
                            <td className="budget"> {item.total}</td>

                            <td>
                              <div className="d-flex align-items-center">
                                <span className="completion mr-2">
                                  {FormatDate(item.end)}
                                </span>
                              </div>
                            </td>
                            <td>
                              <span className="status">
                                {!isNaN(item.salary)
                                  ? FormatNumber(item.salary)
                                  : "Thương luợng"}
                              </span>
                            </td>
                            <td>
                              <span className="status">
                                {FormatDate(item.created_at)}
                              </span>
                            </td>
                            <td>
                              <span className="status">
                                {["Hoạt động", "Đã xóa"][item.is_delete]}
                              </span>
                            </td>
                            <td className="text-right">
                              <div className="dropdown">
                                <Link
                                  className="btn btn-sm btn-outline-default "
                                  role="button"
                                  data-toggle="dropdown"
                                  aria-haspopup="true"
                                  aria-expanded="false"
                                >
                                  <i className="fa fa-sliders-h pr-1"></i>
                                  Hành động
                                </Link>
                                <div className="dropdown-menu dropdown-menu-right dropdown-menu-arrow">
                                  <Link
                                    to={`/viec-lam/${item.id}`}
                                    target="_blank"
                                    className="dropdown-item"
                                  >
                                    <i className="fa fa-external-link-square-alt pr-2"></i>
                                    <span>Xem bài viết</span>
                                  </Link>
                                  <button
                                    data-toggle="modal"
                                    type="button"
                                    data-target={`#ModalDeleteRecruitment_${item.id}`}
                                    className="dropdown-item text-danger"
                                  >
                                    <i className="fa fa-trash pr-2"></i>
                                    <span>Xóa bài viết</span>
                                  </button>
                                </div>
                              </div>
                            </td>
                          </tr>
                          <ModalDeleteRecruiment id={item.id} />
                        </Fragment>
                      ))}
                  </tbody>
                </table>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AdminDanhSachTin;
