import axios from "axios";
import React from "react";
import { NotificationManager } from "react-notifications";
import { api } from "../../../../../../api";
import { gettoken } from "../../../../../../utils/gettoken";
function ModalDeleteRecruiment({ id }) {
  const token = gettoken();
  const handleDeleteRecruitment = () => {
    document.querySelector(`.close_${id}`).click();
    axios
      .post(`${api}/admin/delete-recruitment`, { id, token })
      .then(() => {
        NotificationManager.success(
          "Xoá bài viết thành công",
          "Thành công",
          3000
        );
      })
      .catch(() => {
        NotificationManager.success(
          "Xoá bài viết thành công",
          "Thành công",
          3000
        );
      });
  };
  return (
    <div
      class="modal fade"
      id={`ModalDeleteRecruitment_${id}`}
      tabindex="-1"
      role="dialog"
      aria-labelledby="exampleModalLabel"
      aria-hidden="true"
    >
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">
              Xóa tin tuyển dụng này
            </h5>
            <button
              type="button"
              class="close"
              data-dismiss="modal"
              aria-label="Close"
            >
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div className="d-flex justify-content-center">
              <button
                type="button"
                class={`btn btn-secondary close_${id}`}
                data-dismiss="modal"
              >
                Hủy
              </button>
              <button
                type="button"
                onClick={handleDeleteRecruitment}
                class="btn btn-outline-danger"
              >
                Xóa tin
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ModalDeleteRecruiment;
