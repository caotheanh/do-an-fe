import React from 'react'
import { Redirect, Route } from 'react-router-dom';
import AuthLayout from '../../Layout/AuthLayout';

const AuthRouter = ({isLogin, component: Component, ...rest }) => {
    return (
      <Route
        {...rest}
        render={props =>isLogin?<Redirect
            to={{
              pathname: "/",
            }}
          />:
          <AuthLayout>
            <Component {...props} />
          </AuthLayout>
        }
        
      />
    );
  };
export default AuthRouter
