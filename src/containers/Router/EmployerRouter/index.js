import React from "react";
import { useSelector } from "react-redux";
import { Redirect, Route } from "react-router-dom";
import CommonLayout from "../../Layout/EmployerLayout/Loadable";

const EmployerRouter = ({ statusLogin, component: Component, ...rest }) => {
  const info_login = useSelector((state) => state.info_login);
  return (
    <Route
      {...rest}
      render={(props) =>
        statusLogin && info_login && info_login.type === 1 ? (
          <CommonLayout>
            <Component {...props} />
          </CommonLayout>
        ) : (
          <Redirect
            to={{
              pathname: "/",
            }}
          />
        )
      }
    />
  );
};
export default EmployerRouter;
