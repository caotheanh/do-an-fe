import React from "react";
import ContentLoader from "react-content-loader";

const LoaderCommonRouter = (props) => (
  <ContentLoader
    viewBox="0 0 600 560"
    backgroundColor="#f3f3f3"
    foregroundColor="#ecebeb"
    {...props}
  >
    <rect x="73" y="7" rx="0" ry="0" width="45" height="17" />
    <rect x="125" y="12" rx="0" ry="0" width="36" height="10" />
    <rect x="167" y="12" rx="0" ry="0" width="54" height="10" />
    <rect x="234" y="12" rx="0" ry="0" width="26" height="9" />
    <rect x="271" y="11" rx="0" ry="0" width="34" height="10" />
    <rect x="317" y="10" rx="0" ry="0" width="57" height="10" />
    <circle cx="470" cy="17" r="7" />
    <circle cx="500" cy="17" r="9" />
    <rect x="0" y="33" rx="0" ry="0" width="593" height="304" />
  </ContentLoader>
);

export default LoaderCommonRouter;
