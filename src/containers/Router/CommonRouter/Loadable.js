import Loadable from "react-loadable";
import LoaderCommonRouter from "./LoaderCommonRouter";
export default Loadable({
  loader: () => import("./index"),
  loading() {
    return <LoaderCommonRouter />
  },
});
