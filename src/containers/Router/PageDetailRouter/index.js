import React from 'react'
import { Route } from 'react-router-dom';
import PageDetailLayout from '../../Layout/PageDetailLayout';

const PageDetailRouter = ({ component: Component, ...rest }) => {
    return (
      <Route
        {...rest}
        render={props => (
          <PageDetailLayout>
            <Component {...props} />
          </PageDetailLayout>
        )}
      />
    );
  };
export default PageDetailRouter
