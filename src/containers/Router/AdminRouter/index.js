import React from "react";
import { useSelector } from "react-redux";
import { Redirect, Route } from "react-router-dom";
import AdminLayout from "../../Layout/AdminLayout";

const AdminRouter = ({ statusLogin, component: Component, ...rest }) => {
  const info_login = useSelector((state) => state.info_login);
  return (
    <Route
      render={(props) =>
        statusLogin && info_login && info_login.type === 3 ? (
          <AdminLayout>
            <Component {...props} />
          </AdminLayout>
        ) : (
          <Redirect
            to={{
              pathname: "/",
            }}
          />
        )
      }
    />
  );
};
export default AdminRouter;
