import React from 'react'
import { Route } from 'react-router-dom';
import EditCongTyLayout from '../../Layout/EditCongTyLayout';

const EditCongTyRoute = ({ component: Component, ...rest }) => {
    return (
      <Route
        {...rest}
        render={props => (
          <EditCongTyLayout>
            <Component {...props} />
          </EditCongTyLayout>
        )}
      />
    );
  };
export default EditCongTyRoute
