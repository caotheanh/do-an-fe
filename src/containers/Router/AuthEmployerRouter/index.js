import React from "react";
import { Route } from "react-router";
import EmployerAuthLayout from "../../Layout/EmployerAuthLayout";

function AuthEmployerRouter({ component: Component, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) => (
        <EmployerAuthLayout>
          <Component {...props} />
        </EmployerAuthLayout>
      )}
    />
  );
}

export default AuthEmployerRouter;
