import React from "react";
import { Route } from "react-router";
import CreateCVLayout from "../../Layout/CreateCVLayout";

const CreateCVRouter = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => (
        <CreateCVLayout>
          <Component {...props} />
        </CreateCVLayout>
      )}
    />
  );
};
export default CreateCVRouter;
