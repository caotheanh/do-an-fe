import * as constant from "../Constant";

const initialState = {
  info_user: {},
  is_login: false,
  categories: [],
  info_login: null,
  data_search: {},
  detail_info_user: null,
  point_full_info: {},
};

function rootReducer(state = initialState, action) {
  switch (action.type) {
    case constant.GET_INFO_USER:
      return { counter: state.counter + 1 };
    case constant.SET_INFO_USER:
      return { ...state, info_user: action.info_user };
    case constant.SET_TOKEN:
      return { ...state, token: action.token };
    case constant.SET_IS_LOGIN:
      return { ...state, is_login: action.is_login };
    case constant.SET_CATEGORIES:
      return { ...state, categories: action.categories };
    case constant.SET_INFO_LOGIN:
      return { ...state, info_login: action.info_login };
    case constant.SET_DEFAULT_CV:
      const infoUser = state.info_user;
      return { ...state, info_user: { ...infoUser, cv: action.cv } };
    case constant.SET_DATA_SEARCH:
      return { ...state, data_search: action.data_search };
    case constant.SET_DETAIL_INFO_USER:
      return { ...state, detail_info_user: action.detail_info_user };
    case constant.SET_POINT_FULL_INFO:
      return {
        ...state,
        point_full_info: {
          ...state.point_full_info,
          [action.point.name]: action.point.point,
        },
      };
    default:
      return state;
  }
}

export default rootReducer;
